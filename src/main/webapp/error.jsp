<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="com.evampsaanga.bakcell.common.utilities.Utilities"%>
<%@page import="com.evampsaanga.bakcell.common.utilities.Transactions"%>
<%@page import="com.evampsaanga.bakcell.restclient.RestClient"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@page import="com.evampsaanga.bakcell.common.utilities.GetConfigurations"%>
<%@page import="com.evampsaanga.bakcell.common.utilities.Constants"%>
<%@page import="com.evampsaanga.bakcell.business.TopUpBusiness"%>

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<br />
<div class="row text-center">
  <div>
   <p>Your payment could not be processed. Please contact to Support.</p>
  </div>
</div>
</body>
</html>

<%

    Enumeration en = request.getParameterNames();
	String paymentKey = "";
	String paymentValue = "";

//	enumerate through the keys and extract the value from the keys!
	while (en.hasMoreElements()) {
		paymentKey = (String) en.nextElement();
		paymentValue = request.getParameter(paymentKey);
		System.out.println("JSP : "+paymentKey+" : "+paymentValue);        
	}
//	now call your jsp file (from a browser and add on some paramters) file.jsp?a=12341234&b=apple&c=1.21gigawatts
 
	
	
	Logger logger = Logger.getLogger(TopUpBusiness.class);
	
	JSONObject requestBody = new JSONObject();
 	requestBody.put("paymentKey", paymentValue);
	requestBody.put("paymentStatus", false);
	requestBody.put("msisdn", "555908161");
	requestBody.put("iP", "127.0.0.1");
	requestBody.put("channel", "mobileweb");
	requestBody.put("lang", "3");
	requestBody.put("isB2B", "false");
	
	Utilities.printDebugLog(paymentValue + "-Request received in " + Transactions.GET_CARD_TOKEN_TRANSACTION_NAME
				+ " BUSINESS with data-" + requestBody.toString(), logger);

	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = requestBody.toString();

	String path = GetConfigurations.getESBRoute("processPayment");
	Utilities.printDebugLog(paymentValue + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
	Utilities.printDebugLog(paymentValue + "-Path-" + path, logger);
	Utilities.printDebugLog(paymentValue + "-Request Packet-" + requestJsonESB, logger);

	String res = rc.getResponseFromESB(path, requestJsonESB);

	Utilities.printDebugLog(paymentValue + "-Received response from ESB-" + res, logger);

	if (res != null && !res.isEmpty()) {

		if (Utilities.getValueFromJSON(res, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

			String resData = "";
			resData = Utilities.getValueFromJSON(res, "getCardToken");
			out.println("Response :" + resData + "<br />");
		} 

	} 
	
//  String redirectURL = "https://mystg.bakcell.com:8444/stgbakcell/user/account/topup?result=false&error=500";
	String redirectURL = "https://my.bakcell.com/user/account/topup?result=false&error=500";
	response.sendRedirect(redirectURL);
%>

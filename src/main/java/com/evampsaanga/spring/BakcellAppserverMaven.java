package com.evampsaanga.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.evampsaanga.*")
@EnableAutoConfiguration
public class BakcellAppserverMaven {

    public static void main(String[] args) {
	SpringApplication.run(BakcellAppserverMaven.class, args);
    }

}

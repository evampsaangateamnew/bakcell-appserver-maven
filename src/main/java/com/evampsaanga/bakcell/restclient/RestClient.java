package com.evampsaanga.bakcell.restclient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;

import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Utilities;

public class RestClient {
    Logger logger = Logger.getLogger(RestClient.class);

    public String getResponseFromESB(String url, String postData) throws SocketException {
	String line = "";
	String esbResponse = "";
	String isfromb2b = "";
	String[] reqpkt = postData.split("%");
	if (reqpkt.length > 1) {
	    isfromb2b = reqpkt[1];
	    postData = reqpkt[0];
	}
	try {

	    TrustManager[] trustAllCerts = { new X509TrustManager() {
		@Override
		public X509Certificate[] getAcceptedIssuers() {
		    return null;
		}

		@Override
		public void checkClientTrusted(X509Certificate[] certs, String authType) {
		}

		@Override
		public void checkServerTrusted(X509Certificate[] certs, String authType) {
		}
	    } };
	    SSLContext sc = SSLContext.getInstance("SSL");
	    sc.init(null, trustAllCerts, new SecureRandom());

	    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

	    HostnameVerifier allHostsValid = new HostnameVerifier() {
		@Override
		public boolean verify(String hostname, SSLSession session) {
		    return true;
		}
	    };
	    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

	    URL url_1 = new URL(url);

	    HttpURLConnection conn = (HttpURLConnection) url_1.openConnection();

	    conn.setDoOutput(true);
	    conn.setRequestMethod(GetConfigurations.getConfig("conf_requestMethod"));
	    conn.setRequestProperty("Content-Type", GetConfigurations.getConfig("conf_contentType"));

	    conn.setRequestProperty("credentials", GetConfigurations.getConfig("conf_credentials"));

	    conn.setRequestProperty("isFromB2B", isfromb2b);

	    conn.setConnectTimeout(Integer.parseInt(GetConfigurations.getConfig("conf_connectionTimeOut")));
	    conn.setReadTimeout(Integer.parseInt(GetConfigurations.getConfig("conf_readTimeOut")));
	    conn.setRequestProperty("http.keepAlive", "false");
	    OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

	    writer.write(postData);
	    writer.flush();

	    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    while ((line = reader.readLine()) != null) {

		esbResponse = esbResponse + line;
	    }
	    writer.close();
	    reader.close();
	    conn.disconnect();
	    if (esbResponse.equalsIgnoreCase("null"))
		esbResponse = "{}";
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    if (e instanceof SocketTimeoutException) {
		Utilities.printDebugLog("TIME OUT: API Call failed. ", logger);
	    }

	}
	return esbResponse;
    }

}
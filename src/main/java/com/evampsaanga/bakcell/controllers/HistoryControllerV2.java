/**
 * 
 */
package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.CustomerServicesBusiness;
import com.evampsaanga.bakcell.business.HistoryBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.customerservices.resendpin.ResendPinRequestV2;
import com.evampsaanga.bakcell.models.customerservices.sendpinV2.SendPINRequest;
import com.evampsaanga.bakcell.models.customerservices.sendpinV2.SendPINResponse;
import com.evampsaanga.bakcell.models.history.getoperationshistory.OperationsHistoryRequest;
import com.evampsaanga.bakcell.models.history.getoperationshistory.OperationsHistoryResponse;
import com.evampsaanga.bakcell.models.history.getusagedetails.UsageDetailsRequest;
import com.evampsaanga.bakcell.models.history.getusagedetails.UsageDetailsResponse;
import com.evampsaanga.bakcell.models.history.getusagesummary.UsageSummaryRequest;
import com.evampsaanga.bakcell.models.history.getusagesummary.UsageSummaryResponse;
import com.evampsaanga.bakcell.models.history.verifyaccountdetails.VerifyAccountDetailsRequest;
import com.evampsaanga.bakcell.models.history.verifyaccountdetails.VerifyAccountDetailsResponse;
import com.evampsaanga.bakcell.models.history.verifypin.UsageDetailsVerifyPinResponse;
import com.evampsaanga.bakcell.models.history.verifypin.VerifyPinRequestV2;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */

@RestController
@RequestMapping(Constants.SERVLET_URL + "/historyV2")
public class HistoryControllerV2 {

    Logger logger = Logger.getLogger(HistoryControllerV2.class);

    @Autowired
    CustomerServicesBusiness customerServicesBusiness;

    @Autowired
    HistoryBusiness historyBusiness;

    @RequestMapping(value = "/sendpin", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SendPINResponse sendPIN(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
	    JMSException, ClassNotFoundException, SQLException {
	ObjectMapper mapper = new ObjectMapper();

	SendPINRequest sendPINRequest = new SendPINRequest();
	SendPINResponse sendPINResponse = new SendPINResponse();
	String TRANSACTION_NAME = Transactions.HISTORY_SEND_OTP_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    /*
	     * String msisdn =
	     * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)
	     * ).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    sendPINResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, sendPINResponse.getLogsReport()));

	    String deviceID = servletRequest.getHeader("deviceID");

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    JSONObject jObject = new JSONObject(data);

	    if (jObject.has("individualMsisdn")) {
		jObject.remove("msisdn");
		jObject.put("msisdn", jObject.get("individualMsisdn"));
		jObject.remove("individualMsisdn");
	    }
	    sendPINRequest = mapper.readValue(jObject.toString(), SendPINRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, sendPINRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		sendPINResponse = customerServicesBusiness.sendPinBusiness(msisdn, deviceID, sendPINRequest,
			sendPINResponse);
	    } else {
		sendPINResponse.setCallStatus(Constants.Call_Status_False);
		sendPINResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		sendPINResponse.setResultDesc(requestValidationStatus);
	    }

	    sendPINResponse.getLogsReport().setResponseCode(sendPINResponse.getResultCode());
	    sendPINResponse.getLogsReport().setRequestTime(requestTime);
	    sendPINResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    sendPINResponse.getLogsReport().setResponse(mapper.writeValueAsString(sendPINResponse));

	    // Sending report log into queue. commenting due to db issues
	    sendPINResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(sendPINResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(sendPINResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    sendPINResponse.setCallStatus(Constants.Call_Status_False);

	    sendPINResponse.setResultCode(Constants.EXCEPTION_CODE);
	    sendPINResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    sendPINResponse.getLogsReport().setResponseCode(sendPINResponse.getResultCode());
	    sendPINResponse.getLogsReport().setRequestTime(requestTime);
	    sendPINResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    sendPINResponse.getLogsReport().setResponse(mapper.writeValueAsString(sendPINResponse));

	    // Sending report log into queue.
	    sendPINResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(sendPINResponse.getLogsReport());
	}
	return sendPINResponse;
    }

    @RequestMapping(value = "/historyresendpin", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SendPINResponse resendPIN(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
	    JMSException, ClassNotFoundException, SQLException {
	ObjectMapper mapper = new ObjectMapper();
	ResendPinRequestV2 resendPINRequest = new ResendPinRequestV2();
	SendPINResponse resendPINResponse = new SendPINResponse();
	String TRANSACTION_NAME = Transactions.HISTORY_RESEND_OTP_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    /*
	     * String msisdn =
	     * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)
	     * ).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    resendPINResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, resendPINResponse.getLogsReport()));

	    String deviceID = servletRequest.getHeader("deviceID");

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    JSONObject jObject = new JSONObject(data);
	    if (jObject.has("individualMsisdn")) {
		jObject.remove("msisdn");
		jObject.put("msisdn", jObject.get("individualMsisdn"));
		jObject.remove("individualMsisdn");
	    }
	    resendPINRequest = mapper.readValue(jObject.toString(), ResendPinRequestV2.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, resendPINRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		resendPINResponse = customerServicesBusiness.resendPinBusinessV2(msisdn, deviceID, resendPINRequest,
			resendPINResponse);
	    } else {
		resendPINResponse.setCallStatus(Constants.Call_Status_False);
		resendPINResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		resendPINResponse.setResultDesc(requestValidationStatus);
	    }

	    resendPINResponse.getLogsReport().setResponseCode(resendPINResponse.getResultCode());
	    resendPINResponse.getLogsReport().setRequestTime(requestTime);
	    resendPINResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    resendPINResponse.getLogsReport().setResponse(mapper.writeValueAsString(resendPINResponse));

	    // Sending report log into queue.
	    resendPINResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(resendPINResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(resendPINResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    resendPINResponse.setCallStatus(Constants.Call_Status_False);

	    resendPINResponse.setResultCode(Constants.EXCEPTION_CODE);
	    resendPINResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    resendPINResponse.getLogsReport().setResponseCode(resendPINResponse.getResultCode());
	    resendPINResponse.getLogsReport().setRequestTime(requestTime);
	    resendPINResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    resendPINResponse.getLogsReport().setResponse(mapper.writeValueAsString(resendPINResponse));

	    // Sending report log into queue.
	    resendPINResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(resendPINResponse.getLogsReport());
	}
	return resendPINResponse;
    }

    @RequestMapping(value = "/getusagesummary", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public UsageSummaryResponse getUsageSummary(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	UsageSummaryRequest usageSummaryRequest = new UsageSummaryRequest();
	UsageSummaryResponse usageSummaryResponse = new UsageSummaryResponse();

	String TRANSACTION_NAME = Transactions.GET_CDRS_SUMMARY_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    /*
	     * String msisdn =
	     * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)
	     * ).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    usageSummaryResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, usageSummaryResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    JSONObject jObject = new JSONObject(data);
	    if (jObject.has("individualMsisdn")) {
		jObject.remove("msisdn");
		jObject.put("msisdn", jObject.get("individualMsisdn"));
		jObject.remove("individualMsisdn");
	    }
	    usageSummaryRequest = mapper.readValue(jObject.toString(), UsageSummaryRequest.class);

	    // Specific params for report logging
	    usageSummaryResponse.getLogsReport().setAccountID(usageSummaryRequest.getAccountId());

	    String requestValidationStatus = Validator.validateRequest(msisdn, usageSummaryRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		usageSummaryResponse = historyBusiness.getUsageSummary(msisdn, usageSummaryRequest,
			usageSummaryResponse);
	    } else {
		usageSummaryResponse.setCallStatus(Constants.Call_Status_False);
		usageSummaryResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		usageSummaryResponse.setResultDesc(requestValidationStatus);
	    }

	    usageSummaryResponse.getLogsReport().setResponseCode(usageSummaryResponse.getResultCode());
	    usageSummaryResponse.getLogsReport().setRequestTime(requestTime);
	    usageSummaryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    usageSummaryResponse.getLogsReport().setResponse(mapper.writeValueAsString(usageSummaryResponse));

	    // Sending report log into queue. commenting due to db issue
	    usageSummaryResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(usageSummaryResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(usageSummaryResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    usageSummaryResponse.setCallStatus(Constants.Call_Status_False);

	    usageSummaryResponse.setResultCode(Constants.EXCEPTION_CODE);
	    usageSummaryResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    usageSummaryResponse.getLogsReport().setResponseCode(usageSummaryResponse.getResultCode());
	    usageSummaryResponse.getLogsReport().setRequestTime(requestTime);
	    usageSummaryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    usageSummaryResponse.getLogsReport().setResponse(mapper.writeValueAsString(usageSummaryResponse));

	    // Sending report log into queue.
	    usageSummaryResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(usageSummaryResponse.getLogsReport());
	}
	return usageSummaryResponse;
    }

    @RequestMapping(value = "/getusagedetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public UsageDetailsResponse getDetailedUsageHistory(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	UsageDetailsRequest usageDetailsRequest = new UsageDetailsRequest();
	UsageDetailsResponse usageDetailsResponse = new UsageDetailsResponse();

	String TRANSACTION_NAME = Transactions.GET_CDRS_BY_DATE_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    /*
	     * String msisdn =
	     * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)
	     * ).toString(), Constants.MSISDN_KEY);
	     */

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    JSONObject jObject = new JSONObject(data);
	    if (jObject.has("individualMsisdn")) {
		jObject.remove("msisdn");
		jObject.put("msisdn", jObject.get("individualMsisdn"));
		jObject.remove("individualMsisdn");
	    }
	    usageDetailsRequest = mapper.readValue(jObject.toString(), UsageDetailsRequest.class);

	    // Populating report object before processing business logic.
	    usageDetailsResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, usageDetailsResponse.getLogsReport()));

	    // Specific params for report logging
	    usageDetailsResponse.getLogsReport().setAccountID(usageDetailsRequest.getAccountId());

	    String requestValidationStatus = Validator.validateRequest(msisdn, usageDetailsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		usageDetailsResponse = historyBusiness.getUsageDetails(msisdn, usageDetailsRequest,
			usageDetailsResponse);
	    } else {
		usageDetailsResponse.setCallStatus(Constants.Call_Status_False);
		usageDetailsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		usageDetailsResponse.setResultDesc(requestValidationStatus);
	    }

	    usageDetailsResponse.getLogsReport().setResponseCode(usageDetailsResponse.getResultCode());
	    usageDetailsResponse.getLogsReport().setRequestTime(requestTime);
	    usageDetailsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    usageDetailsResponse.getLogsReport().setResponse(mapper.writeValueAsString(usageDetailsResponse));

	    // Sending report log into queue.
	    usageDetailsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(usageDetailsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(usageDetailsResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    usageDetailsResponse.setCallStatus(Constants.Call_Status_False);

	    usageDetailsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    usageDetailsResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    usageDetailsResponse.getLogsReport().setResponseCode(usageDetailsResponse.getResultCode());
	    usageDetailsResponse.getLogsReport().setRequestTime(requestTime);
	    usageDetailsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    usageDetailsResponse.getLogsReport().setResponse(mapper.writeValueAsString(usageDetailsResponse));

	    // Sending report log into queue.
	    usageDetailsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(usageDetailsResponse.getLogsReport());

	}
	return usageDetailsResponse;
    }

    @RequestMapping(value = "/getoperationshistory", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public OperationsHistoryResponse getOperationsHistory(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();
	OperationsHistoryRequest operationsHistoryRequest = new OperationsHistoryRequest();
	OperationsHistoryResponse operationsHistoryResponse = new OperationsHistoryResponse();

	String TRANSACTION_NAME = Transactions.GET_CDRS_OPERATION_HISTORY_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    /*
	     * String msisdn =
	     * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)
	     * ).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    operationsHistoryResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, operationsHistoryResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    JSONObject jObject = new JSONObject(data);
	    if (jObject.has("individualMsisdn")) {
		jObject.remove("msisdn");
		jObject.put("msisdn", jObject.get("individualMsisdn"));
		jObject.remove("individualMsisdn");
	    }
	    operationsHistoryRequest = mapper.readValue(jObject.toString(), OperationsHistoryRequest.class);

	    // Specific params for report logging
	    operationsHistoryResponse.getLogsReport().setAccountID(operationsHistoryRequest.getAccountId());
	    String requestValidationStatus = Validator.validateRequest(msisdn, operationsHistoryRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		operationsHistoryResponse = historyBusiness.getOperationsHistory(msisdn, operationsHistoryRequest,
			operationsHistoryResponse);
	    } else {
		operationsHistoryResponse.setCallStatus(Constants.Call_Status_False);
		operationsHistoryResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		operationsHistoryResponse.setResultDesc(requestValidationStatus);
	    }

	    operationsHistoryResponse.getLogsReport().setResponseCode(operationsHistoryResponse.getResultCode());
	    operationsHistoryResponse.getLogsReport().setRequestTime(requestTime);
	    operationsHistoryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    operationsHistoryResponse.getLogsReport().setResponse(mapper.writeValueAsString(operationsHistoryResponse));

	    // Sending report log into queue.commenting due to db issues
	    operationsHistoryResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(operationsHistoryResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(operationsHistoryResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    operationsHistoryResponse.setCallStatus(Constants.Call_Status_False);

	    operationsHistoryResponse.setResultCode(Constants.EXCEPTION_CODE);
	    operationsHistoryResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    operationsHistoryResponse.getLogsReport().setResponseCode(operationsHistoryResponse.getResultCode());
	    operationsHistoryResponse.getLogsReport().setRequestTime(requestTime);
	    operationsHistoryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    operationsHistoryResponse.getLogsReport().setResponse(mapper.writeValueAsString(operationsHistoryResponse));

	    // Sending report log into queue.
	    operationsHistoryResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(operationsHistoryResponse.getLogsReport());

	}
	return operationsHistoryResponse;
    }

    @RequestMapping(value = "/verifyaccountdetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public VerifyAccountDetailsResponse verifyAccountDetails(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();
	VerifyAccountDetailsRequest verifyAccountDetailsRequest = new VerifyAccountDetailsRequest();
	VerifyAccountDetailsResponse verifyAccountDetailsResponse = new VerifyAccountDetailsResponse();

	String TRANSACTION_NAME = Transactions.GET_CDRSBY_DATE_OTP_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    /*
	     * String msisdn =
	     * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)
	     * ).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    verifyAccountDetailsResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, verifyAccountDetailsResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    verifyAccountDetailsRequest = mapper.readValue(data, VerifyAccountDetailsRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, verifyAccountDetailsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		verifyAccountDetailsResponse = historyBusiness.verifyAccountDetailsBusiness(msisdn,
			verifyAccountDetailsRequest, verifyAccountDetailsResponse);
	    } else {
		verifyAccountDetailsResponse.setCallStatus(Constants.Call_Status_False);
		verifyAccountDetailsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		verifyAccountDetailsResponse.setResultDesc(requestValidationStatus);
	    }

	    verifyAccountDetailsResponse.getLogsReport().setResponseCode(verifyAccountDetailsResponse.getResultCode());
	    verifyAccountDetailsResponse.getLogsReport().setRequestTime(requestTime);
	    verifyAccountDetailsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    verifyAccountDetailsResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(verifyAccountDetailsResponse));

	    // Sending report log into queue.
	    verifyAccountDetailsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(verifyAccountDetailsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(verifyAccountDetailsResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    verifyAccountDetailsResponse.setCallStatus(Constants.Call_Status_False);

	    verifyAccountDetailsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    verifyAccountDetailsResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    verifyAccountDetailsResponse.getLogsReport().setResponseCode(verifyAccountDetailsResponse.getResultCode());
	    verifyAccountDetailsResponse.getLogsReport().setRequestTime(requestTime);
	    verifyAccountDetailsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    verifyAccountDetailsResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(verifyAccountDetailsResponse));

	    // Sending report log into queue.
	    verifyAccountDetailsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(verifyAccountDetailsResponse.getLogsReport());

	}
	return verifyAccountDetailsResponse;
    }

    @RequestMapping(value = "/verifypin", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public UsageDetailsVerifyPinResponse verifyPin(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();
	VerifyPinRequestV2 usageDetailsVerifyPinRequest = new VerifyPinRequestV2();
	UsageDetailsVerifyPinResponse usageDetailsVerifyPinResponse = new UsageDetailsVerifyPinResponse();

	String TRANSACTION_NAME = Transactions.VERIFY_CDRS_BY_DATE_OTP_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    /*
	     * String msisdn =
	     * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)
	     * ).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    usageDetailsVerifyPinResponse.setLogsReport(Utilities.preBusinessReportLoggingData("", msisdn,
		    TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    usageDetailsVerifyPinResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    JSONObject jObject = new JSONObject(data);
	    if (jObject.has("individualMsisdn")) {
		jObject.remove("msisdn");
		jObject.put("msisdn", jObject.get("individualMsisdn"));
		jObject.remove("individualMsisdn");
	    }
	    usageDetailsVerifyPinRequest = mapper.readValue(jObject.toString(), VerifyPinRequestV2.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, usageDetailsVerifyPinRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		usageDetailsVerifyPinResponse = historyBusiness.verifyPinBusinessV2(msisdn,
			usageDetailsVerifyPinRequest, usageDetailsVerifyPinResponse);

	    } else {
		usageDetailsVerifyPinResponse.setCallStatus(Constants.Call_Status_False);
		usageDetailsVerifyPinResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		usageDetailsVerifyPinResponse.setResultDesc(requestValidationStatus);
	    }

	    usageDetailsVerifyPinResponse.getLogsReport()
		    .setResponseCode(usageDetailsVerifyPinResponse.getResultCode());
	    usageDetailsVerifyPinResponse.getLogsReport().setRequestTime(requestTime);
	    usageDetailsVerifyPinResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    usageDetailsVerifyPinResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(usageDetailsVerifyPinResponse));

	    // Sending report log into queue.
	    usageDetailsVerifyPinResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(usageDetailsVerifyPinResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(usageDetailsVerifyPinResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    usageDetailsVerifyPinResponse.setCallStatus(Constants.Call_Status_False);

	    usageDetailsVerifyPinResponse.setResultCode(Constants.EXCEPTION_CODE);
	    usageDetailsVerifyPinResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    usageDetailsVerifyPinResponse.getLogsReport()
		    .setResponseCode(usageDetailsVerifyPinResponse.getResultCode());
	    usageDetailsVerifyPinResponse.getLogsReport().setRequestTime(requestTime);
	    usageDetailsVerifyPinResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    usageDetailsVerifyPinResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(usageDetailsVerifyPinResponse));

	    // Sending report log into queue.
	    usageDetailsVerifyPinResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(usageDetailsVerifyPinResponse.getLogsReport());

	}
	return usageDetailsVerifyPinResponse;
    }

}

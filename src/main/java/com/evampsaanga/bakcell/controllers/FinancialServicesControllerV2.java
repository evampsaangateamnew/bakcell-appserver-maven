/**
 * 
 */
package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.FinancialServicesBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.financialservices.getloan.GetLoanRequest;
import com.evampsaanga.bakcell.models.financialservices.getloan.GetLoanResponse;
import com.evampsaanga.bakcell.models.financialservices.loanhistory.LoanHistoryRequest;
import com.evampsaanga.bakcell.models.financialservices.loanhistory.LoanHistoryResponse;
import com.evampsaanga.bakcell.models.financialservices.moneytransfer.MoneyTransferRequest;
import com.evampsaanga.bakcell.models.financialservices.moneytransfer.MoneyTransferResponse;
import com.evampsaanga.bakcell.models.financialservices.paymenthistory.PaymentHistoryRequest;
import com.evampsaanga.bakcell.models.financialservices.paymenthistory.PaymentHistoryResponse;
import com.evampsaanga.bakcell.models.financialservices.topups.TopupRequest;
import com.evampsaanga.bakcell.models.financialservices.topups.TopupResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 * 
 */
@RestController
@RequestMapping(Constants.SERVLET_URL + "/financialservicesV2")
public class FinancialServicesControllerV2 {

    Logger logger = Logger.getLogger(FinancialServicesControllerV2.class);
    @Autowired
    FinancialServicesBusiness financialServicesBusiness;

    @RequestMapping(value = "/requesttopup", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public TopupResponse getTopups(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws IOException, JMSException, ClassNotFoundException, SQLException {
	ObjectMapper mapper = new ObjectMapper();
	TopupRequest topupRequest = new TopupRequest();
	TopupResponse topupResponse = new TopupResponse();
	String TRANSACTION_NAME = Transactions.TOPUP_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    /*
	     * String msisdn =
	     * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)
	     * ).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    topupResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, topupResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);
	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);

	    /*
	     * Below JSON modification is done just to mask scratch card number. Purpose is
	     * not to print scratch card number in logs or database.
	     */
	    String tempData = data;
	    String pinCard = Utilities.getValueFromJSON(tempData, "cardPinNumber");
	    pinCard = Utilities.maskString(pinCard, Constants.SCRATCH_CARD_MASKING_COUNT);
	    tempData = Utilities.removeParamsFromJSONObject(tempData, "cardPinNumber");
	    tempData = Utilities.addParamsToJSONObject(tempData, "cardPinNumber", pinCard);

	    Utilities.printInfoLog(msisdn + "-Request Data-" + tempData, logger);
	    topupRequest = mapper.readValue(data, TopupRequest.class);

	    // Logging specific params in report.
	    topupResponse.getLogsReport().setReceiverMsisdn(topupRequest.getTopupnum());
	    topupResponse.getLogsReport().setMediumForTopUp("Scratch Card");
	    topupResponse.getLogsReport().setScratchCardNumber(
		    Utilities.maskString(topupRequest.getCardPinNumber(), Constants.SCRATCH_CARD_MASKING_COUNT));

	    String requestValidationStatus = Validator.validateRequest(msisdn, topupRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		topupRequest.setIsB2B(isFromB2B);
		topupResponse = financialServicesBusiness.getTopupBusiness(msisdn, topupRequest, topupResponse);
	    } else {
		topupResponse.setCallStatus(Constants.Call_Status_False);
		topupResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		topupResponse.setResultDesc(requestValidationStatus);
	    }

	    topupResponse.getLogsReport().setRequestTime(requestTime);
	    topupResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    topupResponse.getLogsReport().setResponse(mapper.writeValueAsString(topupResponse));
	    topupResponse.getLogsReport().setResponseCode(topupResponse.getResultCode());

	    // Sending report log into queue.
	    topupResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(topupResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(topupResponse), logger);

	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    topupResponse.setCallStatus(Constants.Call_Status_False);

	    topupResponse.setResultCode(Constants.EXCEPTION_CODE);
	    topupResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    topupResponse.getLogsReport().setResponseCode(topupResponse.getResultCode());
	    topupResponse.getLogsReport().setRequestTime(requestTime);
	    topupResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    topupResponse.getLogsReport().setResponse(mapper.writeValueAsString(topupResponse));

	    // Sending report log into queue.
	    topupResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(topupResponse.getLogsReport());
	}

	return topupResponse;
    }

    @RequestMapping(value = "/requestmoneytransfer", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public MoneyTransferResponse moneyTransfer(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
	    JMSException, ClassNotFoundException, SQLException {
	ObjectMapper mapper = new ObjectMapper();
	MoneyTransferRequest moneyTransferRequest = new MoneyTransferRequest();
	MoneyTransferResponse moneyTransferResponse = new MoneyTransferResponse();
	String TRANSACTION_NAME = Transactions.TRANSFER_MONEY_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();

	try {
	    /*
	     * String msisdn =
	     * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)
	     * ).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    moneyTransferResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, moneyTransferResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent,isFromB2B);
	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    moneyTransferRequest = mapper.readValue(data, MoneyTransferRequest.class);

	    // Logging specific params in report.
	    moneyTransferResponse.getLogsReport().setReceiverMsisdn(moneyTransferRequest.getTransferee());
	    moneyTransferResponse.getLogsReport().setAmount(moneyTransferRequest.getAmount());

	    String requestValidationStatus = Validator.validateRequest(msisdn, moneyTransferRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		moneyTransferRequest.setIsB2B(isFromB2B);
		moneyTransferResponse = financialServicesBusiness.moneyTransferBusiness(msisdn, moneyTransferRequest,
			moneyTransferResponse);
	    } else {
		moneyTransferResponse.setCallStatus(Constants.Call_Status_False);
		moneyTransferResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		moneyTransferResponse.setResultDesc(requestValidationStatus);
	    }

	    moneyTransferResponse.getLogsReport().setResponseCode(moneyTransferResponse.getResultCode());
	    moneyTransferResponse.getLogsReport().setRequestTime(requestTime);
	    moneyTransferResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    moneyTransferResponse.getLogsReport().setResponse(mapper.writeValueAsString(moneyTransferResponse));

	    // Sending report log into queue.
	    moneyTransferResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(moneyTransferResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(moneyTransferResponse), logger);

	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    moneyTransferResponse.setCallStatus(Constants.Call_Status_False);

	    moneyTransferResponse.setResultCode(Constants.EXCEPTION_CODE);
	    moneyTransferResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    moneyTransferResponse.getLogsReport().setResponseCode(moneyTransferResponse.getResultCode());
	    moneyTransferResponse.getLogsReport().setRequestTime(requestTime);
	    moneyTransferResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    moneyTransferResponse.getLogsReport().setResponse(mapper.writeValueAsString(moneyTransferResponse));

	    // Sending report log into queue.
	    moneyTransferResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(moneyTransferResponse.getLogsReport());

	}

	return moneyTransferResponse;
    }

    @RequestMapping(value = "/getloan", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public GetLoanResponse getLoan(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	GetLoanRequest getLoanRequest = new GetLoanRequest();
	GetLoanResponse getLoanResponse = new GetLoanResponse();

	String TRANSACTION_NAME = Transactions.LOAN_REQUEST_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    /*
	     * String msisdn =
	     * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)
	     * ).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    getLoanResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, getLoanResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent,isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    getLoanRequest = mapper.readValue(data, GetLoanRequest.class);

	    // Logging specific params in report.
	    getLoanResponse.getLogsReport().setAmount(getLoanRequest.getLoanAmount());

	    String requestValidationStatus = Validator.validateRequest(msisdn, getLoanRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		getLoanRequest.setIsB2B(isFromB2B);
		getLoanResponse = financialServicesBusiness.getLoanBusiness(msisdn, getLoanRequest, getLoanResponse);
	    } else {
		getLoanResponse.setCallStatus(Constants.Call_Status_False);
		getLoanResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		getLoanResponse.setResultDesc(requestValidationStatus);
	    }

	    getLoanResponse.getLogsReport().setResponseCode(getLoanResponse.getResultCode());
	    getLoanResponse.getLogsReport().setRequestTime(requestTime);
	    getLoanResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    getLoanResponse.getLogsReport().setResponse(mapper.writeValueAsString(getLoanResponse));

	    // Sending report log into queue.
	    getLoanResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(getLoanResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(getLoanResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    getLoanResponse.setCallStatus(Constants.Call_Status_False);

	    getLoanResponse.setResultCode(Constants.EXCEPTION_CODE);
	    getLoanResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    getLoanResponse.getLogsReport().setResponseCode(getLoanResponse.getResultCode());
	    getLoanResponse.getLogsReport().setRequestTime(requestTime);
	    getLoanResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    getLoanResponse.getLogsReport().setResponse(mapper.writeValueAsString(getLoanResponse));

	    // Sending report log into queue.
	    getLoanResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(getLoanResponse.getLogsReport());
	}
	return getLoanResponse;
    }

    @RequestMapping(value = "/getloanhistory", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public LoanHistoryResponse getLoanHistory(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	LoanHistoryRequest loanHistoryRequest = new LoanHistoryRequest();
	LoanHistoryResponse loanHistoryResponse = new LoanHistoryResponse();

	String TRANSACTION_NAME = Transactions.LOAN_REQUEST_HISTORY_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    /*
	     * String msisdn =
	     * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)
	     * ).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    loanHistoryResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, loanHistoryResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    loanHistoryRequest = mapper.readValue(data, LoanHistoryRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, loanHistoryRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		loanHistoryRequest.setIsB2B(isFromB2B);
		loanHistoryResponse = financialServicesBusiness.getLoanHistory(msisdn, loanHistoryRequest,
			loanHistoryResponse);
	    } else {
		loanHistoryResponse.setCallStatus(Constants.Call_Status_False);
		loanHistoryResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		loanHistoryResponse.setResultDesc(requestValidationStatus);
	    }

	    loanHistoryResponse.getLogsReport().setResponseCode(loanHistoryResponse.getResultCode());
	    loanHistoryResponse.getLogsReport().setRequestTime(requestTime);
	    loanHistoryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    loanHistoryResponse.getLogsReport().setResponse(mapper.writeValueAsString(loanHistoryResponse));

	    // Sending report log into queue.
	    loanHistoryResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(loanHistoryResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(loanHistoryResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    loanHistoryResponse.setCallStatus(Constants.Call_Status_False);

	    loanHistoryResponse.setResultCode(Constants.EXCEPTION_CODE);
	    loanHistoryResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    loanHistoryResponse.getLogsReport().setResponseCode(loanHistoryResponse.getResultCode());
	    loanHistoryResponse.getLogsReport().setRequestTime(requestTime);
	    loanHistoryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    loanHistoryResponse.getLogsReport().setResponse(mapper.writeValueAsString(loanHistoryResponse));

	    // Sending report log into queue.
	    loanHistoryResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(loanHistoryResponse.getLogsReport());

	}
	return loanHistoryResponse;
    }

    @RequestMapping(value = "/getpaymenthistory", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public PaymentHistoryResponse getPaymentHistory(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();
	PaymentHistoryRequest paymentHistoryRequest = new PaymentHistoryRequest();
	PaymentHistoryResponse paymentHistoryResponse = new PaymentHistoryResponse();

	String TRANSACTION_NAME = Transactions.LOAN_PAYMENT_HISTORY_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    /*
	     * String msisdn =
	     * Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)
	     * ).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    paymentHistoryResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, paymentHistoryResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    paymentHistoryRequest = mapper.readValue(data, PaymentHistoryRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, paymentHistoryRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		paymentHistoryRequest.setIsB2B(isFromB2B);
		paymentHistoryResponse = financialServicesBusiness.getPaymentHistory(msisdn, paymentHistoryRequest,
			paymentHistoryResponse);
	    } else {
		paymentHistoryResponse.setCallStatus(Constants.Call_Status_False);
		paymentHistoryResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		paymentHistoryResponse.setResultDesc(requestValidationStatus);
	    }

	    paymentHistoryResponse.getLogsReport().setResponseCode(paymentHistoryResponse.getResultCode());
	    paymentHistoryResponse.getLogsReport().setRequestTime(requestTime);
	    paymentHistoryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    paymentHistoryResponse.getLogsReport().setResponse(mapper.writeValueAsString(paymentHistoryResponse));

	    // Sending report log into queue.
	    paymentHistoryResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(paymentHistoryResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(paymentHistoryResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    paymentHistoryResponse.setCallStatus(Constants.Call_Status_False);

	    paymentHistoryResponse.setResultCode(Constants.EXCEPTION_CODE);
	    paymentHistoryResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    paymentHistoryResponse.getLogsReport().setResponseCode(paymentHistoryResponse.getResultCode());
	    paymentHistoryResponse.getLogsReport().setRequestTime(requestTime);
	    paymentHistoryResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    paymentHistoryResponse.getLogsReport().setResponse(mapper.writeValueAsString(paymentHistoryResponse));

	    // Sending report log into queue.
	    paymentHistoryResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(paymentHistoryResponse.getLogsReport());
	}
	return paymentHistoryResponse;
    }
}
package com.evampsaanga.bakcell.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.GetReportsBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.getreports.DownloadFileRequest;
import com.evampsaanga.bakcell.models.getreports.DownloadFileResponse;
import com.evampsaanga.bakcell.models.getreports.GetReportsRequest;
import com.evampsaanga.bakcell.models.getreports.GetReportsResponse;
import com.evampsaanga.bakcell.models.getreports.SaveFileRequest;
import com.evampsaanga.bakcell.models.getreports.SaveFileResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/notification")
public class NotificationAdminController {
	Logger logger = Logger.getLogger(NotificationAdminController.class);

	@RequestMapping(value = "/getreports", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetReportsResponse getReports(@RequestBody String data,
			@RequestHeader(value = "credentials") String credentials, HttpServletRequest servletRequest)
			throws ClassNotFoundException, SQLException, JsonParseException, JsonMappingException, IOException,
			JMSException {

		ObjectMapper mapper = new ObjectMapper();
		GetReportsBusiness getReportsBusiness = new GetReportsBusiness();
		GetReportsRequest getReportsRequest = new GetReportsRequest();
		GetReportsResponse getReportsResponse = new GetReportsResponse();

		if (credentials.equalsIgnoreCase(Constants.CREDENTIALS_FOR_INTERNAL_CALLS)) {

			String msisdn = "Not Applicable";
			String lang = "3";

			String requestTime = Utilities.getReportDateTime();

			String TRANSACTION_NAME = "GET REPORTS";

			try {

				Utilities.printInfoLog(TRANSACTION_NAME + "-Request Landed in-" + TRANSACTION_NAME, logger);
				Utilities.printInfoLog(TRANSACTION_NAME + "-Request Data-" + data, logger);

				getReportsRequest = mapper.readValue(data, GetReportsRequest.class);

				String requestValidationStatus = Validator.validateRequest(msisdn, getReportsRequest);

				if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

					getReportsResponse = getReportsBusiness.getReports(getReportsRequest, getReportsResponse);

				} else {

					getReportsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
					getReportsResponse.setResultDesc(requestValidationStatus);
				}

				getReportsResponse.getLogsReport().setRequestTime(requestTime);
				getReportsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				getReportsResponse.getLogsReport().setResponse(mapper.writeValueAsString(getReportsResponse));
				getReportsResponse.getLogsReport().setResponseCode(getReportsResponse.getResultCode());

				// Sending report log into queue.
				Utilities.prepareLogReportForQueue(getReportsResponse.getLogsReport());

				Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
						+ mapper.writeValueAsString(getReportsResponse), logger);

			} catch (Exception e) {

				Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);

				getReportsResponse.setCallStatus(Constants.Call_Status_False);
				getReportsResponse.setResultCode(Constants.EXCEPTION_CODE);
				getReportsResponse
						.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

				getReportsResponse.getLogsReport().setResponseCode(getReportsResponse.getResultCode());
				getReportsResponse.getLogsReport().setRequestTime(requestTime);
				getReportsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				getReportsResponse.getLogsReport().setResponse(mapper.writeValueAsString(getReportsResponse));

				// Sending report log into queue.
				Utilities.prepareLogReportForQueue(getReportsResponse.getLogsReport());

			}
		} else {

			getReportsResponse.setCallStatus(Constants.Call_Status_False);
			getReportsResponse.setResultCode(Constants.UNAUTHORIZED_ACCESS);
			getReportsResponse.setResultDesc("UNAUTHORIZED ACCESS.");

		}
		return getReportsResponse;
	}

	@RequestMapping(value = "/savefile", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public SaveFileResponse savePicFile(@RequestBody String data,
			@RequestHeader(value = "credentials") String credentials, HttpServletRequest servletRequest)
			throws ClassNotFoundException, SQLException, JsonParseException, JsonMappingException, IOException,
			JMSException {

		ObjectMapper mapper = new ObjectMapper();
		SaveFileRequest saveFileRequest = new SaveFileRequest();
		SaveFileResponse saveFileResponse = new SaveFileResponse();

		if (credentials.equalsIgnoreCase(Constants.CREDENTIALS_FOR_INTERNAL_CALLS)) {

			String msisdn = "Not Applicable";
			String lang = "3";

			String requestTime = Utilities.getReportDateTime();

			String TRANSACTION_NAME = "GET REPORTS";

			try {

				Utilities.printInfoLog(TRANSACTION_NAME + "-Request Landed in-" + TRANSACTION_NAME, logger);
				Utilities.printInfoLog(TRANSACTION_NAME + "-Request Data-" + data, logger);

				saveFileRequest = mapper.readValue(data, SaveFileRequest.class);

				String requestValidationStatus = Validator.validateRequest(msisdn, saveFileRequest);

				if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
						saveFileResponse.setFileName(new SimpleDateFormat("yyyyMMddhhmmss").format(new Date())
								+ saveFileRequest.getFileName());
						File file = new File("/opt/tomcat/files/" + saveFileResponse.getFileName());
						byte[] decodedBytes = Base64.getDecoder().decode(saveFileRequest.getFile());

						FileOutputStream fos = new FileOutputStream(file);

						fos.write(decodedBytes);
						fos.flush();
						fos.close();

						saveFileResponse.setCallStatus(Constants.Call_Status_True);
						saveFileResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
						saveFileResponse.setResultDesc("successful");
				} else {

					saveFileResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
					saveFileResponse.setResultDesc(requestValidationStatus);
				}

				saveFileResponse.getLogsReport().setRequestTime(requestTime);
				saveFileResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				saveFileResponse.getLogsReport().setResponse(mapper.writeValueAsString(saveFileResponse));
				saveFileResponse.getLogsReport().setResponseCode(saveFileResponse.getResultCode());

				// Sending report log into queue.
				Utilities.prepareLogReportForQueue(saveFileResponse.getLogsReport());

				Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
						+ mapper.writeValueAsString(saveFileResponse), logger);

			} catch (Exception e) {

				Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);

				saveFileResponse.setCallStatus(Constants.Call_Status_False);
				saveFileResponse.setResultCode(Constants.EXCEPTION_CODE);
				saveFileResponse
						.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

				saveFileResponse.getLogsReport().setResponseCode(saveFileResponse.getResultCode());
				saveFileResponse.getLogsReport().setRequestTime(requestTime);
				saveFileResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
				saveFileResponse.getLogsReport().setResponse(mapper.writeValueAsString(saveFileResponse));

				// Sending report log into queue.
				Utilities.prepareLogReportForQueue(saveFileResponse.getLogsReport());

			}
		} else {

			saveFileResponse.setCallStatus(Constants.Call_Status_False);
			saveFileResponse.setResultCode(Constants.UNAUTHORIZED_ACCESS);
			saveFileResponse.setResultDesc("UNAUTHORIZED ACCESS.");

		}
		return saveFileResponse;
	}

}

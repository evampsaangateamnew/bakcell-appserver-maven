/**
 * 
 */
package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.CoreServicesBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.coreservices.getcoreservices.GetCoreServicesRequest;
import com.evampsaanga.bakcell.models.coreservices.getcoreservices.GetCoreServicesResponse;
import com.evampsaanga.bakcell.models.coreservices.processcoreservices.ProcessCoreServicesRequest;
import com.evampsaanga.bakcell.models.coreservices.processcoreservices.ProcessCoreServicesResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping(Constants.SERVLET_URL + "/coreservices")
public class CoreServicesController {
    @Autowired
    CoreServicesBusiness coreServicesBusiness = new CoreServicesBusiness();
    Logger logger = Logger.getLogger(CoreServicesController.class);

    @RequestMapping(value = "/getcoreservices", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public GetCoreServicesResponse getcoreservices(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();

	GetCoreServicesRequest getCoreServicesRequest = new GetCoreServicesRequest();
	GetCoreServicesResponse getCoreServicesResponse = new GetCoreServicesResponse();

	String requestTime = Utilities.getReportDateTime();

	String TRANSACTION_NAME = Transactions.GET_CORE_SERVICES_TRANSACTION_NAME + " CONTROLLER";
	try {

	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    getCoreServicesResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
		    TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    getCoreServicesResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    if (Utilities.hasJSONKey(data, "accountType")) {

		getCoreServicesRequest = mapper.readValue(data, GetCoreServicesRequest.class);
		getCoreServicesRequest.setBrand(tariffType);
		getCoreServicesRequest.setUserType(userType);

		String requestValidationStatus = Validator.validateRequest(msisdn, getCoreServicesRequest);

		if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		    getCoreServicesResponse = coreServicesBusiness.getCoreServicesBusiness(msisdn,
			    getCoreServicesRequest, getCoreServicesResponse, lang);

		} else {
		    getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
		    getCoreServicesResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		    getCoreServicesResponse.setResultDesc(requestValidationStatus);
		}
	    } else {
		getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
		getCoreServicesResponse.setResultCode(Constants.API_FAILURE_CODE);
		getCoreServicesResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
			"appNewVersion.feature.", Utilities.getValueFromJSON(data, "lang")));
	    }
	    getCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
	    getCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    getCoreServicesResponse.getLogsReport().setResponse(mapper.writeValueAsString(getCoreServicesResponse));
	    getCoreServicesResponse.getLogsReport().setResponseCode(getCoreServicesResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(getCoreServicesResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(getCoreServicesResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
	    getCoreServicesResponse.setResultCode(Constants.EXCEPTION_CODE);
	    getCoreServicesResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    getCoreServicesResponse.getLogsReport().setResponseCode(getCoreServicesResponse.getResultCode());
	    getCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
	    getCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    getCoreServicesResponse.getLogsReport().setResponse(mapper.writeValueAsString(getCoreServicesResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(getCoreServicesResponse.getLogsReport());

	}
	return getCoreServicesResponse;
    }

    @RequestMapping(value = "/processcoreservices", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public ProcessCoreServicesResponse processCoreServices(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	ProcessCoreServicesRequest processCoreServicesRequest = new ProcessCoreServicesRequest();
	ProcessCoreServicesResponse processCoreServicesResponse = new ProcessCoreServicesResponse();

	String requestTime = Utilities.getReportDateTime();

	String TRANSACTION_NAME = Transactions.MANIPULATE_CORE_SERVICES_TRANSACTION_NAME + " CONTROLLER";
	try {
	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    processCoreServicesResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
		    TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    processCoreServicesResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    Utilities.printInfoLog(msisdn + "-BRAND-" + tariffType, logger);
	    Utilities.printInfoLog(msisdn + "-USER TYPE-" + userType, logger);
	    if (Utilities.hasJSONKey(data, "accountType")) {
		processCoreServicesRequest = mapper.readValue(data, ProcessCoreServicesRequest.class);
		processCoreServicesRequest.setBrand(tariffType);
		processCoreServicesRequest.setUserType(userType);
		// Specific params for report log
		processCoreServicesResponse.getLogsReport()
			.setCallForwardNumber(processCoreServicesRequest.getNumber());
		String requestValidationStatus = Validator.validateRequest(msisdn, processCoreServicesRequest);

		if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		    processCoreServicesResponse = coreServicesBusiness.processCoreServicesBusiness(msisdn,
			    processCoreServicesRequest, processCoreServicesResponse);

		} else {
		    processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
		    processCoreServicesResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		    processCoreServicesResponse.setResultDesc(requestValidationStatus);
		}
	    } else {
		processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
		processCoreServicesResponse.setResultCode(Constants.API_FAILURE_CODE);
		processCoreServicesResponse
			.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));
	    }
	    processCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
	    processCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    processCoreServicesResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(processCoreServicesResponse));
	    processCoreServicesResponse.getLogsReport().setResponseCode(processCoreServicesResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(processCoreServicesResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(processCoreServicesResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
	    processCoreServicesResponse.setResultCode(Constants.EXCEPTION_CODE);
	    processCoreServicesResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    processCoreServicesResponse.getLogsReport().setResponseCode(processCoreServicesResponse.getResultCode());
	    processCoreServicesResponse.getLogsReport().setRequestTime(requestTime);
	    processCoreServicesResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    processCoreServicesResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(processCoreServicesResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(processCoreServicesResponse.getLogsReport());

	}
	return processCoreServicesResponse;
    }

}

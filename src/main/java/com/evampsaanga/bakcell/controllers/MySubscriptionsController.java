/**
 * 
 */
package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.MySubscriptionsBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.mysubscriptions.getsubscriptions.MySubscriptionsRequest;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponse;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */

@RestController
@RequestMapping(Constants.SERVLET_URL + "/mysubscriptions")
public class MySubscriptionsController {

    Logger logger = Logger.getLogger(MySubscriptionsController.class);
    @Autowired
    MySubscriptionsBusiness mySubscriptionsBusiness;

    @RequestMapping(value = "/getsubscriptions", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SupplementaryOfferingsResponse getSubscriptions(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();

	MySubscriptionsRequest mySubscriptionsRequest = new MySubscriptionsRequest();
	SupplementaryOfferingsResponse mySubscriptionsResponse = new SupplementaryOfferingsResponse();

	String TRANSACTION_NAME = Transactions.MYSUBSCRIPTION_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    mySubscriptionsResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
		    TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    mySubscriptionsResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    mySubscriptionsRequest = mapper.readValue(data, MySubscriptionsRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, mySubscriptionsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		mySubscriptionsResponse = mySubscriptionsBusiness.getSubscriptions(msisdn, mySubscriptionsRequest,
			mySubscriptionsResponse);
		sortSubscriptionOfferings(mySubscriptionsResponse);
	    } else {
		mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);
		mySubscriptionsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		mySubscriptionsResponse.setResultDesc(requestValidationStatus);
	    }

	    mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
	    mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
	    mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(mySubscriptionsResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);

	    mySubscriptionsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    mySubscriptionsResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
	    mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
	    mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());

	}
	return mySubscriptionsResponse;
    }

    private void sortSubscriptionOfferings(SupplementaryOfferingsResponse supplementaryOfferingsResponse) {

	if (supplementaryOfferingsResponse != null && supplementaryOfferingsResponse.getData() != null) {

	    // Sorting Internet offers
	    if (supplementaryOfferingsResponse.getData().getInternet() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getInternet()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting Compaign offers
	    if (supplementaryOfferingsResponse.getData().getCampaign() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getCampaign()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting SMS offers
	    if (supplementaryOfferingsResponse.getData().getSms() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getSms().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting SMS offers
	    if (supplementaryOfferingsResponse.getData().getCall() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getCall().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting TM offers
	    if (supplementaryOfferingsResponse.getData().getTm() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getTm().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting Hybrid offers
	    if (supplementaryOfferingsResponse.getData().getHybrid() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getHybrid()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting Roaming offers
	    if (supplementaryOfferingsResponse.getData().getRoaming() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getRoaming()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrderMS() != null
				    && o2.getHeader().getSortOrderMS() != null) {
				return o1.getHeader().getSortOrderMS().compareTo(o2.getHeader().getSortOrderMS());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	}
    }

    @RequestMapping(value = "/getsubscriptionsforportal", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SupplementaryOfferingsResponse getSubscriptionsForPortal(@RequestBody String data,
	    @RequestHeader(value = "credentials") String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	MySubscriptionsRequest mySubscriptionsRequest = new MySubscriptionsRequest();
	SupplementaryOfferingsResponse mySubscriptionsResponse = new SupplementaryOfferingsResponse();

	String TRANSACTION_NAME = Transactions.MYSUBSCRIPTION__FOR_PORTAL_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(data, Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    mySubscriptionsResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, "", "", data, lang, mySubscriptionsResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    mySubscriptionsRequest = mapper.readValue(data, MySubscriptionsRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, mySubscriptionsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		mySubscriptionsResponse = mySubscriptionsBusiness.getSubscriptions(msisdn, mySubscriptionsRequest,
			mySubscriptionsResponse);
	    } else {
		mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);
		mySubscriptionsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		mySubscriptionsResponse.setResultDesc(requestValidationStatus);
	    }

	    mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
	    mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
	    mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(mySubscriptionsResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    mySubscriptionsResponse.setCallStatus(Constants.Call_Status_False);

	    mySubscriptionsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    mySubscriptionsResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    mySubscriptionsResponse.getLogsReport().setResponseCode(mySubscriptionsResponse.getResultCode());
	    mySubscriptionsResponse.getLogsReport().setRequestTime(requestTime);
	    mySubscriptionsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    mySubscriptionsResponse.getLogsReport().setResponse(mapper.writeValueAsString(mySubscriptionsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(mySubscriptionsResponse.getLogsReport());

	}
	return mySubscriptionsResponse;
    }
}

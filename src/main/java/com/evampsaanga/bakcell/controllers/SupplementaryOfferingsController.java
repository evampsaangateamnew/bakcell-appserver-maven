/**
 * 
 */
package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.SupplementaryOfferingsBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsRequest;
import com.evampsaanga.bakcell.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsResponse;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsRequest;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponse;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping(Constants.SERVLET_URL + "/supplementaryofferings")
public class SupplementaryOfferingsController {

    Logger logger = Logger.getLogger(SupplementaryOfferingsController.class);

    @Autowired
    SupplementaryOfferingsBusiness supplementaryOfferingsBusiness;

    @RequestMapping(value = "/getsupplementaryofferings", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SupplementaryOfferingsResponse getSupplementryOfferings(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();

	SupplementaryOfferingsRequest supplementaryOfferingsRequest = new SupplementaryOfferingsRequest();
	SupplementaryOfferingsResponse supplementaryOfferingsResponse = new SupplementaryOfferingsResponse();

	String TRANSACTION_NAME = Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    supplementaryOfferingsResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
		    TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    supplementaryOfferingsResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    supplementaryOfferingsRequest = mapper.readValue(data, SupplementaryOfferingsRequest.class);
	    String requestValidationStatus = Validator.validateRequest(msisdn, supplementaryOfferingsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		supplementaryOfferingsResponse = supplementaryOfferingsBusiness.getSupplementaryOfferings(msisdn,
			supplementaryOfferingsRequest, supplementaryOfferingsResponse);
		sortSupplementaryOfferings(supplementaryOfferingsResponse);
	    } else {
		supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_False);
		supplementaryOfferingsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		supplementaryOfferingsResponse.setResultDesc(requestValidationStatus);
	    }

	    supplementaryOfferingsResponse.getLogsReport()
		    .setResponseCode(supplementaryOfferingsResponse.getResultCode());
	    supplementaryOfferingsResponse.getLogsReport().setRequestTime(requestTime);
	    supplementaryOfferingsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    supplementaryOfferingsResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(supplementaryOfferingsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(supplementaryOfferingsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(supplementaryOfferingsResponse), logger);
	} catch (Exception e) {

	    e.printStackTrace();

	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_False);
	    supplementaryOfferingsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    supplementaryOfferingsResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));
	    supplementaryOfferingsResponse.getLogsReport()
		    .setResponseCode(supplementaryOfferingsResponse.getResultCode());
	    supplementaryOfferingsResponse.getLogsReport().setRequestTime(requestTime);
	    supplementaryOfferingsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    supplementaryOfferingsResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(supplementaryOfferingsResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(supplementaryOfferingsResponse.getLogsReport());

	}
	/**
	 * Either success/failure, below success code in case of failure is to show
	 * empty cards in screen.
	 */
	supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_True);
	supplementaryOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
	return supplementaryOfferingsResponse;
    }

    private void sortSupplementaryOfferings(SupplementaryOfferingsResponse supplementaryOfferingsResponse) {

	if (supplementaryOfferingsResponse != null && supplementaryOfferingsResponse.getData() != null) {

	    // Sorting Internet offers
	    if (supplementaryOfferingsResponse.getData().getInternet() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getInternet()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting Compaign offers
	    if (supplementaryOfferingsResponse.getData().getCampaign() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getCampaign()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting SMS offers
	    if (supplementaryOfferingsResponse.getData().getSms() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getSms().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting SMS offers
	    if (supplementaryOfferingsResponse.getData().getCall() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getCall().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting TM offers
	    if (supplementaryOfferingsResponse.getData().getTm() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getTm().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting Hybrid offers
	    if (supplementaryOfferingsResponse.getData().getHybrid() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getHybrid()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting Roaming offers
	    if (supplementaryOfferingsResponse.getData().getRoaming() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getRoaming()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	}
    }

    @RequestMapping(value = "/changesupplementaryoffering", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public ChangeSupplementaryOfferingsResponse changeSupplementaryOffering(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();

	ChangeSupplementaryOfferingsRequest changeSupplementaryOfferingRequest = new ChangeSupplementaryOfferingsRequest();
	ChangeSupplementaryOfferingsResponse changeSupplementaryOfferingResponse = new ChangeSupplementaryOfferingsResponse();

	String TRANSACTION_NAME = Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    changeSupplementaryOfferingResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
		    TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    changeSupplementaryOfferingResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    changeSupplementaryOfferingRequest = mapper.readValue(data, ChangeSupplementaryOfferingsRequest.class);

	    // Logging specific params
	    changeSupplementaryOfferingResponse.getLogsReport()
		    .setSupplementaryOfferingId(changeSupplementaryOfferingRequest.getOfferingId());

	    changeSupplementaryOfferingResponse.getLogsReport()
		    .setSupplementaryOfferingName(changeSupplementaryOfferingRequest.getOfferName());
	    changeSupplementaryOfferingResponse.getLogsReport()
		    .setActionType(changeSupplementaryOfferingRequest.getActionType());

	    String requestValidationStatus = Validator.validateRequest(msisdn, changeSupplementaryOfferingRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		changeSupplementaryOfferingResponse = supplementaryOfferingsBusiness.changeSupplementaryOffering(msisdn,
			changeSupplementaryOfferingRequest, changeSupplementaryOfferingResponse);
	    } else {
		changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_False);
		changeSupplementaryOfferingResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		changeSupplementaryOfferingResponse.setResultDesc(requestValidationStatus);
	    }

	    changeSupplementaryOfferingResponse.getLogsReport()
		    .setResponseCode(changeSupplementaryOfferingResponse.getResultCode());
	    changeSupplementaryOfferingResponse.getLogsReport().setRequestTime(requestTime);
	    changeSupplementaryOfferingResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    changeSupplementaryOfferingResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(changeSupplementaryOfferingResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(changeSupplementaryOfferingResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(changeSupplementaryOfferingResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_False);

	    changeSupplementaryOfferingResponse.setResultCode(Constants.EXCEPTION_CODE);
	    changeSupplementaryOfferingResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    changeSupplementaryOfferingResponse.getLogsReport()
		    .setResponseCode(changeSupplementaryOfferingResponse.getResultCode());
	    changeSupplementaryOfferingResponse.getLogsReport().setRequestTime(requestTime);
	    changeSupplementaryOfferingResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    changeSupplementaryOfferingResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(changeSupplementaryOfferingResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(changeSupplementaryOfferingResponse.getLogsReport());

	}
	return changeSupplementaryOfferingResponse;
    }
}

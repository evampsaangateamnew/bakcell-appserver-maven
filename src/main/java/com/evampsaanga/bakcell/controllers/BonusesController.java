package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;
import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.evampsaanga.bakcell.business.BonusesBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.bonus.GetBonusPointsRequest;
import com.evampsaanga.bakcell.models.bonus.GetBonusPointsResponse;
import com.evampsaanga.bakcell.models.bonus.SaveBonusPointsRequest;
import com.evampsaanga.bakcell.models.bonus.SaveBonusPointsResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(Constants.SERVLET_URL + "/bonus")
public class BonusesController {

	Logger logger = Logger.getLogger(BonusesController.class);

	@Autowired
	BonusesBusiness bonusesBusiness;

	@RequestMapping(value = "/getbonuspoints", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public GetBonusPointsResponse getBonusPoints(
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		String data = "{}";
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();

		GetBonusPointsRequest getBonusPointsRequest = new GetBonusPointsRequest();
		GetBonusPointsResponse getBonusPointsResponse = new GetBonusPointsResponse();
		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.GET_BONUS_POINTS_TRANSACTION_NAME + " CONTROLLER";
		try {
			data = jsonObject.toString();
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			getBonusPointsResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					getBonusPointsResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

			Utilities.printInfoLog(msisdn + "-Request Landed in " + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			getBonusPointsRequest = mapper.readValue(data, GetBonusPointsRequest.class);
			String requestValidationStatus = Validator.validateRequest(msisdn, getBonusPointsRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				getBonusPointsResponse = bonusesBusiness.getBonusPointsBusiness(msisdn, getBonusPointsRequest,
						getBonusPointsResponse);

			} else {
				getBonusPointsResponse.setCallStatus(Constants.Call_Status_False);
				getBonusPointsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				getBonusPointsResponse.setResultDesc(requestValidationStatus);
			}

			getBonusPointsResponse.getLogsReport().setRequestTime(requestTime);
			getBonusPointsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getBonusPointsResponse.getLogsReport().setResponse(mapper.writeValueAsString(getBonusPointsResponse));
			getBonusPointsResponse.getLogsReport().setResponseCode(getBonusPointsResponse.getResultCode());

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(getBonusPointsResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(getBonusPointsResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			getBonusPointsResponse.setCallStatus(Constants.Call_Status_False);
			getBonusPointsResponse.setResultCode(Constants.EXCEPTION_CODE);
			getBonusPointsResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			getBonusPointsResponse.getLogsReport().setResponseCode(getBonusPointsResponse.getResultCode());
			getBonusPointsResponse.getLogsReport().setRequestTime(requestTime);
			getBonusPointsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			getBonusPointsResponse.getLogsReport().setResponse(mapper.writeValueAsString(getBonusPointsResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(getBonusPointsResponse.getLogsReport());

		}
		return getBonusPointsResponse;
	}
	
	@RequestMapping(value = "/savebonuspoints", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
	public SaveBonusPointsResponse saveBonusPoints(
			@RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
			@RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
			@RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
			@RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
			@RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
			HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
			JsonMappingException, IOException, JMSException {
		String data = "{}";
		ObjectMapper mapper = new ObjectMapper();
		JSONObject jsonObject = new JSONObject();

		SaveBonusPointsRequest saveBonusPointsRequest = new SaveBonusPointsRequest();
		SaveBonusPointsResponse saveBonusPointsResponse = new SaveBonusPointsResponse();
		String requestTime = Utilities.getReportDateTime();

		String TRANSACTION_NAME = Transactions.SAVE_BONUS_POINTS_TRANSACTION_NAME + " CONTROLLER";
		try {
			data = jsonObject.toString();
			String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
					Constants.MSISDN_KEY);

			// Populating report object before processing business logic.
			saveBonusPointsResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
					servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
					saveBonusPointsResponse.getLogsReport()));

			data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

			Utilities.printInfoLog(msisdn + "-Request Landed in " + TRANSACTION_NAME, logger);
			Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

			saveBonusPointsRequest = mapper.readValue(data, SaveBonusPointsRequest.class);
			String requestValidationStatus = Validator.validateRequest(msisdn, saveBonusPointsRequest);

			if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

				saveBonusPointsResponse = bonusesBusiness.saveBonusPointsBusiness(msisdn, saveBonusPointsRequest,
						saveBonusPointsResponse);

			} else {
				saveBonusPointsResponse.setCallStatus(Constants.Call_Status_False);
				saveBonusPointsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
				saveBonusPointsResponse.setResultDesc(requestValidationStatus);
			}

			saveBonusPointsResponse.getLogsReport().setRequestTime(requestTime);
			saveBonusPointsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			saveBonusPointsResponse.getLogsReport().setResponse(mapper.writeValueAsString(saveBonusPointsResponse));
			saveBonusPointsResponse.getLogsReport().setResponseCode(saveBonusPointsResponse.getResultCode());

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(saveBonusPointsResponse.getLogsReport());

			Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
					+ mapper.writeValueAsString(saveBonusPointsResponse), logger);
		} catch (Exception e) {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
			saveBonusPointsResponse.setCallStatus(Constants.Call_Status_False);
			saveBonusPointsResponse.setResultCode(Constants.EXCEPTION_CODE);
			saveBonusPointsResponse
					.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

			saveBonusPointsResponse.getLogsReport().setResponseCode(saveBonusPointsResponse.getResultCode());
			saveBonusPointsResponse.getLogsReport().setRequestTime(requestTime);
			saveBonusPointsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
			saveBonusPointsResponse.getLogsReport().setResponse(mapper.writeValueAsString(saveBonusPointsResponse));

			// Sending report log into queue.
			Utilities.prepareLogReportForQueue(saveBonusPointsResponse.getLogsReport());

		}
		return saveBonusPointsResponse;
	}
}

/**
 * 
 */
package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.Date;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.DashboardBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.customerservices.forgotpassword.ForgotPasswordResponseV2;
import com.evampsaanga.bakcell.models.dashboardservice.DashboardRequest;
import com.evampsaanga.bakcell.models.dashboardservice.DashboardResponse;
import com.evampsaanga.bakcell.models.dashboardservice.LoginData;
import com.evampsaanga.bakcell.models.dashboardservice.usersgroup.UserGroupRequest;
import com.evampsaanga.bakcell.models.dashboardservice.usersgroup.UserGroupResponseBase;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */

@RestController
@RequestMapping(Constants.SERVLET_URL + "/dashboardservices")
public class DashboardController_V2 {

    Logger logger = Logger.getLogger(DashboardController_V2.class);
    @Autowired
    DashboardBusiness dashboardControllerBusiness;

    @RequestMapping(value = "/gethomepage", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public ForgotPasswordResponseV2 getHomePage(@RequestBody String data,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String UserAgent,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.DEVICE_ID_KEY, defaultValue = Constants.DEVICE_ID_DEFAULT_VALUE) String deviceID,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();

	DashboardRequest dashboardRequest = new DashboardRequest();
	ForgotPasswordResponseV2 forgotPasswordResponseV2 = new ForgotPasswordResponseV2();

	String TRANSACTION_NAME = Transactions.DASHBOARD_TRANSACTION_NAME + " CONTROLLER";

	String requestTime = Utilities.getReportDateTime();
	try {

	    String msisdn = Utilities.getValueFromJSON(data, Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    forgotPasswordResponseV2.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    UserAgent, "", "", data, lang, forgotPasswordResponseV2.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, UserAgent, isFromB2B);
	    // String iP = "\"iP\": \"127.0.0.1\"";
	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    dashboardRequest = mapper.readValue(data, DashboardRequest.class);
	    dashboardRequest.setChannel(UserAgent);
	    dashboardRequest.setLang(lang);
	    dashboardRequest.setIsB2B(isFromB2B);
	    String requestValidationStatus = Validator.validateRequest(msisdn, dashboardRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		forgotPasswordResponseV2 = dashboardControllerBusiness.getloginBusiness(isFromB2B, msisdn, deviceID,
			dashboardRequest, forgotPasswordResponseV2);
	    } else {
		forgotPasswordResponseV2.setCallStatus(Constants.Call_Status_False);
		forgotPasswordResponseV2.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		forgotPasswordResponseV2.setResultDesc(requestValidationStatus);
	    }

	    forgotPasswordResponseV2.getLogsReport().setResponseCode(forgotPasswordResponseV2.getResultCode());
	    forgotPasswordResponseV2.getLogsReport().setRequestTime(requestTime);
	    forgotPasswordResponseV2.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    forgotPasswordResponseV2.getLogsReport().setResponse(mapper.writeValueAsString(forgotPasswordResponseV2));

	    // Sending report log into queue.commenting due to db issues
	    forgotPasswordResponseV2.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(forgotPasswordResponseV2.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(forgotPasswordResponseV2), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    forgotPasswordResponseV2.setCallStatus(Constants.Call_Status_False);
	    forgotPasswordResponseV2.setResultCode(Constants.EXCEPTION_CODE);
	    forgotPasswordResponseV2
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    forgotPasswordResponseV2.getLogsReport().setResponseCode(forgotPasswordResponseV2.getResultCode());
	    forgotPasswordResponseV2.getLogsReport().setRequestTime(requestTime);
	    forgotPasswordResponseV2.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    forgotPasswordResponseV2.getLogsReport().setResponse(mapper.writeValueAsString(forgotPasswordResponseV2));

	    // Sending report log into queue.
	    forgotPasswordResponseV2.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(forgotPasswordResponseV2.getLogsReport());

	}
	return forgotPasswordResponseV2;
    }

    @RequestMapping(value = "/getusergroupdata", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public UserGroupResponseBase getUserandGroupData(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String UserAgent,
	    @RequestHeader(value = Constants.DEVICE_ID_KEY, defaultValue = Constants.DEVICE_ID_DEFAULT_VALUE) String deviceID,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws SocketException, SQLException {
	logger.info("E&S -- Request Land Time " + new Date());
	ObjectMapper mapper = new ObjectMapper();

	UserGroupRequest userGroupRequest = new UserGroupRequest();
	UserGroupResponseBase userGroupResponseBase = new UserGroupResponseBase();
	String TRANSACTION_NAME = Transactions.USER_GROUP_DATA_TRANSACTION_NAME + " CONTROLLER";

	try {

	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    userGroupResponseBase.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    UserAgent, "", "", data, lang, userGroupResponseBase.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, UserAgent, isFromB2B);
	    // String iP = "\"iP\": \"127.0.0.1\"";
	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    userGroupRequest = mapper.readValue(data, UserGroupRequest.class);
	    userGroupRequest.setIsB2B(isFromB2B);
	    userGroupResponseBase = dashboardControllerBusiness.getuserGroupData(isFromB2B, msisdn, deviceID,
		    userGroupRequest);

	} catch (Exception e) {
	    logger.error("Error", e);
	    userGroupResponseBase.setCallStatus(Constants.Call_Status_False);
	    userGroupResponseBase.setResultCode(Constants.EXCEPTION_CODE);
	    userGroupResponseBase
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));
	}

	return userGroupResponseBase;

    }

    @RequestMapping(value = "/gethomepageResume", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public DashboardResponse getHomePageResume(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String UserAgent,
	    @RequestHeader(value = Constants.DEVICE_ID_KEY, defaultValue = Constants.DEVICE_ID_DEFAULT_VALUE) String deviceID,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	logger.info("E&S -- Request Land Time " + new Date());
	ObjectMapper mapper = new ObjectMapper();

	DashboardRequest dashboardRequest = new DashboardRequest();
	DashboardResponse dashboardResponse = new DashboardResponse();

	String TRANSACTION_NAME = Transactions.DASHBOARD_TRANSACTION_NAME + " CONTROLLER";

	String requestTime = Utilities.getReportDateTime();
	try {

	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    String passHash = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    "passHash");

	    // Populating report object before processing business logic.
	    dashboardResponse.setLogsReport(Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), UserAgent, "", "", data, lang, dashboardResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, UserAgent, isFromB2B);
	    // String iP = "\"iP\": \"127.0.0.1\"";
	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    dashboardRequest = mapper.readValue(data, DashboardRequest.class);
	    dashboardRequest.setChannel(UserAgent);
	    dashboardRequest.setLang(lang);
	    dashboardRequest.setPassword("");
	    dashboardRequest.setIsB2B(isFromB2B);

	    dashboardResponse = dashboardControllerBusiness.getDashboardbusiness(isFromB2B, msisdn, deviceID,
		    dashboardRequest, dashboardResponse, passHash);
	    logger.info(dashboardResponse.getResultCode() + "*****" + dashboardResponse.getResultDesc());
	    if (!(dashboardResponse.getResultCode() == null))
		dashboardResponse.getData().setLiveChat(getlink(dashboardRequest.getLang(),
			dashboardRequest.getChannel(), dashboardResponse.getData().getLoginData()));
	    dashboardResponse.getLogsReport().setResponseCode(dashboardResponse.getResultCode());
	    dashboardResponse.getLogsReport().setRequestTime(requestTime);
	    dashboardResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    dashboardResponse.getLogsReport().setResponse(mapper.writeValueAsString(dashboardResponse));

	    // Sending report log into queue.commenting due to db issues
	    dashboardResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(dashboardResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(dashboardResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    dashboardResponse.setCallStatus(Constants.Call_Status_False);
	    dashboardResponse.setResultCode(Constants.EXCEPTION_CODE);
	    dashboardResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    dashboardResponse.getLogsReport().setResponseCode(dashboardResponse.getResultCode());
	    dashboardResponse.getLogsReport().setRequestTime(requestTime);
	    dashboardResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    dashboardResponse.getLogsReport().setResponse(mapper.writeValueAsString(dashboardResponse));

	    // Sending report log into queue.
	    dashboardResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(dashboardResponse.getLogsReport());

	}
	return dashboardResponse;
    }

    private String getlink(String lang, String channel, LoginData loginData) throws JSONException, IOException {
	StringBuilder sBuilder = new StringBuilder();
	try {
	    sBuilder.append(GetConfigurations.getConfigurationFromCache("livechat.baseurl"));
	    // sBuilder.append("https://mystg.bakcell.com:8444/livezilla/enschat.php?ptn=");
	    if (loginData != null) {
		sBuilder.append(loginData.getFirstname()).append(" ");
		sBuilder.append(loginData.getLastname() == null ? "" : loginData.getLastname());
		if (!loginData.getEmail().contains(loginData.getMsisdn()))
		    sBuilder.append("&pte=").append(loginData.getEmail() == null ? "" : loginData.getEmail());
		String language = lang;
		if (language.equals("2"))
		    language = "ru";
		else if (language.equals("3"))
		    language = "en";
		else if (language.equals("4"))
		    language = "az";
		sBuilder.append("&ptl=").append(language).append("&ptp=").append(loginData.getMsisdn());
		sBuilder.append("&hcgs=MQ__&htgs=MQ__&hfk=MQ__");
		if (channel.equals("ios"))
		    sBuilder.append("&epc=I2U4MjM0Mg__&esc=I2U4MjM0Mg__");
		else
		    sBuilder.append("&epc=I0UwMDAzNA__&esc=I0UwMDAzNA__");
		return sBuilder.toString();
	    }
	    logger.info("Live Chat link " + sBuilder.toString());
	} catch (Exception e) {
	    logger.error(e);
	}
	return sBuilder.toString();
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.TariffBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.tariffdetails.TariffRequestV2;
import com.evampsaanga.bakcell.models.tariffdetails.TariffResponseV2;
import com.evampsaanga.bakcell.models.tariffdetails.changetariff.ChangeTariffRequestBulk;
import com.evampsaanga.bakcell.models.tariffdetails.changetariff.ChangeTariffResponseBulk;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */

@RestController
@RequestMapping(Constants.SERVLET_URL + "/tariffservicesV2")
public class TariffsControllerV2 {

    Logger logger = Logger.getLogger(TariffsControllerV2.class);

    @Autowired
    TariffBusiness tariffBusiness;

    @RequestMapping(value = "/gettariffdetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public TariffResponseV2 getTariffDetails(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();
	TariffRequestV2 tariffRequest = new TariffRequestV2();
	TariffResponseV2 tariffResponse = new TariffResponseV2();

	String TRANSACTION_NAME = Transactions.TARRIF_DETAILS_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    /*
	     * String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
	     * decodeString(token)).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    tariffResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, tariffResponse.getLogsReport()));
	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);
	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    tariffRequest = mapper.readValue(data, TariffRequestV2.class);
	    /*
	     * // Store ID value is same as of Language ID.
	     * tariffRequest.setStoreId(tariffRequest.getLang());
	     */

	    String requestValidationStatus = Validator.validateRequest(msisdn, tariffRequest);
	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		tariffRequest.setIsB2B(isFromB2B);
		tariffResponse = tariffBusiness.getTariffDetailsBusinessV2(msisdn, tariffRequest, tariffResponse);
		tariffResponse.getLogsReport().setResponseCode(tariffResponse.getResultCode());
		tariffResponse.getLogsReport().setRequestTime(requestTime);
		tariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		tariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(tariffResponse));

		// Sending report log into queue.
		tariffResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
		Utilities.prepareLogReportForQueue(tariffResponse.getLogsReport());
	    } else {
		tariffResponse.setCallStatus(Constants.Call_Status_False);
		tariffResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		tariffResponse.setResultDesc(requestValidationStatus);
		tariffResponse.getLogsReport().setResponseCode(tariffResponse.getResultCode());
		tariffResponse.getLogsReport().setRequestTime(requestTime);
		tariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		tariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(tariffResponse));

		// Sending report log into queue.
		tariffResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
		Utilities.prepareLogReportForQueue(tariffResponse.getLogsReport());
	    }
	} catch (Exception e) {
	    Utilities.printErrorLog("ERROR:" + e, logger);
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    tariffResponse.setCallStatus(Constants.Call_Status_False);
	    tariffResponse.setResultCode(Constants.EXCEPTION_CODE);

	    tariffResponse.getLogsReport().setResponseCode(tariffResponse.getResultCode());
	    tariffResponse.getLogsReport().setRequestTime(requestTime);
	    tariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    tariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(tariffResponse));

	    // Sending report log into queue.
	    tariffResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(tariffResponse.getLogsReport());

	}
	Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		+ mapper.writeValueAsString(tariffResponse), logger);
	return tariffResponse;
    }

    @RequestMapping(value = "/changetariffbulk", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public ChangeTariffResponseBulk changeTariff(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	ChangeTariffRequestBulk changeTariffRequest = new ChangeTariffRequestBulk();
	ChangeTariffResponseBulk changeTariffResponse = new ChangeTariffResponseBulk();

	String TRANSACTION_NAME = Transactions.CHANGE_TARRIF_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	String msisdn = "";
	try {

	    msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    changeTariffResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    changeTariffResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    changeTariffRequest = mapper.readValue(data, ChangeTariffRequestBulk.class);

	    // Logging specific params

	    String requestValidationStatus = Validator.validateRequest(msisdn, changeTariffRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		Utilities.printInfoLog(msisdn + "-AFTER VALIDATION Request Data-" + requestValidationStatus, logger);
		changeTariffRequest.setIsB2B(isFromB2B);
		changeTariffResponse = tariffBusiness.changeTariffBulkBusiness(msisdn, changeTariffRequest,
			changeTariffResponse);
	    } else {
		changeTariffResponse.setCallStatus(Constants.Call_Status_False);
		changeTariffResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		changeTariffResponse.setResultDesc(requestValidationStatus);
	    }

	    changeTariffResponse.getLogsReport().setResponseCode(changeTariffResponse.getResultCode());
	    changeTariffResponse.getLogsReport().setRequestTime(requestTime);
	    changeTariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    changeTariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(changeTariffResponse));

	    // Sending report log into queue.
	    changeTariffResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(changeTariffResponse.getLogsReport());

	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    changeTariffResponse.setCallStatus(Constants.Call_Status_False);
	    changeTariffResponse.setResultCode(Constants.EXCEPTION_CODE);
	    changeTariffResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    changeTariffResponse.getLogsReport().setResponseCode(changeTariffResponse.getResultCode());
	    changeTariffResponse.getLogsReport().setRequestTime(requestTime);
	    changeTariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    changeTariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(changeTariffResponse));

	    // Sending report log into queue.
	    changeTariffResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(changeTariffResponse.getLogsReport());

	}
	Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		+ mapper.writeValueAsString(changeTariffResponse), logger);
	return changeTariffResponse;
    }
}

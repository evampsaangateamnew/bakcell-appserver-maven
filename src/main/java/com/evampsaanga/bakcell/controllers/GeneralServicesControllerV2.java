/**
 * 
 */
package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.GeneralServicesBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.acceptTNC.AcceptTnCRequest;
import com.evampsaanga.bakcell.models.acceptTNC.AcceptTnCResponse;
import com.evampsaanga.bakcell.models.companyinvoice.CompanyInvoiceDetailRequest;
import com.evampsaanga.bakcell.models.companyinvoice.CompanyInvoiceDetailResponse;
import com.evampsaanga.bakcell.models.companyinvoice.CompanyInvoiceRequest;
import com.evampsaanga.bakcell.models.companyinvoice.CompanyInvoiceResponse;
import com.evampsaanga.bakcell.models.companyinvoice.MsisdnInvoiceDetailRequest;
import com.evampsaanga.bakcell.models.companyinvoice.MsisdnInvoiceDetailResponse;
import com.evampsaanga.bakcell.models.companyinvoice.MsisdnInvoiceRequest;
import com.evampsaanga.bakcell.models.companyinvoice.MsisdnInvoiceResponse;
import com.evampsaanga.bakcell.models.generalservices.contactus.ContactUsRequest;
import com.evampsaanga.bakcell.models.generalservices.contactus.ContactUsResponse;
import com.evampsaanga.bakcell.models.generalservices.faqs.FAQSRequest;
import com.evampsaanga.bakcell.models.generalservices.faqs.FAQSResponse;
import com.evampsaanga.bakcell.models.generalservices.lostreport.ReportLostSIMRequest;
import com.evampsaanga.bakcell.models.generalservices.lostreport.ReportLostSIMResponseV2;
import com.evampsaanga.bakcell.models.generalservices.sendinternetsettings.SendInternetSettingsRequest;
import com.evampsaanga.bakcell.models.generalservices.sendinternetsettings.SendInternetSettingsResponse;
import com.evampsaanga.bakcell.models.generalservices.storelocator.StoreLocatorRequest;
import com.evampsaanga.bakcell.models.generalservices.storelocator.StoreLocatorResponse;
import com.evampsaanga.bakcell.models.generalservices.uploadimage.UploadImageRequest;
import com.evampsaanga.bakcell.models.generalservices.uploadimage.UploadImageResponse;
import com.evampsaanga.bakcell.models.simswap.SimSwapRequest;
import com.evampsaanga.bakcell.models.simswap.SimSwapResponse;
import com.evampsaanga.bakcell.models.verifyappversion.VerifyAppVersionRequest;
import com.evampsaanga.bakcell.models.verifyappversion.VerifyAppVersionResponse;
import com.evampsaanga.bakcell.models.verifyappversion.VerifyAppVersionResponseData;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Aqeel Abbas
 *
 */
@RestController
@RequestMapping(Constants.SERVLET_URL + "/generalservicesV2")
public class GeneralServicesControllerV2 {

    Logger logger = Logger.getLogger(GeneralServicesControllerV2.class);
    @Autowired
    GeneralServicesBusiness generalServicesBusiness;

    @RequestMapping(value = "/getfaqs", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public FAQSResponse getFAQs(
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	JSONObject jsonObject = new JSONObject();
	String data = "{}";
	FAQSRequest faqsRequest = new FAQSRequest();
	FAQSResponse faqsResponse = new FAQSResponse();

	String TRANSACTION_NAME = Transactions.APP_FAQ_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    data = jsonObject.toString(); // use it when there is no request
	    // body required.

	    // Populating report object before processing business logic.
	    faqsResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, faqsResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    faqsRequest = mapper.readValue(data, FAQSRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, faqsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		faqsRequest.setIsB2B(isFromB2B);
		faqsResponse = generalServicesBusiness.faqsBusinessV2(msisdn, faqsRequest, faqsResponse);

	    } else {
		faqsResponse.setCallStatus(Constants.Call_Status_False);
		faqsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		faqsResponse.setResultDesc(requestValidationStatus);
	    }

	    faqsResponse.getLogsReport().setResponseCode(faqsResponse.getResultCode());
	    faqsResponse.getLogsReport().setRequestTime(requestTime);
	    faqsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    faqsResponse.getLogsReport().setResponse(mapper.writeValueAsString(faqsResponse));

	    // Sending report log into queue.
	    faqsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(faqsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(faqsResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    faqsResponse.setCallStatus(Constants.Call_Status_False);

	    faqsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    faqsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    faqsResponse.getLogsReport().setResponseCode(faqsResponse.getResultCode());
	    faqsResponse.getLogsReport().setRequestTime(requestTime);
	    faqsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    faqsResponse.getLogsReport().setResponse(mapper.writeValueAsString(faqsResponse));

	    // Sending report log into queue.
	    faqsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(faqsResponse.getLogsReport());
	}
	return faqsResponse;
    }

    // for phase 2 accept terms and conditions
    @RequestMapping(value = "/acceptTnC", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public AcceptTnCResponse acceptTermsnConditions(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	// variable declaration
	ObjectMapper mapper = new ObjectMapper();
	AcceptTnCRequest acceptTnCRequest = new AcceptTnCRequest();
	AcceptTnCResponse acceptTnCResponse = new AcceptTnCResponse();
	String TRANSACTION_NAME = Transactions.ACCEPT_TNC_NAME_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();

	try {
	    // Populating report object before processing business logic.
	    acceptTnCResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, acceptTnCResponse.getLogsReport()));
	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent,isFromB2B);
	    // printing logs
	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    // Mapping bean
	    acceptTnCRequest = mapper.readValue(data, AcceptTnCRequest.class);
	    // validating request packet
	    String requestValidationStatus = Validator.validateRequest(msisdn, acceptTnCRequest);
	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		// calling business logic
		acceptTnCRequest.setIsB2B(isFromB2B);
		acceptTnCResponse = generalServicesBusiness.AccpetTnCBusiness(msisdn, acceptTnCRequest,
			acceptTnCResponse);
		acceptTnCResponse.getLogsReport().setResponseCode(acceptTnCResponse.getResultCode());
		acceptTnCResponse.getLogsReport().setRequestTime(requestTime);
		acceptTnCResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		acceptTnCResponse.getLogsReport().setResponse(mapper.writeValueAsString(acceptTnCResponse));
		// Sending report log into queue.
		acceptTnCResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
		Utilities.prepareLogReportForQueue(acceptTnCResponse.getLogsReport());
		Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
			+ mapper.writeValueAsString(acceptTnCResponse), logger);
		return acceptTnCResponse;
	    } else {
		logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
		acceptTnCResponse.setCallStatus(Constants.Call_Status_False);
		acceptTnCResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		acceptTnCResponse.setResultDesc(requestValidationStatus);
	    }

	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    acceptTnCResponse.setCallStatus(Constants.Call_Status_False);
	    acceptTnCResponse.setResultCode(Constants.EXCEPTION_CODE);
	    acceptTnCResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));
	    acceptTnCResponse.getLogsReport().setResponseCode(acceptTnCResponse.getResultCode());
	    acceptTnCResponse.getLogsReport().setRequestTime(requestTime);
	    acceptTnCResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    acceptTnCResponse.getLogsReport().setResponse(mapper.writeValueAsString(acceptTnCResponse));

	    // Sending report log into queue.
	    acceptTnCResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(acceptTnCResponse.getLogsReport());
	}
	return acceptTnCResponse;
    }

    @RequestMapping(value = "/getcontactusdetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public ContactUsResponse getContactUsDetails(
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	JSONObject jsonObject = new JSONObject();
	ContactUsRequest contactUsRequest = new ContactUsRequest();
	ContactUsResponse contactUsResponse = new ContactUsResponse();

	String TRANSACTION_NAME = Transactions.CONTACTUS__TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String data = jsonObject.toString();
	    /*
	     * String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
	     * decodeString(token)).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    contactUsResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, contactUsResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    contactUsRequest = mapper.readValue(data, ContactUsRequest.class);
	    // Store ID value is same as of Language ID.
	    contactUsRequest.setStoreId(contactUsRequest.getLang());

	    String requestValidationStatus = Validator.validateRequest(msisdn, contactUsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		contactUsRequest.setIsB2B(isFromB2B);
		contactUsResponse = generalServicesBusiness.contactUsBusiness(msisdn, contactUsRequest,
			contactUsResponse);

	    } else {
		logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
		contactUsResponse.setCallStatus(Constants.Call_Status_False);
		contactUsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		contactUsResponse.setResultDesc(requestValidationStatus);
	    }

	    contactUsResponse.getLogsReport().setResponseCode(contactUsResponse.getResultCode());
	    contactUsResponse.getLogsReport().setRequestTime(requestTime);
	    contactUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    contactUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(contactUsResponse));

	    // Sending report log into queue.
	    contactUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(contactUsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(contactUsResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    contactUsResponse.setCallStatus(Constants.Call_Status_False);

	    contactUsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    contactUsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    contactUsResponse.getLogsReport().setResponseCode(contactUsResponse.getResultCode());
	    contactUsResponse.getLogsReport().setRequestTime(requestTime);
	    contactUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    contactUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(contactUsResponse));

	    // Sending report log into queue.
	    contactUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(contactUsResponse.getLogsReport());

	}
	return contactUsResponse;
    }

    @RequestMapping(value = "/getstoresdetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public StoreLocatorResponse getStoreLocatorDetails(
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();
	JSONObject jsonObject = new JSONObject();
	StoreLocatorRequest storeLocatorRequest = new StoreLocatorRequest();
	StoreLocatorResponse storeLocatorResponse = new StoreLocatorResponse();

	String TRANSACTION_NAME = Transactions.STORE_LOCATOR_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String data = jsonObject.toString(); // use it when there is no request
	    // body required.

	    /*
	     * String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
	     * decodeString(token)).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    storeLocatorResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, storeLocatorResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    storeLocatorRequest = mapper.readValue(data, StoreLocatorRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, storeLocatorRequest);
	    storeLocatorRequest.setIsB2B(isFromB2B);
	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		storeLocatorResponse = generalServicesBusiness.getStoreLocatorsDetailsBusinessV2(msisdn,
			storeLocatorRequest, storeLocatorResponse);
	    } else {
		storeLocatorResponse.setCallStatus(Constants.Call_Status_False);
		storeLocatorResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		storeLocatorResponse.setResultDesc(requestValidationStatus);
	    }

	    storeLocatorResponse.getLogsReport().setResponseCode(storeLocatorResponse.getResultCode());
	    storeLocatorResponse.getLogsReport().setRequestTime(requestTime);
	    storeLocatorResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    storeLocatorResponse.getLogsReport().setResponse(mapper.writeValueAsString(storeLocatorResponse));

	    // Sending report log into queue.
	    storeLocatorResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(storeLocatorResponse.getLogsReport());
	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(storeLocatorResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    storeLocatorResponse.setCallStatus(Constants.Call_Status_False);

	    storeLocatorResponse.setResultCode(Constants.EXCEPTION_CODE);
	    storeLocatorResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    storeLocatorResponse.getLogsReport().setResponseCode(storeLocatorResponse.getResultCode());
	    storeLocatorResponse.getLogsReport().setRequestTime(requestTime);
	    storeLocatorResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    storeLocatorResponse.getLogsReport().setResponse(mapper.writeValueAsString(storeLocatorResponse));

	    // Sending report log into queue.
	    storeLocatorResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(storeLocatorResponse.getLogsReport());
	}

	return storeLocatorResponse;
    }

    @RequestMapping(value = "/reportlostsim", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public ReportLostSIMResponseV2 reportlostsim(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	ReportLostSIMRequest reportLostSIMRequest = new ReportLostSIMRequest();
	ReportLostSIMResponseV2 reportLostSIMResponse = new ReportLostSIMResponseV2();

	String TRANSACTION_NAME = Transactions.REPORT_LOST_SIM_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    JSONObject jObj = new JSONObject(data);

	    // Populating report object before processing business logic.
	    reportLostSIMResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, reportLostSIMResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, jObj.getString("msisdn"), servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    JSONObject jObject = new JSONObject(data);
	    if (jObject.has("individualMsisdn")) {
		jObject.remove("msisdn");
		jObject.put("msisdn", jObject.get("individualMsisdn"));
		jObject.remove("individualMsisdn");
	    }
	    reportLostSIMRequest = mapper.readValue(jObject.toString(), ReportLostSIMRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, reportLostSIMRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		reportLostSIMRequest.setIsB2B(isFromB2B);
		reportLostSIMResponse = generalServicesBusiness.reportLostSIMBusinessV2(msisdn, reportLostSIMRequest,
			reportLostSIMResponse);
	    } else {
		reportLostSIMResponse.setCallStatus(Constants.Call_Status_False);
		reportLostSIMResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		reportLostSIMResponse.setResultDesc(requestValidationStatus);
	    }

	    reportLostSIMResponse.getLogsReport().setResponseCode(reportLostSIMResponse.getResultCode());
	    reportLostSIMResponse.getLogsReport().setRequestTime(requestTime);
	    reportLostSIMResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    reportLostSIMResponse.getLogsReport().setResponse(mapper.writeValueAsString(reportLostSIMResponse));

	    // Sending report log into queue.
	    reportLostSIMResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(reportLostSIMResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(reportLostSIMResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    reportLostSIMResponse.setCallStatus(Constants.Call_Status_False);

	    reportLostSIMResponse.setResultCode(Constants.EXCEPTION_CODE);
	    reportLostSIMResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    reportLostSIMResponse.getLogsReport().setResponseCode(reportLostSIMResponse.getResultCode());
	    reportLostSIMResponse.getLogsReport().setRequestTime(requestTime);
	    reportLostSIMResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    reportLostSIMResponse.getLogsReport().setResponse(mapper.writeValueAsString(reportLostSIMResponse));

	    // Sending report log into queue.
	    reportLostSIMResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(reportLostSIMResponse.getLogsReport());
	}
	return reportLostSIMResponse;
    }

    @RequestMapping(value = "/uploadimage", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public UploadImageResponse uploadProfilePicture(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	UploadImageRequest uploadImageRequest = new UploadImageRequest();
	UploadImageResponse uploadImageResponse = new UploadImageResponse();

	String TRANSACTION_NAME = Transactions.UPLOAD_IMAGE_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    /*
	     * String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
	     * decodeString(token)).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    uploadImageResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, uploadImageResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    String dataWithImage = data;
	    data = Utilities.removeParamsFromJSONObject(data, "image");
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    uploadImageRequest = mapper.readValue(dataWithImage, UploadImageRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, uploadImageRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		uploadImageRequest.setIsB2B(isFromB2B);
		uploadImageResponse = generalServicesBusiness.uploadImageBusiness(msisdn, uploadImageRequest,
			uploadImageResponse);
	    } else {
		uploadImageResponse.setCallStatus(Constants.Call_Status_False);
		uploadImageResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		uploadImageResponse.setResultDesc(requestValidationStatus);
	    }

	    uploadImageResponse.getLogsReport().setResponseCode(uploadImageResponse.getResultCode());
	    uploadImageResponse.getLogsReport().setRequestTime(requestTime);
	    uploadImageResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    uploadImageResponse.getLogsReport().setResponse(mapper.writeValueAsString(uploadImageResponse));

	    // Sending report log into queue.
	    uploadImageResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(uploadImageResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(uploadImageResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    uploadImageResponse.setCallStatus(Constants.Call_Status_False);

	    uploadImageResponse.setResultCode(Constants.EXCEPTION_CODE);
	    uploadImageResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    uploadImageResponse.getLogsReport().setResponseCode(uploadImageResponse.getResultCode());
	    uploadImageResponse.getLogsReport().setRequestTime(requestTime);
	    uploadImageResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    uploadImageResponse.getLogsReport().setResponse(mapper.writeValueAsString(uploadImageResponse));

	    // Sending report log into queue.
	    uploadImageResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(uploadImageResponse.getLogsReport());
	}
	return uploadImageResponse;
    }

    @RequestMapping(value = "/verifyappversion", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public VerifyAppVersionResponse verifyAppVersion(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	VerifyAppVersionRequest verifyAppVersionRequest = new VerifyAppVersionRequest();
	VerifyAppVersionResponse verifyAppVersionResponse = new VerifyAppVersionResponse();

	String TRANSACTION_NAME = Transactions.VERIFY_APP_VERSION_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    // msisdn = "Number not applicable.";

	    String deviceID = servletRequest.getHeader("deviceID");

	    // Populating report object before processing business logic.
	    verifyAppVersionResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, "", "", data, lang, verifyAppVersionResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    verifyAppVersionRequest = mapper.readValue(data, VerifyAppVersionRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, verifyAppVersionRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		verifyAppVersionRequest.setIsB2B(isFromB2B);
		verifyAppVersionResponse = generalServicesBusiness.verifyAppVersionBusiness(msisdn, deviceID,
			verifyAppVersionRequest, verifyAppVersionResponse);
		if (verifyAppVersionResponse.getData() == null) {
		    VerifyAppVersionResponseData appVersionResponseData = new VerifyAppVersionResponseData();
		    verifyAppVersionResponse.setData(appVersionResponseData);
		}

		verifyAppVersionResponse.getData()
			.setPlayStore(GetConfigurations.getConfigurationFromCache("appVersion.update.url.android"));

		verifyAppVersionResponse.getData()
			.setAppStore(GetConfigurations.getConfigurationFromCache("appVersion.update.url.ios"));
	    } else {
		verifyAppVersionResponse.setCallStatus(Constants.Call_Status_False);
		verifyAppVersionResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		verifyAppVersionResponse.setResultDesc(requestValidationStatus);
	    }

	    verifyAppVersionResponse.getLogsReport().setResponseCode(verifyAppVersionResponse.getResultCode());
	    verifyAppVersionResponse.getLogsReport().setRequestTime(requestTime);
	    verifyAppVersionResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    verifyAppVersionResponse.getLogsReport().setResponse(mapper.writeValueAsString(verifyAppVersionResponse));

	    // Sending report log into queue.
	    verifyAppVersionResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(verifyAppVersionResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(verifyAppVersionResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    verifyAppVersionResponse.setCallStatus(Constants.Call_Status_False);

	    verifyAppVersionResponse.setResultCode(Constants.EXCEPTION_CODE);
	    verifyAppVersionResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    verifyAppVersionResponse.getLogsReport().setResponseCode(verifyAppVersionResponse.getResultCode());
	    verifyAppVersionResponse.getLogsReport().setRequestTime(requestTime);
	    verifyAppVersionResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    verifyAppVersionResponse.getLogsReport().setResponse(mapper.writeValueAsString(verifyAppVersionResponse));

	    // Sending report log into queue.
	    verifyAppVersionResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(verifyAppVersionResponse.getLogsReport());
	}
	return verifyAppVersionResponse;
    }

    @RequestMapping(value = "/sendinternetsettings", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SendInternetSettingsResponse sendInternetSettings(
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	JSONObject jsonObject = new JSONObject();
	String data = "{}";
	SendInternetSettingsRequest sendInternetSettingsRequest = new SendInternetSettingsRequest();
	SendInternetSettingsResponse sendInternetSettingsResponse = new SendInternetSettingsResponse();

	String TRANSACTION_NAME = Transactions.SEND_INTERNET_SETTINGS_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    data = jsonObject.toString(); // use it when there is no request
	    // body required.

	    /*
	     * String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
	     * decodeString(token)).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    sendInternetSettingsResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, sendInternetSettingsResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    sendInternetSettingsRequest = mapper.readValue(data, SendInternetSettingsRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, sendInternetSettingsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		sendInternetSettingsRequest.setIsB2B(isFromB2B);
		sendInternetSettingsResponse = generalServicesBusiness.sendInternetSettingsBusiness(msisdn,
			sendInternetSettingsRequest, sendInternetSettingsResponse);
	    } else {
		sendInternetSettingsResponse.setCallStatus(Constants.Call_Status_False);
		sendInternetSettingsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		sendInternetSettingsResponse.setResultDesc(requestValidationStatus);
	    }

	    sendInternetSettingsResponse.getLogsReport().setResponseCode(sendInternetSettingsResponse.getResultCode());
	    sendInternetSettingsResponse.getLogsReport().setRequestTime(requestTime);
	    sendInternetSettingsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    sendInternetSettingsResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(sendInternetSettingsResponse));

	    // Sending report log into queue.
	    sendInternetSettingsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(sendInternetSettingsResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(sendInternetSettingsResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    sendInternetSettingsResponse.setCallStatus(Constants.Call_Status_False);

	    sendInternetSettingsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    sendInternetSettingsResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    sendInternetSettingsResponse.getLogsReport().setResponseCode(sendInternetSettingsResponse.getResultCode());
	    sendInternetSettingsResponse.getLogsReport().setRequestTime(requestTime);
	    sendInternetSettingsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    sendInternetSettingsResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(sendInternetSettingsResponse));

	    // Sending report log into queue.
	    sendInternetSettingsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(sendInternetSettingsResponse.getLogsReport());
	}
	return sendInternetSettingsResponse;
    }

    @RequestMapping(value = "/verify", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SimSwapResponse getSimSwapVerify(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	SimSwapRequest simSwapRequest = new SimSwapRequest();
	SimSwapResponse simSwapResponse = new SimSwapResponse();

	String TRANSACTION_NAME = Transactions.SIMSWAP_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    // data = jsonObject.toString();

	    // Populating report object before processing business logic.
	    simSwapResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, simSwapResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    simSwapRequest = mapper.readValue(data, SimSwapRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, simSwapRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		simSwapRequest.setIsB2B(isFromB2B);
		simSwapResponse = generalServicesBusiness.simswapVerify(msisdn, simSwapRequest, simSwapResponse);

	    } else {
		logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
		simSwapResponse.setCallStatus(Constants.Call_Status_False);
		simSwapResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		simSwapResponse.setResultDesc(requestValidationStatus);
	    }

	    simSwapResponse.getLogsReport().setResponseCode(simSwapResponse.getResultCode());
	    simSwapResponse.getLogsReport().setRequestTime(requestTime);
	    simSwapResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    simSwapResponse.getLogsReport().setResponse(mapper.writeValueAsString(simSwapResponse));

	    // Sending report log into queue.
	    simSwapResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(simSwapResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(simSwapResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    simSwapResponse.setCallStatus(Constants.Call_Status_False);

	    simSwapResponse.setResultCode(Constants.EXCEPTION_CODE);
	    simSwapResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    simSwapResponse.getLogsReport().setResponseCode(simSwapResponse.getResultCode());
	    simSwapResponse.getLogsReport().setRequestTime(requestTime);
	    simSwapResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    simSwapResponse.getLogsReport().setResponse(mapper.writeValueAsString(simSwapResponse));

	    // Sending report log into queue.
	    simSwapResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(simSwapResponse.getLogsReport());

	}
	return simSwapResponse;
    }

    @RequestMapping(value = "/documentValid", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SimSwapResponse getSimSwapDocumentValid(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	SimSwapRequest simSwapRequest = new SimSwapRequest();
	SimSwapResponse simSwapResponse = new SimSwapResponse();

	String TRANSACTION_NAME = Transactions.SIMSWAP_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    // Populating report object before processing business logic.
	    simSwapResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, simSwapResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    simSwapRequest = mapper.readValue(data, SimSwapRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, simSwapRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		simSwapRequest.setIsB2B(isFromB2B);
		simSwapResponse = generalServicesBusiness.simswapDocumentValid(msisdn, simSwapRequest, simSwapResponse);

	    } else {
		logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
		simSwapResponse.setCallStatus(Constants.Call_Status_False);
		simSwapResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		simSwapResponse.setResultDesc(requestValidationStatus);
	    }

	    simSwapResponse.getLogsReport().setResponseCode(simSwapResponse.getResultCode());
	    simSwapResponse.getLogsReport().setRequestTime(requestTime);
	    simSwapResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    simSwapResponse.getLogsReport().setResponse(mapper.writeValueAsString(simSwapResponse));

	    // Sending report log into queue.
	    simSwapResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(simSwapResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(simSwapResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    simSwapResponse.setCallStatus(Constants.Call_Status_False);

	    simSwapResponse.setResultCode(Constants.EXCEPTION_CODE);
	    simSwapResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    simSwapResponse.getLogsReport().setResponseCode(simSwapResponse.getResultCode());
	    simSwapResponse.getLogsReport().setRequestTime(requestTime);
	    simSwapResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    simSwapResponse.getLogsReport().setResponse(mapper.writeValueAsString(simSwapResponse));

	    // Sending report log into queue.
	    simSwapResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(simSwapResponse.getLogsReport());

	}
	return simSwapResponse;
    }

    @RequestMapping(value = "/companysummary", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public CompanyInvoiceResponse getcompanySummary(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	CompanyInvoiceRequest companyInvoiceRequest = new CompanyInvoiceRequest();
	CompanyInvoiceResponse companyInvoiceResponse = new CompanyInvoiceResponse();

	String TRANSACTION_NAME = Transactions.COMPANY_INVOICE_SUMMARY_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    // data = jsonObject.toString();

	    // Populating report object before processing business logic.
	    companyInvoiceResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, companyInvoiceResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    companyInvoiceRequest = mapper.readValue(data, CompanyInvoiceRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, CompanyInvoiceRequest.class);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		companyInvoiceRequest.setIsB2B(isFromB2B);
		companyInvoiceResponse = generalServicesBusiness.getCompanySummary(msisdn, companyInvoiceRequest,
			companyInvoiceResponse);

	    } else {
		logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
		companyInvoiceResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		companyInvoiceResponse.setResultDesc(requestValidationStatus);
	    }

	    companyInvoiceResponse.getLogsReport().setResponseCode(companyInvoiceResponse.getResultCode());
	    companyInvoiceResponse.getLogsReport().setRequestTime(requestTime);
	    companyInvoiceResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    companyInvoiceResponse.getLogsReport().setResponse(mapper.writeValueAsString(companyInvoiceResponse));

	    // Sending report log into queue.
	    companyInvoiceResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(companyInvoiceResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(companyInvoiceResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    companyInvoiceResponse.setCallStatus(Constants.Call_Status_False);

	    companyInvoiceResponse.setResultCode(Constants.EXCEPTION_CODE);
	    companyInvoiceResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    companyInvoiceResponse.getLogsReport().setResponseCode(companyInvoiceResponse.getResultCode());
	    companyInvoiceResponse.getLogsReport().setRequestTime(requestTime);
	    companyInvoiceResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    companyInvoiceResponse.getLogsReport().setResponse(mapper.writeValueAsString(companyInvoiceResponse));

	    // Sending report log into queue.
	    companyInvoiceResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(companyInvoiceResponse.getLogsReport());

	}
	return companyInvoiceResponse;
    }

    @RequestMapping(value = "/companydetail", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public CompanyInvoiceDetailResponse getcompanyDetail(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	CompanyInvoiceDetailRequest companyInvoiceRequest = new CompanyInvoiceDetailRequest();
	CompanyInvoiceDetailResponse companyInvoiceResponse = new CompanyInvoiceDetailResponse();

	String TRANSACTION_NAME = Transactions.COMPANY_INVOICE_DETAILS_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    // data = jsonObject.toString();

	    // Populating report object before processing business logic.
	    companyInvoiceResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, companyInvoiceResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    companyInvoiceRequest = mapper.readValue(data, CompanyInvoiceDetailRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, CompanyInvoiceRequest.class);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		companyInvoiceRequest.setIsB2B(isFromB2B);
		companyInvoiceResponse = generalServicesBusiness.getCompanyDetail(msisdn, companyInvoiceRequest,
			companyInvoiceResponse);

	    } else {
		logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
		companyInvoiceResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		companyInvoiceResponse.setResultDesc(requestValidationStatus);
	    }

	    companyInvoiceResponse.getLogsReport().setResponseCode(companyInvoiceResponse.getResultCode());
	    companyInvoiceResponse.getLogsReport().setRequestTime(requestTime);
	    companyInvoiceResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    companyInvoiceResponse.getLogsReport().setResponse(mapper.writeValueAsString(companyInvoiceResponse));

	    // Sending report log into queue.
	    companyInvoiceResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(companyInvoiceResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(companyInvoiceResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    companyInvoiceResponse.setCallStatus(Constants.Call_Status_False);

	    companyInvoiceResponse.setResultCode(Constants.EXCEPTION_CODE);
	    companyInvoiceResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    companyInvoiceResponse.getLogsReport().setResponseCode(companyInvoiceResponse.getResultCode());
	    companyInvoiceResponse.getLogsReport().setRequestTime(requestTime);
	    companyInvoiceResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    companyInvoiceResponse.getLogsReport().setResponse(mapper.writeValueAsString(companyInvoiceResponse));

	    // Sending report log into queue.
	    companyInvoiceResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(companyInvoiceResponse.getLogsReport());

	}
	return companyInvoiceResponse;
    }

    @RequestMapping(value = "/msisdnSummary", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public MsisdnInvoiceResponse msisdnSummary(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	MsisdnInvoiceRequest msisdnInvoiceRequest = new MsisdnInvoiceRequest();
	MsisdnInvoiceResponse msisdnInvoiceResponse = new MsisdnInvoiceResponse();

	String TRANSACTION_NAME = Transactions.MSISDN_INVOICE_SUMMARY_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    // data = jsonObject.toString();

	    // Populating report object before processing business logic.
	    msisdnInvoiceResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, msisdnInvoiceResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    msisdnInvoiceRequest = mapper.readValue(data, MsisdnInvoiceRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, msisdnInvoiceRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		msisdnInvoiceRequest.setIsB2B(isFromB2B);
		msisdnInvoiceResponse = generalServicesBusiness.msisdnSummary(msisdn, msisdnInvoiceRequest,
			msisdnInvoiceResponse);

	    } else {
		logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
		msisdnInvoiceResponse.setCallStatus(Constants.Call_Status_False);
		msisdnInvoiceResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		msisdnInvoiceResponse.setResultDesc(requestValidationStatus);
	    }

	    msisdnInvoiceResponse.getLogsReport().setResponseCode(msisdnInvoiceResponse.getResultCode());
	    msisdnInvoiceResponse.getLogsReport().setRequestTime(requestTime);
	    msisdnInvoiceResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    msisdnInvoiceResponse.getLogsReport().setResponse(mapper.writeValueAsString(msisdnInvoiceResponse));

	    // Sending report log into queue.
	    msisdnInvoiceResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(msisdnInvoiceResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(msisdnInvoiceResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    msisdnInvoiceResponse.setCallStatus(Constants.Call_Status_False);

	    msisdnInvoiceResponse.setResultCode(Constants.EXCEPTION_CODE);
	    msisdnInvoiceResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    msisdnInvoiceResponse.getLogsReport().setResponseCode(msisdnInvoiceResponse.getResultCode());
	    msisdnInvoiceResponse.getLogsReport().setRequestTime(requestTime);
	    msisdnInvoiceResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    msisdnInvoiceResponse.getLogsReport().setResponse(mapper.writeValueAsString(msisdnInvoiceResponse));

	    // Sending report log into queue.
	    msisdnInvoiceResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(msisdnInvoiceResponse.getLogsReport());

	}
	return msisdnInvoiceResponse;
    }

    @RequestMapping(value = "/msisdndetail", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public MsisdnInvoiceDetailResponse getMsisdnDetail(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	MsisdnInvoiceDetailRequest msisdnInvoiceDetailRequest = new MsisdnInvoiceDetailRequest();
	MsisdnInvoiceDetailResponse msisdnInvoiceDetailResponse = new MsisdnInvoiceDetailResponse();

	String TRANSACTION_NAME = Transactions.MSISDN_DETAIL_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    // data = jsonObject.toString();

	    // Populating report object before processing business logic.
	    msisdnInvoiceDetailResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, msisdnInvoiceDetailResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    msisdnInvoiceDetailRequest = mapper.readValue(data, MsisdnInvoiceDetailRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, msisdnInvoiceDetailRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		msisdnInvoiceDetailRequest.setIsB2B(isFromB2B);
		msisdnInvoiceDetailResponse = generalServicesBusiness.msisdnDetail(msisdn, msisdnInvoiceDetailRequest,
			msisdnInvoiceDetailResponse);

	    } else {
		logger.error("ERROR: <<<<< Validation of Request Packet Failed >>>>>");
		msisdnInvoiceDetailResponse.setCallStatus(Constants.Call_Status_False);
		msisdnInvoiceDetailResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		msisdnInvoiceDetailResponse.setResultDesc(requestValidationStatus);
	    }

	    msisdnInvoiceDetailResponse.getLogsReport().setResponseCode(msisdnInvoiceDetailResponse.getResultCode());
	    msisdnInvoiceDetailResponse.getLogsReport().setRequestTime(requestTime);
	    msisdnInvoiceDetailResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    msisdnInvoiceDetailResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(msisdnInvoiceDetailResponse));

	    // Sending report log into queue.
	    msisdnInvoiceDetailResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(msisdnInvoiceDetailResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(msisdnInvoiceDetailResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    msisdnInvoiceDetailResponse.setCallStatus(Constants.Call_Status_False);

	    msisdnInvoiceDetailResponse.setResultCode(Constants.EXCEPTION_CODE);
	    msisdnInvoiceDetailResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    msisdnInvoiceDetailResponse.getLogsReport().setResponseCode(msisdnInvoiceDetailResponse.getResultCode());
	    msisdnInvoiceDetailResponse.getLogsReport().setRequestTime(requestTime);
	    msisdnInvoiceDetailResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    msisdnInvoiceDetailResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(msisdnInvoiceDetailResponse));

	    // Sending report log into queue.
	    msisdnInvoiceDetailResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(msisdnInvoiceDetailResponse.getLogsReport());

	}
	return msisdnInvoiceDetailResponse;
    }

}

package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.RateUsBusiness;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.getrateus.GetRateUsRequest;
import com.evampsaanga.bakcell.models.getrateus.GetRateUsResponse;
import com.evampsaanga.bakcell.models.rateus.RateUsRequest;
import com.evampsaanga.bakcell.models.rateus.RateUsResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(Constants.SERVLET_URL + "/rateV2")
public class RateUsControllerV2 {
    Logger logger = Logger.getLogger(RateUsControllerV2.class);

    @Autowired
    RateUsBusiness rateUsBusiness;

    @RequestMapping(value = "/rateus", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public RateUsResponse rateus(
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	RateUsRequest rateUsRequest = new RateUsRequest();
	RateUsResponse rateUsResponse = new RateUsResponse();
	String data = "{}";
	String TRANSACTION_NAME = Transactions.RATE_US_TRANSECTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    rateUsResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, rateUsResponse.getLogsReport()));
	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    data = "{}";
	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);
	    rateUsRequest = mapper.readValue(data, RateUsRequest.class);
	    String requestValidationStatus = Validator.validateRequest(msisdn, rateUsRequest);
	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		rateUsRequest.setIsB2B(isFromB2B);
		rateUsResponse = rateUsBusiness.rateus(msisdn, rateUsRequest, rateUsResponse);
		rateUsResponse.getLogsReport().setResponseCode(rateUsResponse.getResultCode());
		rateUsResponse.getLogsReport().setRequestTime(requestTime);
		rateUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		rateUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(rateUsResponse));

		// Sending report log into queue.
		rateUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
		Utilities.prepareLogReportForQueue(rateUsResponse.getLogsReport());
	    } else {
		rateUsResponse.setCallStatus(Constants.Call_Status_False);
		rateUsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		rateUsResponse.setResultDesc(requestValidationStatus);
		rateUsResponse.getLogsReport().setResponseCode(rateUsResponse.getResultCode());
		rateUsResponse.getLogsReport().setRequestTime(requestTime);
		rateUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		rateUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(rateUsResponse));

		// Sending report log into queue.
		rateUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
		Utilities.prepareLogReportForQueue(rateUsResponse.getLogsReport());
	    }

	} catch (Exception e) {
	    Utilities.convertStackTraceToString(e);
	    rateUsResponse.setCallStatus(Constants.Call_Status_False);
	    rateUsResponse.setResultCode(Constants.EXCEPTION_CODE);

	    rateUsResponse.getLogsReport().setResponseCode(rateUsResponse.getResultCode());
	    rateUsResponse.getLogsReport().setRequestTime(requestTime);
	    rateUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    rateUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(rateUsResponse));

	    // Sending report log into queue.
	    rateUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(rateUsResponse.getLogsReport());
	}
	Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		+ mapper.writeValueAsString(rateUsResponse), logger);
	return rateUsResponse;

    }

    @RequestMapping(value = "/getrateus", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public GetRateUsResponse getRateus(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	GetRateUsRequest getRateUsRequest = new GetRateUsRequest();
	GetRateUsResponse getRateUsResponse = new GetRateUsResponse();

	String TRANSACTION_NAME = Transactions.GET_RATE_US_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    getRateUsResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, getRateUsResponse.getLogsReport()));
	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);
	    getRateUsRequest = mapper.readValue(data, GetRateUsRequest.class);
	    String requestValidationStatus = Validator.validateRequest(msisdn, getRateUsRequest);
	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		getRateUsResponse = rateUsBusiness.getRateus(msisdn, getRateUsRequest, getRateUsResponse);

		getRateUsResponse.getLogsReport().setResponseCode(getRateUsResponse.getResultCode());
		getRateUsResponse.getLogsReport().setRequestTime(requestTime);
		getRateUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		getRateUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(getRateUsResponse));

		// Sending report log into queue.
		getRateUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
		Utilities.prepareLogReportForQueue(getRateUsResponse.getLogsReport());
	    } else {
		getRateUsResponse.setCallStatus(Constants.Call_Status_False);
		getRateUsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		getRateUsResponse.setResultDesc(requestValidationStatus);
		getRateUsResponse.getLogsReport().setResponseCode(getRateUsResponse.getResultCode());
		getRateUsResponse.getLogsReport().setRequestTime(requestTime);
		getRateUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		getRateUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(getRateUsResponse));

		// Sending report log into queue.
		getRateUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
		Utilities.prepareLogReportForQueue(getRateUsResponse.getLogsReport());
	    }

	} catch (Exception e) {

	    Utilities.convertStackTraceToString(e);
	    getRateUsResponse.setCallStatus(Constants.Call_Status_False);
	    getRateUsResponse.setResultCode(Constants.EXCEPTION_CODE);

	    getRateUsResponse.getLogsReport().setResponseCode(getRateUsResponse.getResultCode());
	    getRateUsResponse.getLogsReport().setRequestTime(requestTime);
	    getRateUsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    getRateUsResponse.getLogsReport().setResponse(mapper.writeValueAsString(getRateUsResponse));

	    // Sending report log into queue.
	    getRateUsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(getRateUsResponse.getLogsReport());
	}
	Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		+ mapper.writeValueAsString(getRateUsResponse), logger);
	return getRateUsResponse;

    }
}

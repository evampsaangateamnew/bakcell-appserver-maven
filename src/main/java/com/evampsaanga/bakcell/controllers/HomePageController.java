/**
 * 
 */
package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.HomePageBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.homepageservices.HomePageRequest;
import com.evampsaanga.bakcell.models.homepageservices.HomePageResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */

@RestController
@RequestMapping(Constants.SERVLET_URL + "/homepageservices")
public class HomePageController {

    Logger logger = Logger.getLogger(HomePageController.class);

    @Autowired
    HomePageBusiness homePageBusiness;

    @RequestMapping(value = "/gethomepage", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public HomePageResponse getHomePage(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();

	HomePageRequest homePageRequest = new HomePageRequest();
	HomePageResponse homePageResponse = new HomePageResponse();

	String TRANSACTION_NAME = Transactions.HOME_PAGE_TRANSACTION_NAME + " CONTROLLER";

	String requestTime = Utilities.getReportDateTime();
	try {

	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    homePageResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    homePageResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    homePageRequest = mapper.readValue(data, HomePageRequest.class);
	    JSONObject jsonObject = new JSONObject(data);
	    homePageRequest.setCustomerType(jsonObject.getString("subscriberType"));
	    String requestValidationStatus = Validator.validateRequest(msisdn, homePageRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		homePageResponse = homePageBusiness.getHomePageBusiness(msisdn, homePageRequest, homePageResponse);
	    } else {
		homePageResponse.setCallStatus(Constants.Call_Status_False);
		homePageResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		homePageResponse.setResultDesc(requestValidationStatus);
	    }

	    homePageResponse.getLogsReport().setResponseCode(homePageResponse.getResultCode());
	    homePageResponse.getLogsReport().setRequestTime(requestTime);
	    homePageResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    homePageResponse.getLogsReport().setResponse(mapper.writeValueAsString(homePageResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(homePageResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(homePageResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    homePageResponse.setCallStatus(Constants.Call_Status_False);
	    homePageResponse.setResultCode(Constants.EXCEPTION_CODE);
	    homePageResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    homePageResponse.getLogsReport().setResponseCode(homePageResponse.getResultCode());
	    homePageResponse.getLogsReport().setRequestTime(requestTime);
	    homePageResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    homePageResponse.getLogsReport().setResponse(mapper.writeValueAsString(homePageResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(homePageResponse.getLogsReport());

	}
	return homePageResponse;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.TriggersBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.triggers.TriggersRequest;
import com.evampsaanga.bakcell.models.triggers.TriggersResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping(Constants.SERVLET_URL + "/trigger")
public class TriggersController {

    Logger logger = Logger.getLogger(TriggersController.class);
    @Autowired
    TriggersBusiness triggersBusiness;

    @RequestMapping(value = "/refreshcache", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public TriggersResponse refreshCache(@RequestBody String data,
	    @RequestHeader(value = "credentials") String credentials, HttpServletRequest servletRequest)
	    throws ClassNotFoundException, SQLException, JsonParseException, JsonMappingException, IOException,
	    JMSException {
	ObjectMapper mapper = new ObjectMapper();

	TriggersRequest triggersRequest = new TriggersRequest();
	TriggersResponse triggersResponse = new TriggersResponse();

	if (credentials.equalsIgnoreCase(Constants.CREDENTIALS_FOR_INTERNAL_CALLS)) {

	    String lang = "3";

	    String requestTime = Utilities.getReportDateTime();

	    try {

		Utilities.printInfoLog(Transactions.TRIGGERS_TRANSACTION_NAME + "-Request Landed in-"
			+ Transactions.TRIGGERS_TRANSACTION_NAME, logger);
		Utilities.printInfoLog(Transactions.TRIGGERS_TRANSACTION_NAME + "-Request Data-" + data, logger);

		triggersRequest = mapper.readValue(data, TriggersRequest.class);

		triggersResponse = triggersBusiness.refreshCacheBusiness(triggersRequest, triggersResponse);

		triggersResponse.getLogsReport().setRequestTime(requestTime);
		triggersResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		triggersResponse.getLogsReport().setResponse(mapper.writeValueAsString(triggersResponse));
		triggersResponse.getLogsReport().setResponseCode(triggersResponse.getResultCode());

		// Sending report log into queue.
		Utilities.prepareLogReportForQueue(triggersResponse.getLogsReport());

		Utilities.printInfoLog(Transactions.TRIGGERS_TRANSACTION_NAME + "-Response Returned from-"
			+ Transactions.TRIGGERS_TRANSACTION_NAME + "-" + mapper.writeValueAsString(triggersResponse),
			logger);
	    } catch (Exception e) {
		Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		triggersResponse.setCallStatus(Constants.Call_Status_False);
		triggersResponse.setResultCode(Constants.EXCEPTION_CODE);
		triggersResponse
			.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

		triggersResponse.getLogsReport().setResponseCode(triggersResponse.getResultCode());
		triggersResponse.getLogsReport().setRequestTime(requestTime);
		triggersResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		triggersResponse.getLogsReport().setResponse(mapper.writeValueAsString(triggersResponse));

		// Sending report log into queue.
		Utilities.prepareLogReportForQueue(triggersResponse.getLogsReport());

	    }
	} else {
	    triggersResponse.setCallStatus(Constants.Call_Status_False);
	    triggersResponse.setResultCode(Constants.UNAUTHORIZED_ACCESS);
	    triggersResponse.setResultDesc("UNAUTHORIZED ACCESS.");

	}
	return triggersResponse;
    }

}

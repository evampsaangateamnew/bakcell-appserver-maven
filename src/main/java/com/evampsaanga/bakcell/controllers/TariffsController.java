/**
 * 
 */
package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.TariffBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.tariffdetails.TariffRequest;
import com.evampsaanga.bakcell.models.tariffdetails.TariffResponse;
import com.evampsaanga.bakcell.models.tariffdetails.changetariff.ChangeTariffRequest;
import com.evampsaanga.bakcell.models.tariffdetails.changetariff.ChangeTariffResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */

@RestController
@RequestMapping(Constants.SERVLET_URL + "/tariffservices")
public class TariffsController {

    Logger logger = Logger.getLogger(TariffsController.class);
    @Autowired
    TariffBusiness tariffBusiness;

    @RequestMapping(value = "/gettariffdetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public TariffResponse getTariffDetails(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();
	TariffRequest tariffRequest = new TariffRequest();
	TariffResponse tariffResponse = new TariffResponse();

	String TRANSACTION_NAME = Transactions.TARRIF_DETAILS_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);
	    // Populating report object before processing business logic.
	    tariffResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    tariffResponse.getLogsReport()));
	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");
	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    tariffRequest = mapper.readValue(data, TariffRequest.class);
	    // Store ID value is same as of Language ID.
	    tariffRequest.setStoreId(tariffRequest.getLang());

	    String requestValidationStatus = Validator.validateRequest(msisdn, tariffRequest);
	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		tariffResponse = tariffBusiness.getTariffDetailsBusiness(msisdn, tariffRequest, tariffResponse);

	    } else {
		tariffResponse.setCallStatus(Constants.Call_Status_False);
		tariffResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		tariffResponse.setResultDesc(requestValidationStatus);
	    }

	    tariffResponse.getLogsReport().setResponseCode(tariffResponse.getResultCode());
	    tariffResponse.getLogsReport().setRequestTime(requestTime);
	    tariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    tariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(tariffResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(tariffResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(tariffResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    tariffResponse.setCallStatus(Constants.Call_Status_False);
	    tariffResponse.setResultCode(Constants.EXCEPTION_CODE);
	    tariffResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    tariffResponse.getLogsReport().setResponseCode(tariffResponse.getResultCode());
	    tariffResponse.getLogsReport().setRequestTime(requestTime);
	    tariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    tariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(tariffResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(tariffResponse.getLogsReport());

	}
	return tariffResponse;
    }

    @RequestMapping(value = "/changetariff", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public ChangeTariffResponse changeTariff(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	ChangeTariffRequest changeTariffRequest = new ChangeTariffRequest();
	ChangeTariffResponse changeTariffResponse = new ChangeTariffResponse();

	String TRANSACTION_NAME = Transactions.CHANGE_TARRIF_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    changeTariffResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    changeTariffResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    changeTariffRequest = mapper.readValue(data, ChangeTariffRequest.class);

	    // Logging specific params
	    changeTariffResponse.getLogsReport().setPrimaryOfferingName(changeTariffRequest.getTariffName());
	    changeTariffResponse.getLogsReport().setTariffOfferingId(changeTariffRequest.getOfferingId());

	    String requestValidationStatus = Validator.validateRequest(msisdn, changeTariffRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		changeTariffResponse = tariffBusiness.changeTariffBusiness(msisdn, changeTariffRequest,
			changeTariffResponse);
	    } else {
		changeTariffResponse.setCallStatus(Constants.Call_Status_False);
		changeTariffResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		changeTariffResponse.setResultDesc(requestValidationStatus);
	    }

	    changeTariffResponse.getLogsReport().setResponseCode(changeTariffResponse.getResultCode());
	    changeTariffResponse.getLogsReport().setRequestTime(requestTime);
	    changeTariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    changeTariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(changeTariffResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(changeTariffResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(changeTariffResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    changeTariffResponse.setCallStatus(Constants.Call_Status_False);
	    changeTariffResponse.setResultCode(Constants.EXCEPTION_CODE);
	    changeTariffResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    changeTariffResponse.getLogsReport().setResponseCode(changeTariffResponse.getResultCode());
	    changeTariffResponse.getLogsReport().setRequestTime(requestTime);
	    changeTariffResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    changeTariffResponse.getLogsReport().setResponse(mapper.writeValueAsString(changeTariffResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(changeTariffResponse.getLogsReport());

	}
	return changeTariffResponse;
    }
}

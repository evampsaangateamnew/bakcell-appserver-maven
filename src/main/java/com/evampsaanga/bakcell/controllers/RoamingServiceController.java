/**
 * 
 */
package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.RoamingServiceBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.roaming.RoamingServiceRequest;
import com.evampsaanga.bakcell.models.roaming.RoamingServiceResponse;
import com.evampsaanga.bakcell.models.roaming.roamingstatus.RoamingServiceStatusRequest;
import com.evampsaanga.bakcell.models.roaming.roamingstatus.RoamingServiceStatusResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping(Constants.SERVLET_URL + "/roaming")
public class RoamingServiceController {

    Logger logger = Logger.getLogger(RoamingServiceController.class);
    @Autowired
    RoamingServiceBusiness roamingServiceBusiness;

    @RequestMapping(value = "/process", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public RoamingServiceResponse processRoaming(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();
	RoamingServiceRequest roamingServiceRequest = new RoamingServiceRequest();
	RoamingServiceResponse roamingServiceResponse = new RoamingServiceResponse();
	String requestTime = Utilities.getReportDateTime();

	String TRANSACTION_NAME = Transactions.ROAMING_SERVICE_PROCESS_TRANSECTION_NAME + " CONTROLLER";
	try {

	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    roamingServiceResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    roamingServiceResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in updated-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    roamingServiceRequest = mapper.readValue(data, RoamingServiceRequest.class);
	    String requestValidationStatus = Validator.validateRequest(msisdn, roamingServiceRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		roamingServiceResponse = roamingServiceBusiness.processRoamingServiceBusiness(msisdn,
			roamingServiceRequest, roamingServiceResponse);

	    } else {
		roamingServiceResponse.setCallStatus(Constants.Call_Status_False);
		roamingServiceResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		roamingServiceResponse.setResultDesc(requestValidationStatus);
	    }

	    roamingServiceResponse.getLogsReport().setRequestTime(requestTime);
	    roamingServiceResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    roamingServiceResponse.getLogsReport().setResponse(mapper.writeValueAsString(roamingServiceResponse));
	    roamingServiceResponse.getLogsReport().setResponseCode(roamingServiceResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(roamingServiceResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(roamingServiceResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    roamingServiceResponse.setCallStatus(Constants.Call_Status_False);
	    roamingServiceResponse.setResultCode(Constants.EXCEPTION_CODE);
	    roamingServiceResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    roamingServiceResponse.getLogsReport().setResponseCode(roamingServiceResponse.getResultCode());
	    roamingServiceResponse.getLogsReport().setRequestTime(requestTime);
	    roamingServiceResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    roamingServiceResponse.getLogsReport().setResponse(mapper.writeValueAsString(roamingServiceResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(roamingServiceResponse.getLogsReport());

	}
	return roamingServiceResponse;
    }

    @RequestMapping(value = "/status", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public RoamingServiceStatusResponse getRoamingStatus(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();
	RoamingServiceStatusRequest roamingServiceStatusRequest = new RoamingServiceStatusRequest();
	RoamingServiceStatusResponse roamingServiceStatusResponse = new RoamingServiceStatusResponse();
	String requestTime = Utilities.getReportDateTime();

	String TRANSACTION_NAME = Transactions.ROAMING_SERVICE_STATUS_TRANSECTION_NAME + " CONTROLLER";
	try {

	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    roamingServiceStatusResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
		    TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    roamingServiceStatusResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in updated-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    roamingServiceStatusRequest = mapper.readValue(data, RoamingServiceStatusRequest.class);
	    String requestValidationStatus = Validator.validateRequest(msisdn, roamingServiceStatusRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		roamingServiceStatusResponse = roamingServiceBusiness.getRoamingServiceStatusBusiness(msisdn,
			roamingServiceStatusRequest, roamingServiceStatusResponse);

	    } else {
		roamingServiceStatusResponse.setCallStatus(Constants.Call_Status_False);
		roamingServiceStatusResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		roamingServiceStatusResponse.setResultDesc(requestValidationStatus);
	    }

	    roamingServiceStatusResponse.getLogsReport().setRequestTime(requestTime);
	    roamingServiceStatusResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    roamingServiceStatusResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(roamingServiceStatusResponse));
	    roamingServiceStatusResponse.getLogsReport().setResponseCode(roamingServiceStatusResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(roamingServiceStatusResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(roamingServiceStatusResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    roamingServiceStatusResponse.setCallStatus(Constants.Call_Status_False);
	    roamingServiceStatusResponse.setResultCode(Constants.EXCEPTION_CODE);
	    roamingServiceStatusResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    roamingServiceStatusResponse.getLogsReport().setResponseCode(roamingServiceStatusResponse.getResultCode());
	    roamingServiceStatusResponse.getLogsReport().setRequestTime(requestTime);
	    roamingServiceStatusResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    roamingServiceStatusResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(roamingServiceStatusResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(roamingServiceStatusResponse.getLogsReport());

	}
	return roamingServiceStatusResponse;
    }
}

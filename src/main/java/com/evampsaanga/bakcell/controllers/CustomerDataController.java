package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.CustomerDataBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.customerdata.CustomerDataRequest;
import com.evampsaanga.bakcell.models.customerdata.CustomerDataResponse;
import com.evampsaanga.bakcell.models.customerdata.HomePageResponseV2;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(Constants.SERVLET_URL + "/customerdataservice")
public class CustomerDataController {
    Logger logger = Logger.getLogger(CustomerDataController.class);
    @Autowired
    CustomerDataBusiness customerDataBusiness;

    @RequestMapping(value = "/getcustomerdata", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public HomePageResponseV2 getHomePage(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();
	Utilities.printInfoLog(data + "-Request Recieved on  getcustomerdata ", logger);
	CustomerDataRequest customerDataRequest = new CustomerDataRequest();
	CustomerDataResponse customerDataResponse = new CustomerDataResponse();
	HomePageResponseV2 homePageResponse = new HomePageResponseV2();
	String TRANSACTION_NAME = Transactions.CUSTOMERDATA_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(data, Constants.MSISDN_KEY);
	    // Populating report object before processing business logic.
	    customerDataResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, "", "", data, lang, customerDataResponse.getLogsReport()));
	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "true");
	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    customerDataRequest = mapper.readValue(data, CustomerDataRequest.class);
	    customerDataRequest.setChannel(userAgent);
	    customerDataRequest.setLang(lang);
	    customerDataRequest.setMsisdn(msisdn);
	    String requestValidationStatus = Validator.validateRequest(msisdn, customerDataRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		homePageResponse = customerDataBusiness.getCustomerData(customerDataRequest, customerDataResponse);
		logger.info("HOME PAGE RESPONSE" + mapper.writeValueAsString(homePageResponse));
		homePageResponse.getLogsReport().setResponseCode(homePageResponse.getResultCode());
		homePageResponse.getLogsReport().setRequestTime(requestTime);
		homePageResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		homePageResponse.getLogsReport().setResponse(mapper.writeValueAsString(homePageResponse));

	    } else {
		homePageResponse.setCallStatus(Constants.Call_Status_False);
		homePageResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		homePageResponse.setResultDesc(requestValidationStatus);
	    }

	    // Sending report log into queue. commenting due to db issues
	    // Utilities.prepareLogReportForQueue(homePageResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(homePageResponse), logger);

	} catch (Exception e) {
	    // Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    logger.error("ERROR:", e);
	    homePageResponse.setCallStatus("false");
	    // EXCEPTION_CODE
	    homePageResponse.setResultCode("500");
	    homePageResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    homePageResponse.getLogsReport().setResponseCode(homePageResponse.getResultCode());
	    homePageResponse.getLogsReport().setRequestTime(requestTime);
	    homePageResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    homePageResponse.getLogsReport().setResponse(mapper.writeValueAsString(homePageResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(homePageResponse.getLogsReport());
	}

	return homePageResponse;

    }
}

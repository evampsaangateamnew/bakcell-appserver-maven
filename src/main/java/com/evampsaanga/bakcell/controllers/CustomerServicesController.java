/**
 * 
 */
package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.CustomerServicesBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.customerservices.appresume.AppResumeRequest;
import com.evampsaanga.bakcell.models.customerservices.appresume.AppResumeResponse;
import com.evampsaanga.bakcell.models.customerservices.authenticateuser.AuthenticateUserRequest;
import com.evampsaanga.bakcell.models.customerservices.authenticateuser.AuthenticateUserResponse;
import com.evampsaanga.bakcell.models.customerservices.changebillinglanguage.ChangeBillingLanguageRequest;
import com.evampsaanga.bakcell.models.customerservices.changebillinglanguage.ChangeBillingLanguageResponse;
import com.evampsaanga.bakcell.models.customerservices.changepassword.ChangePasswordRequest;
import com.evampsaanga.bakcell.models.customerservices.changepassword.ChangePasswordResponse;
import com.evampsaanga.bakcell.models.customerservices.forgotpassword.ForgotPasswordRequest;
import com.evampsaanga.bakcell.models.customerservices.forgotpassword.ForgotPasswordResponse;
import com.evampsaanga.bakcell.models.customerservices.logout.LogoutRequest;
import com.evampsaanga.bakcell.models.customerservices.logout.LogoutResponse;
import com.evampsaanga.bakcell.models.customerservices.resendpin.ResendPINRequest;
import com.evampsaanga.bakcell.models.customerservices.resendpin.ResendPINResponse;
import com.evampsaanga.bakcell.models.customerservices.savecustomer.SaveCustomerRequest;
import com.evampsaanga.bakcell.models.customerservices.savecustomer.SaveCustomerResponse;
import com.evampsaanga.bakcell.models.customerservices.signup.SignupRequest;
import com.evampsaanga.bakcell.models.customerservices.signup.SignupResponse;
import com.evampsaanga.bakcell.models.customerservices.updatecustomeremail.UpdateCustomerEmailRequest;
import com.evampsaanga.bakcell.models.customerservices.updatecustomeremail.UpdateCustomerEmailResponse;
import com.evampsaanga.bakcell.models.customerservices.verifyotp.VerifyOTPRequest;
import com.evampsaanga.bakcell.models.customerservices.verifyotp.VerifyOTPResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
@RestController
@RequestMapping(Constants.SERVLET_URL + "/customerservices")
public class CustomerServicesController {
    Logger logger = Logger.getLogger(CustomerServicesController.class);
    @Autowired
    CustomerServicesBusiness customerServicesBusiness;

    @RequestMapping(value = "/signup", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SignupResponse signup(@RequestBody String data,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();

	SignupRequest signupRequest = new SignupRequest();
	SignupResponse signupResponse = new SignupResponse();
	String TRANSACTION_NAME = Transactions.SIGNUP_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    String msisdn = Utilities.getValueFromJSON(data, Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    // At this step, We don't have userType and tariff type of
	    // subscriber's MSISDN. Therefore sending empty values instead.
	    signupResponse.setLogsReport(Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), userAgent, "", "", data, lang, signupResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    signupRequest = mapper.readValue(data, SignupRequest.class);
	    String requestValidationStatus = Validator.validateRequest(msisdn, signupRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		signupResponse = customerServicesBusiness.signupBusiness(msisdn, signupRequest, signupResponse);
	    } else {
		signupResponse.setCallStatus(Constants.Call_Status_False);
		signupResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		signupResponse.setResultDesc(requestValidationStatus);
	    }

	    signupResponse.getLogsReport().setRequestTime(requestTime);
	    signupResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    signupResponse.getLogsReport().setResponse(mapper.writeValueAsString(signupResponse));
	    signupResponse.getLogsReport().setResponseCode(signupResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(signupResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(signupResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    signupResponse.setCallStatus(Constants.Call_Status_False);

	    signupResponse.setResultCode(Constants.EXCEPTION_CODE);
	    signupResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    signupResponse.getLogsReport().setRequestTime(requestTime);
	    signupResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    signupResponse.getLogsReport().setResponse(mapper.writeValueAsString(signupResponse));
	    signupResponse.getLogsReport().setResponseCode(signupResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(signupResponse.getLogsReport());
	}
	return signupResponse;
    }

    @RequestMapping(value = "/verifyotp", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public VerifyOTPResponse verifyOTP(@RequestBody String data,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	VerifyOTPRequest verifyOTPRequest = new VerifyOTPRequest();
	VerifyOTPResponse verifyOTPResponse = new VerifyOTPResponse();
	String TRANSACTION_NAME = Transactions.SIGNUP_VERIFY_OTP_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    String msisdn = Utilities.getValueFromJSON(data, Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    verifyOTPResponse.setLogsReport(Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), userAgent, "", "", data, lang, verifyOTPResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    verifyOTPRequest = mapper.readValue(data, VerifyOTPRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, verifyOTPRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		verifyOTPResponse = customerServicesBusiness.verifyOTPBusiness(msisdn, verifyOTPRequest,
			verifyOTPResponse);
	    } else {
		verifyOTPResponse.setCallStatus(Constants.Call_Status_False);
		verifyOTPResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		verifyOTPResponse.setResultDesc(requestValidationStatus);
	    }

	    verifyOTPResponse.getLogsReport().setRequestTime(requestTime);
	    verifyOTPResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    verifyOTPResponse.getLogsReport().setResponse(mapper.writeValueAsString(verifyOTPResponse));
	    verifyOTPResponse.getLogsReport().setResponseCode(verifyOTPResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(verifyOTPResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(verifyOTPResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    verifyOTPResponse.setCallStatus(Constants.Call_Status_False);

	    verifyOTPResponse.setResultCode(Constants.EXCEPTION_CODE);
	    verifyOTPResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    verifyOTPResponse.getLogsReport().setResponseCode(verifyOTPResponse.getResultCode());
	    verifyOTPResponse.getLogsReport().setRequestTime(requestTime);
	    verifyOTPResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    verifyOTPResponse.getLogsReport().setResponse(mapper.writeValueAsString(verifyOTPResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(verifyOTPResponse.getLogsReport());
	}
	return verifyOTPResponse;
    }

    @RequestMapping(value = "/savecustomer", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public SaveCustomerResponse saveCustomer(@RequestBody String data,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	SaveCustomerRequest saveCustomerRequest = new SaveCustomerRequest();
	SaveCustomerResponse saveCustomerResponse = new SaveCustomerResponse();

	String TRANSACTION_NAME = Transactions.SAVE_CUSTOMER_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(data, Constants.MSISDN_KEY);
	    String deviceID = servletRequest.getHeader("deviceID");

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    saveCustomerRequest = mapper.readValue(data, SaveCustomerRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, saveCustomerRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		saveCustomerResponse = customerServicesBusiness.saveCustomerBusiness(msisdn, deviceID,
			saveCustomerRequest, saveCustomerResponse);
	    } else {
		saveCustomerResponse.setCallStatus(Constants.Call_Status_False);
		saveCustomerResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		saveCustomerResponse.setResultDesc(requestValidationStatus);
	    }

	    // Populating report object after generating token.
	    if (saveCustomerResponse.getResultCode().equalsIgnoreCase(Constants.APP_SERVER_SUCCESS_CODE)) {
		saveCustomerResponse.setLogsReport(Utilities.preBusinessReportLoggingData(
			saveCustomerResponse.getData().getCustomerData().getToken(), msisdn, TRANSACTION_NAME,
			servletRequest.getRemoteAddr(), userAgent,
			saveCustomerResponse.getData().getCustomerData().getSubscriberType(),
			saveCustomerResponse.getData().getCustomerData().getBrandName(), data, lang,
			saveCustomerResponse.getLogsReport()));
	    }

	    saveCustomerResponse.getLogsReport().setRequestTime(requestTime);
	    saveCustomerResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    saveCustomerResponse.getLogsReport().setResponse(mapper.writeValueAsString(saveCustomerResponse));
	    saveCustomerResponse.getLogsReport().setResponseCode(saveCustomerResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(saveCustomerResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(saveCustomerResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    saveCustomerResponse.setCallStatus(Constants.Call_Status_False);

	    saveCustomerResponse.setResultCode(Constants.EXCEPTION_CODE);
	    saveCustomerResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    saveCustomerResponse.getLogsReport().setResponseCode(saveCustomerResponse.getResultCode());
	    saveCustomerResponse.getLogsReport().setRequestTime(requestTime);
	    saveCustomerResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    saveCustomerResponse.getLogsReport().setResponse(mapper.writeValueAsString(saveCustomerResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(saveCustomerResponse.getLogsReport());
	}
	return saveCustomerResponse;
    }

    @RequestMapping(value = "/resendpin", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public ResendPINResponse resendPIN(@RequestBody String data,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    HttpServletRequest servletRequest) throws JsonParseException, JsonMappingException, IOException,
	    JMSException, ClassNotFoundException, SQLException {
	ObjectMapper mapper = new ObjectMapper();
	ResendPINRequest resendPINRequest = new ResendPINRequest();
	ResendPINResponse resendPINResponse = new ResendPINResponse();
	String TRANSACTION_NAME = Transactions.RESEND_OTP_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    String msisdn = Utilities.getValueFromJSON(data, Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    resendPINResponse.setLogsReport(Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), userAgent, "", "", data, lang, resendPINResponse.getLogsReport()));

	    String deviceID = servletRequest.getHeader("deviceID");

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    resendPINRequest = mapper.readValue(data, ResendPINRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, resendPINRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		resendPINResponse = customerServicesBusiness.resendPinBusiness(msisdn, deviceID, resendPINRequest,
			resendPINResponse);
	    } else {
		resendPINResponse.setCallStatus(Constants.Call_Status_False);
		resendPINResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		resendPINResponse.setResultDesc(requestValidationStatus);
	    }

	    resendPINResponse.getLogsReport().setRequestTime(requestTime);
	    resendPINResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    resendPINResponse.getLogsReport().setResponse(mapper.writeValueAsString(resendPINResponse));
	    resendPINResponse.getLogsReport().setResponseCode(resendPINResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(resendPINResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(resendPINResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    resendPINResponse.setCallStatus(Constants.Call_Status_False);

	    resendPINResponse.setResultCode(Constants.EXCEPTION_CODE);
	    resendPINResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    resendPINResponse.getLogsReport().setResponseCode(resendPINResponse.getResultCode());
	    resendPINResponse.getLogsReport().setRequestTime(requestTime);
	    resendPINResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    resendPINResponse.getLogsReport().setResponse(mapper.writeValueAsString(resendPINResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(resendPINResponse.getLogsReport());
	}
	return resendPINResponse;
    }

    @RequestMapping(value = "/authenticateuser", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public AuthenticateUserResponse authenticateUser(@RequestBody String data,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	AuthenticateUserRequest authenticateUserRequest = new AuthenticateUserRequest();
	AuthenticateUserResponse authenticateUserResponse = new AuthenticateUserResponse();

	String TRANSACTION_NAME = Transactions.LOGIN_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    String msisdn = Utilities.getValueFromJSON(data, Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    // At this step, We don't have userType and tariff type of
	    // subscriber's MSISDN. Therefore sending empty values instead.

	    String deviceID = servletRequest.getHeader("deviceID");

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    authenticateUserRequest = mapper.readValue(data, AuthenticateUserRequest.class);
	    Utilities.printInfoLog(msisdn + "-Check-1" + authenticateUserRequest.getMsisdn(), logger);
	    String requestValidationStatus = Validator.validateRequest(msisdn, authenticateUserRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		authenticateUserResponse = customerServicesBusiness.authenticateuser(msisdn, deviceID,
			authenticateUserRequest, authenticateUserResponse);
	    } else {
		authenticateUserResponse.setCallStatus(Constants.Call_Status_False);
		authenticateUserResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		authenticateUserResponse.setResultDesc(requestValidationStatus);
	    }
	    // Populating report object after generating token.
	    if (authenticateUserResponse.getResultCode().equalsIgnoreCase(Constants.APP_SERVER_SUCCESS_CODE)) {
		authenticateUserResponse.setLogsReport(Utilities.preBusinessReportLoggingData(
			authenticateUserResponse.getData().getCustomerData().getToken(), msisdn, TRANSACTION_NAME,
			servletRequest.getRemoteAddr(), userAgent,
			authenticateUserResponse.getData().getCustomerData().getSubscriberType(),
			authenticateUserResponse.getData().getCustomerData().getBrandName(), data, lang,
			authenticateUserResponse.getLogsReport()));
	    }

	    authenticateUserResponse.getLogsReport().setRequestTime(requestTime);
	    authenticateUserResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    authenticateUserResponse.getLogsReport().setResponse(mapper.writeValueAsString(authenticateUserResponse));
	    authenticateUserResponse.getLogsReport().setResponseCode(authenticateUserResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(authenticateUserResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(authenticateUserResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    authenticateUserResponse.setCallStatus(Constants.Call_Status_False);
	    authenticateUserResponse.setResultCode(Constants.EXCEPTION_CODE);
	    authenticateUserResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    authenticateUserResponse.getLogsReport().setRequestTime(requestTime);
	    authenticateUserResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    authenticateUserResponse.getLogsReport().setResponse(mapper.writeValueAsString(authenticateUserResponse));
	    authenticateUserResponse.getLogsReport().setResponseCode(authenticateUserResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(authenticateUserResponse.getLogsReport());
	}
	return authenticateUserResponse;
    }

    @RequestMapping(value = "/forgotpassword", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public ForgotPasswordResponse forgotPassword(@RequestBody String data,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest();
	ForgotPasswordResponse forgotPasswordResponse = new ForgotPasswordResponse();

	String TRANSACTION_NAME = Transactions.FORGOT_PASSWORD_TRANSACTION_NAME + "  CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    String msisdn = Utilities.getValueFromJSON(data, Constants.MSISDN_KEY);
	    String deviceID = servletRequest.getHeader("deviceID");

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    forgotPasswordRequest = mapper.readValue(data, ForgotPasswordRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, forgotPasswordRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		forgotPasswordResponse = customerServicesBusiness.forgotPasswordBusiness(msisdn, deviceID,
			forgotPasswordRequest, forgotPasswordResponse);
	    } else {
		forgotPasswordResponse.setCallStatus(Constants.Call_Status_False);
		forgotPasswordResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		forgotPasswordResponse.setResultDesc(requestValidationStatus);
	    }

	    // Populating report object after generating token.
	    if (forgotPasswordResponse.getResultCode().equalsIgnoreCase(Constants.APP_SERVER_SUCCESS_CODE)) {
		forgotPasswordResponse.setLogsReport(Utilities.preBusinessReportLoggingData(
			forgotPasswordResponse.getData().getCustomerData().getToken(), msisdn, TRANSACTION_NAME,
			servletRequest.getRemoteAddr(), userAgent,
			forgotPasswordResponse.getData().getCustomerData().getSubscriberType(),
			forgotPasswordResponse.getData().getCustomerData().getBrandName(), data, lang,
			forgotPasswordResponse.getLogsReport()));
	    }

	    forgotPasswordResponse.getLogsReport().setRequestTime(requestTime);
	    forgotPasswordResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    forgotPasswordResponse.getLogsReport().setResponse(mapper.writeValueAsString(forgotPasswordResponse));
	    forgotPasswordResponse.getLogsReport().setResponseCode(forgotPasswordResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(forgotPasswordResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(forgotPasswordResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    forgotPasswordResponse.setCallStatus(Constants.Call_Status_False);

	    forgotPasswordResponse.setResultCode(Constants.EXCEPTION_CODE);
	    forgotPasswordResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    forgotPasswordResponse.getLogsReport().setResponseCode(forgotPasswordResponse.getResultCode());
	    forgotPasswordResponse.getLogsReport().setRequestTime(requestTime);
	    forgotPasswordResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    forgotPasswordResponse.getLogsReport().setResponse(mapper.writeValueAsString(forgotPasswordResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(forgotPasswordResponse.getLogsReport());
	}
	return forgotPasswordResponse;
    }

    @RequestMapping(value = "/changepassword", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public ChangePasswordResponse changePassword(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();

	ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
	ChangePasswordResponse changePasswordResponse = new ChangePasswordResponse();

	String TRANSACTION_NAME = Transactions.CHANGE_PASSWORD_TRANSACTION_NAME + "  CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    changePasswordResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    changePasswordResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    changePasswordRequest = mapper.readValue(data, ChangePasswordRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, changePasswordRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		changePasswordResponse = customerServicesBusiness.changePasswordBusiness(msisdn, changePasswordRequest,
			changePasswordResponse);
	    } else {
		changePasswordResponse.setCallStatus(Constants.Call_Status_False);
		changePasswordResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		changePasswordResponse.setResultDesc(requestValidationStatus);
	    }

	    changePasswordResponse.getLogsReport().setRequestTime(requestTime);
	    changePasswordResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    changePasswordResponse.getLogsReport().setResponse(mapper.writeValueAsString(changePasswordResponse));
	    changePasswordResponse.getLogsReport().setResponseCode(changePasswordResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(changePasswordResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(changePasswordResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    changePasswordResponse.setCallStatus(Constants.Call_Status_False);

	    changePasswordResponse.setResultCode(Constants.EXCEPTION_CODE);
	    changePasswordResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    changePasswordResponse.getLogsReport().setResponseCode(changePasswordResponse.getResultCode());
	    changePasswordResponse.getLogsReport().setRequestTime(requestTime);
	    changePasswordResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    changePasswordResponse.getLogsReport().setResponse(mapper.writeValueAsString(changePasswordResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(changePasswordResponse.getLogsReport());
	}
	return changePasswordResponse;
    }

    @RequestMapping(value = "/updatecustomeremail", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public UpdateCustomerEmailResponse updateCustomerEmail(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();

	UpdateCustomerEmailRequest updateCustomerEmailRequest = new UpdateCustomerEmailRequest();
	UpdateCustomerEmailResponse updateCustomerEmailResponse = new UpdateCustomerEmailResponse();

	String TRANSACTION_NAME = Transactions.UPDATE_CUSTOMER_EMAIL_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {

	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    updateCustomerEmailResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
		    TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    updateCustomerEmailResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    updateCustomerEmailRequest = mapper.readValue(data, UpdateCustomerEmailRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, updateCustomerEmailRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		updateCustomerEmailResponse = customerServicesBusiness.updateCustomerEmailBusiness(msisdn,
			updateCustomerEmailRequest, updateCustomerEmailResponse);
	    } else {
		updateCustomerEmailResponse.setCallStatus(Constants.Call_Status_False);
		updateCustomerEmailResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		updateCustomerEmailResponse.setResultDesc(requestValidationStatus);
	    }

	    updateCustomerEmailResponse.getLogsReport().setRequestTime(requestTime);
	    updateCustomerEmailResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    updateCustomerEmailResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(updateCustomerEmailResponse));
	    updateCustomerEmailResponse.getLogsReport().setResponseCode(updateCustomerEmailResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(updateCustomerEmailResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(updateCustomerEmailResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    updateCustomerEmailResponse.setCallStatus(Constants.Call_Status_False);

	    updateCustomerEmailResponse.setResultCode(Constants.EXCEPTION_CODE);
	    updateCustomerEmailResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    updateCustomerEmailResponse.getLogsReport().setResponseCode(updateCustomerEmailResponse.getResultCode());
	    updateCustomerEmailResponse.getLogsReport().setRequestTime(requestTime);
	    updateCustomerEmailResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    updateCustomerEmailResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(updateCustomerEmailResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(updateCustomerEmailResponse.getLogsReport());
	}
	return updateCustomerEmailResponse;
    }

    @RequestMapping(value = "/changebillinglanguage", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public ChangeBillingLanguageResponse changeBillingLanugage(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	ChangeBillingLanguageRequest changeBillingLanguageRequest = new ChangeBillingLanguageRequest();
	ChangeBillingLanguageResponse changeBillingLanguageResponse = new ChangeBillingLanguageResponse();

	String TRANSACTION_NAME = Transactions.CHANGE_BILLING_LANGUAGE_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);

	    // Populating report object before processing business logic.
	    changeBillingLanguageResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn,
		    TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    changeBillingLanguageResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    changeBillingLanguageRequest = mapper.readValue(data, ChangeBillingLanguageRequest.class);

	    // Logging specific params in report.
	    changeBillingLanguageResponse.getLogsReport()
		    .setBillingLanugage(changeBillingLanguageRequest.getLanguage());

	    String requestValidationStatus = Validator.validateRequest(msisdn, changeBillingLanguageRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		changeBillingLanguageResponse = customerServicesBusiness.changeBillingLanguageBusiness(msisdn,
			changeBillingLanguageRequest, changeBillingLanguageResponse);
	    } else {
		changeBillingLanguageResponse.setCallStatus(Constants.Call_Status_False);
		changeBillingLanguageResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		changeBillingLanguageResponse.setResultDesc(requestValidationStatus);
	    }

	    changeBillingLanguageResponse.getLogsReport().setRequestTime(requestTime);
	    changeBillingLanguageResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    changeBillingLanguageResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(changeBillingLanguageResponse));
	    changeBillingLanguageResponse.getLogsReport()
		    .setResponseCode(changeBillingLanguageResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(changeBillingLanguageResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(changeBillingLanguageResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    changeBillingLanguageResponse.setCallStatus(Constants.Call_Status_False);

	    changeBillingLanguageResponse.setResultCode(Constants.EXCEPTION_CODE);
	    changeBillingLanguageResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    changeBillingLanguageResponse.getLogsReport()
		    .setResponseCode(changeBillingLanguageResponse.getResultCode());
	    changeBillingLanguageResponse.getLogsReport().setRequestTime(requestTime);
	    changeBillingLanguageResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    changeBillingLanguageResponse.getLogsReport()
		    .setResponse(mapper.writeValueAsString(changeBillingLanguageResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(changeBillingLanguageResponse.getLogsReport());
	}
	return changeBillingLanguageResponse;
    }

    @RequestMapping(value = "/appresume", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public AppResumeResponse appResume(@RequestBody String data,
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	AppResumeRequest appResumeRequest = new AppResumeRequest();
	AppResumeResponse appResumeResponse = new AppResumeResponse();

	String TRANSACTION_NAME = Transactions.APP_RESUME_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);
	    String deviceID = servletRequest.getHeader("deviceID");
	    String passHash = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    "passHash");

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    appResumeRequest = mapper.readValue(data, AppResumeRequest.class);
	    String requestValidationStatus = Validator.validateRequest(msisdn, appResumeRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		appResumeResponse = customerServicesBusiness.appResumeBusiness(msisdn, deviceID, passHash,
			appResumeRequest, appResumeResponse);
	    } else {
		appResumeResponse.setCallStatus(Constants.Call_Status_False);
		appResumeResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		appResumeResponse.setResultDesc(requestValidationStatus);
	    }

	    // Populating report object after generating token.
	    if (appResumeResponse.getResultCode().equalsIgnoreCase(Constants.APP_SERVER_SUCCESS_CODE)) {
		appResumeResponse.setLogsReport(
			Utilities.preBusinessReportLoggingData(appResumeResponse.getData().getCustomerData().getToken(),
				msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(), userAgent,
				appResumeResponse.getData().getCustomerData().getSubscriberType(),
				appResumeResponse.getData().getCustomerData().getBrandName(), data, lang,
				appResumeResponse.getLogsReport()));
	    }

	    appResumeResponse.getLogsReport().setRequestTime(requestTime);
	    appResumeResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    appResumeResponse.getLogsReport().setResponse(mapper.writeValueAsString(appResumeResponse));
	    appResumeResponse.getLogsReport().setResponseCode(appResumeResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(appResumeResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(appResumeResponse), logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    appResumeResponse.setCallStatus(Constants.Call_Status_False);

	    appResumeResponse.setResultCode(Constants.EXCEPTION_CODE);
	    appResumeResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    appResumeResponse.getLogsReport().setRequestTime(requestTime);
	    appResumeResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    appResumeResponse.getLogsReport().setResponse(mapper.writeValueAsString(appResumeResponse));
	    appResumeResponse.getLogsReport().setResponseCode(appResumeResponse.getResultCode());

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(appResumeResponse.getLogsReport());
	}
	return appResumeResponse;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public LogoutResponse logout(
	    @RequestHeader(value = Constants.TOKEN_KEY, defaultValue = Constants.TOKEN_DEFAULT_VALUE) String token,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	String data = "{}";
	JSONObject jsonObject = new JSONObject();
	LogoutRequest logoutRequest = new LogoutRequest();
	LogoutResponse logoutResponse = new LogoutResponse();

	String TRANSACTION_NAME = Transactions.LOGOUT_TRANSACTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    data = jsonObject.toString();

	    String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.decodeString(token)).toString(),
		    Constants.MSISDN_KEY);
	    String deviceID = servletRequest.getHeader("deviceID");

	    // Populating report object before processing business logic.
	    logoutResponse.setLogsReport(Utilities.preBusinessReportLoggingData(token, msisdn, TRANSACTION_NAME,
		    servletRequest.getRemoteAddr(), userAgent, userType, tariffType, data, lang,
		    logoutResponse.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, "false");

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    logoutRequest = mapper.readValue(data, LogoutRequest.class);

	    String requestValidationStatus = Validator.validateRequest(msisdn, logoutRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		logoutResponse = customerServicesBusiness.logoutBusiness(msisdn, deviceID, logoutRequest,
			logoutResponse);
	    } else {
		logoutResponse.setCallStatus(Constants.Call_Status_False);
		logoutResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		logoutResponse.setResultDesc(requestValidationStatus);
	    }

	    logoutResponse.getLogsReport().setResponseCode(logoutResponse.getResultCode());
	    logoutResponse.getLogsReport().setRequestTime(requestTime);
	    logoutResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    logoutResponse.getLogsReport().setResponse(mapper.writeValueAsString(logoutResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(logoutResponse.getLogsReport());

	    Utilities.printInfoLog(msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-"
		    + mapper.writeValueAsString(logoutResponse), logger);
	} catch (Exception e) {

	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    logoutResponse.setCallStatus(Constants.Call_Status_False);

	    logoutResponse.setResultCode(Constants.EXCEPTION_CODE);
	    logoutResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    logoutResponse.getLogsReport().setResponseCode(logoutResponse.getResultCode());
	    logoutResponse.getLogsReport().setRequestTime(requestTime);
	    logoutResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    logoutResponse.getLogsReport().setResponse(mapper.writeValueAsString(logoutResponse));

	    // Sending report log into queue.
	    Utilities.prepareLogReportForQueue(logoutResponse.getLogsReport());
	}
	return logoutResponse;
    }
}

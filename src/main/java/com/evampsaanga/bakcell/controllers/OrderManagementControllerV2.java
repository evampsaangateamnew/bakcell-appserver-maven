package com.evampsaanga.bakcell.controllers;

import java.io.IOException;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evampsaanga.bakcell.business.OrderManagementBusiness;
import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.common.utilities.Validator;
import com.evampsaanga.bakcell.models.cancelpending.CancelPendingRequest;
import com.evampsaanga.bakcell.models.cancelpending.CancelPendingResponse;
import com.evampsaanga.bakcell.models.orderdetails.OrderDetailsAppServerResponse;
import com.evampsaanga.bakcell.models.orderdetails.OrderDetailsRequest;
import com.evampsaanga.bakcell.models.ordermanagementinsert.ChangeLimitInitialMaximumRequest;
import com.evampsaanga.bakcell.models.ordermanagementinsert.OrderManagementInsertRequest;
import com.evampsaanga.bakcell.models.ordermanagementinsert.OrderManagementInsertResponse;
import com.evampsaanga.bakcell.models.ordermanagementinsert.changeLimitInitialMaximumResponse;
import com.evampsaanga.bakcell.models.retryfailed.RetryFailedRequest;
import com.evampsaanga.bakcell.models.retryfailed.RetryFailedResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Aqeel Abbas
 *
 */
@RestController
@RequestMapping(Constants.SERVLET_URL + "/ordermanagement")
public class OrderManagementControllerV2 {
    Logger logger = Logger.getLogger(OrderManagementControllerV2.class);

    @Autowired
    OrderManagementBusiness orderManagementBusiness;

    @RequestMapping(value = "/retryfailed", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public RetryFailedResponse retryFailed(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws SQLException, ClassNotFoundException, IOException, JMSException {

	ObjectMapper mapper = new ObjectMapper();
	RetryFailedRequest retryFailedRequest = new RetryFailedRequest();
	RetryFailedResponse retryFailedResponse = new RetryFailedResponse();
	String TRANSACTION_NAME = Transactions.RETRY_FAILED_TRANSECTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    // Populating report object before processing business logic.
	    retryFailedResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, retryFailedResponse.getLogsReport()));
	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);
	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    retryFailedRequest = mapper.readValue(data, RetryFailedRequest.class);
	    String requestValidationStatus = Validator.validateRequest(msisdn, retryFailedRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		retryFailedRequest.setIsB2B(isFromB2B);
		retryFailedResponse = orderManagementBusiness.retryFailed(msisdn, retryFailedRequest,
			retryFailedResponse);
		retryFailedResponse.getLogsReport().setResponseCode(retryFailedResponse.getResultCode());
		retryFailedResponse.getLogsReport().setRequestTime(requestTime);
		retryFailedResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		retryFailedResponse.getLogsReport().setResponse(mapper.writeValueAsString(retryFailedResponse));

		// Sending report log into queue.
		retryFailedResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
		Utilities.prepareLogReportForQueue(retryFailedResponse.getLogsReport());

	    } else {
		retryFailedResponse.setCallStatus(Constants.Call_Status_False);
		retryFailedResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		retryFailedResponse.setResultDesc(requestValidationStatus);
		retryFailedResponse.getLogsReport().setResponseCode(retryFailedResponse.getResultCode());
		retryFailedResponse.getLogsReport().setRequestTime(requestTime);
		retryFailedResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		retryFailedResponse.getLogsReport().setResponse(mapper.writeValueAsString(retryFailedResponse));

		// Sending report log into queue.
		retryFailedResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
		Utilities.prepareLogReportForQueue(retryFailedResponse.getLogsReport());
	    }

	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    retryFailedResponse.setCallStatus(Constants.Call_Status_False);

	    retryFailedResponse.setResultCode(Constants.EXCEPTION_CODE);
	    retryFailedResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    retryFailedResponse.getLogsReport().setResponseCode(retryFailedResponse.getResultCode());
	    retryFailedResponse.getLogsReport().setRequestTime(requestTime);
	    retryFailedResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    retryFailedResponse.getLogsReport().setResponse(mapper.writeValueAsString(retryFailedResponse));

	    // Sending report log into queue.
	    retryFailedResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(retryFailedResponse.getLogsReport());
	}

	return retryFailedResponse;

    }

    @RequestMapping(value = "/cancel", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public CancelPendingResponse cancelPending(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws SQLException, ClassNotFoundException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	CancelPendingRequest cancelPendingRequest = new CancelPendingRequest();
	CancelPendingResponse cancelPendingResponse = new CancelPendingResponse();
	String TRANSACTION_NAME = Transactions.CANCAL_PENDING_TRANSECTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    // Populating report object before processing business logic.
	    cancelPendingResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, cancelPendingResponse.getLogsReport()));
	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);
	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    cancelPendingRequest = mapper.readValue(data, CancelPendingRequest.class);
	    String requestValidationStatus = Validator.validateRequest(msisdn, cancelPendingRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		cancelPendingRequest.setIsB2B(isFromB2B);
		cancelPendingResponse = orderManagementBusiness.cancelPending(msisdn, cancelPendingRequest,
			cancelPendingResponse);
		cancelPendingResponse.getLogsReport().setResponseCode(cancelPendingResponse.getResultCode());
		cancelPendingResponse.getLogsReport().setRequestTime(requestTime);
		cancelPendingResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		cancelPendingResponse.getLogsReport().setResponse(mapper.writeValueAsString(cancelPendingResponse));

		// Sending report log into queue.
		cancelPendingResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
		Utilities.prepareLogReportForQueue(cancelPendingResponse.getLogsReport());

	    } else {
		cancelPendingResponse.setCallStatus(Constants.Call_Status_False);
		cancelPendingResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		cancelPendingResponse.setResultDesc(requestValidationStatus);
		cancelPendingResponse.getLogsReport().setResponseCode(cancelPendingResponse.getResultCode());
		cancelPendingResponse.getLogsReport().setRequestTime(requestTime);
		cancelPendingResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		cancelPendingResponse.getLogsReport().setResponse(mapper.writeValueAsString(cancelPendingResponse));

		// Sending report log into queue.
		cancelPendingResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
		Utilities.prepareLogReportForQueue(cancelPendingResponse.getLogsReport());
	    }

	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    cancelPendingResponse.setCallStatus(Constants.Call_Status_False);

	    cancelPendingResponse.setResultCode(Constants.EXCEPTION_CODE);
	    cancelPendingResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    cancelPendingResponse.getLogsReport().setResponseCode(cancelPendingResponse.getResultCode());
	    cancelPendingResponse.getLogsReport().setRequestTime(requestTime);
	    cancelPendingResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    cancelPendingResponse.getLogsReport().setResponse(mapper.writeValueAsString(cancelPendingResponse));

	    // Sending report log into queue.
	    cancelPendingResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(cancelPendingResponse.getLogsReport());
	}
	return cancelPendingResponse;

    }

    @RequestMapping(value = "/orderdetails", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public OrderDetailsAppServerResponse orderDetails(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws SQLException, ClassNotFoundException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	OrderDetailsRequest orderdetailsRequest = new OrderDetailsRequest();
	OrderDetailsAppServerResponse orderdetailsResponse = new OrderDetailsAppServerResponse();
	String TRANSACTION_NAME = Transactions.ORDER_DETAILS_TRANSECTION_NAME + " CONTROLLER";
	String requestTime = Utilities.getReportDateTime();
	try {
	    // Populating report object before processing business logic.
	    orderdetailsResponse.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, orderdetailsResponse.getLogsReport()));
	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);
	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);

	    orderdetailsRequest = mapper.readValue(data, OrderDetailsRequest.class);
	    String requestValidationStatus = Validator.validateRequest(msisdn, orderdetailsRequest);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {

		orderdetailsRequest.setIsB2B(isFromB2B);
		orderdetailsResponse = orderManagementBusiness.orderDetails(msisdn, orderdetailsRequest,
			orderdetailsResponse);
		orderdetailsResponse.getLogsReport().setResponseCode(orderdetailsResponse.getResultCode());
		orderdetailsResponse.getLogsReport().setRequestTime(requestTime);
		orderdetailsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		orderdetailsResponse.getLogsReport().setResponse(mapper.writeValueAsString(orderdetailsResponse));

		// Sending report log into queue.
		orderdetailsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
		Utilities.prepareLogReportForQueue(orderdetailsResponse.getLogsReport());

	    } else {
		orderdetailsResponse.setCallStatus(Constants.Call_Status_False);
		orderdetailsResponse.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		orderdetailsResponse.setResultDesc(requestValidationStatus);
		orderdetailsResponse.getLogsReport().setResponseCode(orderdetailsResponse.getResultCode());
		orderdetailsResponse.getLogsReport().setRequestTime(requestTime);
		orderdetailsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
		orderdetailsResponse.getLogsReport().setResponse(mapper.writeValueAsString(orderdetailsResponse));

		// Sending report log into queue.
		orderdetailsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
		Utilities.prepareLogReportForQueue(orderdetailsResponse.getLogsReport());
	    }

	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    orderdetailsResponse.setCallStatus(Constants.Call_Status_False);

	    orderdetailsResponse.setResultCode(Constants.EXCEPTION_CODE);
	    orderdetailsResponse
		    .setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    orderdetailsResponse.getLogsReport().setResponseCode(orderdetailsResponse.getResultCode());
	    orderdetailsResponse.getLogsReport().setRequestTime(requestTime);
	    orderdetailsResponse.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    orderdetailsResponse.getLogsReport().setResponse(mapper.writeValueAsString(orderdetailsResponse));

	    // Sending report log into queue.
	    orderdetailsResponse.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(orderdetailsResponse.getLogsReport());
	}
	return orderdetailsResponse;

    }

    @RequestMapping(value = "/insertorderbulk", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public OrderManagementInsertResponse insertOrderManagementBulk(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	OrderManagementInsertRequest request = new OrderManagementInsertRequest();
	OrderManagementInsertResponse response = new OrderManagementInsertResponse();

	String requestTime = Utilities.getReportDateTime();
	Utilities.printInfoLog(msisdn + "-Request Data in Controller-" + data, logger);

	String TRANSACTION_NAME = Transactions.INSERT_BULK_ORDER_TRANSACTION_NAME + " CONTROLLER";
	try {
	    /*
	     * String msisdn = Utilities.getValueFromJSON(Utilities.splitToken(Utilities.
	     * decodeString(token)).toString(), Constants.MSISDN_KEY);
	     */

	    // Populating report object before processing business logic.
	    response.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, response.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    if (Utilities.hasJSONKey(data, "recieverMsisdn")) {
		// Specific params for report log
		request = mapper.readValue(data, OrderManagementInsertRequest.class);
		String requestValidationStatus = Validator.validateRequest(msisdn, request);

		if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		    request.setIsB2B(isFromB2B);
		    Utilities.printInfoLog(msisdn + "-IN IF CONTROLLER-" + request, logger);
		    response = orderManagementBusiness.orderMangementInsert(msisdn, request, response);

		} else {
		    response.setCallStatus(Constants.Call_Status_False);
		    response.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		    response.setResultDesc(requestValidationStatus);
		}
	    } else {
		response.setCallStatus(Constants.Call_Status_False);
		response.setResultCode(Constants.API_FAILURE_CODE);
		response.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("appNewVersion.feature.",
			Utilities.getValueFromJSON(data, "lang")));
	    }
	    response.getLogsReport().setRequestTime(requestTime);
	    response.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    response.getLogsReport().setResponse(mapper.writeValueAsString(response));
	    response.getLogsReport().setResponseCode(response.getResultCode());

	    // Sending report log into queue.
	    response.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(response.getLogsReport());

	    Utilities.printInfoLog(
		    msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-" + mapper.writeValueAsString(response),
		    logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    response.setCallStatus(Constants.Call_Status_False);
	    response.setResultCode(Constants.EXCEPTION_CODE);
	    response.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    response.getLogsReport().setResponseCode(response.getResultCode());
	    response.getLogsReport().setRequestTime(requestTime);
	    response.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    response.getLogsReport().setResponse(mapper.writeValueAsString(response));

	    // Sending report log into queue.
	    response.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(response.getLogsReport());

	}
	return response;
    }

    @RequestMapping(value = "/changelimitminmax", method = RequestMethod.POST, headers = Constants.CONTENT_TYPE_DEFAULT_VALUE)
    public changeLimitInitialMaximumResponse changeLimitMinimumMaximum(@RequestBody String data,
	    @RequestHeader(value = Constants.MSISDN_KEY, defaultValue = Constants.MSISDN_DEFAULT_VALUE) String msisdn,
	    @RequestHeader(value = Constants.LANGUAGE_KEY, defaultValue = Constants.LANGUAGE_DEFAULT_VALUE) String lang,
	    @RequestHeader(value = Constants.USER_AGENT_KEY, defaultValue = Constants.USER_AGENT_DEFAULT_VALUE) String userAgent,
	    @RequestHeader(value = Constants.SUBSCRIBER_TYPE_KEY, defaultValue = Constants.SUBSCRIBER_TYPE_DEFAULT_VALUE) String userType,
	    @RequestHeader(value = Constants.TARIFF_TYPE_KEY, defaultValue = Constants.TARIFF_TYPE_DEFAULT_VALUE) String tariffType,
	    @RequestHeader(value = Constants.APP_PHASE_KEY, defaultValue = Constants.APP_PHASE_DEFAULT_VALUE) String isFromB2B,
	    HttpServletRequest servletRequest) throws ClassNotFoundException, SQLException, JsonParseException,
	    JsonMappingException, IOException, JMSException {
	ObjectMapper mapper = new ObjectMapper();
	ChangeLimitInitialMaximumRequest request = new ChangeLimitInitialMaximumRequest();
	changeLimitInitialMaximumResponse response = new changeLimitInitialMaximumResponse();

	String requestTime = Utilities.getReportDateTime();
	Utilities.printInfoLog(msisdn + "-Request Data in Controller-" + data, logger);

	String TRANSACTION_NAME = Transactions.CHANGE_LIMIT_BULK_GET_MAX_MIN_LIMIT_TRANSACTION_NAME + " CONTROLLER";
	try {

	    response.setLogsReport(
		    Utilities.preBusinessReportLoggingData("", msisdn, TRANSACTION_NAME, servletRequest.getRemoteAddr(),
			    userAgent, userType, tariffType, data, lang, response.getLogsReport()));

	    data = Utilities.addAttributesToRequest(data, msisdn, servletRequest, lang, userAgent, isFromB2B);

	    Utilities.printInfoLog(msisdn + "-Request Landed in minMax-" + TRANSACTION_NAME, logger);
	    Utilities.printInfoLog(msisdn + "-Request Data-" + data, logger);
	    // Specific params for report log
	    request = mapper.readValue(data, ChangeLimitInitialMaximumRequest.class);
	    String requestValidationStatus = Validator.validateRequest(msisdn, request);

	    if (requestValidationStatus.equalsIgnoreCase(Constants.VALIDATION_SUCCESS_CODE)) {
		request.setIsB2B(isFromB2B);
		Utilities.printInfoLog(msisdn + "-IN IF CONTROLLER-" + request, logger);
		response = orderManagementBusiness.changeLimitMinMax(msisdn, request, response);

	    } else {
		response.setCallStatus(Constants.Call_Status_False);
		response.setResultCode(Constants.VALIDATION_FAILURE_CODE);
		response.setResultDesc(requestValidationStatus);
	    }

	    response.getLogsReport().setRequestTime(requestTime);
	    response.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    response.getLogsReport().setResponse(mapper.writeValueAsString(response));
	    response.getLogsReport().setResponseCode(response.getResultCode());

	    // Sending report log into queue.
	    response.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(response.getLogsReport());

	    Utilities.printInfoLog(
		    msisdn + "-Response Returned from-" + TRANSACTION_NAME + "-" + mapper.writeValueAsString(response),
		    logger);
	} catch (Exception e) {
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    response.setCallStatus(Constants.Call_Status_False);
	    response.setResultCode(Constants.EXCEPTION_CODE);
	    response.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("unexpected.error", lang));

	    response.getLogsReport().setResponseCode(response.getResultCode());
	    response.getLogsReport().setRequestTime(requestTime);
	    response.getLogsReport().setResponseTime(Utilities.getReportDateTime());
	    response.getLogsReport().setResponse(mapper.writeValueAsString(response));

	    // Sending report log into queue.
	    response.getLogsReport().setAppVersion(Constants.APP_VERSION_PHASE2);
	    Utilities.prepareLogReportForQueue(response.getLogsReport());

	}
	return response;
    }
}

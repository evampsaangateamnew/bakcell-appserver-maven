package com.evampsaanga.bakcell.business;

import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.text.ParseException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.customerdata.CustomerDataRequest;
import com.evampsaanga.bakcell.models.customerdata.CustomerDataResponse;
import com.evampsaanga.bakcell.models.customerdata.CustomerDataResponseData;
import com.evampsaanga.bakcell.models.customerdata.HomePageResponseDataV2;
import com.evampsaanga.bakcell.models.customerdata.HomePageResponseV2;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.PredefinedDataRequest;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.PredefinedDataResponse;
import com.evampsaanga.bakcell.models.homepageservices.HomePageRequest;
import com.evampsaanga.bakcell.models.homepageservices.HomePageResponseData;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class CustomerDataBusiness {
    Logger logger = Logger.getLogger(CustomerServicesBusiness.class);

    public HomePageResponseV2 getCustomerData(CustomerDataRequest customerDataRequest,
	    CustomerDataResponse customerDataResponse) throws JSONException, IOException, SQLException {
	Utilities.printDebugLog(customerDataRequest.getMsisdn() + "-Request received in "
		+ Transactions.CUSTOMER_DATA_TRANSACTION_NAME + " with data-" + customerDataRequest.toString(), logger);
	CustomerDataResponseData customerDataResponseData = new CustomerDataResponseData();
	RestClient rClient = new RestClient();
	ObjectMapper mapper = new ObjectMapper();
	HomePageResponseV2 homePageResponse = new HomePageResponseV2();
	String requestJsonESB = mapper.writeValueAsString(customerDataRequest);

	HomePageRequest homePageRequest = new HomePageRequest();
	String path = GetConfigurations.getESBRoute("getCustomerInfo");
	Utilities.printDebugLog(customerDataRequest.getMsisdn() + "-Request Call to ESB", logger);
	Utilities.printDebugLog(customerDataRequest.getMsisdn() + "-Path-" + path, logger);
	Utilities.printDebugLog(customerDataRequest.getMsisdn() + "-Request Packet-" + requestJsonESB, logger);

	customerDataResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rClient.getResponseFromESB(path, requestJsonESB);
	customerDataResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
	response = Utilities.removeParamsFromJSONObject(response, "supplementaryOfferingList");
	response = Utilities.removeParamsFromJSONObject(response, "PrimaryOffering");
	logger.info("<<<<<<<<<<<<<<<<<<<< Customer Info >>>>>>>>>>>>>>>>>>>>" + response);
	Utilities.printDebugLog(customerDataRequest.getMsisdn() + "-Received response from ESB-" + response, logger);
	if (response != null && !response.isEmpty()) {
	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		/*
		 * customerDataResponseData =
		 * mapper.readValue(Utilities.getValueFromJSON(response, "customerData"),
		 * CustomerDataResponseData.class);
		 */
		JSONObject jsonObject = new JSONObject(response);
		JSONObject customerData = jsonObject.getJSONObject("customerData");

		/**
		 * Adding fix for profile image
		 */
		customerData.put("imageURL", GetConfigurations.getConfigurationFromCache("profileImageDownloadLink")
			+ customerDataRequest.getMsisdn() + ".jpg");

		String customerD = Utilities.removeParamsFromJSONObject(customerData.toString(),
			"supplementaryOfferingList");
		customerD = Utilities.removeParamsFromJSONObject(customerD, "PrimaryOffering");
		logger.info("After Removal of unused fields:" + customerD.toString());
		Utilities.printDebugLog("After Removal of unused fields:" + customerD.toString(), logger);
		customerDataResponseData = mapper.readValue(customerD, CustomerDataResponseData.class);

		/*
		 * resData.getCustomerData().setBillingLanguage(Utilities
		 * .remapBillingLanguageForApp(Utilities.getValueFromJSON(customerInfo,
		 * "billingLanguage")));
		 */

		customerDataResponseData.setBillingLanguage(Utilities.remapBillingLanguageForApp(
			Utilities.getValueFromJSON(customerData.toString(), "billingLanguage")));
		// String customerType = customerData.getString("customerType");
		String brandId = customerData.getString("brandId");
		String offeringId = customerData.getString("offeringId");
		homePageRequest.setBrandId(brandId);
		homePageRequest.setChannel(customerDataRequest.getChannel());
		homePageRequest.setiP(customerDataRequest.getiP());
		homePageRequest.setLang(customerDataRequest.getLang());
		homePageRequest.setMsisdn(customerDataRequest.getMsisdn());
		homePageRequest.setOfferingId(offeringId);
		// homePageRequest.setCustomerType(customerType);
		homePageRequest.setCustomerType("postpaid");
		homePageRequest.setIsB2B(customerDataRequest.getIsB2B());
		requestJsonESB = mapper.writeValueAsString(homePageRequest);

		Utilities.printDebugLog(customerDataRequest.getMsisdn() + "-Request received in "
			+ Transactions.HOME_PAGE_TRANSACTION_NAME + " BUSINESS with data-" + requestJsonESB.toString(),
			logger);

		customerDataResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		path = GetConfigurations.getESBRoute("getHomePage");
		response = rClient.getResponseFromESB(path, requestJsonESB);
		customerDataResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
		Utilities.printDebugLog(
			"home page response from ESB. Path is:" + path + "-Received response from ESB-" + response,
			logger);
		logger.info("Fetching info for Predefined Data");
		PredefinedDataResponse predefinedDataResponse = new PredefinedDataResponse();
		PredefinedDataRequest predefinedDataRequest = new PredefinedDataRequest();
		predefinedDataRequest.setOfferingId(offeringId);
		predefinedDataRequest.setChannel(customerDataRequest.getChannel());
		predefinedDataRequest.setiP(customerDataRequest.getiP());
		predefinedDataRequest.setLang(customerDataRequest.getLang());
		predefinedDataRequest.setMsisdn(customerDataRequest.getMsisdn());
		predefinedDataRequest.setIsB2B(customerDataRequest.getIsB2B());
		CustomerServicesBusiness customerServicesBusiness = new CustomerServicesBusiness();
		HomePageResponseDataV2 responseDataV2 = new HomePageResponseDataV2();
		try {
		    predefinedDataResponse = customerServicesBusiness.getPredefinedData(customerDataRequest.getMsisdn(),
			    predefinedDataRequest, predefinedDataResponse, offeringId);
		    responseDataV2.setPredefinedData(predefinedDataResponse);
		    responseDataV2.getPredefinedData().setRoamingVisible(Utilities.getRoamingVisibleFlag(customerDataRequest.getMsisdn(), customerDataResponseData.getSubscriberType(), customerDataResponseData.getCustomerType()));
		} catch (Exception e) {
		    logger.error("Unable to get predefined Data", e);
		    responseDataV2.setPredefinedData(null);
		}
		// Logging ESB response code and description.
		/*
		 * homePageResponse.setLogsReport(Utilities.logESBParamsintoReportLog(
		 * requestJsonESB, response, homePageResponse.getLogsReport()));
		 */
		HomePageResponseData resData = new HomePageResponseData();
		if (response != null && !response.isEmpty()) {

		    if (Utilities.getValueFromJSON(response, "returnCode")
			    .equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
			try {
			    resData = mapper.readValue(Utilities.getValueFromJSON(response, "data"),
				    HomePageResponseData.class);

			    // Setting labels
			    resData = setLabels(resData, homePageRequest.getLang(), homePageRequest.getBrandId());

			    // set initial date for installments.
			    resData = setInstallmentsInitialDate(resData);
			    responseDataV2.setHomePageData(resData);
			    responseDataV2.setCustomerInfo(customerDataResponseData);
			    homePageResponse.setData(responseDataV2);
			    homePageResponse.setCallStatus(Constants.Call_Status_True);
			    homePageResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
			    homePageResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
				    homePageRequest.getLang()));
			} catch (Exception e) {
			    logger.error("Json parsing Error:", e);
			}

		    } else {
			homePageResponse.setCallStatus(Constants.Call_Status_False);
			/*
			 * resData = mapper.readValue(Utilities.getValueFromJSON(response, "data"),
			 * HomePageResponseData.class); responseDataV2.setHomePageData(resData);
			 * responseDataV2.setCustomerInfo(customerDataResponseData);
			 * homePageResponse.setData(responseDataV2);
			 */
			homePageResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
			homePageResponse.setResultDesc(Utilities.getErrorMessageFromDB("home.page",
				homePageRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
		    }
		} else {
		    logger.error("Null response received from customer data api");
		    homePageResponse.setCallStatus(Constants.Call_Status_False);
		    homePageResponse.setResultCode(Constants.API_FAILURE_CODE);
		    homePageResponse.setResultDesc(GetMessagesMappings
			    .getMessageFromResourceBundle("connectivity.error", homePageRequest.getLang()));
		}

	    }
	} else {
	    homePageResponse.setCallStatus(Constants.Call_Status_False);
	    homePageResponse.setResultCode(Constants.API_FAILURE_CODE);
	    homePageResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", homePageRequest.getLang()));

	}

	return homePageResponse;
    }

    private String getNextDateForInstallments(String date) throws ParseException {

	return Utilities.getNextMonth(Utilities.getDateFromString(date, Constants.DATE_FORMAT));

    }

    private HomePageResponseData setInstallmentsInitialDate(HomePageResponseData resData) {
	resData.getInstallments().getInstallments().forEach((installment) -> {
	    try {
		installment.setNextPaymentInitialDate(installment.getPurchaseDateValue());

		if (!installment.getRemainingPeriodBeginDateValue().isEmpty())
		    installment.setNextPaymentValue(getNextDateForInstallments(Utilities.getCurrentDateTime()));
	    } catch (ParseException e) {

		try {
		    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		} catch (SocketException e1) {
		    // TODO Auto-generated catch block
		    e1.printStackTrace();
		}
	    }
	});
	return resData;
    }

    private HomePageResponseData setLabels(HomePageResponseData resData, String lang, String brandId)
	    throws SocketException, SQLException {

	// Setting Prepaid Balance Labels
	resData.getBalance().getPrepaid().getMainWallet()
		.setBalanceTypeName(GetMessagesMappings.getLabelsFromResourceBundle("mainWalletLabel", lang));
	resData.getBalance().getPrepaid().getCountryWideWallet()
		.setBalanceTypeName(GetMessagesMappings.getLabelsFromResourceBundle("countryWideLabel", lang));
	resData.getBalance().getPrepaid().getBounusWallet()
		.setBalanceTypeName(GetMessagesMappings.getLabelsFromResourceBundle("bonusWalletLabel", lang));

	// Setting Postpaid Balance Labels
	resData.getBalance().getPostpaid()
		.setAvailableCreditLabel(GetMessagesMappings.getLabelsFromResourceBundle("availableCreditLabel", lang));
	resData.getBalance().getPostpaid()
		.setBalanceLabel(GetMessagesMappings.getLabelsFromResourceBundle("balanceLabel", lang));
	resData.getBalance().getPostpaid()
		.setCorporateLabel(GetMessagesMappings.getLabelsFromResourceBundle("corporateLabel", lang));
	resData.getBalance().getPostpaid()
		.setCurrentCreditLabel(GetMessagesMappings.getLabelsFromResourceBundle("currentCreditLabel", lang));
	resData.getBalance().getPostpaid()
		.setIndividualLabel(GetMessagesMappings.getLabelsFromResourceBundle("individualLabel", lang));
	resData.getBalance().getPostpaid().setOutstandingIndividualDebtLabel(
		GetMessagesMappings.getLabelsFromResourceBundle("outstandingIndividualDeptLabel", lang));

	// Setting Installments Labels
	resData.getInstallments()
		.setInstallmentTitle(GetMessagesMappings.getLabelsFromResourceBundle("installmentTitle", lang));
	resData.getInstallments().getInstallments().forEach((Installment) -> {
	    try {
		Installment.setAmountLabel(GetMessagesMappings.getLabelsFromResourceBundle("amountLabel", lang));
		Installment
			.setNextPaymentLabel(GetMessagesMappings.getLabelsFromResourceBundle("nextPaymentLabel", lang));
		Installment.setPurchaseDateLabel(
			GetMessagesMappings.getLabelsFromResourceBundle("purchaseDateLabel", lang));
		Installment.setRemainingAmountLabel(
			GetMessagesMappings.getLabelsFromResourceBundle("remainingAmountLabel", lang));
		Installment.setRemainingPeriodBeginDateLabel(
			GetMessagesMappings.getLabelsFromResourceBundle("remainingPeriodBeginDateLabel", lang));
		Installment.setRemainingPeriodEndDateLabel(
			GetMessagesMappings.getLabelsFromResourceBundle("remainingPeriodEndDateLabel", lang));

		Installment.setRemainingPeriodLabel(
			GetMessagesMappings.getLabelsFromResourceBundle("remainingPeriodLabel", lang));
	    } catch (Exception e) {
		try {
		    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		} catch (SocketException e1) {

		    e1.printStackTrace();
		}
	    }
	});

	// Setting MRC Labels
	resData.getMrc().setMrcTitleLabel(GetMessagesMappings
		.getLabelsFromResourceBundle("mrcTitleLabel." + resData.getMrc().getMrcType().toLowerCase(), lang));
	resData.getMrc().setMrcDateLabel(GetMessagesMappings.getLabelsFromResourceBundle("mrcDateLabel", lang));
	if (resData.getMrc().getMrcTitleValue() != null && resData.getMrc().getMrcTitleValue().equalsIgnoreCase("FREE"))
	    resData.getMrc().setMrcTitleValue(GetMessagesMappings.getLabelsFromResourceBundle("mrcValueFree", lang));

	// Setting Credit Labels
	if (brandId.equalsIgnoreCase("1770078090"))
	    resData.getCredit().setCreditTitleLabel(
		    GetMessagesMappings.getLabelsFromResourceBundle("creditTitleLabelKlass", lang));
	else if (brandId.equalsIgnoreCase("1970006532"))
	    resData.getCredit()
		    .setCreditTitleLabel(GetMessagesMappings.getLabelsFromResourceBundle("creditTitleLabelCin", lang));
	if (resData.getCredit().getCreditDateLabel().equalsIgnoreCase(Constants.CREDIT_EXPIRATION_LABEL_BLOCK_ONE)) {
	    resData.getCredit()
		    .setCreditDateLabel(GetMessagesMappings.getLabelsFromResourceBundle("creditDateLabel.1", lang));
	} else if (resData.getCredit().getCreditDateLabel()
		.equalsIgnoreCase(Constants.CREDIT_EXPIRATION_LABEL_BLOCK_TWO)) {
	    resData.getCredit()
		    .setCreditDateLabel(GetMessagesMappings.getLabelsFromResourceBundle("creditDateLabel.2", lang));
	} else if (resData.getCredit().getCreditDateLabel()
		.equalsIgnoreCase(Constants.CREDIT_EXPIRATION_LABEL_EXPIRED)) {
	    resData.getCredit()
		    .setCreditDateLabel(GetMessagesMappings.getLabelsFromResourceBundle("creditDateLabel.3", lang));
	} else {
	    resData.getCredit().setCreditDateLabel("");
	}

	// Setting Free Resource Labels
	resData.getFreeResources().getFreeResources().forEach((FreeResource) -> {

	    try {
		FreeResource.setResourcesTitleLabel(GetMessagesMappings.getLabelsFromResourceBundle(
			"resourcesTitleLabel." + FreeResource.getResourceType().toLowerCase(), lang));
		FreeResource.setResourceUnitName(GetMessagesMappings
			.getLabelsFromResourceBundle(FreeResource.getResourceUnitName().toLowerCase(), lang));
	    } catch (Exception e) {
		try {
		    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		} catch (Exception e1) {
		    // TODO Auto-generated catch block
		    e1.printStackTrace();
		}
	    }

	});

	// Setting Free Resource Roaming Labels
	resData.getFreeResources().getFreeResourcesRoaming().forEach((FreeResource) -> {
	    try {
		FreeResource.setResourcesTitleLabel(GetMessagesMappings.getLabelsFromResourceBundle(
			"resourcesTitleLabel.Roaming." + FreeResource.getResourceType().toLowerCase(), lang));

		FreeResource.setResourceUnitName(GetMessagesMappings
			.getLabelsFromResourceBundle(FreeResource.getResourceUnitName().toLowerCase(), lang));
	    } catch (Exception e) {
		try {
		    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		} catch (Exception e1) {
		    // TODO Auto-generated catch block
		    e1.printStackTrace();
		}
	    }

	});

	return resData;
    }

}

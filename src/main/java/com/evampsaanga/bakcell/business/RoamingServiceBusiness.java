package com.evampsaanga.bakcell.business;

import java.net.SocketException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.roaming.RoamingServiceRequest;
import com.evampsaanga.bakcell.models.roaming.RoamingServiceResponse;
import com.evampsaanga.bakcell.models.roaming.RoamingServiceResponseData;
import com.evampsaanga.bakcell.models.roaming.roamingstatus.RoamingServiceStatusRequest;
import com.evampsaanga.bakcell.models.roaming.roamingstatus.RoamingServiceStatusResponse;
import com.evampsaanga.bakcell.models.roaming.roamingstatus.RoamingServiceStatusResponseData;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class RoamingServiceBusiness {
    Logger logger = Logger.getLogger(RoamingServiceBusiness.class);

    public RoamingServiceResponse processRoamingServiceBusiness(String msisdn,
	    RoamingServiceRequest roamingServiceRequest, RoamingServiceResponse roamingServiceResponse)
	    throws SQLException, SocketException, JSONException, JsonProcessingException {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ROAMING_SERVICE_PROCESS_TRANSECTION_NAME
		+ " BUSINESS with data-" + roamingServiceRequest.toString(), logger);

	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(roamingServiceRequest);
	requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "productId",
		roamingServiceRequest.getOfferingId());
	requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "selectFlag",
		roamingServiceRequest.getActionType());
	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "offeringId");
	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "actionType");

	String path = GetConfigurations.getESBRoute("processRoamingService");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	roamingServiceResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	roamingServiceResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	roamingServiceResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, roamingServiceResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		roamingServiceResponse.setCallStatus(Constants.Call_Status_True);
		roamingServiceResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		roamingServiceResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", roamingServiceRequest.getLang()));
		RoamingServiceResponseData data = new RoamingServiceResponseData();
		data.setMessage(
			GetMessagesMappings.getMessageFromResourceBundle("success", roamingServiceRequest.getLang()));
		roamingServiceResponse.setData(data);
		return roamingServiceResponse;

	    } else {

		roamingServiceResponse.setCallStatus(Constants.Call_Status_False);
		roamingServiceResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		roamingServiceResponse.setResultDesc(Utilities.getErrorMessageFromDB("roaming.service",
			roamingServiceRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
		roamingServiceResponse.setData(null);
	    }
	} else

	{

	    roamingServiceResponse.setCallStatus(Constants.Call_Status_False);
	    roamingServiceResponse.setResultCode(Constants.API_FAILURE_CODE);
	    roamingServiceResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    roamingServiceRequest.getLang()));
	    roamingServiceResponse.setData(null);
	}

	return roamingServiceResponse;
    }

    public RoamingServiceStatusResponse getRoamingServiceStatusBusiness(String msisdn,
	    RoamingServiceStatusRequest roamingServiceStatusRequest,
	    RoamingServiceStatusResponse roamingServiceStatusResponse)
	    throws JSONException, SocketException, JsonProcessingException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ROAMING_SERVICE_STATUS_TRANSECTION_NAME
		+ " BUSINESS with data-" + roamingServiceStatusRequest.toString(), logger);

	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(roamingServiceStatusRequest);
	requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "productId",
		roamingServiceStatusRequest.getOfferingId());

	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "offeringId");
	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "actionType");

	String path = GetConfigurations.getESBRoute("getRoamingStatusService");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	roamingServiceStatusResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	roamingServiceStatusResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	roamingServiceStatusResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		roamingServiceStatusResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		roamingServiceStatusResponse.setCallStatus(Constants.Call_Status_True);
		roamingServiceStatusResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		roamingServiceStatusResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			roamingServiceStatusRequest.getLang()));
		RoamingServiceStatusResponseData data = new RoamingServiceStatusResponseData();

		data.setActive(Utilities.getValueFromJSON(response, "active").equalsIgnoreCase("true") ? true : false);
		roamingServiceStatusResponse.setData(data);
		return roamingServiceStatusResponse;

	    } else {
		roamingServiceStatusResponse.setCallStatus(Constants.Call_Status_False);
		roamingServiceStatusResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		roamingServiceStatusResponse.setResultDesc(Utilities.getErrorMessageFromDB("roaming.service",
			roamingServiceStatusRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
		roamingServiceStatusResponse.setData(null);
	    }
	} else

	{

	    roamingServiceStatusResponse.setCallStatus(Constants.Call_Status_False);
	    roamingServiceStatusResponse.setResultCode(Constants.API_FAILURE_CODE);
	    roamingServiceStatusResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", roamingServiceStatusRequest.getLang()));
	    roamingServiceStatusResponse.setData(null);
	}

	Utilities.printInfoLog(
		msisdn + "-Response Returned from Business" + mapper.writeValueAsString(roamingServiceStatusResponse),
		logger);
	return roamingServiceStatusResponse;
    }
}

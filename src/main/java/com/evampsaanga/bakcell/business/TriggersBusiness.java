/**
 * 
 */
package com.evampsaanga.bakcell.business;

import java.net.SocketException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.utilities.AppCache;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.triggers.TriggersRequest;
import com.evampsaanga.bakcell.models.triggers.TriggersResponse;

/**
 * @author Evamp & Saanga
 *
 */
@Service
public class TriggersBusiness {

    Logger logger = Logger.getLogger(TriggersBusiness.class);

    public TriggersResponse refreshCacheBusiness(TriggersRequest triggersRequest, TriggersResponse triggersResponse)
	    throws SocketException {
	Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME
		+ "-Request received in TRIGGERS BUSINESS with data-" + triggersRequest.toString(), logger);

	if (triggersRequest.getCacheType().equalsIgnoreCase("1")) {

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger received for FAQs", logger);

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size before refresh:"
		    + AppCache.getHashmapFAQs().size(), logger);

	    AppCache.getHashmapFAQs().clear();

	    Utilities.printDebugLog(Constants.TRIGGERS_CACHE_AFTER_REFRESH + AppCache.getHashmapFAQs().size(), logger);

	    if (AppCache.getHashmapFAQs().size() == 0) {
		triggersResponse.setCallStatus(Constants.Call_Status_True);
		triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

	    } else {
		triggersResponse.setCallStatus(Constants.Call_Status_False);
		triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

	    }
	} else if (triggersRequest.getCacheType().equalsIgnoreCase("2")) {

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger received for CONTACT US", logger);

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size before refresh:"
		    + AppCache.getHashmapContactUs().size(), logger);

	    AppCache.getHashmapContactUs().clear();

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size After refresh:"
		    + AppCache.getHashmapContactUs().size(), logger);

	    if (AppCache.getHashmapContactUs().size() == 0) {
		triggersResponse.setCallStatus(Constants.Call_Status_True);
		triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

	    } else {
		triggersResponse.setCallStatus(Constants.Call_Status_False);
		triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
		triggersResponse.setResultDesc("Failed to refresh cache.");

	    }
	} else if (triggersRequest.getCacheType().equalsIgnoreCase("3")) {

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger received for APP MENU", logger);

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size before refresh:"
		    + AppCache.getHashmapAppMenu().size(), logger);

	    AppCache.getHashmapAppMenu().clear();

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size After refresh:"
		    + AppCache.getHashmapAppMenu().size(), logger);

	    if (AppCache.getHashmapAppMenu().size() == 0) {
		triggersResponse.setCallStatus(Constants.Call_Status_True);
		triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

	    } else {
		triggersResponse.setCallStatus(Constants.Call_Status_False);
		triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
		triggersResponse.setResultDesc("Failed to refresh cache.");

	    }
	} else if (triggersRequest.getCacheType().equalsIgnoreCase("4")) {

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger received for STORE LOCATOR",
		    logger);

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size before refresh:"
		    + AppCache.getHashmapStoreLocator().size(), logger);

	    AppCache.getHashmapStoreLocator().clear();

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size After refresh:"
		    + AppCache.getHashmapStoreLocator().size(), logger);

	    if (AppCache.getHashmapStoreLocator().size() == 0) {
		triggersResponse.setCallStatus(Constants.Call_Status_True);
		triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

	    } else {
		triggersResponse.setCallStatus(Constants.Call_Status_False);
		triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
		triggersResponse.setResultDesc("Failed to refresh cache.");

	    }
	} else if (triggersRequest.getCacheType().equalsIgnoreCase("5")) {

	    Utilities.printDebugLog(
		    Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger received for SUPPLEMENTARY OFFERINGS", logger);

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size before refresh:"
		    + AppCache.getHashmapSupplementaryOfferings().size(), logger);

	    AppCache.getHashmapSupplementaryOfferings().clear();

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size After refresh:"
		    + AppCache.getHashmapSupplementaryOfferings().size(), logger);

	    if (AppCache.getHashmapSupplementaryOfferings().size() == 0) {
		triggersResponse.setCallStatus(Constants.Call_Status_True);
		triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

	    } else {
		triggersResponse.setCallStatus(Constants.Call_Status_False);
		triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
		triggersResponse.setResultDesc("Failed to refresh cache.");

	    }
	} else if (triggersRequest.getCacheType().equalsIgnoreCase("6")) {

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger received for TARIFFS", logger);

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size before refresh:"
		    + AppCache.getHashmapTariffs().size(), logger);

	    AppCache.getHashmapTariffs().clear();

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size After refresh:"
		    + AppCache.getHashmapTariffs().size(), logger);

	    if (AppCache.getHashmapTariffs().size() == 0) {
		triggersResponse.setCallStatus(Constants.Call_Status_True);
		triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

	    } else {
		triggersResponse.setCallStatus(Constants.Call_Status_False);
		triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

	    }
	} else if (triggersRequest.getCacheType().equalsIgnoreCase("7")) {

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger received for Predefined Data",
		    logger);

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size before refresh:"
		    + AppCache.getHashmapPredefinedData().size(), logger);

	    AppCache.getHashmapPredefinedData().clear();
	    AppCache.getHashmapTariffMigration().clear();

	    Utilities.printDebugLog(Constants.TRIGGERS_CACHE_AFTER_REFRESH + AppCache.getHashmapPredefinedData().size(),
		    logger);

	    if (AppCache.getHashmapPredefinedData().size() == 0) {
		triggersResponse.setCallStatus(Constants.Call_Status_True);
		triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

	    } else {
		triggersResponse.setCallStatus(Constants.Call_Status_False);
		triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

	    }
	} else if (triggersRequest.getCacheType().equalsIgnoreCase("8")) {

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger received for My Subscriptions",
		    logger);

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size before refresh:"
		    + AppCache.getHashmapMySubscriptions().size(), logger);

	    AppCache.getHashmapMySubscriptions().clear();

	    Utilities.printDebugLog(
		    Constants.TRIGGERS_CACHE_AFTER_REFRESH + AppCache.getHashmapMySubscriptions().size(), logger);

	    if (AppCache.getHashmapMySubscriptions().size() == 0) {
		triggersResponse.setCallStatus(Constants.Call_Status_True);
		triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

	    } else {
		triggersResponse.setCallStatus(Constants.Call_Status_False);
		triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

	    }
	} else if (triggersRequest.getCacheType().equalsIgnoreCase("9")) {

	    Utilities.printDebugLog(
		    Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger received for APP SERVER Configurations", logger);

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size before refresh:"
		    + AppCache.getHashmapConfigurations().size(), logger);

	    AppCache.getHashmapConfigurations().clear();

	    Utilities.printDebugLog(Constants.TRIGGERS_CACHE_AFTER_REFRESH + AppCache.getHashmapConfigurations().size(),
		    logger);

	    if (AppCache.getHashmapConfigurations().size() == 0) {
		triggersResponse.setCallStatus(Constants.Call_Status_True);
		triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

	    } else {
		triggersResponse.setCallStatus(Constants.Call_Status_False);
		triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

	    }
	} else if (triggersRequest.getCacheType().equalsIgnoreCase("10")) {

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger received for Transaction Names",
		    logger);

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size before refresh:"
		    + AppCache.getHashmapTransactionNames().size(), logger);

	    AppCache.getHashmapTransactionNames().clear();

	    Utilities.printDebugLog(
		    Constants.TRIGGERS_CACHE_AFTER_REFRESH + AppCache.getHashmapTransactionNames().size(), logger);

	    if (AppCache.getHashmapTransactionNames().size() == 0) {
		triggersResponse.setCallStatus(Constants.Call_Status_True);
		triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

	    } else {
		triggersResponse.setCallStatus(Constants.Call_Status_False);
		triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

	    }
	} 
	//11 is trigger for golden pay  messages map
	else if (triggersRequest.getCacheType().equalsIgnoreCase("11")) {

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger received for golden pay messages",
		    logger);

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size before refresh:"
		    + AppCache.getGoldenPayMessages().size(), logger);

	    AppCache.getGoldenPayMessages().clear();

	    Utilities.printDebugLog(
		    Constants.TRIGGERS_CACHE_AFTER_REFRESH + AppCache.getGoldenPayMessages().size(), logger);

	    if (AppCache.getGoldenPayMessages().size() == 0) {
		triggersResponse.setCallStatus(Constants.Call_Status_True);
		triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

	    } else {
		triggersResponse.setCallStatus(Constants.Call_Status_False);
		triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

	    }
	}
	
	else if (triggersRequest.getCacheType().equalsIgnoreCase("13")) {

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger received for Surveys",
		    logger);

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger cache size before refresh:"
		    + AppCache.getHashMapSurveys().size(), logger);

	    AppCache.getHashMapSurveys().clear();

	    Utilities.printDebugLog(
		    Constants.TRIGGERS_CACHE_AFTER_REFRESH + AppCache.getHashMapSurveys().size(), logger);

	    if (AppCache.getHashMapSurveys().size() == 0) {
		triggersResponse.setCallStatus(Constants.Call_Status_True);
		triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

	    } else {
		triggersResponse.setCallStatus(Constants.Call_Status_False);
		triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

	    }
	}
	
	// clearing cache for all transactions 0 means all
	else if (triggersRequest.getCacheType().equalsIgnoreCase("0")) {

	    Utilities.printDebugLog(Transactions.TRIGGERS_TRANSACTION_NAME + "Trigger received for full cache",
		    logger);


	    AppCache.getHashmapFAQs().clear();
	    AppCache.getHashmapContactUs().clear();
	    AppCache.getHashmapAppMenu().clear();
	    AppCache.getHashmapStoreLocator().clear();
	    AppCache.getHashmapSupplementaryOfferings().clear();
	    AppCache.getHashmapTariffs().clear();
	    AppCache.getHashmapPredefinedData().clear();
	    AppCache.getHashmapMySubscriptions().clear();
	    AppCache.getHashmapConfigurations().clear();
	    AppCache.getHashmapTransactionNames().clear();
	    AppCache.getHashmapTariffMigration().clear();
	    AppCache.getGoldenPayMessages().clear();
	    AppCache.getHashMapSurveys().clear();

	    if (AppCache.getHashmapTransactionNames().size() == 0) {
		triggersResponse.setCallStatus(Constants.Call_Status_True);
		triggersResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE);

	    } else {
		triggersResponse.setCallStatus(Constants.Call_Status_False);
		triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
		triggersResponse.setResultDesc(Constants.TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE);

	    }
	} else {
	    triggersResponse.setCallStatus(Constants.Call_Status_False);
	    triggersResponse.setResultCode(Constants.API_FAILURE_CODE);
	    triggersResponse.setResultDesc("Transactions.TRIGGERS_TRANSACTION_NAME+Cache against key ("
		    + triggersRequest.getCacheType() + ") not exist in Bakcell E-care system.");

	}

	return triggersResponse;
    }

}

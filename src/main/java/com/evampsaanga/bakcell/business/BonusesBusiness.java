package com.evampsaanga.bakcell.business;

import java.io.IOException;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.bonus.GetBonusPointsRequest;
import com.evampsaanga.bakcell.models.bonus.GetBonusPointsResponse;
import com.evampsaanga.bakcell.models.bonus.SaveBonusPointsRequest;
import com.evampsaanga.bakcell.models.bonus.SaveBonusPointsResponse;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class BonusesBusiness {
	Logger logger = Logger.getLogger(BonusesBusiness.class);

	public GetBonusPointsResponse getBonusPointsBusiness(String msisdn, GetBonusPointsRequest getBonusPointsRequest,
			GetBonusPointsResponse getBonusPointsResponse) throws JSONException, IOException, SQLException {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_BONUS_POINTS_TRANSACTION_NAME
				+ " BUSINESS with data-" + getBonusPointsRequest.toString(), logger);

//		RestClient rc = new RestClient();
//		ObjectMapper mapper = new ObjectMapper();
//
//		String requestJsonESB = mapper.writeValueAsString(getBonusPointsRequest);
//
//		String path = GetConfigurations.getESBRoute("getBonusPoints");
//		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
//		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
//		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
//
//		getBonusPointsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
//		String response = rc.getResponseFromESB(path, requestJsonESB);
//		getBonusPointsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
//
//		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
//
//		// Logging ESB response code and description.
//		getBonusPointsResponse.setLogsReport(
//				Utilities.logESBParamsintoReportLog(requestJsonESB, response, getBonusPointsResponse.getLogsReport()));
//
//		if (response != null && !response.isEmpty()) {
//
//			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
//
//				getBonusPointsResponse.setCallStatus(Constants.Call_Status_True);
//				getBonusPointsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
//				getBonusPointsResponse.setResultDesc(
//						GetMessagesMappings.getMessageFromResourceBundle("success", getBonusPointsRequest.getLang()));
//
//			} else {
//				getBonusPointsResponse.setCallStatus(Constants.Call_Status_False);
//				getBonusPointsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
//				getBonusPointsResponse.setResultDesc(Utilities.getErrorMessageFromDB("connectivity.error",
//						getBonusPointsRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
//			}
//		} else {
//
//			getBonusPointsResponse.setCallStatus(Constants.Call_Status_False);
//			getBonusPointsResponse.setResultCode(Constants.API_FAILURE_CODE);
//			getBonusPointsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
//					getBonusPointsRequest.getLang()));
//		}

		// Dummy Data
		getBonusPointsResponse.setBonusPoints("3000");
		getBonusPointsResponse.setCallStatus(Constants.Call_Status_True);
		getBonusPointsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		getBonusPointsResponse.setResultDesc(
				GetMessagesMappings.getMessageFromResourceBundle("success", getBonusPointsRequest.getLang()));

		return getBonusPointsResponse;
	}

	public SaveBonusPointsResponse saveBonusPointsBusiness(String msisdn, SaveBonusPointsRequest saveBonusPointsRequest,
			SaveBonusPointsResponse saveBonusPointsResponse) throws JSONException, IOException, SQLException {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SAVE_BONUS_POINTS_TRANSACTION_NAME
				+ " BUSINESS with data-" + saveBonusPointsRequest.toString(), logger);

//		RestClient rc = new RestClient();
//		ObjectMapper mapper = new ObjectMapper();
//
//		String requestJsonESB = mapper.writeValueAsString(saveBonusPointsRequest);
//
//		String path = GetConfigurations.getESBRoute("saveBonusPoints");
//		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
//		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
//		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
//
//		saveBonusPointsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
//		String response = rc.getResponseFromESB(path, requestJsonESB);
//		saveBonusPointsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
//
//		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
//
//		// Logging ESB response code and description.
//		saveBonusPointsResponse.setLogsReport(
//				Utilities.logESBParamsintoReportLog(requestJsonESB, response, saveBonusPointsResponse.getLogsReport()));
//
//		if (response != null && !response.isEmpty()) {
//
//			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
//
//				saveBonusPointsResponse.setCallStatus(Constants.Call_Status_True);
//				saveBonusPointsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
//				saveBonusPointsResponse.setResultDesc(
//						GetMessagesMappings.getMessageFromResourceBundle("success", saveBonusPointsRequest.getLang()));
//
//			} else {
//				saveBonusPointsResponse.setCallStatus(Constants.Call_Status_False);
//				saveBonusPointsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
//				saveBonusPointsResponse.setResultDesc(Utilities.getErrorMessageFromDB("connectivity.error",
//						saveBonusPointsRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
//			}
//		} else {
//
//			saveBonusPointsResponse.setCallStatus(Constants.Call_Status_False);
//			saveBonusPointsResponse.setResultCode(Constants.API_FAILURE_CODE);
//			saveBonusPointsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
//					saveBonusPointsRequest.getLang()));
//		}

		// Dummy Data
		saveBonusPointsResponse.setCallStatus(Constants.Call_Status_True);
		saveBonusPointsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		saveBonusPointsResponse.setResultDesc(
				GetMessagesMappings.getMessageFromResourceBundle("success", saveBonusPointsRequest.getLang()));

		return saveBonusPointsResponse;
	}
}

package com.evampsaanga.bakcell.business;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.plasticcard.AddPaymentSchedulerRequest;
import com.evampsaanga.bakcell.models.plasticcard.AddPaymentSchedulerResponse;
import com.evampsaanga.bakcell.models.plasticcard.CardDetails;
import com.evampsaanga.bakcell.models.plasticcard.DeleteFastPaymentRequest;
import com.evampsaanga.bakcell.models.plasticcard.DeleteFastPaymentResponse;
import com.evampsaanga.bakcell.models.plasticcard.DeletePaymentSchedulerRequest;
import com.evampsaanga.bakcell.models.plasticcard.DeletePaymentSchedulerResponse;
import com.evampsaanga.bakcell.models.plasticcard.DeleteSavedCardRequest;
import com.evampsaanga.bakcell.models.plasticcard.DeleteSavedCardResponse;
import com.evampsaanga.bakcell.models.plasticcard.FastPaymentDetails;
import com.evampsaanga.bakcell.models.plasticcard.FastPaymentRequest;
import com.evampsaanga.bakcell.models.plasticcard.FastPaymentResponse;
import com.evampsaanga.bakcell.models.plasticcard.FastPaymentResponseData;
import com.evampsaanga.bakcell.models.plasticcard.MakePaymentRequest;
import com.evampsaanga.bakcell.models.plasticcard.MakePaymentResponse;
import com.evampsaanga.bakcell.models.plasticcard.MakePaymentResponseData;
import com.evampsaanga.bakcell.models.plasticcard.PaymentKeyRequest;
import com.evampsaanga.bakcell.models.plasticcard.PaymentKeyResponse;
import com.evampsaanga.bakcell.models.plasticcard.PaymentKeyResponseData;
import com.evampsaanga.bakcell.models.plasticcard.PaymentSchedulerData;
import com.evampsaanga.bakcell.models.plasticcard.SavedCardRequest;
import com.evampsaanga.bakcell.models.plasticcard.SavedCardResponse;
import com.evampsaanga.bakcell.models.plasticcard.SavedCardResponseData;
import com.evampsaanga.bakcell.models.plasticcard.ScheduledPaymentsRequest;
import com.evampsaanga.bakcell.models.plasticcard.ScheduledPaymentsResponse;
import com.evampsaanga.bakcell.models.plasticcard.ScheduledPaymentsResponseData;
import com.evampsaanga.bakcell.models.plasticcard.VerifyCardRequest;
import com.evampsaanga.bakcell.models.plasticcard.VerifyCardResponse;
import com.evampsaanga.bakcell.models.plasticcard.VerifyCardResponseData;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class TopUpBusiness {

	Logger logger = Logger.getLogger(TopUpBusiness.class);

	public SavedCardResponse getSavedCardsBusiness(String msisdn, String deviceID, SavedCardRequest savedCardRequest,
			SavedCardResponse savedCardResponse) throws JSONException, IOException, SQLException {

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_SAVED_CARDS_TRANSACTION_NAME
				+ " BUSINESS with data-" + savedCardRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(savedCardRequest);

		String path = GetConfigurations.getESBRoute("getSavedCards");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		savedCardResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		savedCardResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		savedCardResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, savedCardResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				List<CardDetails> resData = new ArrayList<>();
				resData = Arrays.asList(mapper.readValue(Utilities.getValueFromJSON(response, "singleCardDetails"),
						CardDetails[].class));
				SavedCardResponseData savedCardResponseData = new SavedCardResponseData();
				savedCardResponseData.setCardDetails(resData);
				savedCardResponseData.setLastAmount(Utilities.getValueFromJSON(response, "lastAmount"));
				savedCardResponse.setData(savedCardResponseData);
				savedCardResponse.setCallStatus(Constants.Call_Status_True);
				savedCardResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				savedCardResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", savedCardRequest.getLang()));

			} else {
				savedCardResponse.setCallStatus(Constants.Call_Status_False);
				savedCardResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				savedCardResponse.setResultDesc(Utilities.getErrorMessageFromDB("connectivity.error",
						savedCardRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			savedCardResponse.setCallStatus(Constants.Call_Status_False);
			savedCardResponse.setResultCode(Constants.API_FAILURE_CODE);
			savedCardResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					savedCardRequest.getLang()));
		}
		
		return savedCardResponse;

	}

	public FastPaymentResponse getFastPaymentBusiness(String msisdn, String deviceID,
			FastPaymentRequest fastPaymentRequest, FastPaymentResponse fastPaymentResponse)
			throws JSONException, IOException, SQLException {
		
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_SAVED_CARDS_TRANSACTION_NAME
				+ " BUSINESS with data-" + fastPaymentRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(fastPaymentRequest);

		String path = GetConfigurations.getESBRoute("getFastPayments");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		fastPaymentResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		fastPaymentResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		fastPaymentResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, fastPaymentResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				List<FastPaymentDetails> resData = new ArrayList<>();
				resData = Arrays.asList(mapper.readValue(Utilities.getValueFromJSON(response, "fastPaymentDetails"),
						FastPaymentDetails[].class));
				FastPaymentResponseData fastPaymentResponseData = new FastPaymentResponseData();
				fastPaymentResponseData.setFastPaymentDetails(resData);
				
				fastPaymentResponse.setData(fastPaymentResponseData);
				fastPaymentResponse.setCallStatus(Constants.Call_Status_True);
				fastPaymentResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				fastPaymentResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", fastPaymentRequest.getLang()));

			} else {
				fastPaymentResponse.setCallStatus(Constants.Call_Status_False);
				fastPaymentResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				fastPaymentResponse.setResultDesc(Utilities.getErrorMessageFromDB("connectivity.error",
						fastPaymentRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			fastPaymentResponse.setCallStatus(Constants.Call_Status_False);
			fastPaymentResponse.setResultCode(Constants.API_FAILURE_CODE);
			fastPaymentResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					fastPaymentRequest.getLang()));
		}

		
//		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.INITIATE_PAYMENT_TRANSACTION_NAME
//				+ " BUSINESS with data-" + fastPaymentRequest.toString(), logger);
//		
//		FastPaymentDetails dummyDataOne = new FastPaymentDetails();
//		dummyDataOne.setAmount("20.00");
//		dummyDataOne.setTopupNumber("558120973");
//		dummyDataOne.setCardType(Constants.PLASTIC_CARD_TYPE_MASTER);
//		dummyDataOne.setPaymentKey("123456");
//		
//		FastPaymentDetails dummyDataTwo = new FastPaymentDetails();
//		dummyDataTwo.setAmount("20.00");
//		dummyDataTwo.setTopupNumber("558120974");
//		dummyDataTwo.setCardType(Constants.PLASTIC_CARD_TYPE_VISA);
//		dummyDataTwo.setPaymentKey("123456");
//		
//		FastPaymentResponseData fastPaymentResponseData = new FastPaymentResponseData();
//		
//		fastPaymentResponseData.setFastPaymentDetails(Arrays.asList(
//				dummyDataOne, dummyDataTwo));
//		fastPaymentResponse.setData(fastPaymentResponseData);
//		
//		fastPaymentResponse.setCallStatus(Constants.Call_Status_True);
//		fastPaymentResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
//		fastPaymentResponse.setResultDesc(
//				GetMessagesMappings.getMessageFromResourceBundle("success", fastPaymentRequest.getLang()));
//		
//		Utilities.printDebugLog(msisdn + "-Received response-" + fastPaymentResponse, logger);
		
		return fastPaymentResponse;

	}

	public PaymentKeyResponse getPaymentKey(String msisdn, String deviceID, PaymentKeyRequest paymentKeyRequest,
			PaymentKeyResponse paymentKeyResponse) throws JSONException, IOException, SQLException {
		
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.INITIATE_PAYMENT_TRANSACTION_NAME
				+ " BUSINESS with data-" + paymentKeyRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(paymentKeyRequest);

		String path = GetConfigurations.getESBRoute("initiatePayment");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		paymentKeyResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		paymentKeyResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		paymentKeyResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, paymentKeyResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				PaymentKeyResponseData resData = new PaymentKeyResponseData();
				resData.setPaymentKey(Utilities.getValueFromJSON(response, "payment_key1"));

				paymentKeyResponse.setData(resData);
				paymentKeyResponse.setCallStatus(Constants.Call_Status_True);
				paymentKeyResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				paymentKeyResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", paymentKeyRequest.getLang()));

			} else {
				paymentKeyResponse.setCallStatus(Constants.Call_Status_False);
				paymentKeyResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				paymentKeyResponse.setResultDesc(Utilities.getErrorMessageFromDB("connectivity.error",
						paymentKeyRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			paymentKeyResponse.setCallStatus(Constants.Call_Status_False);
			paymentKeyResponse.setResultCode(Constants.API_FAILURE_CODE);
			paymentKeyResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					paymentKeyRequest.getLang()));
		}

		return paymentKeyResponse;

	}

	public VerifyCardResponse verifyCard(String msisdn, String deviceID, VerifyCardRequest verifyCardRequest,
			VerifyCardResponse verifyCardResponse) throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.VERIFY_CARD_TRANSACTION_NAME
				+ " BUSINESS with data-" + verifyCardRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(verifyCardRequest);

		String path = GetConfigurations.getESBRoute("verifyCard");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		verifyCardResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		verifyCardResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		verifyCardResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, verifyCardResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				VerifyCardResponseData resData = new VerifyCardResponseData();
				resData.setReturnCode(Utilities.getValueFromJSON(response, "returnCode"));

				verifyCardResponse.setData(resData);
				verifyCardResponse.setCallStatus(Constants.Call_Status_True);
				verifyCardResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				verifyCardResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", verifyCardRequest.getLang()));

			} else {
				verifyCardResponse.setCallStatus(Constants.Call_Status_False);
				verifyCardResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				verifyCardResponse.setResultDesc(Utilities.getErrorMessageFromDB("connectivity.error",
						verifyCardRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			verifyCardResponse.setCallStatus(Constants.Call_Status_False);
			verifyCardResponse.setResultCode(Constants.API_FAILURE_CODE);
			verifyCardResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					verifyCardRequest.getLang()));
		}

		return verifyCardResponse;

	}
	
	public MakePaymentResponse makePaymentBusiness(String msisdn, String deviceID, MakePaymentRequest makePaymentRequest,
			MakePaymentResponse makePaymentResponse) throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.VERIFY_CARD_TRANSACTION_NAME
				+ " BUSINESS with data-" + makePaymentRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(makePaymentRequest);

		String path = GetConfigurations.getESBRoute("makePayment");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		makePaymentResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		makePaymentResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		makePaymentResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, makePaymentResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				MakePaymentResponseData resData = new MakePaymentResponseData();
				resData.setResponseData(Utilities.getValueFromJSON(response, "returnCode"));

				makePaymentResponse.setData(resData);
				makePaymentResponse.setCallStatus(Constants.Call_Status_True);
				makePaymentResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				makePaymentResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", makePaymentRequest.getLang()));

			} else {
				makePaymentResponse.setCallStatus(Constants.Call_Status_False);
				makePaymentResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				makePaymentResponse.setResultDesc(Utilities.getErrorMessageFromDB("connectivity.error",
						makePaymentRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			makePaymentResponse.setCallStatus(Constants.Call_Status_False);
			makePaymentResponse.setResultCode(Constants.API_FAILURE_CODE);
			makePaymentResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					makePaymentRequest.getLang()));
		}

		
//		makePaymentResponse.setData(null);
//		makePaymentResponse.setCallStatus(Constants.Call_Status_True);
//		makePaymentResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
//		makePaymentResponse.setResultDesc(
//				GetMessagesMappings.getMessageFromResourceBundle("success", makePaymentRequest.getLang()));

		return makePaymentResponse;

	}

	public DeleteFastPaymentResponse deleteFastPaymentBusiness(String msisdn, String deviceID, DeleteFastPaymentRequest deletePaymentRequest,
			DeleteFastPaymentResponse deletePaymentResponse) throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.DELETE_PAYMENT_TRANSACTION_NAME
				+ " BUSINESS with data-" + deletePaymentRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(deletePaymentRequest);

		String path = GetConfigurations.getESBRoute("deletePayment");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		deletePaymentResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		deletePaymentResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		deletePaymentResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, deletePaymentResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				String resData = Utilities.getValueFromJSON(response, "returnCode");

//				deletePaymentResponse.setData(resData);
				deletePaymentResponse.setCallStatus(Constants.Call_Status_True);
				deletePaymentResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				deletePaymentResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", deletePaymentRequest.getLang()));

			} else {
				deletePaymentResponse.setCallStatus(Constants.Call_Status_False);
				deletePaymentResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				deletePaymentResponse.setResultDesc(Utilities.getErrorMessageFromDB("connectivity.error",
						deletePaymentRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			deletePaymentResponse.setCallStatus(Constants.Call_Status_False);
			deletePaymentResponse.setResultCode(Constants.API_FAILURE_CODE);
			deletePaymentResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					deletePaymentRequest.getLang()));
		}

		
//		deletePaymentResponse.setReturnCode(null);
//		deletePaymentResponse.setCallStatus(Constants.Call_Status_True);
//		deletePaymentResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
//		deletePaymentResponse.setResultDesc(
//				GetMessagesMappings.getMessageFromResourceBundle("success", deletePaymentRequest.getLang()));
//
		return deletePaymentResponse;

	}
	
	
	public DeleteSavedCardResponse deleteSavedCardBusiness(String msisdn, String deviceID, DeleteSavedCardRequest deleteSavedCardRequest,
			DeleteSavedCardResponse deleteSavedCardResponse) throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.DELETE_SAVED_CARD_TRANSACTION_NAME
				+ " BUSINESS with data-" + deleteSavedCardRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(deleteSavedCardRequest);

		String path = GetConfigurations.getESBRoute("deleteSavedCard");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		deleteSavedCardResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		deleteSavedCardResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		deleteSavedCardResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, deleteSavedCardResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				String resData = Utilities.getValueFromJSON(response, "returnCode");

//				deleteSavedCardResponse.setData(resData);
				deleteSavedCardResponse.setCallStatus(Constants.Call_Status_True);
				deleteSavedCardResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				deleteSavedCardResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", deleteSavedCardRequest.getLang()));

			} else {
				deleteSavedCardResponse.setCallStatus(Constants.Call_Status_False);
				deleteSavedCardResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				deleteSavedCardResponse.setResultDesc(Utilities.getErrorMessageFromDB("connectivity.error",
						deleteSavedCardRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			deleteSavedCardResponse.setCallStatus(Constants.Call_Status_False);
			deleteSavedCardResponse.setResultCode(Constants.API_FAILURE_CODE);
			deleteSavedCardResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					deleteSavedCardRequest.getLang()));
		}

		
//		deleteSavedCardResponse.setReturnCode(null);
//		deleteSavedCardResponse.setCallStatus(Constants.Call_Status_True);
//		deleteSavedCardResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
//		deleteSavedCardResponse.setResultDesc(
//				GetMessagesMappings.getMessageFromResourceBundle("success", deleteSavedCardRequest.getLang()));

		return deleteSavedCardResponse;

	}
	
	public AddPaymentSchedulerResponse addPaymentSchedulerBusiness(String msisdn, String deviceID, AddPaymentSchedulerRequest addPaymentSchedulerRequest,
			AddPaymentSchedulerResponse addPaymentSchedulerResponse) throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ADD_PAYMENT_SCHEDULER_TRANSACTION_NAME
				+ " BUSINESS with data-" + addPaymentSchedulerRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(addPaymentSchedulerRequest);

		String path = GetConfigurations.getESBRoute("addPaymentScheduler");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		addPaymentSchedulerResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		addPaymentSchedulerResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		addPaymentSchedulerResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, addPaymentSchedulerResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				String resData = Utilities.getValueFromJSON(response, "returnCode");

//				addPaymentSchedulerResponse.setResultCode(resData);
				addPaymentSchedulerResponse.setCallStatus(Constants.Call_Status_True);
				addPaymentSchedulerResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				addPaymentSchedulerResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", addPaymentSchedulerRequest.getLang()));

			} else {
				addPaymentSchedulerResponse.setCallStatus(Constants.Call_Status_False);
				addPaymentSchedulerResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				addPaymentSchedulerResponse.setResultDesc(Utilities.getErrorMessageFromDB("connectivity.error",
						addPaymentSchedulerRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			addPaymentSchedulerResponse.setCallStatus(Constants.Call_Status_False);
			addPaymentSchedulerResponse.setResultCode(Constants.API_FAILURE_CODE);
			addPaymentSchedulerResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					addPaymentSchedulerRequest.getLang()));
		}

//		addPaymentSchedulerResponse.setReturnCode(null);
//		addPaymentSchedulerResponse.setCallStatus(Constants.Call_Status_True);
//		addPaymentSchedulerResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
//		addPaymentSchedulerResponse.setResultDesc(
//				GetMessagesMappings.getMessageFromResourceBundle("success", addPaymentSchedulerRequest.getLang()));

		return addPaymentSchedulerResponse;

	}
	
	public DeletePaymentSchedulerResponse deletePaymentSchedulerBusiness(String msisdn, String deviceID, DeletePaymentSchedulerRequest deletePaymentSchedulerRequest,
			DeletePaymentSchedulerResponse deletePaymentSchedulerResponse) throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.DELETE_PAYMENT_SCHEDULER_TRANSACTION_NAME
				+ " BUSINESS with data-" + deletePaymentSchedulerRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(deletePaymentSchedulerRequest);

		String path = GetConfigurations.getESBRoute("deletePaymentScheduler");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		deletePaymentSchedulerResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		deletePaymentSchedulerResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		deletePaymentSchedulerResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, deletePaymentSchedulerResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				String resData = Utilities.getValueFromJSON(response, "returnCode");

				deletePaymentSchedulerResponse.setReturnCode(resData);
				deletePaymentSchedulerResponse.setCallStatus(Constants.Call_Status_True);
				deletePaymentSchedulerResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				deletePaymentSchedulerResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", deletePaymentSchedulerRequest.getLang()));

			} else {
				deletePaymentSchedulerResponse.setCallStatus(Constants.Call_Status_False);
				deletePaymentSchedulerResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				deletePaymentSchedulerResponse.setResultDesc(Utilities.getErrorMessageFromDB("connectivity.error",
						deletePaymentSchedulerRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			deletePaymentSchedulerResponse.setCallStatus(Constants.Call_Status_False);
			deletePaymentSchedulerResponse.setResultCode(Constants.API_FAILURE_CODE);
			deletePaymentSchedulerResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					deletePaymentSchedulerRequest.getLang()));
		}

//		deletePaymentSchedulerResponse.setReturnCode(null);
//		deletePaymentSchedulerResponse.setCallStatus(Constants.Call_Status_True);
//		deletePaymentSchedulerResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
//		deletePaymentSchedulerResponse.setResultDesc(
//				GetMessagesMappings.getMessageFromResourceBundle("success", deletePaymentSchedulerRequest.getLang()));
//
		return deletePaymentSchedulerResponse;

	}

	
	public ScheduledPaymentsResponse getScheduledPaymentsBusiness(String msisdn, String deviceID, ScheduledPaymentsRequest scheduledPaymentsRequest,
			ScheduledPaymentsResponse scheduledPaymentsResponse) throws JSONException, IOException, SQLException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_SCHEDULED_PAYMENTS_TRANSACTION_NAME
				+ " BUSINESS with data-" + scheduledPaymentsRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		String requestJsonESB = mapper.writeValueAsString(scheduledPaymentsRequest);

		String path = GetConfigurations.getESBRoute("getScheduledPayments");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< -Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		scheduledPaymentsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		scheduledPaymentsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		scheduledPaymentsResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, scheduledPaymentsResponse.getLogsReport()));

		if (response != null && !response.isEmpty()) {

			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				ScheduledPaymentsResponseData data = new ScheduledPaymentsResponseData();
				List<PaymentSchedulerData> resData = new ArrayList<>();
				resData = Arrays.asList(mapper.readValue(Utilities.getValueFromJSON(response, "scheduledPaymentDetailsList"),
						PaymentSchedulerData[].class));
				
				data.setData(resData);
				
				scheduledPaymentsResponse.setData(data);
				scheduledPaymentsResponse.setCallStatus(Constants.Call_Status_True);
				scheduledPaymentsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				scheduledPaymentsResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", scheduledPaymentsRequest.getLang()));

			} else {
				scheduledPaymentsResponse.setCallStatus(Constants.Call_Status_False);
				scheduledPaymentsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				scheduledPaymentsResponse.setResultDesc(Utilities.getErrorMessageFromDB("connectivity.error",
						scheduledPaymentsRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
			}
		} else {

			scheduledPaymentsResponse.setCallStatus(Constants.Call_Status_False);
			scheduledPaymentsResponse.setResultCode(Constants.API_FAILURE_CODE);
			scheduledPaymentsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					scheduledPaymentsRequest.getLang()));
		}

//		ScheduledPaymentsResponseData resData = new ScheduledPaymentsResponseData();
//		
//		PaymentSchedulerData dummyData = new PaymentSchedulerData();
//		dummyData.setAmount("3");
//		dummyData.setBillingCycle(Constants.MONTHLY_SCHEDULER);
//		dummyData.setRecurrenceNumber("1234455");
//		dummyData.setSavedCardId("1");
//		dummyData.setStartDate("11-12-2020");
//		
//		resData.setData(Arrays.asList(dummyData));
//		
//		scheduledPaymentsResponse.setData(resData);
//		scheduledPaymentsResponse.setCallStatus(Constants.Call_Status_True);
//		scheduledPaymentsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
//		scheduledPaymentsResponse.setResultDesc(
//				GetMessagesMappings.getMessageFromResourceBundle("success", scheduledPaymentsRequest.getLang()));

		return scheduledPaymentsResponse;

	}
}

/**
 * 
 */
package com.evampsaanga.bakcell.business;

import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.coreservices.getcoreservices.CoreServicesBaseClass;
import com.evampsaanga.bakcell.models.coreservices.getcoreservices.CoreServicesCategoryItem;
import com.evampsaanga.bakcell.models.coreservices.getcoreservices.GetCoreServicesRequest;
import com.evampsaanga.bakcell.models.coreservices.getcoreservices.GetCoreServicesResponse;
import com.evampsaanga.bakcell.models.coreservices.getcoreservices.GetCoreServicesResponseData;
import com.evampsaanga.bakcell.models.coreservices.processcoreservices.ProcessCoreServicesRequest;
import com.evampsaanga.bakcell.models.coreservices.processcoreservices.ProcessCoreServicesRequestV2;
import com.evampsaanga.bakcell.models.coreservices.processcoreservices.ProcessCoreServicesResponse;
import com.evampsaanga.bakcell.models.coreservices.processcoreservices.ProcessCoreServicesResponseData;
import com.evampsaanga.bakcell.models.quickservices.sendfreesms.SendFreeSMSResponseDataV2;
import com.evampsaanga.bakcell.models.quickservices.sendfreesms.SendFreeSMSResponseV2;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & saanga
 *
 */
@Service
public class CoreServicesBusiness {
    Logger logger = Logger.getLogger(CoreServicesBusiness.class);

    public ProcessCoreServicesResponse processCoreServicesBusiness(String msisdn,
	    ProcessCoreServicesRequest processCoreServicesRequest,
	    ProcessCoreServicesResponse processCoreServicesResponse) throws JSONException, IOException, SQLException {
	Utilities
		.printDebugLog(msisdn + "-Request received in " + Transactions.MANIPULATE_CORE_SERVICES_TRANSACTION_NAME
			+ " BUSINESS with data-" + processCoreServicesRequest.toString(), logger);

	ProcessCoreServicesResponseData resData = new ProcessCoreServicesResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	if (processCoreServicesRequest.getAccountType().contains(" ")) {
	    processCoreServicesRequest.setAccountType(processCoreServicesRequest.getAccountType().split(" ")[0]);
	}
	String requestJsonESB = mapper.writeValueAsString(processCoreServicesRequest);

	String path = GetConfigurations.getESBRoute("processCoreServices");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	processCoreServicesResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	processCoreServicesResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	processCoreServicesResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		processCoreServicesResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		resData.setMessage(Utilities.getValueFromJSON(response, "returnMsg"));

		processCoreServicesResponse.setData(resData);
		processCoreServicesResponse.setCallStatus(Constants.Call_Status_True);
		processCoreServicesResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		processCoreServicesResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			processCoreServicesRequest.getLang()));

	    } else if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase("1681684299")) {

			resData.setMessage(Utilities.getValueFromJSON(response, "returnMsg"));
			processCoreServicesResponse.setData(resData);
			processCoreServicesResponse.setCallStatus(Constants.Call_Status_True);
			processCoreServicesResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
			if (processCoreServicesRequest.getActionType().equalsIgnoreCase("3")) {
				Utilities.printDebugLog(msisdn + "-PAYG Deactivated", logger);
				processCoreServicesResponse.setResultDesc(GetMessagesMappings
						.getMessageFromResourceBundle("payg.deactivate", processCoreServicesRequest.getLang()));
			} else {
				Utilities.printDebugLog(msisdn + "-PAYG Activated", logger);
				processCoreServicesResponse.setResultDesc(GetMessagesMappings
						.getMessageFromResourceBundle("payg.activate", processCoreServicesRequest.getLang()));
			}

		} else {

		processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
		processCoreServicesResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		processCoreServicesResponse
			.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
				Utilities.getValueFromJSON(response, "returnCode") + "."
					+ processCoreServicesRequest.getActionType(),
				processCoreServicesRequest.getLang()));
	    }
	} else {

	    processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
	    processCoreServicesResponse.setResultCode(Constants.API_FAILURE_CODE);
	    processCoreServicesResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", processCoreServicesRequest.getLang()));
	}

	return processCoreServicesResponse;
    }

    /*
     * public static void main(String[] args) throws JsonParseException,
     * JsonMappingException, IOException, JSONException { ObjectMapper mapper = new
     * ObjectMapper();; String
     * resp="{\"returnMsg\":\"Sucessfull\",\"returnCode\":\"200\",\"data\":{\"coreServices\":[{\"coreServiceCategory\":\"Forwarded to\",\"coreServicesList\":[{\"id\":1,\"name\":\"Forward to:\",\"description\":\"Forward all incoming calls\",\"categoryId\":1,\"price\":null,\"validity\":\"Daily\",\"offeringId\":\"165811270\",\"status\":\"Suspended\",\"storeId\":\"3\",\"sortOrder\":1,\"forwardNumber\":\"\",\"renewable\":0,\"freeFor\":\"Postpaid-Business-Individual\"}]},{\"coreServiceCategory\":\"I'm back\",\"coreServicesList\":[{\"id\":2,\"name\":null,\"description\":\"Notify callers who tried to reach you that you are back to network.\",\"categoryId\":2,\"price\":null,\"validity\":\"Monthly\",\"offeringId\":\"102341307\",\"status\":\"In active\",\"storeId\":\"3\",\"sortOrder\":0,\"forwardNumber\":\"\",\"renewable\":1,\"freeFor\":\"\"}]},{\"coreServiceCategory\":\"I Called You\",\"coreServicesList\":[{\"id\":3,\"name\":\"Turned off\",\"description\":\"Get SMS about missed calls when your number was turned off\",\"categoryId\":3,\"price\":null,\"validity\":null,\"offeringId\":\"984855324\",\"status\":\"In active\",\"storeId\":\"3\",\"sortOrder\":0,\"forwardNumber\":\"\",\"renewable\":0,\"freeFor\":\"\"},{\"id\":4,\"name\":\"Busy\",\"description\":\"Get SMS about missed calls when your number was busy.\",\"categoryId\":3,\"price\":null,\"validity\":null,\"offeringId\":\"1302341086\",\"status\":\"In active\",\"storeId\":\"3\",\"sortOrder\":0,\"forwardNumber\":\"\",\"renewable\":0,\"freeFor\":\"\"}]},{\"coreServiceCategory\":\"Testing\",\"coreServicesList\":[{\"id\":6,\"name\":\"Testing\",\"description\":\"Testing the core services\",\"categoryId\":5,\"price\":null,\"validity\":null,\"offeringId\":\"12121212121\",\"status\":\"In active\",\"storeId\":\"3\",\"sortOrder\":0,\"forwardNumber\":\"\",\"renewable\":0,\"freeFor\":\"\"}]}]}}"
     * ; //String Test = mapper.readValue(Utilities.getValueFromJSON(new
     * JSONObject(resp), "data"),GetCoreServicesResponseData.class);
     * 
     * String testtttt=Utilities.getValueFromJSON(resp, "data");
     * System.out.println(testtttt);
     * 
     * GetCoreServicesResponseData resData =
     * mapper.readValue(testtttt,GetCoreServicesResponseData.class); }
     */

    public GetCoreServicesResponse getCoreServicesBusiness(String msisdn, GetCoreServicesRequest getCoreServicesRequest,
	    GetCoreServicesResponse getCoreServicesResponse, String lang)
	    throws JSONException, IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_CORE_SERVICES_TRANSACTION_NAME
		+ " BUSINESS with data-" + getCoreServicesRequest.toString(), logger);

	GetCoreServicesResponseData resData = new GetCoreServicesResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	if (getCoreServicesRequest.getAccountType().contains(" ")) {
	    getCoreServicesRequest.setAccountType(getCoreServicesRequest.getAccountType().split(" ")[0]);
	}

	String path = "";
	if (msisdn.contains("bulk")) {
	    path = GetConfigurations.getESBRoute("getCoreServicesbulk");
	} else if (getCoreServicesRequest.getIsFrom().equalsIgnoreCase("B2B")
		|| getCoreServicesRequest.getIsB2B().equalsIgnoreCase("true")) {
	    path = GetConfigurations.getESBRoute("getCoreServices");
	    getCoreServicesRequest.setIsFrom("B2B");
	} else {
	    path = GetConfigurations.getESBRoute("getCoreServices");
	    getCoreServicesRequest.setIsFrom("B2C");
	}
	String requestJsonESB = mapper.writeValueAsString(getCoreServicesRequest);
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	getCoreServicesResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	// String response = HardCodedResponses.GET_CORE_SERVICES;

	getCoreServicesResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
	// Logging ESB response code and description.
	getCoreServicesResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, getCoreServicesResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		resData = mapper.readValue(Utilities.getValueFromJSON(response, "data"),
			GetCoreServicesResponseData.class);
		// Setting coreservices labels.
		setLabels(resData, lang);
		getCoreServicesResponse.setData(resData);
		getCoreServicesResponse.setCallStatus(Constants.Call_Status_True);
		getCoreServicesResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		getCoreServicesResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", getCoreServicesRequest.getLang()));

	    } else {

		getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
		getCoreServicesResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		getCoreServicesResponse.setResultDesc(Utilities.getErrorMessageFromDB("core.services",
			getCoreServicesRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    getCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
	    getCoreServicesResponse.setResultCode(Constants.API_FAILURE_CODE);
	    getCoreServicesResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    getCoreServicesRequest.getLang()));
	}

	return getCoreServicesResponse;
    }

    private GetCoreServicesResponseData setLabels(GetCoreServicesResponseData resData, String lang)
	    throws SocketException, SQLException {
	if (resData != null) {
	    if (resData.getCoreServices() != null && resData.getCoreServices().size() > 0) {
		for (CoreServicesBaseClass coreServiceBase : resData.getCoreServices()) {
		    if (coreServiceBase != null && coreServiceBase.getCoreServicesList() != null
			    && coreServiceBase.getCoreServicesList().size() > 0) {
			for (CoreServicesCategoryItem coreServicesCategoryItem : coreServiceBase
				.getCoreServicesList()) {

			    if (coreServicesCategoryItem.getRenewable() != null
				    && coreServicesCategoryItem.getRenewable().equals(1)) {
				coreServicesCategoryItem.setValidityLabel(
					GetMessagesMappings.getLabelsFromResourceBundle("validityLabel.renew", lang));
				coreServicesCategoryItem.setProgressTitle(GetMessagesMappings
					.getLabelsFromResourceBundle("progressTitleLabel.renew", lang));
				coreServicesCategoryItem.setProgressDateLabel(GetMessagesMappings
					.getLabelsFromResourceBundle("progressDateLabel.renew", lang));
			    } else {
				coreServicesCategoryItem.setValidityLabel(
					GetMessagesMappings.getLabelsFromResourceBundle("validityLabel.expire", lang));
				coreServicesCategoryItem.setProgressTitle(GetMessagesMappings
					.getLabelsFromResourceBundle("progressTitleLabel.expire", lang));
				coreServicesCategoryItem.setProgressDateLabel(GetMessagesMappings
					.getLabelsFromResourceBundle("progressDateLabel.expire", lang));
			    }
			}
		    }
		}
	    }
	}
	return resData;
    }

    public SendFreeSMSResponseV2 processCoreServicesBusiness(String msisdn,
	    ProcessCoreServicesRequestV2 processCoreServicesRequest, SendFreeSMSResponseV2 processCoreServicesResponse)
	    throws SocketException, JSONException, SQLException, JsonProcessingException {

	Utilities.printDebugLog(
		msisdn + "-Request received in " + Transactions.MANIPULATE_BULK_CORE_SERVICES_TRANSACTION_NAME
			+ " BUSINESS with data-" + processCoreServicesRequest.toString(),
		logger);
	SendFreeSMSResponseDataV2 resData = new SendFreeSMSResponseDataV2();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(processCoreServicesRequest);

	String path = GetConfigurations.getESBRoute("insertorders");
	Utilities.printDebugLog(msisdn + "<<<<<<<<< Request Call to ESB >>>>>>>>>", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	processCoreServicesResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	processCoreServicesResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	processCoreServicesResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		processCoreServicesResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		/*
		 * resData = mapper.readValue(Utilities.getValueFromJSON(response,
		 * "responseMsg"), SendFreeSMSResponseDataV2.class);
		 */
		resData.setResponseMsg(GetMessagesMappings.getMessageFromResourceBundle("success",
			processCoreServicesRequest.getLang()));
		processCoreServicesResponse.setData(resData);
		processCoreServicesResponse.setCallStatus(Constants.Call_Status_True);
		processCoreServicesResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		processCoreServicesResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			processCoreServicesRequest.getLang()));

	    } else {
		processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
		processCoreServicesResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		processCoreServicesResponse.setResultDesc(Utilities.getErrorMessageFromDB("free.sms",
			processCoreServicesRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    processCoreServicesResponse.setCallStatus(Constants.Call_Status_False);
	    processCoreServicesResponse.setResultCode(Constants.API_FAILURE_CODE);
	    processCoreServicesResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", processCoreServicesRequest.getLang()));
	}

	return processCoreServicesResponse;
    }

}
package com.evampsaanga.bakcell.business;

import java.net.SocketException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.actionhistory.ActionHistoryOrderList;
import com.evampsaanga.bakcell.models.actionhistory.ActionHistoryRequest;
import com.evampsaanga.bakcell.models.actionhistory.ActionHistoryResponse;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 * 
 */
@Service
public class ActionHistoryBusiness {
    Logger logger = Logger.getLogger(AppMenuBusiness.class);

    public ActionHistoryResponse getActionHistory(String msisdn, ActionHistoryRequest request,
	    ActionHistoryResponse actionHistoryResponse)
	    throws SocketException, JsonProcessingException, JSONException, SQLException {
	ObjectMapper mapper = new ObjectMapper();
	String requestJsonESB = mapper.writeValueAsString(request);

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ACTION_HISTORY_TRANSACTION_NAME
		+ " BUSINESS with data-" + requestJsonESB, logger);

	RestClient rc = new RestClient();
	ActionHistoryOrderList orderList = new ActionHistoryOrderList();

	String path = GetConfigurations.getESBRoute("actionhistory");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	actionHistoryResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	actionHistoryResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	actionHistoryResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, actionHistoryResponse.getLogsReport()));
	if (response != null && !response.isEmpty()) {
	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		String responseData = Utilities.removeParamsFromJSONObject(response, "returnMsg"); // new
												   // JSONObject(response).get("orderList").toString();
		responseData = Utilities.removeParamsFromJSONObject(responseData, "returnCode");
		logger.info("*******************************************************************");
		logger.info("********************** responseData ***************" + responseData);
		logger.info("===================================================================");
		try {

		    orderList = mapper.readValue(responseData, ActionHistoryOrderList.class);
		    orderList = setLabels(orderList, request.getLang());
		    actionHistoryResponse.setData(orderList);

		    actionHistoryResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    actionHistoryResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", request.getLang()));
		    actionHistoryResponse.setCallStatus(Constants.Call_Status_True);
		    return actionHistoryResponse;
		} catch (Exception e) {
		    logger.error("ERROR:", e);
		    actionHistoryResponse.setCallStatus(Constants.Call_Status_False);
		    actionHistoryResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    actionHistoryResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		    return actionHistoryResponse;
		}

	    } else {
		actionHistoryResponse.setCallStatus(Constants.Call_Status_False);
		actionHistoryResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		actionHistoryResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
		return actionHistoryResponse;
	    }
	} else {
	    actionHistoryResponse.setCallStatus(Constants.Call_Status_False);
	    actionHistoryResponse.setResultCode(Constants.API_FAILURE_CODE);
	    actionHistoryResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", request.getLang()));
	    return actionHistoryResponse;
	}
    }

    private ActionHistoryOrderList setLabels(ActionHistoryOrderList actionHistoryOrderList, String lang)
	    throws SocketException, SQLException {
	if (actionHistoryOrderList != null && actionHistoryOrderList.getOrderList() != null
		&& actionHistoryOrderList.getOrderList().size() > 0) {
	    for (int i = 0; i < actionHistoryOrderList.getOrderList().size(); i++) {
		actionHistoryOrderList.getOrderList().get(i)
			.setOrderType(GetMessagesMappings.getLabelsFromResourceBundle("ordertype."
				+ actionHistoryOrderList.getOrderList().get(i).getOrderType().replaceAll(" ", ""),
				lang));
		actionHistoryOrderList.getOrderList().get(i)
			.setOrderStatus(GetMessagesMappings.getLabelsFromResourceBundle(
				"orderstatus." + actionHistoryOrderList.getOrderList().get(i).getOrderStatus(), lang));

	    }
	}
	return actionHistoryOrderList;
    }

}
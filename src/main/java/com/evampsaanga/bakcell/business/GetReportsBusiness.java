package com.evampsaanga.bakcell.business;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.getreports.GetReportsRequest;
import com.evampsaanga.bakcell.models.getreports.GetReportsResponse;
import com.evampsaanga.bakcell.models.getreports.ReportData;

@Service
public class GetReportsBusiness {
	Logger logger = Logger.getLogger(GetReportsBusiness.class);

	String reportpath = "/opt/tomcat/reports";

	public GetReportsResponse getReports(GetReportsRequest getReportsRequest, GetReportsResponse getReportsResponse)
			throws IOException {
		Utilities.printDebugLog("-Request received in GET REPORTS BUSINESS with data-" + getReportsRequest.toString(),
				logger);

		File file = new File("/opt/tomcat/reports");

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File f, String name) {
				return name.endsWith(".csv");
			}
		};
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		List<ReportData> reportDataList = new ArrayList<>();

		for (String filename : file.list(filter)) {
			ReportData reportData = new ReportData();

			String pathString = "/opt/tomcat/reports/" + filename;

			File csvFile = new File(pathString);

			if (csvFile.isDirectory()) {
				reportData.setLastModified(sdf.format(csvFile.lastModified()));
				reportData.setFileName(filename);
				byte[] fileContent = Files.readAllBytes(csvFile.toPath());
				reportData.setFile(Base64.getEncoder().encodeToString(fileContent));
				reportDataList.add(reportData);
			}

		}

		getReportsResponse.setReportData(reportDataList);
		getReportsResponse.setCallStatus(Constants.Call_Status_True);
		getReportsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		getReportsResponse.setResultDesc("successful");
		Utilities.printDebugLog("Response Returned from GET REPORTS BUSINESS -" + getReportsResponse.toString(),
				logger);

		return getReportsResponse;
	}

}

package com.evampsaanga.bakcell.business;

import java.net.SocketException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.ulduzumV2.UlduzumRequest;
import com.evampsaanga.bakcell.models.ulduzumV2.getCodes.GetCodesData;
import com.evampsaanga.bakcell.models.ulduzumV2.getCodes.GetCodesResponse;
import com.evampsaanga.bakcell.models.ulduzumV2.getMerchantDetails.GetMerchantDeatilEsbResponse;
import com.evampsaanga.bakcell.models.ulduzumV2.getMerchantDetails.GetMerchantDetailRequest;
import com.evampsaanga.bakcell.models.ulduzumV2.getMerchantDetails.GetMerchantDetailResponse;
import com.evampsaanga.bakcell.models.ulduzumV2.getMerchantDetails.MerchantDeatilData;
import com.evampsaanga.bakcell.models.ulduzumV2.getMerchants.Category;
import com.evampsaanga.bakcell.models.ulduzumV2.getMerchants.Data;
import com.evampsaanga.bakcell.models.ulduzumV2.getMerchants.GetMerchantsEsbResponse;
import com.evampsaanga.bakcell.models.ulduzumV2.getMerchants.GetMerchantsRequest;
import com.evampsaanga.bakcell.models.ulduzumV2.getMerchants.GetMerchantsResponse;
import com.evampsaanga.bakcell.models.ulduzumV2.getMerchants.Merchant;
import com.evampsaanga.bakcell.models.ulduzumV2.getUnusedCodes.GetUnsedData;
import com.evampsaanga.bakcell.models.ulduzumV2.getUnusedCodes.GetUnusedCodesEsbResponse;
import com.evampsaanga.bakcell.models.ulduzumV2.getUnusedCodes.GetUnusedCodesResponse;
import com.evampsaanga.bakcell.models.ulduzumV2.getUsageHistory.DataGetUsageHistory;
import com.evampsaanga.bakcell.models.ulduzumV2.getUsageHistory.GetUsageHistoryEsbResponse;
import com.evampsaanga.bakcell.models.ulduzumV2.getUsageHistory.GetUsageHistoryRequest;
import com.evampsaanga.bakcell.models.ulduzumV2.getUsageHistory.GetUsageHistoryResponse;
import com.evampsaanga.bakcell.models.ulduzumV2.getUsageTotals.DataUsageTotals;
import com.evampsaanga.bakcell.models.ulduzumV2.getUsageTotals.GetUsageTotalsEsbResponse;
import com.evampsaanga.bakcell.models.ulduzumV2.getUsageTotals.GetUsageTotalsResponse;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class UlduzumBusinessV2 {
	Logger logger = Logger.getLogger(UlduzumBusinessV2.class);

	// Start of getMerchant API, business

	public GetMerchantsResponse getMerchants(String msisdn, GetMerchantsRequest request,
			GetMerchantsResponse getMerchantsResponse)
			throws SocketException, JsonProcessingException, JSONException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		String requestJsonESB = mapper.writeValueAsString(request);

		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ULDUZUM_GET_MERCHANTS_TRANSACTION_NAME
				+ " BUSINESS with data-" + requestJsonESB, logger);

		RestClient rc = new RestClient();
		GetMerchantsEsbResponse getMerchantsEsbResponse = new GetMerchantsEsbResponse();

		String path = GetConfigurations.getESBRoute("getmerchantsv2");

		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		getMerchantsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		getMerchantsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		getMerchantsResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, getMerchantsResponse.getLogsReport()));
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				getMerchantsEsbResponse = mapper.readValue(response, GetMerchantsEsbResponse.class);
				Data data = new Data();

				data.setCategories(filterCategories(getMerchantsEsbResponse.getData()));
				data.setData(getMerchantsEsbResponse.getData());
				getMerchantsResponse.setData(data);
				getMerchantsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				getMerchantsResponse
						.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", request.getLang()));
				getMerchantsResponse.setCallStatus(Constants.Call_Status_True);
				return getMerchantsResponse;

			} else {
				getMerchantsResponse.setCallStatus(Constants.Call_Status_False);
				getMerchantsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				getMerchantsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
						Utilities.getValueFromJSON(response, "ulduzum.failure."), request.getLang()));
				return getMerchantsResponse;
			}
		} else {
			getMerchantsResponse.setCallStatus(Constants.Call_Status_False);
			getMerchantsResponse.setResultCode(Constants.API_FAILURE_CODE);
			getMerchantsResponse.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", request.getLang()));
			return getMerchantsResponse;
		}
	}

	private List<Category> filterCategories(List<Merchant> data) {
		List<Category> categories = new ArrayList<Category>();
		if (data != null && data.size() > 0) {
			data.stream().forEach(p -> {
				int categoryId = p.getCategory_id();
				String categoryName = p.getCategory_name();
				Category category = new Category();
				category.setCategory_id(categoryId);
				category.setCategory_name(categoryName);
				categories.add(category);
			});
		}
		return categories.stream().filter(distinctByKey(p -> p.getCategory_id())).collect(Collectors.toList());
	}

	private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {

		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

	public GetMerchantDetailResponse getMerchantDetails(String msisdn, GetMerchantDetailRequest request,
			GetMerchantDetailResponse getMerchantDetailResponse)
			throws SocketException, JsonProcessingException, JSONException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		String requestJsonESB = mapper.writeValueAsString(request);

		Utilities.printDebugLog(
				msisdn + "-Request received in " + Transactions.ULDUZUM_GET_MERCHANT_DETAIL_TRANSACTION_NAME
				+ " BUSINESS with data-" + requestJsonESB, logger);

		RestClient rc = new RestClient();
		GetMerchantDeatilEsbResponse getMerchantDeatilEsbResponse = new GetMerchantDeatilEsbResponse();

		String path = GetConfigurations.getESBRoute("getmerchantdetail");// ESB Route Key for getMerchantDetail

		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		getMerchantDetailResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		getMerchantDetailResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		getMerchantDetailResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
				getMerchantDetailResponse.getLogsReport()));
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				getMerchantDeatilEsbResponse = mapper.readValue(response, GetMerchantDeatilEsbResponse.class);
				MerchantDeatilData data = new MerchantDeatilData();
				data.setData(getMerchantDeatilEsbResponse.getData());
				getMerchantDetailResponse.setData(data);
				getMerchantDetailResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				getMerchantDetailResponse
						.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", request.getLang()));
				getMerchantDetailResponse.setCallStatus(Constants.Call_Status_True);
				return getMerchantDetailResponse;

			} else {
				getMerchantDetailResponse.setCallStatus(Constants.Call_Status_False);
				getMerchantDetailResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				getMerchantDetailResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
						Utilities.getValueFromJSON(response, "ulduzum.failure."), request.getLang()));
				return getMerchantDetailResponse;
			}
		} else {
			getMerchantDetailResponse.setCallStatus(Constants.Call_Status_False);
			getMerchantDetailResponse.setResultCode(Constants.API_FAILURE_CODE);
			getMerchantDetailResponse.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", request.getLang()));
			return getMerchantDetailResponse;
		}
	}

	public GetCodesResponse getCodes(String msisdn, UlduzumRequest request, GetCodesResponse getMerchantsResponse)
			throws SocketException, JsonProcessingException, JSONException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		String requestJsonESB = mapper.writeValueAsString(request);

		Utilities.printDebugLog(msisdn + "-Request received in "
				+ Transactions.ULDUZUM_GET_UNUSED_CODES_TRANSACTION_NAME + " BUSINESS with data-" + requestJsonESB,
				logger);

		RestClient rc = new RestClient();
//		GetMerchantsEsbResponse getMerchantsEsbResponse = new GetMerchantsEsbResponse();

		String path = GetConfigurations.getESBRoute("getcodes");

		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		getMerchantsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
//		String response = "{}";
		getMerchantsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		getMerchantsResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, getMerchantsResponse.getLogsReport()));
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
//				getMerchantsEsbResponse = mapper.readValue(response, GetMerchantsEsbResponse.class);
				GetCodesData data = new GetCodesData();
				data.setCode(Utilities.getValueFromJSON(Utilities.getValueFromJSON(response,"data"), "code"));// 10001
//				data.setCode("10001");
				getMerchantsResponse.setData(data);
				getMerchantsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				getMerchantsResponse
						.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", request.getLang()));
				getMerchantsResponse.setCallStatus(Constants.Call_Status_True);
				return getMerchantsResponse;

			} else {
				getMerchantsResponse.setCallStatus(Constants.Call_Status_False);
				getMerchantsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				getMerchantsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("ulduzum."+
						Utilities.getValueFromJSON(response, "returnCode"), request.getLang()));
				return getMerchantsResponse;
			}
		} else {
			getMerchantsResponse.setCallStatus(Constants.Call_Status_False);
			getMerchantsResponse.setResultCode(Constants.API_FAILURE_CODE);
			getMerchantsResponse.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", request.getLang()));
			return getMerchantsResponse;
		}
	}

	public GetUsageTotalsResponse getUsageTotals(String msisdn, UlduzumRequest request,
			GetUsageTotalsResponse getUsageTotalsResponse)
			throws SocketException, JsonProcessingException, JSONException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		String requestJsonESB = mapper.writeValueAsString(request);

		Utilities.printDebugLog(msisdn + "-Request received in "
				+ Transactions.ULDUZUM_GET_UNUSED_CODES_TRANSACTION_NAME + " BUSINESS with data-" + requestJsonESB,
				logger);

		RestClient rc = new RestClient();
		GetUsageTotalsEsbResponse getUsageTotalsEsbResponse = new GetUsageTotalsEsbResponse();

		String path = GetConfigurations.getESBRoute("getusagetotals");

		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		getUsageTotalsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
//		String response = "{}";
		getUsageTotalsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		getUsageTotalsResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, getUsageTotalsResponse.getLogsReport()));
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				getUsageTotalsEsbResponse = mapper.readValue(response, GetUsageTotalsEsbResponse.class);
				getUsageTotalsResponse.setData(getUsageTotalsEsbResponse.getData());
				
				/*
				 * DataUsageTotals t= new DataUsageTotals();
				 * t.setSegmentType("Ulduzum Premium"); t.setLastUnusedCode("10001");
				 * t.setDailyLimitLeft("9"); t.setTotalRedemptions("225");
				 * t.setTotalAmount("4212.75"); t.setTotalDiscounted("3477.26");
				 * t.setTotalDiscount("735.49"); t.setDailyLimitTotal(10);
				 * getUsageTotalsResponse.setData(t);
				 */
				
				
				getUsageTotalsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				getUsageTotalsResponse
						.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", request.getLang()));
				getUsageTotalsResponse.setCallStatus(Constants.Call_Status_True);
				return getUsageTotalsResponse;

			} else {
				getUsageTotalsResponse.setCallStatus(Constants.Call_Status_False);
				getUsageTotalsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				getUsageTotalsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
						Utilities.getValueFromJSON(response, "ulduzum.failure."), request.getLang()));
				return getUsageTotalsResponse;
			}
		} else {
			getUsageTotalsResponse.setCallStatus(Constants.Call_Status_False);
			getUsageTotalsResponse.setResultCode(Constants.API_FAILURE_CODE);
			getUsageTotalsResponse.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", request.getLang()));
			return getUsageTotalsResponse;
		}
	}

	public GetUsageHistoryResponse getUsageHistory(String msisdn, GetUsageHistoryRequest request,
			GetUsageHistoryResponse getUsageHistoryResponse)
			throws SocketException, JsonProcessingException, JSONException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		String requestJsonESB = mapper.writeValueAsString(request);

		Utilities.printDebugLog(msisdn + "-Request received in "
				+ Transactions.ULDUZUM_GET_UNUSED_CODES_TRANSACTION_NAME + " BUSINESS with data-" + requestJsonESB,
				logger);

		RestClient rc = new RestClient();
		GetUsageHistoryEsbResponse getUsageTotalsEsbResponse = new GetUsageHistoryEsbResponse();

		String path = GetConfigurations.getESBRoute("getusagehistory");

		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		getUsageHistoryResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
//		String response = "{}";
		getUsageHistoryResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		getUsageHistoryResponse.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, getUsageHistoryResponse.getLogsReport()));
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				getUsageTotalsEsbResponse = mapper.readValue(response, GetUsageHistoryEsbResponse.class);

				getUsageHistoryResponse.getData().setUsageHistoryList(getUsageTotalsEsbResponse.getData());

				/*
				 * List<DataGetUsageHistory> l = new ArrayList<DataGetUsageHistory>();
				 * DataGetUsageHistory h = new DataGetUsageHistory(); h.setId("3403172");
				 * h.setMerchantId("12046"); h.setCategoryName("TV v  onlayn xidm tl r");
				 * h.setCategoryId("22"); h.setIdenticalCode("4196");
				 * h.setRedemptionDate("2019­11­12"); h.setRedemptionTime("12:47:18");
				 * h.setAmount("1"); h.setDiscountAmount("0"); h.setDiscountedAmount("1");
				 * h.setDiscountPercent("1"); h.setPaymentType("CASH"); l.add(h);
				 * GetUsageHistoryEsbResponse u = new GetUsageHistoryEsbResponse();
				 * u.setData(l); getUsageHistoryResponse.getData().setUsageHistoryList(l);
				 */

				getUsageHistoryResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				getUsageHistoryResponse
						.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", request.getLang()));
				getUsageHistoryResponse.setCallStatus(Constants.Call_Status_True);
				return getUsageHistoryResponse;

			} else {
				getUsageHistoryResponse.setCallStatus(Constants.Call_Status_False);
				getUsageHistoryResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				getUsageHistoryResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
						Utilities.getValueFromJSON(response, "ulduzum.failure."), request.getLang()));
				return getUsageHistoryResponse;
			}
		} else {
			getUsageHistoryResponse.setCallStatus(Constants.Call_Status_False);
			getUsageHistoryResponse.setResultCode(Constants.API_FAILURE_CODE);
			getUsageHistoryResponse.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", request.getLang()));
			return getUsageHistoryResponse;
		}
	}

	public GetUnusedCodesResponse getUnsedCodes(String msisdn, UlduzumRequest request,
			GetUnusedCodesResponse responsefrombusiness)
			throws SocketException, JsonProcessingException, JSONException, SQLException {
		ObjectMapper mapper = new ObjectMapper();
		String requestJsonESB = mapper.writeValueAsString(request);

		Utilities.printDebugLog(msisdn + "-Request received in "
				+ Transactions.ULDUZUM_GET_UNUSED_CODES_TRANSACTION_NAME + " BUSINESS with data-" + requestJsonESB,
				logger);

		RestClient rc = new RestClient();
		GetUnusedCodesEsbResponse orderList = new GetUnusedCodesEsbResponse();

		String path = GetConfigurations.getESBRoute("getunusedcodes");

		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		responsefrombusiness.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
//		String response = "{}";
		responsefrombusiness.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

		// Logging ESB response code and description.
		responsefrombusiness.setLogsReport(
				Utilities.logESBParamsintoReportLog(requestJsonESB, response, responsefrombusiness.getLogsReport()));
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

				Utilities.printInfoLog("<<<<<<<< responseData ESB >>>>>>>>" + response, logger);
				try {
					orderList = mapper.readValue(response, GetUnusedCodesEsbResponse.class);
					responsefrombusiness.getData().setUnusedcodeslist(orderList.getData());
					
					/*
					 * List<GetUnsedData> list = new ArrayList<GetUnsedData>(); GetUnsedData d = new
					 * GetUnsedData(); d.setIdenticalCode("10001");
					 * d.setInsertDate("2019­11­17 23:44:22"); list.add(d); GetUnsedData d1 = new
					 * GetUnsedData(); d1.setIdenticalCode("10002");
					 * d1.setInsertDate("2019­11­18 14:18:31"); list.add(d1);
					 * responsefrombusiness.getData().setUnusedcodeslist(list);
					 */
							
					responsefrombusiness.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
					responsefrombusiness.setResultDesc(
							GetMessagesMappings.getMessageFromResourceBundle("success", request.getLang()));
					responsefrombusiness.setCallStatus(Constants.Call_Status_True);
					return responsefrombusiness;
				} catch (Exception e) {
					Utilities.convertStackTraceToString(e);
					responsefrombusiness.setCallStatus(Constants.Call_Status_False);
					responsefrombusiness.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
					responsefrombusiness.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
					return responsefrombusiness;
				}

			} else {
				responsefrombusiness.setCallStatus(Constants.Call_Status_False);
				responsefrombusiness.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				responsefrombusiness.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
				return responsefrombusiness;
			}
		} else {
			responsefrombusiness.setCallStatus(Constants.Call_Status_False);
			responsefrombusiness.setResultCode(Constants.API_FAILURE_CODE);
			responsefrombusiness.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", request.getLang()));
			return responsefrombusiness;
		}
	}

	public static void main(String[] args) throws JSONException {
		String response = "{\"returnMsg\":\"Sucessfull\",\"returnCode\":\"200\",\"data\":{\"code\":\"4217\"}}";
		System.out.println(Utilities.getValueFromJSON(Utilities.getValueFromJSON(response,"data"), "code"));
	}
	
}

package com.evampsaanga.bakcell.business;

import java.net.SocketException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.getrateus.GetRateUsRequest;
import com.evampsaanga.bakcell.models.getrateus.GetRateUsResponse;
import com.evampsaanga.bakcell.models.getrateus.GetRateUsResponseData;
import com.evampsaanga.bakcell.models.rateus.RateUsRequest;
import com.evampsaanga.bakcell.models.rateus.RateUsResponse;
import com.evampsaanga.bakcell.models.rateus.RateUsResponseData;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class RateUsBusiness {
    Logger logger = Logger.getLogger(TariffBusiness.class);

    public RateUsResponse rateus(String msisdn, RateUsRequest rateUsRequest, RateUsResponse rateUsResponse)
	    throws SocketException, JsonProcessingException, JSONException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.RATE_US_TRANSECTION_NAME
		+ " BUSINESS with data-" + rateUsRequest.toString(), logger);
	RateUsResponseData resData = new RateUsResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(rateUsRequest);

	String path = GetConfigurations.getESBRoute("rateus");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "tariffName");
	rateUsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());

	String response = rc.getResponseFromESB(path, requestJsonESB);
	rateUsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	rateUsResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, rateUsResponse.getLogsReport()));
	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData.setResponseMsg(
			GetMessagesMappings.getMessageFromResourceBundle("success", rateUsRequest.getLang()));

		rateUsResponse.setData(resData);
		rateUsResponse.setCallStatus(Constants.Call_Status_True);
		rateUsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		rateUsResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", rateUsRequest.getLang()));
		return rateUsResponse;

	    } else {

		rateUsResponse.setCallStatus(Constants.Call_Status_False);
		rateUsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		rateUsResponse.setResultDesc(Utilities.getErrorMessageFromDB("connectivity.error",
			rateUsRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));

	    }
	} else {

	    rateUsResponse.setCallStatus(Constants.Call_Status_False);
	    rateUsResponse.setResultCode(Constants.API_FAILURE_CODE);
	    rateUsResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", rateUsRequest.getLang()));
	}

	return rateUsResponse;
    }

    public GetRateUsResponse getRateus(String msisdn, GetRateUsRequest getRateUsRequest,
	    GetRateUsResponse getRateUsResponse)
	    throws SocketException, JsonProcessingException, JSONException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_RATE_US_TRANSACTION_NAME
		+ " BUSINESS with data-" + getRateUsRequest.toString(), logger);
	GetRateUsResponseData resData = new GetRateUsResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();
	Map<String, String> requestMap = new HashMap<>();
	requestMap.put("lang", "3");
	requestMap.put("type", "attributes");
	requestMap.put("entityId", getRateUsRequest.getEntityId());

	String path = GetConfigurations.getConfigurationFromCache("magento.app.appresume.url");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestMap.toString(), logger);

	String requestJsonESB = mapper.writeValueAsString(requestMap);

	getRateUsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());

	String response = rc.getResponseFromESB(path, requestJsonESB);
	getRateUsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	getRateUsResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, getRateUsResponse.getLogsReport()));
	if (response != null && !response.isEmpty()) {

	    if (Utilities.hasJSONKey(response, "pic_tnc") && Utilities.hasJSONKey(response, "rateus_android")
		    && Utilities.hasJSONKey(response, "rateus_ios")) {

		resData.setPic_tnc(Utilities.getValueFromJSON(response, "pic_tnc"));
		if (Utilities.getValueFromJSON(response, "rateus_android").length() > 0)
		    resData.setRateus_android(Utilities.getValueFromJSON(response, "rateus_android"));
		if (Utilities.getValueFromJSON(response, "rateus_ios").length() > 0)
		    resData.setRateus_ios(Utilities.getValueFromJSON(response, "rateus_ios"));
		getRateUsResponse.setData(resData);
		getRateUsResponse.setCallStatus(Constants.Call_Status_True);
		getRateUsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		getRateUsResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", getRateUsRequest.getLang()));

	    } else {

		getRateUsResponse.setCallStatus(Constants.Call_Status_False);
		getRateUsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		getRateUsResponse.setResultDesc(Utilities.getErrorMessageFromDB("connectivity.error",
			getRateUsRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));

	    }

	} else {

	    getRateUsResponse.setCallStatus(Constants.Call_Status_False);
	    getRateUsResponse.setResultCode(Constants.API_FAILURE_CODE);
	    getRateUsResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", getRateUsRequest.getLang()));
	}
	return getRateUsResponse;
    }
}
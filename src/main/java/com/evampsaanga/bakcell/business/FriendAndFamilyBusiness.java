/**
 * 
 */
package com.evampsaanga.bakcell.business;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.friendandfamily.addfnf.AddFriendAndFamilyRequest;
import com.evampsaanga.bakcell.models.friendandfamily.addfnf.AddFriendAndFamilyResponse;
import com.evampsaanga.bakcell.models.friendandfamily.deletefnf.DeleteFriendAndFamilyRequest;
import com.evampsaanga.bakcell.models.friendandfamily.deletefnf.DeleteFriendAndFamilyResponse;
import com.evampsaanga.bakcell.models.friendandfamily.getfnf.FNF;
import com.evampsaanga.bakcell.models.friendandfamily.getfnf.FriendAndFamilyResponseData;
import com.evampsaanga.bakcell.models.friendandfamily.getfnf.GetFriendAndFamilyRequest;
import com.evampsaanga.bakcell.models.friendandfamily.getfnf.GetFriendAndFamilyResponse;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
@Service
public class FriendAndFamilyBusiness {
    Logger logger = Logger.getLogger(FriendAndFamilyBusiness.class);

    public GetFriendAndFamilyResponse getFnf(String msisdn, GetFriendAndFamilyRequest getFriendAndFamilyRequest,
	    GetFriendAndFamilyResponse getFriendAndFamilyResponse)
	    throws JSONException, ParseException, FileNotFoundException, IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_FNF_TRANSACTION_NAME
		+ "  BUSINESS with data-" + getFriendAndFamilyRequest.toString(), logger);

	FriendAndFamilyResponseData resData = new FriendAndFamilyResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(getFriendAndFamilyRequest);

	String path = GetConfigurations.getESBRoute("getFnF");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	// String response = HardCodedResponses.GET_FNF;

	getFriendAndFamilyResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	getFriendAndFamilyResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	getFriendAndFamilyResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		getFriendAndFamilyResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData.setFnfList(this.fetchFNFList(msisdn, response));
		getFriendAndFamilyResponse.setData(resData);
		getFriendAndFamilyResponse.getData().setFnfLimit(Utilities.getValueFromJSON(response, "fnfLimit"));
		getFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_True);
		getFriendAndFamilyResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		getFriendAndFamilyResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			getFriendAndFamilyRequest.getLang()));

	    } else {
		getFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
		getFriendAndFamilyResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		getFriendAndFamilyResponse.setResultDesc(Utilities.getErrorMessageFromDB("get.fnf",
			getFriendAndFamilyRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    getFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
	    getFriendAndFamilyResponse.setResultCode(Constants.API_FAILURE_CODE);
	    getFriendAndFamilyResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", getFriendAndFamilyRequest.getLang()));
	}

	return getFriendAndFamilyResponse;

    }

    public AddFriendAndFamilyResponse addFnf(String msisdn, AddFriendAndFamilyRequest addFriendAndFamilyRequest,
	    AddFriendAndFamilyResponse addFriendAndFamilyResponse) throws JSONException, ParseException,
	    FileNotFoundException, IOException, InterruptedException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ADD_FNF_TRANSACTION_NAME
		+ " BUSINESS with data-" + addFriendAndFamilyRequest.toString(), logger);

	FriendAndFamilyResponseData resData = new FriendAndFamilyResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();
	GetFriendAndFamilyRequest getFriendAndFamilyRequest = new GetFriendAndFamilyRequest();
	GetFriendAndFamilyResponse getFriendAndFamilyResponse = new GetFriendAndFamilyResponse();

	getFriendAndFamilyRequest.setChannel(addFriendAndFamilyRequest.getChannel());
	getFriendAndFamilyRequest.setiP(addFriendAndFamilyRequest.getiP());
	getFriendAndFamilyRequest.setMsisdn(addFriendAndFamilyRequest.getMsisdn());
	getFriendAndFamilyRequest.setLang(addFriendAndFamilyRequest.getLang());
	getFriendAndFamilyRequest.setOfferingId(addFriendAndFamilyRequest.getOfferingId());
	getFriendAndFamilyRequest.setIsB2B(addFriendAndFamilyRequest.getIsB2B());
	
	String requestJsonESB = mapper.writeValueAsString(addFriendAndFamilyRequest);
	requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "actionType", "");
	requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "fnfNumber",
		Utilities.getValueFromJSON(requestJsonESB, "addMsisdn"));
	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "addMsisdn");

	String path = GetConfigurations.getESBRoute("manipulateFNF");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	// String response = HardCodedResponses.ADD_FNF;

	addFriendAndFamilyResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	addFriendAndFamilyResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	addFriendAndFamilyResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		addFriendAndFamilyResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		Thread.sleep(5000);
		getFriendAndFamilyResponse = this.getFnf(msisdn, getFriendAndFamilyRequest, getFriendAndFamilyResponse);
		resData.setFnfList(getFriendAndFamilyResponse.getData().getFnfList());
		addFriendAndFamilyResponse.setData(resData);
		addFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_True);
		addFriendAndFamilyResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		addFriendAndFamilyResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			addFriendAndFamilyRequest.getLang()));
	    } else {
		addFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
		addFriendAndFamilyResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		addFriendAndFamilyResponse.setResultDesc(Utilities.getErrorMessageFromDB("add.fnf",
			addFriendAndFamilyRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    addFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
	    addFriendAndFamilyResponse.setResultCode(Constants.API_FAILURE_CODE);
	    addFriendAndFamilyResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", addFriendAndFamilyRequest.getLang()));
	}

	return addFriendAndFamilyResponse;
    }

    public DeleteFriendAndFamilyResponse deleteFnf(String msisdn,
	    DeleteFriendAndFamilyRequest deleteFriendAndFamilyRequest,
	    DeleteFriendAndFamilyResponse deleteFriendAndFamilyResponse) throws JSONException, ParseException,
	    FileNotFoundException, IOException, InterruptedException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.DELETE_FNF_TRANSACTION_NAME
		+ " BUSINESS with data-" + deleteFriendAndFamilyRequest.toString(), logger);

	FriendAndFamilyResponseData resData = new FriendAndFamilyResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();
	GetFriendAndFamilyRequest getFriendAndFamilyRequest = new GetFriendAndFamilyRequest();
	GetFriendAndFamilyResponse getFriendAndFamilyResponse = new GetFriendAndFamilyResponse();

	getFriendAndFamilyRequest.setChannel(deleteFriendAndFamilyRequest.getChannel());
	getFriendAndFamilyRequest.setiP(deleteFriendAndFamilyRequest.getiP());
	getFriendAndFamilyRequest.setMsisdn(deleteFriendAndFamilyRequest.getMsisdn());
	getFriendAndFamilyRequest.setLang(deleteFriendAndFamilyRequest.getLang());
	getFriendAndFamilyRequest.setOfferingId(deleteFriendAndFamilyRequest.getOfferingId());
	getFriendAndFamilyRequest.setIsB2B(deleteFriendAndFamilyRequest.getIsB2B());
	String requestJsonESB = mapper.writeValueAsString(deleteFriendAndFamilyRequest);

	requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "actionType",
		Utilities.getValueFromJSON(requestJsonESB, "deleteMsisdn"));
	requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "fnfNumber", "");
	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "deleteMsisdn");

	String path = GetConfigurations.getESBRoute("manipulateFNF");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	deleteFriendAndFamilyResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	deleteFriendAndFamilyResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	deleteFriendAndFamilyResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		deleteFriendAndFamilyResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		Thread.sleep(5000);
		getFriendAndFamilyResponse = this.getFnf(msisdn, getFriendAndFamilyRequest, getFriendAndFamilyResponse);
		resData.setFnfList(getFriendAndFamilyResponse.getData().getFnfList());
		deleteFriendAndFamilyResponse.setData(resData);
		deleteFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_True);
		deleteFriendAndFamilyResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		deleteFriendAndFamilyResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			deleteFriendAndFamilyRequest.getLang()));
	    } else {
		deleteFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
		deleteFriendAndFamilyResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		deleteFriendAndFamilyResponse.setResultDesc(Utilities.getErrorMessageFromDB("delete.fnf",
			deleteFriendAndFamilyRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    deleteFriendAndFamilyResponse.setCallStatus(Constants.Call_Status_False);
	    deleteFriendAndFamilyResponse.setResultCode(Constants.API_FAILURE_CODE);
	    deleteFriendAndFamilyResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", deleteFriendAndFamilyRequest.getLang()));
	}

	return deleteFriendAndFamilyResponse;
    }

    private List<FNF> fetchFNFList(String msisdn, String response)
	    throws JSONException, ParseException, SocketException {
	List<FNF> fnfList = new ArrayList<>();
	Utilities.printDebugLog(msisdn + "-" + response, logger);

	JSONArray fnfArrayList = new JSONArray(Utilities.getValueFromJSON(response, "list"));
	for (int i = 0; i < fnfArrayList.length(); i++) {
	    JSONObject fnfObj = fnfArrayList.getJSONObject(i);

	    FNF fnf = new FNF();
	    fnf.setMsisdn(fnfObj.getString("msisdn"));
	    fnf.setCreatedDate(fnfObj.getString("date"));
	    fnfList.add(fnf);

	}
	return fnfList;
    }

}

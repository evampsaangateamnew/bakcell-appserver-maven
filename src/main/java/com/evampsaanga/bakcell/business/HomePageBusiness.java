/**
 * 
 */
package com.evampsaanga.bakcell.business;

import java.net.SocketException;
import java.sql.SQLException;
import java.text.ParseException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.homepageservices.HomePageRequest;
import com.evampsaanga.bakcell.models.homepageservices.HomePageResponse;
import com.evampsaanga.bakcell.models.homepageservices.HomePageResponseData;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
@Service
public class HomePageBusiness {
    Logger logger = Logger.getLogger(HomePageBusiness.class);

    public HomePageResponse getHomePageBusiness(String msisdn, HomePageRequest homePageRequest,
	    HomePageResponse homePageResponse) throws ParseException, Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.HOME_PAGE_TRANSACTION_NAME
		+ " BUSINESS with data-" + homePageRequest.toString(), logger);

	HomePageResponseData resData = new HomePageResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(homePageRequest);

	// * Redefining request PARAMS for ESB request, As request params are
	// * different at Mobile and ESB end.

	// old Line , this is commented by saboor
	// String subscriberType = Utilities.getValueFromJSON(requestJsonESB,
	// "subscriberType");
	String subscriberType = Utilities.getValueFromJSON(requestJsonESB, "customerType");
	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "subscriberType");
	requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "customerType", subscriberType);

	String path = GetConfigurations.getESBRoute("getHomePage");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	homePageResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	homePageResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	homePageResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, homePageResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData = mapper.readValue(Utilities.getValueFromJSON(response, "data"), HomePageResponseData.class);

		// Setting labels
		resData = setLabels(resData, homePageRequest.getLang(), homePageRequest.getBrandId());

		// set initial date for installments.
		resData = setInstallmentsInitialDate(resData);

		homePageResponse.setData(resData);
		homePageResponse.setCallStatus(Constants.Call_Status_True);
		homePageResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		homePageResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", homePageRequest.getLang()));

	    } else {
		homePageResponse.setCallStatus(Constants.Call_Status_False);
		homePageResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		homePageResponse.setResultDesc(Utilities.getErrorMessageFromDB("home.page", homePageRequest.getLang(),
			Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {
	    homePageResponse.setCallStatus(Constants.Call_Status_False);
	    homePageResponse.setResultCode(Constants.API_FAILURE_CODE);
	    homePageResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", homePageRequest.getLang()));
	}

	return homePageResponse;
    }

    private HomePageResponseData setLabels(HomePageResponseData resData, String lang, String brandId)
	    throws SocketException, SQLException {

	// Setting Prepaid Balance Labels
	resData.getBalance().getPrepaid().getMainWallet()
		.setBalanceTypeName(GetMessagesMappings.getLabelsFromResourceBundle("mainWalletLabel", lang));
	resData.getBalance().getPrepaid().getCountryWideWallet()
		.setBalanceTypeName(GetMessagesMappings.getLabelsFromResourceBundle("countryWideLabel", lang));
	resData.getBalance().getPrepaid().getBounusWallet()
		.setBalanceTypeName(GetMessagesMappings.getLabelsFromResourceBundle("bonusWalletLabel", lang));

	// Setting Postpaid Balance Labels
	resData.getBalance().getPostpaid()
		.setAvailableCreditLabel(GetMessagesMappings.getLabelsFromResourceBundle("availableCreditLabel", lang));
	resData.getBalance().getPostpaid()
		.setBalanceLabel(GetMessagesMappings.getLabelsFromResourceBundle("balanceLabel", lang));
	resData.getBalance().getPostpaid()
		.setCorporateLabel(GetMessagesMappings.getLabelsFromResourceBundle("corporateLabel", lang));
	resData.getBalance().getPostpaid()
		.setCurrentCreditLabel(GetMessagesMappings.getLabelsFromResourceBundle("currentCreditLabel", lang));
	resData.getBalance().getPostpaid()
		.setIndividualLabel(GetMessagesMappings.getLabelsFromResourceBundle("individualLabel", lang));
	resData.getBalance().getPostpaid().setOutstandingIndividualDebtLabel(
		GetMessagesMappings.getLabelsFromResourceBundle("outstandingIndividualDeptLabel", lang));
	resData.getBalance()
		.setMinAmountLabel(GetMessagesMappings.getLabelsFromResourceBundle("minimumamountlabel", lang));
	// Setting Installments Labels
	resData.getInstallments()
		.setInstallmentTitle(GetMessagesMappings.getLabelsFromResourceBundle("installmentTitle", lang));
	resData.getInstallments().getInstallments().forEach((Installment) -> {
	    try {
		Installment.setAmountLabel(GetMessagesMappings.getLabelsFromResourceBundle("amountLabel", lang));
		Installment
			.setNextPaymentLabel(GetMessagesMappings.getLabelsFromResourceBundle("nextPaymentLabel", lang));
		Installment.setPurchaseDateLabel(
			GetMessagesMappings.getLabelsFromResourceBundle("purchaseDateLabel", lang));
		Installment.setRemainingAmountLabel(
			GetMessagesMappings.getLabelsFromResourceBundle("remainingAmountLabel", lang));
		Installment.setRemainingPeriodBeginDateLabel(
			GetMessagesMappings.getLabelsFromResourceBundle("remainingPeriodBeginDateLabel", lang));
		Installment.setRemainingPeriodEndDateLabel(
			GetMessagesMappings.getLabelsFromResourceBundle("remainingPeriodEndDateLabel", lang));

		Installment.setRemainingPeriodLabel(
			GetMessagesMappings.getLabelsFromResourceBundle("remainingPeriodLabel", lang));
	    } catch (Exception e) {
		try {
		    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		} catch (SocketException e1) {

		    e1.printStackTrace();
		}
	    }
	});

	// Setting MRC Labels
	resData.getMrc().setMrcTitleLabel(GetMessagesMappings
		.getLabelsFromResourceBundle("mrcTitleLabel." + resData.getMrc().getMrcType().toLowerCase(), lang));
	resData.getMrc().setMrcDateLabel(GetMessagesMappings.getLabelsFromResourceBundle("mrcDateLabel", lang));
	if (resData.getMrc().getMrcTitleValue() != null && resData.getMrc().getMrcTitleValue().equalsIgnoreCase("FREE"))
	    resData.getMrc().setMrcTitleValue(GetMessagesMappings.getLabelsFromResourceBundle("mrcValueFree", lang));

	// Setting Credit Labels
	if (brandId.equalsIgnoreCase("1770078090"))
	    resData.getCredit().setCreditTitleLabel(
		    GetMessagesMappings.getLabelsFromResourceBundle("creditTitleLabelKlass", lang));
	else if (brandId.equalsIgnoreCase("1970006532"))
	    resData.getCredit()
		    .setCreditTitleLabel(GetMessagesMappings.getLabelsFromResourceBundle("creditTitleLabelCin", lang));
	if (resData.getCredit().getCreditDateLabel().equalsIgnoreCase(Constants.CREDIT_EXPIRATION_LABEL_BLOCK_ONE)) {
	    resData.getCredit()
		    .setCreditDateLabel(GetMessagesMappings.getLabelsFromResourceBundle("creditDateLabel.1", lang));
	} else if (resData.getCredit().getCreditDateLabel()
		.equalsIgnoreCase(Constants.CREDIT_EXPIRATION_LABEL_BLOCK_TWO)) {
	    resData.getCredit()
		    .setCreditDateLabel(GetMessagesMappings.getLabelsFromResourceBundle("creditDateLabel.2", lang));
	} else if (resData.getCredit().getCreditDateLabel()
		.equalsIgnoreCase(Constants.CREDIT_EXPIRATION_LABEL_EXPIRED)) {
	    resData.getCredit()
		    .setCreditDateLabel(GetMessagesMappings.getLabelsFromResourceBundle("creditDateLabel.3", lang));
	} else {
	    resData.getCredit().setCreditDateLabel("");
	}

	// Setting Free Resource Labels
	resData.getFreeResources().getFreeResources().forEach((FreeResource) -> {

	    try {
		FreeResource.setResourcesTitleLabel(GetMessagesMappings.getLabelsFromResourceBundle(
			"resourcesTitleLabel." + FreeResource.getResourceType().toLowerCase(), lang));
		FreeResource.setResourceUnitName(GetMessagesMappings
			.getLabelsFromResourceBundle(FreeResource.getResourceUnitName().toLowerCase(), lang));
	    } catch (Exception e) {
		try {
		    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		} catch (Exception e1) {
		    // TODO Auto-generated catch block
		    e1.printStackTrace();
		}
	    }

	});

	// Setting Free Resource Roaming Labels
	resData.getFreeResources().getFreeResourcesRoaming().forEach((FreeResource) -> {
	    try {
		FreeResource.setResourcesTitleLabel(GetMessagesMappings.getLabelsFromResourceBundle(
			"resourcesTitleLabel.Roaming." + FreeResource.getResourceType().toLowerCase(), lang));

		FreeResource.setResourceUnitName(GetMessagesMappings
			.getLabelsFromResourceBundle(FreeResource.getResourceUnitName().toLowerCase(), lang));
	    } catch (Exception e) {
		try {
		    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		} catch (Exception e1) {
		    // TODO Auto-generated catch block
		    e1.printStackTrace();
		}
	    }

	});

	return resData;
    }

    private HomePageResponseData setInstallmentsInitialDate(HomePageResponseData resData) {
	resData.getInstallments().getInstallments().forEach((installment) -> {
	    try {
		installment.setNextPaymentInitialDate(installment.getPurchaseDateValue());

		if (!installment.getRemainingPeriodBeginDateValue().isEmpty())
		    installment.setNextPaymentValue(getNextDateForInstallments(Utilities.getCurrentDateTime()));
	    } catch (ParseException e) {

		try {
		    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		} catch (SocketException e1) {
		    // TODO Auto-generated catch block
		    e1.printStackTrace();
		}
	    }
	});
	return resData;
    }

    private String getNextDateForInstallments(String date) throws ParseException {

	return Utilities.getNextMonth(Utilities.getDateFromString(date, Constants.DATE_FORMAT));

    }

}

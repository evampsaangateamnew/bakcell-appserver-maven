/**
 * 
 */
package com.evampsaanga.bakcell.business;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.history.getoperationshistory.OperationsHistoryRequest;
import com.evampsaanga.bakcell.models.history.getoperationshistory.OperationsHistoryResponse;
import com.evampsaanga.bakcell.models.history.getoperationshistory.OperationsHistoryResponseData;
import com.evampsaanga.bakcell.models.history.getusagedetails.UsageDetailsRequest;
import com.evampsaanga.bakcell.models.history.getusagedetails.UsageDetailsResponse;
import com.evampsaanga.bakcell.models.history.getusagedetails.UsageDetailsResponseData;
import com.evampsaanga.bakcell.models.history.getusagesummary.SummaryRecordDetails;
import com.evampsaanga.bakcell.models.history.getusagesummary.UsageSummaryRequest;
import com.evampsaanga.bakcell.models.history.getusagesummary.UsageSummaryResponse;
import com.evampsaanga.bakcell.models.history.getusagesummary.UsageSummaryResponseData;
import com.evampsaanga.bakcell.models.history.verifyaccountdetails.VerifyAccountDetailsRequest;
import com.evampsaanga.bakcell.models.history.verifyaccountdetails.VerifyAccountDetailsResponse;
import com.evampsaanga.bakcell.models.history.verifyaccountdetails.VerifyAccountDetailsResponseData;
import com.evampsaanga.bakcell.models.history.verifypassport.VerifyPassportRequest;
import com.evampsaanga.bakcell.models.history.verifypassport.VerifyPassportResponse;
import com.evampsaanga.bakcell.models.history.verifypin.UsageDetailsVerifyPinRequest;
import com.evampsaanga.bakcell.models.history.verifypin.UsageDetailsVerifyPinResponse;
import com.evampsaanga.bakcell.models.history.verifypin.UsageDetailsVerifyPinResponseData;
import com.evampsaanga.bakcell.models.history.verifypin.VerifyPinRequestV2;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
@Service
public class HistoryBusiness {
    Logger logger = Logger.getLogger(HistoryBusiness.class);

    public UsageSummaryResponse getUsageSummary(String msisdn, UsageSummaryRequest usageSummaryRequest,
	    UsageSummaryResponse usageSummaryResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_CDRS_SUMMARY_TRANSACTION_NAME
		+ " BUSINESS with data-" + usageSummaryRequest.toString(), logger);

	UsageSummaryResponseData resData = new UsageSummaryResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(usageSummaryRequest);

	String path = GetConfigurations.getESBRoute("getUsageSummary");

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	// String response = HardCodedResponses.USAGE_HISTORY_SUMMARY;

	usageSummaryResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	usageSummaryResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	usageSummaryResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, usageSummaryResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData = this.parseUsageSummaryData(Utilities.getValueFromJSON(response, "summaryResponseBody"),
			resData, usageSummaryRequest.getLang());

		usageSummaryResponse.setData(resData);
		usageSummaryResponse.setCallStatus(Constants.Call_Status_True);
		usageSummaryResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		usageSummaryResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", usageSummaryRequest.getLang()));
	    } else {
		usageSummaryResponse.setCallStatus(Constants.Call_Status_False);
		usageSummaryResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		usageSummaryResponse.setResultDesc(Utilities.getErrorMessageFromDB("usage.summary",
			usageSummaryRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }

	} else {

	    usageSummaryResponse.setCallStatus(Constants.Call_Status_False);
	    usageSummaryResponse.setResultCode(Constants.API_FAILURE_CODE);

	    usageSummaryResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    usageSummaryRequest.getLang()));
	}

	return usageSummaryResponse;
    }

    public UsageDetailsResponse getUsageDetails(String msisdn, UsageDetailsRequest usageDetailsRequest,
	    UsageDetailsResponse usageDetailsResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_CDRS_BY_DATE_TRANSACTION_NAME
		+ " BUSINESS with data-" + usageDetailsRequest.toString(), logger);

	UsageDetailsResponseData resData = new UsageDetailsResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(usageDetailsRequest);

	String path = GetConfigurations.getESBRoute("getUsageDetails");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	// String response = HardCodedResponses.USAGE_HISTORY_DETAIL;

	usageDetailsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	usageDetailsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	usageDetailsResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, usageDetailsResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData = mapper.readValue(response, UsageDetailsResponseData.class);
		usageDetailsResponse.setData(resData);
		usageDetailsResponse.setCallStatus(Constants.Call_Status_True);
		usageDetailsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		usageDetailsResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", usageDetailsRequest.getLang()));
	    } else {
		usageDetailsResponse.setCallStatus(Constants.Call_Status_False);
		usageDetailsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		usageDetailsResponse.setResultDesc(Utilities.getErrorMessageFromDB("usage.detail",
			usageDetailsRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }

	} else {

	    usageDetailsResponse.setCallStatus(Constants.Call_Status_False);
	    usageDetailsResponse.setResultCode(Constants.API_FAILURE_CODE);

	    usageDetailsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    usageDetailsRequest.getLang()));
	}

	return usageDetailsResponse;
    }

    public OperationsHistoryResponse getOperationsHistory(String msisdn,
	    OperationsHistoryRequest operationsHistoryRequest, OperationsHistoryResponse operationsHistoryResponse)
	    throws Exception {

	Utilities.printDebugLog(
		msisdn + "-Request received in " + Transactions.GET_CDRS_OPERATION_HISTORY_TRANSACTION_NAME
			+ " BUSINESS with data-" + operationsHistoryRequest.toString(),
		logger);

	OperationsHistoryResponseData resData = new OperationsHistoryResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(operationsHistoryRequest);

	String path = GetConfigurations.getESBRoute("getOperationsHistory");

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	operationsHistoryResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	operationsHistoryResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	operationsHistoryResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		operationsHistoryResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData = mapper.readValue(response, OperationsHistoryResponseData.class);
		operationsHistoryResponse.setData(resData);
		operationsHistoryResponse.setCallStatus(Constants.Call_Status_True);
		operationsHistoryResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		operationsHistoryResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			operationsHistoryRequest.getLang()));
	    } else {
		operationsHistoryResponse.setCallStatus(Constants.Call_Status_False);
		operationsHistoryResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		operationsHistoryResponse.setResultDesc(Utilities.getErrorMessageFromDB("operation.history",
			operationsHistoryRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }

	} else {

	    operationsHistoryResponse.setCallStatus(Constants.Call_Status_False);
	    operationsHistoryResponse.setResultCode(Constants.API_FAILURE_CODE);

	    operationsHistoryResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", operationsHistoryRequest.getLang()));
	}

	return operationsHistoryResponse;
    }

    public VerifyAccountDetailsResponse verifyAccountDetailsBusiness(String msisdn,
	    VerifyAccountDetailsRequest verifyAccountDetailsRequest,
	    VerifyAccountDetailsResponse verifyAccountDetailsResponse)
	    throws JSONException, FileNotFoundException, IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_CDRSBY_DATE_OTP_TRANSACTION_NAME
		+ " BUSINESS with data-" + verifyAccountDetailsRequest.toString(), logger);

	VerifyAccountDetailsResponseData resData = new VerifyAccountDetailsResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	VerifyPassportRequest verifyPassportRequest = new VerifyPassportRequest();
	VerifyPassportResponse verifyPassportResponse = new VerifyPassportResponse();

	verifyPassportRequest.setChannel(verifyAccountDetailsRequest.getChannel());
	verifyPassportRequest.setiP(verifyAccountDetailsRequest.getiP());
	verifyPassportRequest.setLang(verifyAccountDetailsRequest.getLang());
	verifyPassportRequest.setMsisdn(verifyAccountDetailsRequest.getMsisdn());
	verifyPassportRequest.setPassportNumber(verifyAccountDetailsRequest.getPassportNumber());
	Utilities.printDebugLog(msisdn + "-Request for Passport-" + verifyPassportResponse.toString(), logger);

	verifyPassportResponse = this.verifyPassport(msisdn, verifyPassportRequest, verifyPassportResponse);

	Utilities.printDebugLog(msisdn + "-Received response for Passport-" + verifyPassportResponse.toString(),
		logger);

	if (verifyPassportResponse.getResultCode().equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

	    String requestJsonESB = mapper.writeValueAsString(verifyAccountDetailsRequest);

	    String path = GetConfigurations.getESBRoute("getPinForUsageDetails");

	    Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	    Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	    Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	    // String response = HardCodedResponses.VERIFY_ACCOUNT_DETAILS;
	    verifyAccountDetailsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	    String response = rc.getResponseFromESB(path, requestJsonESB);
	    verifyAccountDetailsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	    Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	    // Logging ESB response code and description.
	    verifyAccountDetailsResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		    verifyAccountDetailsResponse.getLogsReport()));

	    if (response != null && !response.isEmpty()) {

		if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		    // resData.setPin(Utilities.getValueFromJSON(response,
		    // "pin"));
		    verifyAccountDetailsResponse.setData(resData);
		    verifyAccountDetailsResponse.setCallStatus(Constants.Call_Status_True);
		    verifyAccountDetailsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		    verifyAccountDetailsResponse.setResultDesc(GetMessagesMappings
			    .getMessageFromResourceBundle("success", verifyAccountDetailsRequest.getLang()));
		} else {
		    verifyAccountDetailsResponse.setCallStatus(Constants.Call_Status_False);
		    verifyAccountDetailsResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    verifyAccountDetailsResponse.setResultDesc(Utilities
			    .getErrorMessageFromDB("usage.history.account.id.verification",
				    verifyAccountDetailsRequest.getLang(),
				    Utilities.getValueFromJSON(response, "returnCode"))
			    .replace("{0}", verifyAccountDetailsRequest.getMsisdn()));
		}

	    } else {

		verifyAccountDetailsResponse.setCallStatus(Constants.Call_Status_False);
		verifyAccountDetailsResponse.setResultCode(Constants.API_FAILURE_CODE);

		verifyAccountDetailsResponse.setResultDesc(GetMessagesMappings
			.getMessageFromResourceBundle("connectivity.error", verifyAccountDetailsRequest.getLang()));
	    }
	} else {
	    verifyAccountDetailsResponse.setCallStatus(verifyPassportResponse.getCallStatus());
	    verifyAccountDetailsResponse.setResultCode(verifyPassportResponse.getResultCode());
	    verifyAccountDetailsResponse.setResultDesc(verifyPassportResponse.getResultDesc());
	}
	return verifyAccountDetailsResponse;
    }

    // for Phase 2
    public UsageDetailsVerifyPinResponse verifyPinBusinessV2(String msisdn,
	    VerifyPinRequestV2 usageDetailsVerifyPinRequest,
	    UsageDetailsVerifyPinResponse usageDetailsVerifyPinResponse)
	    throws JSONException, IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.VERIFY_CDRS_BY_DATE_OTP_TRANSACTION_NAME
		+ " BUSINESS with data-" + usageDetailsVerifyPinRequest.toString(), logger);

	UsageDetailsVerifyPinResponseData resData = new UsageDetailsVerifyPinResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(usageDetailsVerifyPinRequest);

	String path = GetConfigurations.getESBRoute("verifyPinV2");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	usageDetailsVerifyPinResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	usageDetailsVerifyPinResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	usageDetailsVerifyPinResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		usageDetailsVerifyPinResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData.setMessage(Utilities.getValueFromJSON(response, "returnMsg"));
		usageDetailsVerifyPinResponse.setData(resData);
		usageDetailsVerifyPinResponse.setCallStatus(Constants.Call_Status_True);
		usageDetailsVerifyPinResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		usageDetailsVerifyPinResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			usageDetailsVerifyPinRequest.getLang()));
	    } else {
		usageDetailsVerifyPinResponse.setCallStatus(Constants.Call_Status_False);
		usageDetailsVerifyPinResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		usageDetailsVerifyPinResponse.setResultDesc(Utilities.getErrorMessageFromDB(
			"usage.history.otp.verification", usageDetailsVerifyPinRequest.getLang(),
			Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    usageDetailsVerifyPinResponse.setCallStatus(Constants.Call_Status_False);
	    usageDetailsVerifyPinResponse.setResultCode(Constants.API_FAILURE_CODE);

	    usageDetailsVerifyPinResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", usageDetailsVerifyPinRequest.getLang()));
	}

	return usageDetailsVerifyPinResponse;

    }

    public UsageDetailsVerifyPinResponse verifyPinBusiness(String msisdn,
	    UsageDetailsVerifyPinRequest usageDetailsVerifyPinRequest,
	    UsageDetailsVerifyPinResponse usageDetailsVerifyPinResponse)
	    throws JSONException, IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.VERIFY_CDRS_BY_DATE_OTP_TRANSACTION_NAME
		+ " BUSINESS with data-" + usageDetailsVerifyPinRequest.toString(), logger);

	UsageDetailsVerifyPinResponseData resData = new UsageDetailsVerifyPinResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(usageDetailsVerifyPinRequest);

	String path = GetConfigurations.getESBRoute("verifyPin");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	usageDetailsVerifyPinResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	usageDetailsVerifyPinResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	usageDetailsVerifyPinResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		usageDetailsVerifyPinResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData.setMessage(Utilities.getValueFromJSON(response, "returnMsg"));
		usageDetailsVerifyPinResponse.setData(resData);
		usageDetailsVerifyPinResponse.setCallStatus(Constants.Call_Status_True);
		usageDetailsVerifyPinResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		usageDetailsVerifyPinResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			usageDetailsVerifyPinRequest.getLang()));
	    } else {
		usageDetailsVerifyPinResponse.setCallStatus(Constants.Call_Status_False);
		usageDetailsVerifyPinResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		usageDetailsVerifyPinResponse.setResultDesc(Utilities.getErrorMessageFromDB(
			"usage.history.otp.verification", usageDetailsVerifyPinRequest.getLang(),
			Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    usageDetailsVerifyPinResponse.setCallStatus(Constants.Call_Status_False);
	    usageDetailsVerifyPinResponse.setResultCode(Constants.API_FAILURE_CODE);

	    usageDetailsVerifyPinResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", usageDetailsVerifyPinRequest.getLang()));
	}

	return usageDetailsVerifyPinResponse;

    }

    public VerifyPassportResponse verifyPassport(String msisdn, VerifyPassportRequest verifyPassportRequest,
	    VerifyPassportResponse verifyPassportResponse) throws JSONException, IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.MNP_GET_SUBSCRIBER_TRANSACTION_NAME
		+ " BUSINESS with data-" + verifyPassportRequest.toString(), logger);

	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(verifyPassportRequest);

	String path = GetConfigurations.getESBRoute("verifyPassport");

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	verifyPassportResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	verifyPassportResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	verifyPassportResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, verifyPassportResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		verifyPassportResponse.setCallStatus(Constants.Call_Status_True);
		verifyPassportResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		verifyPassportResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));

	    } else {
		verifyPassportResponse.setCallStatus(Constants.Call_Status_False);
		verifyPassportResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		verifyPassportResponse.setResultDesc(GetMessagesMappings
			.getMessageFromResourceBundle("esb.unsuccess.message", verifyPassportRequest.getLang()));

	    }
	} else {

	    verifyPassportResponse.setCallStatus(Constants.Call_Status_False);
	    verifyPassportResponse.setResultCode(Constants.API_FAILURE_CODE);

	    verifyPassportResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    verifyPassportRequest.getLang()));
	}

	return verifyPassportResponse;

    }

    private UsageSummaryResponseData parseUsageSummaryData(String valueFromJSON, UsageSummaryResponseData resData,
	    String lang) throws JsonParseException, JsonMappingException, IOException, JSONException, SQLException {

	ObjectMapper mapper = new ObjectMapper();
	List<SummaryRecordDetails> summaryList = new ArrayList<>();

	summaryList.add(this.setName(
		mapper.readValue(Utilities.getValueFromJSON(valueFromJSON, "voiceRecords"), SummaryRecordDetails.class),
		"voice", lang));
	summaryList.add(this.setName(
		mapper.readValue(Utilities.getValueFromJSON(valueFromJSON, "smsRecords"), SummaryRecordDetails.class),
		"sms", lang));

	summaryList.add(this.setName(
		mapper.readValue(Utilities.getValueFromJSON(valueFromJSON, "dataRecords"), SummaryRecordDetails.class),
		"data", lang));

	summaryList.add(this.setName(
		mapper.readValue(Utilities.getValueFromJSON(valueFromJSON, "otherRecords"), SummaryRecordDetails.class),
		"others", lang));

	resData.setSummaryList(summaryList);

	return resData;
    }

    private SummaryRecordDetails setName(SummaryRecordDetails recordDetailsObj, String mappingKey, String lang)
	    throws IOException, SQLException {
	recordDetailsObj.setName(GetMessagesMappings.getLabelsFromResourceBundle(mappingKey, lang));
	return recordDetailsObj;
    }

}

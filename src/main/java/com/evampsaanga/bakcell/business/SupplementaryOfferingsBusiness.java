/**
 * 
 */
package com.evampsaanga.bakcell.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.AppCache;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.customerservices.customerinfo.CustomerInfoRequest;
import com.evampsaanga.bakcell.models.customerservices.customerinfo.CustomerInfoResponseData;
import com.evampsaanga.bakcell.models.mysubscriptions.getsubscriptions.MySubscriptionsRequest;
import com.evampsaanga.bakcell.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsRequest;
import com.evampsaanga.bakcell.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsRequestBulk;
import com.evampsaanga.bakcell.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsResponse;
import com.evampsaanga.bakcell.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsResponseData;
import com.evampsaanga.bakcell.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsResponseDatabulk;
import com.evampsaanga.bakcell.models.supplementaryofferings.changesupplementaryoffering.ChangeSupplementaryOfferingsResponsebulk;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.ParseSupplementaryOfferingsResponse;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsByIDsRequest;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsRequest;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponse;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponseData;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.header.GroupedOfferAttributes;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
@Service
public class SupplementaryOfferingsBusiness {
    Logger logger = Logger.getLogger(SupplementaryOfferingsBusiness.class);

    public SupplementaryOfferingsResponse getSupplementaryOfferings(String msisdn,
	    SupplementaryOfferingsRequest supplementaryOfferingsRequest,
	    SupplementaryOfferingsResponse supplementaryOfferingsResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME
		+ " BUSINESS with data-" + supplementaryOfferingsRequest.toString(), logger);
	ObjectMapper mapper = new ObjectMapper();
	// Get key which will be used to store and retrieve data from hash map.
	String supplementaryOfferingsCacheKey = this.getKeyForCache(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS,
		supplementaryOfferingsRequest);

	if (AppCache.getHashmapSupplementaryOfferings().containsKey(supplementaryOfferingsCacheKey)) {
	    Utilities.printDebugLog(msisdn + "-SUPPLEMENTARY OFFERINGS" + Constants.CACHE_EXISTS_DESCRIPTION + ""
		    + supplementaryOfferingsCacheKey, logger);
	    supplementaryOfferingsResponse = AppCache.getHashmapSupplementaryOfferings()
		    .get(supplementaryOfferingsCacheKey);
	    supplementaryOfferingsResponse.getLogsReport().setIsCached("true");
	    Utilities.printInfoLog(msisdn + "-Received From customerDataInternalCall SuplemntraryResponseIN_IF_CACHE-"
		    + mapper.writeValueAsString(supplementaryOfferingsResponse), logger);
	    // return supplementaryOfferingsResponse;

	} else {
	    Utilities.printDebugLog(msisdn + "-SUPPLEMENTARY OFFERINGS" + Constants.CACHE_DOES_NOT_EXIST_DESCRIPTION
		    + supplementaryOfferingsCacheKey, logger);
	    SupplementaryOfferingsResponseData resData = new SupplementaryOfferingsResponseData();
	    RestClient rc = new RestClient();

	    String requestJsonESB = mapper.writeValueAsString(supplementaryOfferingsRequest);

	    String path = GetConfigurations.getESBRoute("getSupplementaryOfferings");
	    Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	    Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	    Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	    supplementaryOfferingsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	    String response = rc.getResponseFromESB(path, requestJsonESB);
	    supplementaryOfferingsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	    Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	    if (response != null && !response.isEmpty()) {
		// Converting String to JSON as String.
		// response = Utilities.stringToJSONString(response);

		// Logging ESB response code and description.
		supplementaryOfferingsResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB,
			response, supplementaryOfferingsResponse.getLogsReport()));

		if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		    resData = ParseSupplementaryOfferingsResponse.parseSupplementaryOfferingsData(msisdn,
			    Utilities.getValueFromJSON(response, "data"), resData);

		    // SupplementaryOfferingsResponseData response=TmProcessing(resData);

		    // Storing response in hashmap.

		    supplementaryOfferingsResponse.setData(resData);
		    Utilities.printInfoLog(
			    msisdn + "-Received From customerDataInternalCall SuplemntraryResponsesBeforeCache-"
				    + mapper.writeValueAsString(supplementaryOfferingsResponse),
			    logger);
		    AppCache.getHashmapSupplementaryOfferings().put(supplementaryOfferingsCacheKey,
			    supplementaryOfferingsResponse);
		    // Caching Time-stamp

		    Utilities.printInfoLog(msisdn
			    + "-Received From customerDataInternalCall SuplemntraryResponsesAFTERCache-"
			    + mapper.writeValueAsString(
				    AppCache.getHashmapSupplementaryOfferings().get(supplementaryOfferingsCacheKey)),
			    logger);
		    AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS,
			    Utilities.getTimeStampForCache(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS));

		    supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_True);
		    supplementaryOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    supplementaryOfferingsResponse.setResultDesc(GetMessagesMappings
			    .getMessageFromResourceBundle("success", supplementaryOfferingsRequest.getLang()));

		} else {
		    /*
		     * This else is needed just to show empty cards on mobile by sending success APP
		     * SERVER code
		     */
		    supplementaryOfferingsResponse.setData(resData);
		    supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_True);
		    supplementaryOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    supplementaryOfferingsResponse.setResultDesc(Utilities.getErrorMessageFromDB("supplementary.offers",
			    supplementaryOfferingsRequest.getLang(),
			    Utilities.getValueFromJSON(response, "returnCode")));
		}

	    } else {
		supplementaryOfferingsResponse.setData(resData);
		supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_False);
		supplementaryOfferingsResponse.setResultCode(Constants.API_FAILURE_CODE);

		supplementaryOfferingsResponse.setResultDesc(GetMessagesMappings
			.getMessageFromResourceBundle("connectivity.error", supplementaryOfferingsRequest.getLang()));
	    }

	}

	//
	// preparing customerInfo Request Packet
	CustomerInfoResponseData custInfoResponse = new CustomerInfoResponseData();
	CustomerInfoRequest customerInfoRequest = new CustomerInfoRequest();

	customerInfoRequest.setChannel(supplementaryOfferingsRequest.getChannel());
	customerInfoRequest.setiP(supplementaryOfferingsRequest.getiP());
	customerInfoRequest.setLang(supplementaryOfferingsRequest.getLang());
	customerInfoRequest.setMsisdn(supplementaryOfferingsRequest.getMsisdn());
	customerInfoRequest.setIsB2B(supplementaryOfferingsRequest.getIsB2B());

	Utilities.printInfoLog(msisdn + "-Received From customerDataInternalCall SuplemntraryResponseBeforeMatch-"
		+ mapper.writeValueAsString(supplementaryOfferingsResponse), logger);

	CustomerServicesBusiness customerBusiness = new CustomerServicesBusiness();
	custInfoResponse = customerBusiness.getCustomerData(msisdn, "", customerInfoRequest, custInfoResponse,
		Constants.TOKEN_CREATION_CALL_TYPE_EXTERNAL, "", "");
	ArrayList<SupplementryOfferingsData> offers = new ArrayList<SupplementryOfferingsData>();
	if (custInfoResponse.getResultCode().equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
	    Utilities.printDebugLog(msisdn + "-Received From customerDataInternalCall OfferingId-"
		    + custInfoResponse.getSupplementaryOfferingList().get(0).getOfferingId(), logger);
	    ArrayList<String> offeringId = new ArrayList<String>();

	    for (int i = 0; i < custInfoResponse.getSupplementaryOfferingList().size(); i++) {
		offeringId.add(custInfoResponse.getSupplementaryOfferingList().get(i).getOfferingId());
	    }
	    Utilities.printDebugLog(msisdn + "-Received From customerDataInternalCall OfferingIdS-" + offeringId,
		    logger);
	    if (supplementaryOfferingsResponse.getData() != null
		    && supplementaryOfferingsResponse.getData().getTm() != null
		    && supplementaryOfferingsResponse.getData().getTm().getOffers() != null) {
		Utilities.printDebugLog(msisdn + "-Received From customerDataInternalCall TM OFFERS YES found-",
			logger);
		for (int j = 0; j < supplementaryOfferingsResponse.getData().getTm().getOffers().size(); j++) {
		    Utilities.printDebugLog(msisdn + "" + supplementaryOfferingsResponse.getData().getTm().getOffers()
			    .get(j).getHeader().getPreReqOfferId(), logger);
		    Utilities.printDebugLog(msisdn + "-Received From customerDataInternalCall preReqOfferingIdFromTM-"
			    + supplementaryOfferingsResponse.getData().getTm().getOffers().get(j).getHeader()
				    .getPreReqOfferId(),
			    logger);
		    if (offeringId.contains(supplementaryOfferingsResponse.getData().getTm().getOffers().get(j)
			    .getHeader().getPreReqOfferId())) {
			Utilities.printDebugLog(
				msisdn + "-Received From customerDataInternalCall preReqOfferingIdFromTM YES-"
					+ supplementaryOfferingsResponse.getData().getTm().getOffers().get(j)
						.getHeader().getPreReqOfferId(),
				logger);
			offers.add(supplementaryOfferingsResponse.getData().getTm().getOffers().get(j));

		    } else {
			Utilities.printDebugLog(
				msisdn + "-Received From customerDataInternalCall preReqOffering NOT found-"
					+ supplementaryOfferingsResponse.getData().getTm().getOffers().get(j)
						.getHeader().getPreReqOfferId(),
				logger);

		    }
		}

		SupplementaryOfferingsResponse responseupdated = new SupplementaryOfferingsResponse(
			supplementaryOfferingsResponse);
		// responseupdated =

		responseupdated.getData().getTm().setOffers(offers);

		return responseupdated;
	    } else {
		SupplementaryOfferingsResponse responseupdated = new SupplementaryOfferingsResponse(
			supplementaryOfferingsResponse);
		//
		responseupdated.getData().getTm().setOffers(offers);

		return responseupdated;
	    }
	}

	else {
	    Utilities.printDebugLog(
		    msisdn + "-Received From customerDataInternalCall Failed" + custInfoResponse.getResultCode(),
		    logger);
	    supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_False);
	    supplementaryOfferingsResponse.setResultCode(Constants.API_FAILURE_CODE);
	    supplementaryOfferingsResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", supplementaryOfferingsRequest.getLang()));
	}
	Utilities.printDebugLog(
		"Supplementary  Response in Business: " + mapper.writeValueAsString(supplementaryOfferingsResponse),
		logger);
	Utilities.printDebugLog("Perform Sorting", logger);
	sortSupplementaryOfferings(supplementaryOfferingsResponse);
	return supplementaryOfferingsResponse;
    }

    public SupplementaryOfferingsResponse getSupplementaryOfferingsByIDs(String msisdn,
	    SupplementaryOfferingsByIDsRequest supplementaryOfferingsByIDsRequestst,
	    SupplementaryOfferingsResponse supplementaryOfferingsResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME
		+ " BY IDs BUSINESS with data-" + supplementaryOfferingsByIDsRequestst.toString(), logger);

	// Get key which will be used to store and retrieve data from hash map.
	// String supplementaryOfferingsCacheKey =
	// this.getKeyForCacheMySubscriptions(Constants.HASH_KEY_My_SUBSCRIPTIONS,
	// offeringId, supplementaryOfferingsByIDsRequestst);

	SupplementaryOfferingsResponseData resData = new SupplementaryOfferingsResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	// supplementaryOfferingsByIDsRequestst.getOfferingIds().add("1376368122");
	String requestJsonESB = mapper.writeValueAsString(supplementaryOfferingsByIDsRequestst);

	// String path =
	// GetConfigurations.getESBRoute("getSupplementaryOfferingsByIDs");
	// String path =
	// "http://10.220.48.129/MAG_DEV01/rest/V1/api/mySubscriptionsOfferingIds";
	String path = GetConfigurations.getConfigurationFromCache("magento.app.supplementary.url");
	// Configur"http://10.220.48.211/stgbakcell/rest/V1/api/mySubscriptionsOfferingIds";

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	supplementaryOfferingsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	supplementaryOfferingsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	if (response != null && !response.isEmpty()) {
	    // Converting String to JSON as String.
	    // response = Utilities.stringToJSONString(response);

	    // Logging ESB response code and description.
	    supplementaryOfferingsResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		    supplementaryOfferingsResponse.getLogsReport()));

	    // if (Utilities.getValueFromJSON(response,
	    // "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
	    if (Utilities.getValueFromJSON(response, "resultCode").equalsIgnoreCase("132")) {

		resData = ParseSupplementaryOfferingsResponse.parseSupplementaryOfferingsData(msisdn,
			Utilities.getValueFromJSON(response, "data"), resData);
		for (int i = 0; i < resData.getInternet().getOffers().size(); i++) {
		    GroupedOfferAttributes groupOffer = resData.getInternet().getOffers().get(i).getHeader()
			    .getOfferGroup();
		    if (groupOffer != null && groupOffer.getGroupName().equalsIgnoreCase("")
			    && groupOffer.getGroupValue().equalsIgnoreCase("")) {
			resData.getInternet().getOffers().get(i).getHeader().setOfferGroup(null);
		    }
		}
		supplementaryOfferingsResponse.setData(resData);

		// Storing response in hashmap.
		this.prepareCache(resData, supplementaryOfferingsByIDsRequestst);
		// Caching Time-stamp
		AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS,
			Utilities.getTimeStampForCache(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS));

		supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_True);
		supplementaryOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		supplementaryOfferingsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			supplementaryOfferingsByIDsRequestst.getLang()));

	    } else {
		/*
		 * This else is needed just to show empty cards on mobile by sending success APP
		 * SERVER code
		 */
		supplementaryOfferingsResponse.setData(resData);
		supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_True);
		supplementaryOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		supplementaryOfferingsResponse.setResultDesc(Utilities.getErrorMessageFromDB("supplementary.offers",
			supplementaryOfferingsByIDsRequestst.getLang(),
			Utilities.getValueFromJSON(response, "resultCode")));
	    }

	} else {
	    supplementaryOfferingsResponse.setData(resData);
	    supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_False);
	    supplementaryOfferingsResponse.setResultCode(Constants.API_FAILURE_CODE);

	    supplementaryOfferingsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
		    "connectivity.error", supplementaryOfferingsByIDsRequestst.getLang()));
	}

	return supplementaryOfferingsResponse;
    }

    // for phase 2
    public SupplementaryOfferingsResponse getSupplementaryOfferingsByIDsV2(String msisdn,
	    SupplementaryOfferingsByIDsRequest supplementaryOfferingsByIDsRequestst,
	    SupplementaryOfferingsResponse supplementaryOfferingsResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SUPPLEMENTARY_SERVICES_TRANSACTION_NAME
		+ " BY IDs BUSINESS with data-" + supplementaryOfferingsByIDsRequestst.toString(), logger);

	// Get key which will be used to store and retrieve data from hash map.
	// String supplementaryOfferingsCacheKey =
	// this.getKeyForCacheMySubscriptions(Constants.HASH_KEY_My_SUBSCRIPTIONS,
	// offeringId, supplementaryOfferingsByIDsRequestst);

	SupplementaryOfferingsResponseData resData = new SupplementaryOfferingsResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	// supplementaryOfferingsByIDsRequestst.getOfferingIds().add("1376368122");
	String requestJsonESB = mapper.writeValueAsString(supplementaryOfferingsByIDsRequestst);

	String path = GetConfigurations.getESBRoute("getSupplementaryOfferingsByIDsV2");
	// String path =
	// "http://10.220.48.129/MAG_DEV01/rest/V1/api/mySubscriptionsOfferingIds";
	// String path =
	// "http://10.220.48.211/stgbakcell/rest/V1/api/mySubscriptionsOfferingIds";

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	supplementaryOfferingsResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	supplementaryOfferingsResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	if (response != null && !response.isEmpty()) {
	    // Converting String to JSON as String.
	    // response = Utilities.stringToJSONString(response);

	    // Logging ESB response code and description.
	    supplementaryOfferingsResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		    supplementaryOfferingsResponse.getLogsReport()));

	    // if (Utilities.getValueFromJSON(response,
	    // "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData = ParseSupplementaryOfferingsResponse.parseSupplementaryOfferingsData(msisdn,
			Utilities.getValueFromJSON(response, "data"), resData);
		for (int i = 0; i < resData.getInternet().getOffers().size(); i++) {
		    GroupedOfferAttributes groupOffer = resData.getInternet().getOffers().get(i).getHeader()
			    .getOfferGroup();
		    if (groupOffer != null && groupOffer.getGroupName().equalsIgnoreCase("")
			    && groupOffer.getGroupValue().equalsIgnoreCase("")) {
			resData.getInternet().getOffers().get(i).getHeader().setOfferGroup(null);
		    }
		}
		supplementaryOfferingsResponse.setData(resData);

		// Storing response in hashmap.
		this.prepareCache(resData, supplementaryOfferingsByIDsRequestst);
		// Caching Time-stamp
		AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS,
			Utilities.getTimeStampForCache(Constants.HASH_KEY_SUPPLEMENTARY_OFFERINGS));

		supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_True);
		supplementaryOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		supplementaryOfferingsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success",
			supplementaryOfferingsByIDsRequestst.getLang()));

	    } else {
		/*
		 * This else is needed just to show empty cards on mobile by sending success APP
		 * SERVER code
		 */
		supplementaryOfferingsResponse.setData(resData);
		supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_True);
		supplementaryOfferingsResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		supplementaryOfferingsResponse.setResultDesc(Utilities.getErrorMessageFromDB("supplementary.offers",
			supplementaryOfferingsByIDsRequestst.getLang(),
			Utilities.getValueFromJSON(response, "resultCode")));
	    }

	} else {
	    supplementaryOfferingsResponse.setData(resData);
	    supplementaryOfferingsResponse.setCallStatus(Constants.Call_Status_False);
	    supplementaryOfferingsResponse.setResultCode(Constants.API_FAILURE_CODE);

	    supplementaryOfferingsResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
		    "connectivity.error", supplementaryOfferingsByIDsRequestst.getLang()));
	}

	return supplementaryOfferingsResponse;
    }

    private void prepareCache(SupplementaryOfferingsResponseData resData, SupplementaryOfferingsByIDsRequest lang) {
	for (int i = 0; i < resData.getCall().getOffers().size(); i++) {
	    AppCache.getHashmapMySubscriptions().put(this.getKeyForCacheMySubscriptions(
		    resData.getCall().getOffers().get(i).getHeader().getOfferingId(), lang), resData);
	}
	for (int i = 0; i < resData.getCampaign().getOffers().size(); i++) {
	    AppCache.getHashmapMySubscriptions().put(this.getKeyForCacheMySubscriptions(
		    resData.getCampaign().getOffers().get(i).getHeader().getOfferingId(), lang), resData);
	}
	for (int i = 0; i < resData.getHybrid().getOffers().size(); i++) {
	    AppCache.getHashmapMySubscriptions().put(this.getKeyForCacheMySubscriptions(
		    resData.getHybrid().getOffers().get(i).getHeader().getOfferingId(), lang), resData);
	}
	for (int i = 0; i < resData.getInternet().getOffers().size(); i++) {
	    AppCache.getHashmapMySubscriptions().put(this.getKeyForCacheMySubscriptions(
		    resData.getInternet().getOffers().get(i).getHeader().getOfferingId(), lang), resData);
	}
	for (int i = 0; i < resData.getSms().getOffers().size(); i++) {
	    AppCache.getHashmapMySubscriptions().put(this.getKeyForCacheMySubscriptions(
		    resData.getSms().getOffers().get(i).getHeader().getOfferingId(), lang), resData);
	}
	for (int i = 0; i < resData.getRoaming().getOffers().size(); i++) {
	    AppCache.getHashmapMySubscriptions().put(this.getKeyForCacheMySubscriptions(
		    resData.getRoaming().getOffers().get(i).getHeader().getOfferingId(), lang), resData);
	}
	for (int i = 0; i < resData.getTm().getOffers().size(); i++) {
	    AppCache.getHashmapMySubscriptions().put(this.getKeyForCacheMySubscriptions(
		    resData.getTm().getOffers().get(i).getHeader().getOfferingId(), lang), resData);
	}

    }

    public ChangeSupplementaryOfferingsResponse changeSupplementaryOffering(String msisdn,
	    ChangeSupplementaryOfferingsRequest changeSupplementaryOfferingRequest,
	    ChangeSupplementaryOfferingsResponse changeSupplementaryOfferingResponse) throws Exception {
	Utilities.printDebugLog(
		msisdn + "-Request received in " + Transactions.CHANGE_SUPPLIMENTRY_OFFERING_TRANSACTION_NAME
			+ " BUSINESS with data-" + changeSupplementaryOfferingRequest.toString(),
		logger);

	MySubscriptionsBusiness mySubscriptionsBusiness = new MySubscriptionsBusiness();
	MySubscriptionsRequest mySubscriptionsRequest = new MySubscriptionsRequest();
	SupplementaryOfferingsResponse mySubscriptionsResponse = new SupplementaryOfferingsResponse();

	// Preparing request for My Subscriptions.
	mySubscriptionsRequest.setChannel(changeSupplementaryOfferingRequest.getChannel());
	mySubscriptionsRequest.setiP(changeSupplementaryOfferingRequest.getiP());
	mySubscriptionsRequest.setLang(changeSupplementaryOfferingRequest.getLang());
	mySubscriptionsRequest.setMsisdn(changeSupplementaryOfferingRequest.getMsisdn());
	mySubscriptionsRequest.setOfferingName(changeSupplementaryOfferingRequest.getOfferingName());
	mySubscriptionsRequest.setIsB2B(changeSupplementaryOfferingRequest.getIsB2B());
	ChangeSupplementaryOfferingsResponseData resData = new ChangeSupplementaryOfferingsResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(changeSupplementaryOfferingRequest);

	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "offerName");

	String path = GetConfigurations.getESBRoute("changeSupplementaryOffer");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	changeSupplementaryOfferingResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	changeSupplementaryOfferingResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	if (response != null && !response.isEmpty()) {
	    // Logging ESB response code and description.
	    changeSupplementaryOfferingResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB,
		    response, changeSupplementaryOfferingResponse.getLogsReport()));

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData.setMessage(GetMessagesMappings.getMessageFromResourceBundle("success",
			changeSupplementaryOfferingRequest.getLang()));
		mySubscriptionsResponse = mySubscriptionsBusiness.getSubscriptions(msisdn, mySubscriptionsRequest,
			mySubscriptionsResponse);
		resData.setMySubscriptionsData(mySubscriptionsResponse.getData());

		changeSupplementaryOfferingResponse.setData(resData);
		changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_True);
		changeSupplementaryOfferingResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		changeSupplementaryOfferingResponse.setResultDesc(GetMessagesMappings
			.getMessageFromResourceBundle("success", changeSupplementaryOfferingRequest.getLang()));

	    } else {
		changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_False);
		changeSupplementaryOfferingResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		changeSupplementaryOfferingResponse.setResultDesc(Utilities.getErrorMessageFromDB(
			"change.supplementary.offer", changeSupplementaryOfferingRequest.getLang(),
			Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_False);
	    changeSupplementaryOfferingResponse.setResultCode(Constants.API_FAILURE_CODE);
	    changeSupplementaryOfferingResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", changeSupplementaryOfferingRequest.getLang()));
	}

	return changeSupplementaryOfferingResponse;

    }

    public ChangeSupplementaryOfferingsResponsebulk changeSupplementaryOfferingbulk(String msisdn,
	    ChangeSupplementaryOfferingsRequestBulk changeSupplementaryOfferingRequest,
	    ChangeSupplementaryOfferingsResponsebulk changeSupplementaryOfferingResponse) throws Exception {
	Utilities.printDebugLog(
		msisdn + "-Request received in " + Transactions.CHANGE_SUPPLIMENTRY_OFFERING_BULK_TRANSACTION_NAME
			+ " BUSINESS with data-" + changeSupplementaryOfferingRequest.toString(),
		logger);

	ChangeSupplementaryOfferingsResponseDatabulk resData = new ChangeSupplementaryOfferingsResponseDatabulk();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(changeSupplementaryOfferingRequest);

	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "offerName");

	String path = GetConfigurations.getESBRoute("insertorders");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	changeSupplementaryOfferingResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	changeSupplementaryOfferingResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	if (response != null && !response.isEmpty()) {
	    // Logging ESB response code and description.
	    changeSupplementaryOfferingResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB,
		    response, changeSupplementaryOfferingResponse.getLogsReport()));

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData.setMessage(GetMessagesMappings.getMessageFromResourceBundle("success",
			changeSupplementaryOfferingRequest.getLang()));

		changeSupplementaryOfferingResponse.setData(resData);
		changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_True);
		changeSupplementaryOfferingResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		changeSupplementaryOfferingResponse.setResultDesc(GetMessagesMappings
			.getMessageFromResourceBundle("success", changeSupplementaryOfferingRequest.getLang()));

	    } else {

		changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_False);
		changeSupplementaryOfferingResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		changeSupplementaryOfferingResponse.setResultDesc(Utilities.getErrorMessageFromDB(
			"change.supplementary.offer", changeSupplementaryOfferingRequest.getLang(),
			Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    changeSupplementaryOfferingResponse.setCallStatus(Constants.Call_Status_False);
	    changeSupplementaryOfferingResponse.setResultCode(Constants.API_FAILURE_CODE);
	    changeSupplementaryOfferingResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", changeSupplementaryOfferingRequest.getLang()));
	}

	return changeSupplementaryOfferingResponse;

    }

    private String getKeyForCache(String hashKeySupplementaryOfferings,
	    SupplementaryOfferingsRequest supplementaryOfferingsRequest) {
	return hashKeySupplementaryOfferings + "." + supplementaryOfferingsRequest.getOfferingName() + "."
		+ supplementaryOfferingsRequest.getBrandName() + "." + supplementaryOfferingsRequest.getLang();
    }

    private String getKeyForCacheMySubscriptions(String offeringID,
	    SupplementaryOfferingsByIDsRequest supplementaryOfferingsByIDsRequest) {
	return Constants.HASH_KEY_My_SUBSCRIPTIONS + "." + offeringID + "."
		+ supplementaryOfferingsByIDsRequest.getLang();
    }

    private void sortSupplementaryOfferings(SupplementaryOfferingsResponse supplementaryOfferingsResponse) {

	if (supplementaryOfferingsResponse != null && supplementaryOfferingsResponse.getData() != null) {

	    // Sorting Internet offers
	    if (supplementaryOfferingsResponse.getData().getInternet() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getInternet()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting Compaign offers
	    if (supplementaryOfferingsResponse.getData().getCampaign() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getCampaign()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting SMS offers
	    if (supplementaryOfferingsResponse.getData().getSms() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getSms().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting SMS offers
	    if (supplementaryOfferingsResponse.getData().getCall() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getCall().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting TM offers
	    if (supplementaryOfferingsResponse.getData().getTm() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getTm().getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting Hybrid offers
	    if (supplementaryOfferingsResponse.getData().getHybrid() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getHybrid()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    // Sorting Roaming offers
	    if (supplementaryOfferingsResponse.getData().getRoaming() != null) {
		List<SupplementryOfferingsData> offers = supplementaryOfferingsResponse.getData().getRoaming()
			.getOffers();
		if (offers != null && offers.size() > 0) {
		    Collections.sort(offers, new Comparator<SupplementryOfferingsData>() {
			@Override
			public int compare(SupplementryOfferingsData o1, SupplementryOfferingsData o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	}
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.business;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.AppCache;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.tariffdetails.ParseTariffsResponse;
import com.evampsaanga.bakcell.models.tariffdetails.ParseTariffsResponseV2;
import com.evampsaanga.bakcell.models.tariffdetails.TariffRequest;
import com.evampsaanga.bakcell.models.tariffdetails.TariffRequestV2;
import com.evampsaanga.bakcell.models.tariffdetails.TariffResponse;
import com.evampsaanga.bakcell.models.tariffdetails.TariffResponseData;
import com.evampsaanga.bakcell.models.tariffdetails.TariffResponseDataV2;
import com.evampsaanga.bakcell.models.tariffdetails.TariffResponseV2;
import com.evampsaanga.bakcell.models.tariffdetails.changetariff.ChangeTariffRequest;
import com.evampsaanga.bakcell.models.tariffdetails.changetariff.ChangeTariffRequestBulk;
import com.evampsaanga.bakcell.models.tariffdetails.changetariff.ChangeTariffResponse;
import com.evampsaanga.bakcell.models.tariffdetails.changetariff.ChangeTariffResponseBulk;
import com.evampsaanga.bakcell.models.tariffdetails.changetariff.ChangeTariffResponseData;
import com.evampsaanga.bakcell.models.tariffdetails.postpaid.corporate.Corporate;
import com.evampsaanga.bakcell.models.tariffdetails.postpaid.individual.Individual;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.Cin;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.description.Classification;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass.Klass;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
@Service
public class TariffBusiness {
    Logger logger = Logger.getLogger(TariffBusiness.class);

    public TariffResponse getTariffDetailsBusiness(String msisdn, TariffRequest tariffRequest,
	    TariffResponse tariffResponse) throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.TARRIF_DETAILS_TRANSACTION_NAME
		+ " BUSINESS with data-" + tariffRequest.toString(), logger);

	// Get key which will be used to store and retrieve data from hash map.
	String tariffsCacheKey = this.getKeyForCache(Constants.HASH_KEY_TARIFFS, tariffRequest);

	if (AppCache.getHashmapTariffs().containsKey(tariffsCacheKey)) {
	    Utilities.printDebugLog(msisdn + "-TARIFFS" + Constants.CACHE_EXISTS_DESCRIPTION + "" + tariffsCacheKey,
		    logger);
	    tariffResponse = AppCache.getHashmapTariffs().get(tariffsCacheKey);
	    tariffResponse.getLogsReport().setIsCached("true");
	    return tariffResponse;

	} else {
	    Utilities.printDebugLog(
		    msisdn + "-TARIFFS" + Constants.CACHE_DOES_NOT_EXIST_DESCRIPTION + "" + tariffsCacheKey, logger);
	    TariffResponseData resData = new TariffResponseData();
	    RestClient rc = new RestClient();
	    ObjectMapper mapper = new ObjectMapper();
	    // need to remove
	    // TODO
	    // comment these 2 ifs
	    if (tariffRequest.getCustomerType().contains("Individual")) {
		tariffRequest.setCustomerType("Individual");
	    }
	    if (tariffRequest.getCustomerType().contains("Corporate")) {
		tariffRequest.setCustomerType("Corporate");
	    }

	    String requestJsonESB = mapper.writeValueAsString(tariffRequest);

	    String path = GetConfigurations.getESBRoute("gettariffDetialsUpdated");
	    Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	    Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	    Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	    tariffResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	    String response = rc.getResponseFromESB(path, requestJsonESB);
	    tariffResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	    Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	    if (response != null && !response.isEmpty()) {
		// Converting String to JSON as String.
		// response = Utilities.stringToJSONString(response);

		// Logging ESB response code and description.
		tariffResponse.setLogsReport(
			Utilities.logESBParamsintoReportLog(requestJsonESB, response, tariffResponse.getLogsReport()));

		if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		    resData = ParseTariffsResponse.parseTariffResponseData(msisdn,
			    Utilities.getValueFromJSON(response, "data"), resData);

		    tariffResponse.setData(resData);

		    // Storing response in hashmap.
		    AppCache.getHashmapTariffs().put(tariffsCacheKey, tariffResponse);

		    // Caching Time-stamp
		    AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_TARIFFS,
			    Utilities.getTimeStampForCache(Constants.HASH_KEY_TARIFFS));

		    tariffResponse.setCallStatus(Constants.Call_Status_True);
		    tariffResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		    tariffResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", tariffRequest.getLang()));

		} else {
		    tariffResponse.setCallStatus(Constants.Call_Status_False);
		    tariffResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    tariffResponse.setResultDesc(Utilities.getErrorMessageFromDB("get.tariffs", tariffRequest.getLang(),
			    Utilities.getValueFromJSON(response, "returnCode")));
		}
	    } else {

		tariffResponse.setCallStatus(Constants.Call_Status_False);
		tariffResponse.setResultCode(Constants.API_FAILURE_CODE);

		tariffResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
			tariffRequest.getLang()));
	    }

	    Utilities.printDebugLog("Tariff Response in Business: " + mapper.writeValueAsString(tariffResponse),
		    logger);
	    Utilities.printDebugLog("Perform Sorting", logger);
	    sortTariffResponse(tariffResponse);

	    if (tariffResponse.getData() != null && tariffResponse.getData().getPostpaid() != null
		    && tariffResponse.getData().getPostpaid().getIndividual() != null) {
		for (int i = 0; i < tariffResponse.getData().getPostpaid().getIndividual().size(); i++) {
		    if (tariffResponse.getData().getPostpaid().getIndividual().get(i).getDescription() != null) {
			if (tariffResponse.getData().getPostpaid().getIndividual().get(i).getDescription()
				.getClassification() == null) {
			    tariffResponse.getData().getPostpaid().getIndividual().get(i).getDescription()
				    .setClassification(new Classification());
			}
		    }
		}
	    }

	    if (tariffResponse.getData() != null && tariffResponse.getData().getPostpaid() != null
		    && tariffResponse.getData().getPostpaid().getCorporate() != null) {
		for (int i = 0; i < tariffResponse.getData().getPostpaid().getCorporate().size(); i++) {
		    if (tariffResponse.getData().getPostpaid().getCorporate().get(i).getDescription() != null) {
			if (tariffResponse.getData().getPostpaid().getCorporate().get(i).getDescription()
				.getClassification() == null) {
			    tariffResponse.getData().getPostpaid().getCorporate().get(i).getDescription()
				    .setClassification(new Classification());
			}
		    }
		}
	    }
	    return tariffResponse;
	}

    }

    public TariffResponseV2 getTariffDetailsBusinessV2(String msisdn, TariffRequestV2 tariffRequest,
	    TariffResponseV2 tariffResponse) throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.TARRIF_DETAILS_TRANSACTION_NAME
		+ " BUSINESS with data-" + tariffRequest.toString(), logger);

	// Get key which will be used to store and retrieve data from hash map.
	String tariffsCacheKey = this.getKeyForCacheV2(Constants.HASH_KEY_TARIFFS, tariffRequest);

	if (AppCache.getHashmapTariffs().containsKey(tariffsCacheKey)) {
	    Utilities.printDebugLog(msisdn + "-TARIFFS" + Constants.CACHE_EXISTS_DESCRIPTION + "" + tariffsCacheKey,
		    logger);
	    tariffResponse = AppCache.getHashmapTariffsV2().get(tariffsCacheKey);
	    tariffResponse.getLogsReport().setIsCached("true");
	    return tariffResponse;

	} else {
	    Utilities.printDebugLog(
		    msisdn + "-TARIFFS" + Constants.CACHE_DOES_NOT_EXIST_DESCRIPTION + "" + tariffsCacheKey, logger);
	    TariffResponseDataV2 resData = new TariffResponseDataV2();
	    RestClient rc = new RestClient();
	    ObjectMapper mapper = new ObjectMapper();

	    /*
	     * if (tariffRequest.getCustomerType().contains("Individual")) {
	     * tariffRequest.setCustomerType("Individual"); } if
	     * (tariffRequest.getCustomerType().contains("Corporate")) {
	     * tariffRequest.setCustomerType("Corporate"); }
	     */

	    String requestJsonESB = mapper.writeValueAsString(tariffRequest);

	    String path = GetConfigurations.getESBRoute("gettariffDetialsV2");
	    Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	    Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	    Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	    tariffResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	    String response = rc.getResponseFromESB(path, requestJsonESB);
	    tariffResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	    Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	    if (response != null && !response.isEmpty()) {

		// Converting String to JSON as String.
		// response = Utilities.stringToJSONString(response);

		// Logging ESB response code and description.
		tariffResponse.setLogsReport(
			Utilities.logESBParamsintoReportLog(requestJsonESB, response, tariffResponse.getLogsReport()));

		if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		    resData = ParseTariffsResponseV2.parseTariffResponseData(msisdn,
			    Utilities.getValueFromJSON(response, "data"), resData);

		    tariffResponse.setData(resData);

		    // Storing response in hashmap.
		    AppCache.getHashmapTariffsV2().put(tariffsCacheKey, tariffResponse);

		    // Caching Time-stamp
		    AppCache.getHashmapTimestamps().put(Constants.HASH_KEY_TARIFFS,
			    Utilities.getTimeStampForCache(Constants.HASH_KEY_TARIFFS));

		    tariffResponse.setCallStatus(Constants.Call_Status_True);
		    tariffResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		    tariffResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", tariffRequest.getLang()));

		} else {
		    tariffResponse.setCallStatus(Constants.Call_Status_False);
		    tariffResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    tariffResponse.setResultDesc(Utilities.getErrorMessageFromDB("get.tariffs", tariffRequest.getLang(),
			    Utilities.getValueFromJSON(response, "returnCode")));
		}
	    } else {

		tariffResponse.setCallStatus(Constants.Call_Status_False);
		tariffResponse.setResultCode(Constants.API_FAILURE_CODE);

		tariffResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
			tariffRequest.getLang()));
	    }
	    logger.info("Response Returned from business-" + mapper.writeValueAsString(tariffResponse));
	    return tariffResponse;
	}

    }

    public ChangeTariffResponse changeTariffBusiness(String msisdn, ChangeTariffRequest changeTariffRequest,
	    ChangeTariffResponse changeTariffResponse)
	    throws JSONException, FileNotFoundException, IOException, SQLException {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.CHANGE_TARRIF_TRANSACTION_NAME
		+ " BUSINESS with data-" + changeTariffRequest.toString(), logger);

	ChangeTariffResponseData resData = new ChangeTariffResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(changeTariffRequest);

	String path = GetConfigurations.getESBRoute("changeTariff");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "tariffName");
	changeTariffResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	changeTariffResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	changeTariffResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, changeTariffResponse.getLogsReport()));
	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData.setMessage(
			GetMessagesMappings.getMessageFromResourceBundle("success", changeTariffRequest.getLang()));

		changeTariffResponse.setData(resData);
		changeTariffResponse.setCallStatus(Constants.Call_Status_True);
		changeTariffResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		changeTariffResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", changeTariffRequest.getLang()));

	    } else {

		changeTariffResponse.setCallStatus(Constants.Call_Status_False);
		changeTariffResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		changeTariffResponse.setResultDesc(Utilities.getErrorMessageFromDB("change.tariff",
			changeTariffRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));

	    }
	} else {

	    changeTariffResponse.setCallStatus(Constants.Call_Status_False);
	    changeTariffResponse.setResultCode(Constants.API_FAILURE_CODE);
	    changeTariffResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    changeTariffRequest.getLang()));
	}

	return changeTariffResponse;
    }

    // START OF changeTariff BULK B2B

    public ChangeTariffResponseBulk changeTariffBulkBusiness(String msisdn, ChangeTariffRequestBulk changeTariffRequest,
	    ChangeTariffResponseBulk changeTariffResponse)
	    throws JSONException, FileNotFoundException, IOException, SQLException {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.CHANGE_TARRIF_TRANSACTION_NAME
		+ " BUSINESS with data-" + changeTariffRequest.toString(), logger);

	ChangeTariffResponseData resData = new ChangeTariffResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(changeTariffRequest);

	String path = GetConfigurations.getESBRoute("insertorders");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "tariffName");
	changeTariffResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	changeTariffResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	changeTariffResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, changeTariffResponse.getLogsReport()));
	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData.setMessage(
			GetMessagesMappings.getMessageFromResourceBundle("success", changeTariffRequest.getLang()));

		changeTariffResponse.setData(resData);
		changeTariffResponse.setCallStatus(Constants.Call_Status_True);
		changeTariffResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		changeTariffResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", changeTariffRequest.getLang()));

	    } else {

		changeTariffResponse.setCallStatus(Constants.Call_Status_False);
		changeTariffResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		changeTariffResponse.setResultDesc(Utilities.getErrorMessageFromDB("change.tariff",
			changeTariffRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));

	    }
	} else {

	    changeTariffResponse.setCallStatus(Constants.Call_Status_False);
	    changeTariffResponse.setResultCode(Constants.API_FAILURE_CODE);
	    changeTariffResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    changeTariffRequest.getLang()));
	}

	return changeTariffResponse;
    }

    // END Of changeTariff BULK B2B

    private String getKeyForCacheV2(String hashKeyTariffs, TariffRequestV2 tariffRequest) {

	return hashKeyTariffs + /* "." + tariffRequest.getCustomerType() + "." + */ tariffRequest
		.getLang() /*
			    * + "." + tariffRequest.getSubscribedOfferingName() + "." +
			    * tariffRequest.getSubscriberType()
			    */;
    }

    // TODO
    // need to change this as pr commented for phase2
    private String getKeyForCache(String hashKeyTariffs, TariffRequest tariffRequest) {

	return hashKeyTariffs + "." + tariffRequest.getCustomerType() + "." + tariffRequest.getStoreId() + "."
		+ tariffRequest.getSubscribedOfferingName() + "." + tariffRequest.getSubscriberType();
    }

    private void sortTariffResponse(TariffResponse tariffResponse) {
	TariffResponseData tariffResponseData = tariffResponse.getData();
	if (tariffResponseData != null) {
	    // Sort prepaid tariff
	    if (tariffResponseData.getPrepaid() != null) {
		// Sort prepaid cin tariff
		if (tariffResponseData.getPrepaid().getCin() != null
			&& tariffResponseData.getPrepaid().getCin().size() > 0) {
		    Collections.sort(tariffResponseData.getPrepaid().getCin(), new Comparator<Cin>() {
			@Override
			public int compare(Cin o1, Cin o2) {

			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
		// Sort prepaid klass tariff
		if (tariffResponseData.getPrepaid().getKlass() != null
			&& tariffResponseData.getPrepaid().getKlass().size() > 0) {
		    Collections.sort(tariffResponseData.getPrepaid().getKlass(), new Comparator<Klass>() {
			@Override
			public int compare(Klass o1, Klass o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }

	    if (tariffResponseData.getPostpaid() != null) {
		// Sort postpaid Corporate tariff
		if (tariffResponseData.getPostpaid().getCorporate() != null
			&& tariffResponseData.getPostpaid().getCorporate().size() > 0) {
		    Collections.sort(tariffResponseData.getPostpaid().getCorporate(), new Comparator<Corporate>() {
			@Override
			public int compare(Corporate o1, Corporate o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}

		// Sort postpaid Individual tariff
		if (tariffResponseData.getPostpaid().getIndividual() != null
			&& tariffResponseData.getPostpaid().getIndividual().size() > 0) {
		    Collections.sort(tariffResponseData.getPostpaid().getIndividual(), new Comparator<Individual>() {
			@Override
			public int compare(Individual o1, Individual o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}

		// Sort postpaid Klass tariff
		if (tariffResponseData.getPostpaid().getKlassPostpaid() != null
			&& tariffResponseData.getPostpaid().getKlassPostpaid().size() > 0) {
		    Collections.sort(tariffResponseData.getPostpaid().getKlassPostpaid(), new Comparator<Klass>() {
			@Override
			public int compare(Klass o1, Klass o2) {
			    if (o1.getHeader() != null && o2.getHeader() != null
				    && o1.getHeader().getSortOrder() != null && o2.getHeader().getSortOrder() != null) {
				return o1.getHeader().getSortOrder().compareTo(o2.getHeader().getSortOrder());
			    } else {
				return Constants.MAX_INT;
			    }
			}
		    });
		}
	    }
	}
    }
}
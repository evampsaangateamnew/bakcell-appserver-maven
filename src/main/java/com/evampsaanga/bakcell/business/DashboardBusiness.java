/**
 * 
 */
package com.evampsaanga.bakcell.business;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.customerservices.forgotpassword.ForgotPasswordResponseDataV2;
import com.evampsaanga.bakcell.models.customerservices.forgotpassword.ForgotPasswordResponseV2;
import com.evampsaanga.bakcell.models.dashboardservice.DashboardRequest;
import com.evampsaanga.bakcell.models.dashboardservice.DashboardResponse;
import com.evampsaanga.bakcell.models.dashboardservice.DashboardResponseData;
import com.evampsaanga.bakcell.models.dashboardservice.LoginData;
import com.evampsaanga.bakcell.models.dashboardservice.balancepic.BalancePicData;
import com.evampsaanga.bakcell.models.dashboardservice.queryinvoice.QueryInvoiceData;
import com.evampsaanga.bakcell.models.dashboardservice.usersgroup.UserGroupData;
import com.evampsaanga.bakcell.models.dashboardservice.usersgroup.UserGroupRequest;
import com.evampsaanga.bakcell.models.dashboardservice.usersgroup.UserGroupResponseBase;
import com.evampsaanga.bakcell.models.dashboardservice.usersgroup.UsersGroupData;
import com.evampsaanga.bakcell.models.dashboardservice.usersgroup.UsersGroupResponse;
import com.evampsaanga.bakcell.models.dashboardservice.usersgroup.UsersGroupResponseData;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.PredefinedDataResponseDataV2;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
@Service
public class DashboardBusiness {
    Logger logger = Logger.getLogger(DashboardBusiness.class);

    public DashboardResponse getDashboardbusiness(String isFromB2B, String msisdn, String deviceID,
	    DashboardRequest dashboardRequest, DashboardResponse dashboardResponse, String passHash)
	    throws ParseException, Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.HOME_PAGE_TRANSACTION_NAME
		+ " BUSINESS with data-" + dashboardRequest.toString(), logger);

	DashboardResponseData resData = new DashboardResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(dashboardRequest);
	// requestJsonESB.concat("%" + isFromB2B);
	/*
	 * Redefining request PARAMS for ESB request, As request params are different at
	 * Mobile and ESB end.
	 */
	// requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB,
	// "subscriberType");
	// requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB,
	// "customerType", subscriberType);

	String path = GetConfigurations.getESBRoute("getdashboardinfo");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	dashboardResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	logger.info("E&S -- Request Land Time in Business" + new Date());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	logger.info("E&S -- Response Time in Business" + new Date());
	dashboardResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	dashboardResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, dashboardResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		// mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
		// false);
		logger.info("E&S -- Before Parsing Time in Business " + new Date());
		resData = parsejson(msisdn, deviceID, response, dashboardResponse, dashboardRequest.getLang(), passHash,
			dashboardRequest.getIsB2B());
		logger.info("E&S -- After Parseing Time in Business " + new Date());
		if ("400".equals(resData.getReturnCode()) && resData.getLoginData().equals(null)) {
		    dashboardResponse.setCallStatus(Constants.Call_Status_False);
		    dashboardResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    dashboardResponse.setResultDesc(Utilities.getErrorMessageFromDB("home.page",
			    dashboardRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
		    dashboardResponse.setData(null);
		} else if (resData.getReturnCode().equalsIgnoreCase(Constants.INVALID_TOKEN)) {
		    dashboardResponse.setData(resData);
		    dashboardResponse.setCallStatus(Constants.Call_Status_False);
		    dashboardResponse.setResultCode(Constants.INVALID_TOKEN);
		    dashboardResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", dashboardRequest.getLang()));

		} else {
		    dashboardResponse.setData(resData);
		    dashboardResponse.setCallStatus(Constants.Call_Status_True);
		    dashboardResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    dashboardResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", dashboardRequest.getLang()));

		}

	    } else {
		dashboardResponse.setCallStatus(Constants.Call_Status_False);
		dashboardResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		dashboardResponse.setResultDesc(Utilities.getErrorMessageFromDB("home.page",
			dashboardRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
		dashboardResponse.setData(null);
	    }
	} else {
	    dashboardResponse.setCallStatus(Constants.Call_Status_False);
	    dashboardResponse.setResultCode(Constants.API_FAILURE_CODE);
	    dashboardResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", dashboardRequest.getLang()));
	    dashboardResponse.setData(null);
	}
	logger.info("E&S -- Response Dispatch Time in Business" + new Date());
	return dashboardResponse;
    }

    public UserGroupResponseBase getuserGroupData(String isFromB2B, String msisdn, String deviceID,
	    UserGroupRequest userGroupRequest) throws ParseException, Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.USER_GROUP_DATA_TRANSACTION_NAME
		+ " BUSINESS with data-" + userGroupRequest.toString(), logger);
	UsersGroupResponse usersGroupResponse = new UsersGroupResponse();
	UserGroupResponseBase userGroupResponseBase = new UserGroupResponseBase();
	UsersGroupResponse resData = new UsersGroupResponse();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(userGroupRequest);

	String path = GetConfigurations.getESBRoute("getusergroupdata");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	userGroupResponseBase.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	logger.info("E&S -- Request Land Time in Business" + new Date());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	logger.info("E&S -- Response Time in Business" + new Date());
	userGroupResponseBase.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	userGroupResponseBase.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, userGroupResponseBase.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		// mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
		// false);
		logger.info("E&S -- Before Parsing Time in Business " + new Date());
		resData = parsejsonforGroupData(msisdn, deviceID, response, usersGroupResponse);
		logger.info("E&S -- After Parseing Time in Business " + new Date());
		userGroupResponseBase.setData(resData);
		userGroupResponseBase.setCallStatus(Constants.Call_Status_True);
		userGroupResponseBase.setResultCode(Constants.VALIDATION_SUCCESS_CODE);
		userGroupResponseBase.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", userGroupRequest.getLang()));
	    } else {
		userGroupResponseBase.setCallStatus(Constants.Call_Status_False);
		userGroupResponseBase.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		userGroupResponseBase.setResultDesc(Utilities.getErrorMessageFromDB("home.page",
			userGroupRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
		userGroupResponseBase.setData(null);
	    }
	} else {
	    userGroupResponseBase.setCallStatus(Constants.Call_Status_False);
	    userGroupResponseBase.setResultCode(Constants.API_FAILURE_CODE);
	    userGroupResponseBase.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", userGroupRequest.getLang()));
	    userGroupResponseBase.setData(null);
	}
	logger.info("E&S -- Response Dispatch Time in Business" + new Date());
	return userGroupResponseBase;
    }

    private UsersGroupResponse parsejsonforGroupData(String msisdn, String deviceID, String response,
	    UsersGroupResponse usersGroupResponse) throws JSONException, JsonProcessingException {
	ObjectMapper mapper = new ObjectMapper();
	JSONObject jsonObject = new JSONObject(response);
	JSONObject obj = new JSONObject();
	try {
	    obj = jsonObject.getJSONObject("groupData");
	    @SuppressWarnings("rawtypes")
	    Iterator key = obj.keys();
	    key = obj.keys();
	    UserGroupData userGroups = new UserGroupData();

	    ArrayList<UsersGroupResponseData> usersGroupResponseData = new ArrayList<>();
	    logger.info("E&S -- Response time before calculating group data " + new Date());
	    while (key.hasNext()) {

		ArrayList<UsersGroupData> usersGroupData = new ArrayList<>();
		String currentkey = (String) key.next();
		JSONArray jsonarr = obj.getJSONObject(currentkey).getJSONArray("usersGroupData");
		String groupName = obj.getJSONObject(currentkey).getString("groupName");
		UsersGroupResponseData resGroupData = new UsersGroupResponseData();
		resGroupData.setGroupName(groupName);
		int count = 0;
		// resGroupData.setGroupName(currentkey);
		for (int j = 0; j < jsonarr.length(); j++) {
		    count++;
		    usersGroupData.add(mapper.readValue((jsonarr.getString(j)), UsersGroupData.class));
		    // System.out.println(usersGroupData[i]);
		}
		resGroupData.setUserCount(String.valueOf(count));
		resGroupData.setUsersData(usersGroupData);
		usersGroupResponseData.add(resGroupData);
		// usersGroupResponse.setUserGroups(userGroups);(usersGroupResponseData);
	    }
	    userGroups.setUsersGroupData(usersGroupResponseData);
	    usersGroupResponse.setGroupData(userGroups);
	    logger.info("E&S -- Response time after calculating group data " + new Date());
	    ArrayList<UsersGroupData> users = new ArrayList<>();
	    ArrayList<UsersGroupResponseData> userGroups1 = usersGroupResponse.getGroupData().getUsersGroupData();
	    for (int i = 0; i < userGroups1.size(); i++) {
		users.addAll(userGroups1.get(i).getUsersData());
	    }
	    usersGroupResponse.setUsers(users);
	    logger.info("E&S -- Response time after calculating user group data " + new Date());
	} catch (Exception e) {
	    logger.error("ERROR", e);
	    usersGroupResponse.setGroupData(null);
	}
	logger.info("User Group Response :" + mapper.writeValueAsString(usersGroupResponse));
	return usersGroupResponse;
    }

    public ForgotPasswordResponseV2 getloginBusiness(String isFromB2B, String msisdn, String deviceID,
	    DashboardRequest dashboardRequest, ForgotPasswordResponseV2 forgotPasswordResponse)
	    throws ParseException, Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.HOME_PAGE_TRANSACTION_NAME
		+ " BUSINESS with data-" + dashboardRequest.toString(), logger);

	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(dashboardRequest);
	requestJsonESB.concat("%" + isFromB2B);
	/*
	 * Redefining request PARAMS for ESB request, As request params are different at
	 * Mobile and ESB end.
	 */
	// requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB,
	// "subscriberType");
	// requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB,
	// "customerType", subscriberType);

	String path = GetConfigurations.getESBRoute("loginUser");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	forgotPasswordResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	forgotPasswordResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	forgotPasswordResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, forgotPasswordResponse.getLogsReport()));
	LoginData loginRes = new LoginData();
	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		// mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
		// false);

		loginRes = mapper.readValue(Utilities.getValueFromJSON(response, "loginData"), LoginData.class);
		// PredefinedDataResponseDataV2 predefinedData =
		// mapper.readValue(Utilities.getValueFromJSON(response,
		// "predefinedData"), PredefinedDataResponseDataV2.class);
		String emailAndPassHash = "";
		Utilities.printDebugLog(msisdn + "<<<<<<< Email and password hash Befor IF ELSE", logger);
		if (dashboardRequest.getIsB2B().equalsIgnoreCase("true")) {
		    logger.info("<<<<<<<< Email and password hash Before IF  B2B is");
		    emailAndPassHash = Utilities.getEmailByEntityIdV2(loginRes.getEntity_id(), loginRes.getMsisdn(),
			    loginRes.getCustomer_id());
		    logger.info("<<<<<<<< Email and password hash B2B is:" + emailAndPassHash);
		} else {
		    logger.info("<<<<<<<< Email and password hash Before IF B2C is");
		    emailAndPassHash = Utilities.getEmailByEntityId(loginRes.getEntity_id(), loginRes.getMsisdn(),
			    loginRes.getCustomer_id());
		    logger.info("<<<<<<<<< Email and password hash B2C is:" + emailAndPassHash);
		}
		loginRes.setToken(Utilities.prepareToken(msisdn, deviceID, emailAndPassHash.split(";")[1]));
		// resData.setLoginData(loginData);
		ForgotPasswordResponseDataV2 dataV2 = new ForgotPasswordResponseDataV2();
		dataV2.setLoginData(loginRes);
		forgotPasswordResponse.setCallStatus(Constants.Call_Status_True);
		forgotPasswordResponse.setResultCode(Constants.VALIDATION_SUCCESS_CODE);
		// forgotPasswordResponse.setResultDesc(resultDesc);
		forgotPasswordResponse.setData(dataV2);

	    } else {
		forgotPasswordResponse.setCallStatus(Constants.Call_Status_False);
		forgotPasswordResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		forgotPasswordResponse.setResultDesc(Utilities.getErrorMessageFromDB("home.page",
			dashboardRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
		forgotPasswordResponse.setData(null);
	    }
	} else {
	    forgotPasswordResponse.setCallStatus(Constants.Call_Status_False);
	    forgotPasswordResponse.setResultCode(Constants.API_FAILURE_CODE);
	    forgotPasswordResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", dashboardRequest.getLang()));
	    forgotPasswordResponse.setData(null);
	}

	return forgotPasswordResponse;
    }

    private DashboardResponseData parsejson(String msisdn, String deviceID, String response,
	    DashboardResponse dashboardResponse, String lang, String passHash, String isB2B)
	    throws JSONException, JsonParseException, JsonMappingException, IOException {
	DashboardResponseData dashboardResponseData = new DashboardResponseData();
	JSONObject jsonObject = new JSONObject(response);
	// logger.info("E&S -- Response in parser " + response);
	ObjectMapper mapper = new ObjectMapper();
	// String loginData = json.getJSONObject("loginData").toString();
	LoginData loginRes = new LoginData();
	PredefinedDataResponseDataV2 predefinedData = new PredefinedDataResponseDataV2();
	String userCount = jsonObject.getString("userCount");
	dashboardResponseData.setUserCount(userCount);
	try {
	    loginRes = mapper.readValue(Utilities.getValueFromJSON(jsonObject.toString(), "loginData"),
		    LoginData.class);
	    loginRes.setCustom_profile_image(GetConfigurations.getConfigurationFromCache("profileImageDownloadLink")
		    + loginRes.getCustom_profile_image());
	    predefinedData = mapper.readValue(Utilities.getValueFromJSON(jsonObject.toString(), "predefinedData"),
		    PredefinedDataResponseDataV2.class);
	    if(lang.equalsIgnoreCase("4"))
	    	predefinedData.setContent("<div class=\"term\"> <div class=\"termHead\"> <h2>Məqsəd</h2> </div> <div class=\"termBody\"> <p>Bu bildiriş abunə&ccedil;iləri elektron formada g&ouml;stəriləcək m&uuml;vafiq xidmətlərin şərtləri barədə məlumatlandırmaq, habelə bu xidmətlərin g&ouml;stərilməsi zamanı abunə&ccedil;iyə aid şəxsi məlumatların &uuml;&ccedil;&uuml;nc&uuml; şəxslərə a&ccedil;ıqlanmasının qarşısını almaq məqsədilə hazırlanmışdır. Aşağıdakıları nəzərdən ke&ccedil;irərək qeydiyyat prosesinə davam etdiyiniz halda Siz, &ldquo;Bakcell&rdquo; MMC (bundan sonra &ldquo;Bakcell&rdquo;)  tərəfindən m&uuml;əyyən olunan m&uuml;vafiq qayda və şərtlərin qəbulunu təsdiq edirsiniz.</p> </div> </div> <div class=\"term\"> <div class=\"termHead\"> <h2>Məlumat təhl&uuml;kəsizliyi</h2> </div> <div class=\"termBody\"> <p>&ldquo;Mənim Bakcellim&rdquo; xidməti Bakcell-ə məxsusdur və bu xidmətdən istifadə edən abunə&ccedil;ilərin məlumat təhl&uuml;kəsizliyi Bakcell &uuml;&ccedil;&uuml;n b&ouml;y&uuml;k əhəmiyyət kəsb edir. Abunə&ccedil;ilərin qeydiyyata alınmış və gələcəkdə qeydiyyata alınacaq şəxsi məlumatları bir mobil operator kimi Bakcell tərəfindən ciddi qorunur və qorunmağa davam edəcəkdir. Bu baxımdan, məlumat təhl&uuml;kəsizliyinizin təmin olunması &uuml;&ccedil;&uuml;n abunə&ccedil;i tərəfindən şifrənin başqa şəxsə verilməsi yolverilməzdir. &ldquo;Mənim Bakcellim&rdquo; xidməti &ccedil;ər&ccedil;ivəsində şifrənin &uuml;&ccedil;&uuml;nc&uuml; şəxsə verilməsi və ya bunun başqa bir şəxs tərəfindən abunə&ccedil;inin n&ouml;mrəsindən istifadə zamanı əldə edilməsi halında, abunə&ccedil;iyə aid məlumatların a&ccedil;ıqlanmasına, habelə aparılan əməliyyatlara g&ouml;rə məsuliyyəti yalnız abunə&ccedil;i &ouml;z&uuml; daşıyır. &Ouml;z&uuml;nə xidmət kanalları vasitəsilə təqdim edilən xidmətlərlə əlaqədar abunə&ccedil;i &uuml;&ccedil;&uuml;n aşağıdakı istifadə qayda və şərtlərilə tanış olmaq vacibdir. Aşağıdakı qayda və şərtlər Bakcell tərəfindən abunə&ccedil;iyə &ldquo;Mənim Bakcellim&rdquo; xidməti vasitəsilə təqdim olunan xidmətlər &uuml;zrə tərəflərin h&uuml;quq və &ouml;hdəliklərini ehtiva edir.</p> </div> </div> <div class=\"term\"> <div class=\"termHead\"> <h2>Abunə&ccedil;inin &ouml;hdəlikləri</h2> </div> <div class=\"termBody\"> <p>Abunə&ccedil;i bu qayda və şərtləri qəbul etməklə &ldquo;Mənim Bakcellim&rdquo; xidməti vasitəsilə əldə edəcəyi məlumatlara və həyata ke&ccedil;irəcəyi əməliyyatlara g&ouml;rə yalnız &ouml;z&uuml; tək səlahiyyətli şəxs olduğunu, Bakcell tərəfindən təqdim olunan xidmətlər &ccedil;ər&ccedil;ivəsində aparılan hər hansı bir əməliyyatın yalnız şəxsən həyata ke&ccedil;irildiyini, xidmətdən istifadəyə g&ouml;rə məsul şəxs olduğunu, şifrə istifadəsilə həyata ke&ccedil;irilən əməliyyatlar &uuml;&ccedil;&uuml;n &ldquo;tərəfimdən istifadə edilməyib&rdquo; etirazından imtina etməsini, şifrəsinin başqa şəxs tərəfindən mənimsənməsi halında m&uuml;vafiq araşdırmanın aparılmasına g&ouml;rə Bakcell-in &ouml;hdəlik daşımadığını, şifrənin &uuml;&ccedil;&uuml;nc&uuml; şəxs tərəfindən istifadəsi zamanı yaranan hər hansı ziyana g&ouml;rə şəxsən məsuliyyət daşıdığını, &ouml;z şifrəsinin gizli saxlanılması &uuml;&ccedil;&uuml;n lazımi tədbir g&ouml;rməyi və istənilən halda yuxarıda sadalanan hallarla bağlı Bakcell-in hər hansı məsuliyyət daşımadığını təsdiq edir. Şifrə olaraq &ldquo;Mənim Bakcellim&rdquo; xidmətinə qeydiyyat zamanı təqdim olunan kod və &uuml;mumi hallarda xidmətlərdən istifadə zamanı təqdim və ya tələb olunan məlumatlar nəzərdə tutulur.</p> </div> </div> <div class=\"term\"> <div class=\"termHead\"> <h2>Şərtlər</h2> </div> <div class=\"termBody\"><ol> <li>Bakcell, &ouml;z&uuml;nə xidmət kanalı vasitəsilə təqdim olunan xidmətlərin davamlı olacağına zəmanət vermir, xidmətlərin şərtlərində sonradan dəyişiklik etmək, habelə xidmətləri dayandırmaq h&uuml;ququnu &ouml;z&uuml;ndə saxlayır.</li> <li>Bakcell bu qayda və şərtlərin pozulması halında, xidmətlərdən bilərəkdən yanlış və ya sui-istifadə edildikdə, habelə istifadə&ccedil;i tərəfindən təhl&uuml;kəsizlik tədbirlərinin pozulması zamanı abunə&ccedil;iyə əvvəlcədən xəbərdarlıq etmədən onun şifrəsini ləğv edə və m&uuml;vafiq xidmətlərin g&ouml;stərilməsini dayandıra bilər.</li> <li>Abunə&ccedil;iyə bu xidmətlərin g&ouml;stərilməsi zamanı m&uuml;vafiq şifrənin bilərəkdən və ya kobud ehtiyatsızlıqdan &uuml;&ccedil;&uuml;nc&uuml; şəxsə verilməsi nəticəsində abunə&ccedil;inin şəxsi anket və s. məlumatlarının a&ccedil;ıqlanmasına g&ouml;rə, Bakcell məsuliyyət daşımır və buna g&ouml;rə cavabdehlik tam olaraq abunə&ccedil;inin &ouml;z &uuml;zərində qalır.</li> <li>Abunə&ccedil;i, &ouml;z şifrəsinin &uuml;&ccedil;&uuml;nc&uuml; şəxs tərəfindən istifadəsindən ş&uuml;bhələndiyi halda, o, Bakcell-ə bu xidmətlərin g&ouml;stərilməsinin dayandırılması barədə m&uuml;raciət edə bilər.</li> <li>Bakcell-dən asılı olmayan hər hansı texnoloji problem səbəbindən &ldquo;Mənim Bakcellim&rdquo; xidmətinin g&ouml;stərilməməsinə g&ouml;rə Bakcell məsuliyyət daşmır.</li> <li>Bakcell gələcəkdə birtərəfli qaydada bu qayda və şərtlərdə dəyişiklik etmək, habelə &ldquo;Mənim Bakcellim&rdquo; xidmətini ləğv etmək və ya dayandırmaq h&uuml;ququnu &ouml;z&uuml;ndə saxlayır.</li> <li>Bakcell lazımi informasiya təhl&uuml;kəsizliyi tədbirləri g&ouml;rmədiyi halda abunə&ccedil;ilərə aid məlumatların &uuml;&ccedil;&uuml;nc&uuml; şəxslərə &ouml;t&uuml;r&uuml;lməsinə g&ouml;rə məsuliyyət daşıyır.</li> </ol></div> </div>");
	    if(lang.equalsIgnoreCase("2"))
	    	predefinedData.setContent("<div class=\"term\"> <div class=\"termHead\"> <h2>Цель</h2> </div> <div class=\"termBody\"> <p>Настоящее извещение было подготовлено с целью информирования абонентов об условиях соответствующих услуг, которые будут предоставляться в электронной форме, а также для предотвращения раскрытия персональных данных абонентов третьим лицам во время оказания таких услуг. Продолжив процесс регистрации после ознакомления с нижеследующим текстом, вы подтверждаете свое согласие с соответствующими правилами и условиями, установленными ООО &laquo;Bakcell&raquo; (далее по тексту &mdash; Bakcell).</p> </div> </div> <div class=\"term\"> <div class=\"termHead\"> <h2>Безопасность информации</h2> </div> <div class=\"termBody\"> <p>Услуга &laquo;Мой Bakcell&raquo; принадлежит компании Bakcell, и безопасность информации абонентов, пользующихся этой услугой, крайне важна для Bakcell. Компания обеспечивает, и будет обеспечивать строгую конфиденциальность персональных данных абонентов, которые уже прошли регистрацию или пройдут регистрацию в будущем. С этой целью, для обеспечения безопасности данных, не допускается передача пароля абонентом другому лицу. В случае если пароль в рамках услуги &laquo;Мой Bakcell&raquo; был передан другому лицу, или другое лицо получило пароль во время использования номера абонента, вся ответственность за раскрытие информации, касающейся абонента, а также за проводимые операции, возлагается на самого абонента. Абоненту важно ознакомиться с нижеследующими правилами и условиями использования услуг, предоставляемыми посредством каналов самообслуживания. Приведенные ниже правила и условия включают права и обязанности сторон в отношении обслуживания, предоставляемого компанией Bakcell абоненту посредством услуги &laquo;Мой Bakcell&raquo;.</p> </div> </div> <div class=\"term\"> <div class=\"termHead\"> <h2>Обязанности абонента</h2> </div> <div class=\"termBody\"> <p>Принимая нижеследующие правила и условия, абонент подтверждает, что является единственным правомочным лицом в отношении данных, которые он будет получать посредством услуги &laquo;Мой Bakcell&raquo;, и проводимых операций; лично осуществляет все операции, проводимые в рамках услуг, предоставляемых Bakcell; является лицом, ответственным за пользование услугой; отказывается от таких возражений как &laquo;пароль использовался не мной&raquo; в отношении операций, проведенных с использованием пароля; Bakcell не несет обязательств по проведению расследования в случае использования пароля третьим лицом; лично несет ответственность за любой ущерб, причиненный во время использования пароля третьим лицом; принимает необходимые меры для сохранения пароля в тайне, а также, что Bakcell не несет никакой ответственности в перечисленных выше случаях. Под паролем подразумевается код, предоставляемый в момент регистрации в услуге &laquo;Мой Bakcell&raquo;, и в целом данные, предоставляемые или требуемые во время пользования услугами.</p> </div> </div> <div class=\"term\"> <div class=\"termHead\"> <h2>Условия</h2> </div> <div class=\"termBody\"><ol> <li>Bakcell не дает гарантий в отношении постоянного предоставления услуг посредством канала самообслуживания, и сохраняет за собой право вносить изменения в условия услуг, а также прекращать их действие в будущем.</li> <li>В случае нарушения настоящих правил и условий, умышленного неправильного пользования или злоупотребления услугами, а также нарушения мер безопасности абонентом, Bakcell может аннулировать пароль абонента и оказание соответствующих услуг без предварительного уведомления.</li> <li>Bakcell не несет ответственности за раскрытие персональных анкетных и других данных в результате умышленной или вызванной явной неосторожностью передачи пароля третьему лицу во время оказания услуг; ответственность за вышеперечисленное в полном объеме несет абонент.</li> <li>Если у абонента возникают подозрения, что принадлежащий ему (ей) пароль используется третьим лицом, он (она) может обратиться в компанию Bakcell для приостановления оказания услуг</li> <li>Bakcell не несет ответственности за не предоставление услуги &laquo;Мой Bakcell&raquo; по каким-либо техническим причинам, не зависящим от Bakcell.</li> <li>Bakcell сохраняет право в будущем вносить изменения в настоящие правила и условия в одностороннем порядке, а также аннулировать или приостановить действие услуги &laquo;Мой Bakcell&raquo;.</li> <li>Backell несет ответственность за передачу данных абонента третьим лицам, в случае если компания не предприняла необходимые меры по обеспечению информационной безопасности.</li> </ol></div> </div>");
	    if(lang.equalsIgnoreCase("3"))
	    	predefinedData.setContent("<div class=\"term\"> <div class=\"termHead\"> <h2>Objective</h2> </div> <div class=\"termBody\"> <p>This notification is intended to inform the subscribers about the terms of relevant e-services, as well as to prevent the disclosure of subscriber information to any third parties during the rendering of such services. By continuing the registration process after reviewing the below, you hereby agree to the relevant terms and conditions defined by &ldquo;Bakcell&rdquo; LLC (hereinafter &ldquo;Bakcell&rdquo;).</p> </div> </div> <div class=\"term\"> <div class=\"termHead\"> <h2>Data security</h2> </div> <div class=\"termBody\"> <p>&ldquo;My Bakcell&rdquo; is a property of Bakcell and security of subscribers using this service is very important for Bakcell. Bakcell, being a mobile operator, strictly protects and will continue to protect any personal information of subscribers that is recorded or will be recorded in the future. As such, disclosure of password by a subscriber to third parties is unacceptable. In cases when the password is given to a third party or if the subscriber&rsquo;s number is used by a third party, the subscriber shall be solely responsible for disclosure of his/her personal information, as well as for any resulting transactions. It is important for the subscribers to familiarize themselves with the following terms and conditions with regard to the services provided through self-service channels. The following terms and conditions define the rights and obligations of parties in respect of the services provided to a subscriber by means of &ldquo;My Bakcell&rdquo; service.</p> </div> </div> <div class=\"term\"> <div class=\"termHead\"> <h2>Obligations of the subscriber</h2> </div> <div class=\"termBody\"> <p>By accepting these terms and conditions the subscriber acknowledges that he/she is solely responsible for any information obtained and for any transactions performed through &ldquo;My Bakcell&rdquo; service, that transactions performed within the services provided by Bakcell shall be executed only by him/her personally, he/she will be liable for usage of the service, waives &ldquo;Password not used by me&rdquo; denial rights for the transactions performed using the password, Bakcell is not liable for conducting relevant investigations in case of acquisition of his/her password by a third party, he/she is solely liable for any damages caused by the usage of his/her password by a third party, he/she will take necessary actions to safeguard the password and that Bakcell shall be under no liability or responsibility whatsoever for any of the above cases. The password shall mean a code submitted during the registration for &ldquo;My Bakcell&rdquo; service and, in general, any data submitted or required during usage of the services.</p> </div> </div> <div class=\"term\"> <div class=\"termHead\"> <h2>Conditions</h2> </div> <div class=\"termBody\"><ol> <li>Bakcell does not guarantee the continuity and availability of the services provided through self-service channel and retains the right to modify the terms, as well as to suspend the services.</li> <li>Bakcell may cancel the password and terminate the services without prior notification in case of violation of these terms and conditions, willful misuse and abuse of the services, as well as in case of violation of safety measures by the subscriber.</li> <li>Bakcell shall not be liable for disclosure of personal profile and other data of the subscriber due to willful or negligent propagation of the password to a third party and the subscriber shall be fully liable for such action.</li> <li>The subscriber may request Bakcell to suspend the services in cases when he/she suspects that his/her password is used by a third party.</li> <li>Bakcell shall not be liable for interruptions in &ldquo;My bakcell&rdquo; services due to any technical problems beyond Bakcell&rsquo;s control.</li> <li>Bakcell retains the right for any future unilateral modifications to these terms and conditions, including termination or suspension of &ldquo;My Bakcell&rdquo; services.</li> <li>Bakcell takes a responsibility for the transfer of subscriber data to the third parties, in case the company has not taken steps and measures to ensure personal information security.</li> </ol></div> </div>");
	    predefinedData.setChangeTariffPopUpMessage(
		    GetConfigurations.getConfigurationFromCache("changetariff.popup.message." + lang));
	    String emailAndPassHash = "";
	    if (isB2B.equalsIgnoreCase("true")) {
		emailAndPassHash = Utilities.getEmailByEntityIdV2(loginRes.getEntity_id(), loginRes.getMsisdn(),
			loginRes.getCustomer_id());
	    } else {
		emailAndPassHash = Utilities.getEmailByEntityId(loginRes.getEntity_id(), loginRes.getMsisdn(),
			loginRes.getCustomer_id());
	    }

	    logger.info("<<<<<<<<< Email and password hash is:" + emailAndPassHash);
	    loginRes.setToken(Utilities.prepareToken(msisdn, deviceID, emailAndPassHash.split(";")[1]));
	    if (!isHashMatched(msisdn, passHash, loginRes)) {
		dashboardResponseData.setLoginData(null);
		dashboardResponseData.setReturnCode(Constants.INVALID_TOKEN);
		dashboardResponseData
			.setReturnMsg(GetMessagesMappings.getMessageFromResourceBundle("token.session.expired", lang));
		return dashboardResponseData;
	    }
	    dashboardResponseData.setReturnCode("00");
	    dashboardResponseData.setReturnMsg("Successful");
	    dashboardResponseData.setLoginData(loginRes);
	    dashboardResponseData.setPredefinedData(predefinedData);
	    // Group Data
	    // JSONObject obj = new JSONObject();
	    /*
	     * try { obj = jsonObject.getJSONObject("groupData"); Iterator key = obj.keys();
	     * UsersGroupResponse usersGroupResponse = new UsersGroupResponse();
	     * ArrayList<UsersGroupResponseData> usersGroupResponseData = new ArrayList<>();
	     * ArrayList<String> groupNames = new ArrayList<>();
	     * logger.info("E&S -- Response time before calculating group names " + new
	     * Date()); while (key.hasNext()) { groupNames.add((String) key.next()); } key =
	     * obj.keys(); logger.info("E&S -- Response time before calculating group data "
	     * + new Date()); while (key.hasNext()) { String currentkey = (String)
	     * key.next(); JSONArray jsonarr =
	     * obj.getJSONObject(currentkey).getJSONArray("usersGroupData");
	     * ArrayList<UsersGroupData> usersGroupData = new ArrayList<>();
	     * UsersGroupResponseData resGroupData = new UsersGroupResponseData(); int count
	     * = 0; resGroupData.setGroupName(currentkey); for (int j = 0; j <
	     * jsonarr.length(); j++) { count++; usersGroupData .add((UsersGroupData)
	     * mapper.readValue((jsonarr.getString(j)), UsersGroupData.class)); //
	     * System.out.println(usersGroupData[i]); }
	     * resGroupData.setUsersData(usersGroupData);
	     * resGroupData.setUserCount(String.valueOf(count));
	     * usersGroupResponseData.add(resGroupData);
	     * 
	     * usersGroupResponse.setUsersGroupData(usersGroupResponseData);
	     * dashboardResponseData.setGroupData(usersGroupResponse); }
	     * logger.info("E&S -- Response time after calculating group data " + new
	     * Date()); } catch (Exception e) { e.printStackTrace();
	     * dashboardResponseData.setGroupData(null);
	     * dashboardResponseData.setReturnCode("400");
	     * dashboardResponseData.setReturnMsg("Not Successful"); }
	     */

	    /*
	     * try { logger.info("E&S -- Response time before calculating group user data "
	     * + new Date()); ArrayList<UsersGroupData> users = new ArrayList<>();
	     * ArrayList<UsersGroupResponseData> userGroups =
	     * dashboardResponseData.getGroupData().getUsersGroupData(); for (int i = 0; i <
	     * userGroups.size(); i++) { users.addAll(userGroups.get(i).getUsersData()); }
	     * dashboardResponseData.setUsers(users);
	     * logger.info("E&S -- Response time after calculating user group data " + new
	     * Date()); } catch (Exception e) { e.printStackTrace();
	     * dashboardResponseData.setUsers(null);
	     * dashboardResponseData.setReturnCode("400");
	     * dashboardResponseData.setReturnMsg("Not Successful"); }
	     */

	    try {
		Object objs = jsonObject.get("queryBalancePicResponseData");
		BalancePicData balancePicData = new BalancePicData();
		balancePicData = mapper.readValue(objs.toString(), BalancePicData.class);
		logger.info("Balance pic response: " + mapper.writeValueAsString(balancePicData));
		dashboardResponseData.setQueryBalancePicResponseData(balancePicData);
	    } catch (Exception e) {
		e.printStackTrace();
		dashboardResponseData.setQueryBalancePicResponseData(null);
		dashboardResponseData.setReturnCode("400");
		dashboardResponseData.setReturnMsg("Not Successful");

	    }

	    JSONArray jsonarr = new JSONArray();
	    try {
		if (!(jsonObject.get("queryInvoiceResponseData") instanceof JSONArray)) {
		    logger.info("<<<<<<<<<<<<<<< Invoice Response Data is empty >>>>>>>>>>>>>>>");
		} else {
		    jsonarr = jsonObject.getJSONArray("queryInvoiceResponseData");
		    QueryInvoiceData[] queryInvoiceData = new QueryInvoiceData[jsonarr.length()];
		    for (int i = 0; i < jsonarr.length(); i++) {

			queryInvoiceData[i] = mapper.readValue((jsonarr.getString(i)), QueryInvoiceData.class);
			queryInvoiceData[i].setDueDateDisp(GetConfigurations
				.getConfigurationFromCache(new StringBuilder("labels.duedate.").append(lang).toString())
				+ ": " + queryInvoiceData[i].getDueDate());
			queryInvoiceData[i].setInvoiceDateDisp(GetConfigurations.getConfigurationFromCache(
				new StringBuilder("labels.issuedate.").append(lang).toString()) + ": "
				+ queryInvoiceData[i].getInvoiceDateDisp());
			// System.out.println(queryInvoiceData[i].getDueDate());
		    }
		    dashboardResponseData.setQueryInvoiceResponseData(queryInvoiceData);
		}
	    } catch (Exception e) {
		e.printStackTrace();
		logger.error("INVOICE ERROR", e);
		dashboardResponseData.setQueryInvoiceResponseData(null);
		dashboardResponseData.setReturnCode("400");
		dashboardResponseData.setReturnMsg("Not Successful");
	    }
	} catch (Exception e) {
	    logger.error("Login Error:", e);
	    dashboardResponseData.setLoginData(null);
	    dashboardResponseData.setReturnCode("400");
	    dashboardResponseData.setReturnMsg("Not Successful");
	}

	return dashboardResponseData;
    }

    private boolean isHashMatched(String msisdn, String passHash, LoginData customerInfo)
	    throws ClassNotFoundException, SQLException, JSONException, UnsupportedEncodingException, SocketException {
	/**
	 * We already called Database to get Email and Password hash at the time of
	 * Customer Info and stored hash into token. Therefore, rather than calling DB
	 * again to get hash, getting updated hash from token to compare it with old
	 * token which we got from requested call.
	 *
	 */

	JSONObject tokenSplitted = Utilities.splitToken(Utilities.decodeString(customerInfo.getToken()));
	if (tokenSplitted.has("passHash")) {
	    String hashFromDB = Utilities.getValueFromJSON(
		    Utilities.splitToken(Utilities.decodeString(customerInfo.getToken())).toString(), "passHash");
	    Utilities.printDebugLog(msisdn + "- OLD PASSWORD HASH OBTAINED FROM TOKEN" + passHash, logger);
	    Utilities.printDebugLog(msisdn + "- NEW PASSWORD HASH FETCHED FROM MAGENTO-" + hashFromDB, logger);
	    if (passHash.equals(hashFromDB))
		Utilities.printDebugLog(msisdn + "- PASSWORD HASH MATCHED SUCCESSFULLY", logger);
	    else
		Utilities.printDebugLog(msisdn + "- PASSWORD HASH MATCHED FAILED", logger);
	    return ((passHash.equals(hashFromDB)));
	}
	return false;
	// return true;
    }

    public static void main(String[] args) throws JsonParseException, JsonMappingException, JSONException, IOException {
	String jsonstring = "{\"returnMsg\":\"Successful\",\"returnCode\":\"200\",\"loginData\":{\"entity_id\":\"227\",\"website_id\":\"1\",\"email\":"
		+ "\"newpic1@Test.com\",\"group_id\":\"4\",\"store_id\":\"1\",\"created_at\":\"2018-07-09 06:38:29\",\"updated_at\":\"2018-08-06 07:09:52\",\"is_active\":\"1\",\"disable_auto_group_change\":\"0\",\"created_in\":\"Default Store View\",\"firstname\":\"muhammad\",\"lastname\":\"Gufran\",\"suffix\":\"w\",\"password_hash\":\"3e163f36dca3224fe5cfdd0214097c14bea7732e986b4eef8c79afc77b74ef9e:xxxxxxxx:1\",\"rp_token\":\"4dd3b3e7b6b3f837e285df4188fda566\",\"rp_token_created_at\":\"2018-07-09 06:38:29\",\"default_billing\":\"0\",\"default_shipping\":\"0\",\"failures_num\":\"0\",\"msisdn\":\"pic_new\",\"customer_type\":\"postpaid\",\"account_id\":\"13131\",\"language\":\"en\",\"customer_id\":\"60001133\",\"channel\":\"web\",\"pic_allowed_tariffs\":\"770094093,1570082771,745348537,1270094233,1770082919,370082492,1155373228,355630537,1655630758,720469688,1420469348,1560986698,1320455071,1894870629,1295589733,1195647029,1974577397,1470253339,970250220,1770250307,1471646836,771646823,371615150,1471614990,770253481,1971646822,1070249776,1371646816,770253711,1870253814,1370253941,1471637124,170249065,970254022,1370250082,1871646816,1271646829,1771646816,1474578416,574578738,374579254,771614575,1137898528,1770083072,370094560,1670006710,770094391,1970082271,770249383,1270249534,1470249646,1571636728,1171636872,1870253615,1270249962,1171646816,1571821605,1671614769,771646833\",\"pic_tariffs_permissions\":\"PaybySubs-Upgrade,PaybySubs-Downgrade,PartPay-Upgrade,PartPay-Downgrade,FullPay-Upgrade\",\"pic_allowed_offers\":\"1126233544,526256441,1024100713,1125909660,1225913124,1725914513,1125997735,525998280,1426001158,526002541,978106505,1401459443,1913038075,1151806699,1250446338,1650511655,1420537226,1920570459,1120572128,520572937,376367781,1076367958,1376368122,322355824,1176367625,1978106711,1285890379,985891194,1932907452,1032915331,981899074,1178034269,122206970,1925294975,1778035257,920816437,1024017055,162099730,213123,906471273,1107769122,1606281323,1106368938,1906462715,1806463473,902735639,1505075143,1105075286,1505075428,506471346,1875518077,1251814288,1951814324,1651814390,751814423,1724443692,1173626120,1673626906,973627303,773626625,1673627024,1712326619,1115034347,1915034377,515034399,1815034421,730552510,1530579330,530579362,1230636525,1694871972,1695587927,1195588377,1576369121,1476368948,1076369206,1176369383,1976369604,1876369702,582648597,576369294,378020456,1178020599,1678020928,1578021015,1032976233,378020771,1663227146,1480287500,520819100,1978021082,1222030439,1720819067,1064004019,778027206,380287894,1017940386,717874807,1417874746,1917940298,1778038778,1480289370,378034097,1081743468,1878033925,1578033984,178034042,1661927660,1260989508,765649560,1764339698,161929155,520819486,160986927,520819438,320819392,1255373036,1755547152,1455547240,1117874986,917874949,320550114,716329997,516329907\",\"password_unhashed\":\"temp\",\"otp_source\":\"sms\",\"reward_update_notification\":\"0\",\"reward_warning_notification\":\"0\",\"pic_status\":\"1\",\"pic_new_reset\":\"0\",\"pic_tnc\":\"1\",\"pic_veon\":\"5555\",\"custom_profile_image\":\"pic_new.jpg\",\"pic_serial_no\":\"555\",\"pic_pin\":\"555\",\"otp_msisdn\":\"12345677788\"},\"groupData\":{\"Testing\":{\"groupName\":\"Testing\",\"userCount\":8,\"usersGroupData\":[{\"msisdn\":\"558120743\",\"tarif_id\":\"1270249534\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=14099=IN/VAS\",\"cust_last_name\":\"Testing\"},{\"msisdn\":\"555957427\",\"tarif_id\":\"371615150\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60201133\",\"group_cust_name\":\"Testing\",\"group_id\":\"1\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15445=IN/VAS\",\"cust_last_name\":\"Testing\"},{\"msisdn\":\"555957473\",\"tarif_id\":\"1270249962\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=14121=IN/VASTesting\",\"cust_last_name\":\"Testing\"},{\"msisdn\":\"555957474\",\"tarif_id\":\"771646833\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=13153=IN/VASTesting\",\"cust_last_name\":\"Testing\"},{\"msisdn\":\"555957497\",\"tarif_id\":\"1870253615\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15515=IN/VAS\",\"cust_last_name\":\"Testing\"},{\"msisdn\":\"555957754\",\"tarif_id\":\"770253711\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15772=IN/VAS\",\"cust_last_name\":\"Testing\"},{\"msisdn\":\"555957756\",\"tarif_id\":\"1370253941\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15774=IN/VAS\",\"cust_last_name\":\"Testing\"},{\"msisdn\":\"555957757\",\"tarif_id\":\"970254022\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15775=IN/VAS\",\"cust_last_name\":\"Testing\"}]}},\"users\":{\"555957497\":{\"msisdn\":\"555957497\",\"tarif_id\":\"1870253615\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15515=IN/VAS\",\"cust_last_name\":\"Testing\"},\"558120743\":{\"msisdn\":\"558120743\",\"tarif_id\":\"1270249534\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=14099=IN/VAS\",\"cust_last_name\":\"Testing\"},\"555957473\":{\"msisdn\":\"555957473\",\"tarif_id\":\"1270249962\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=14121=IN/VASTesting\",\"cust_last_name\":\"Testing\"},\"555957474\":{\"msisdn\":\"555957474\",\"tarif_id\":\"771646833\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=13153=IN/VASTesting\",\"cust_last_name\":\"Testing\"},\"555957754\":{\"msisdn\":\"555957754\",\"tarif_id\":\"770253711\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15772=IN/VAS\",\"cust_last_name\":\"Testing\"},\"555957756\":{\"msisdn\":\"555957756\",\"tarif_id\":\"1370253941\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15774=IN/VAS\",\"cust_last_name\":\"Testing\"},\"555957427\":{\"msisdn\":\"555957427\",\"tarif_id\":\"371615150\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60201133\",\"group_cust_name\":\"Testing\",\"group_id\":\"1\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15445=IN/VAS\",\"cust_last_name\":\"Testing\"},\"555957757\":{\"msisdn\":\"555957757\",\"tarif_id\":\"970254022\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15775=IN/VAS\",\"cust_last_name\":\"Testing\"}},"
		+ "\"queryBalancePicResponseData\":{\"total_limit\":202026141059,\"available_credit\":202026141059,\"outstanding_debt\":0,\"unbilled_balance\":0},\"queryInvoiceResponseData\": [{\"invoice_amount\": \"6781297961\",\"invoice_date\": \"20180601050551\",\"due_date\": \"20180701000000\"},{\"invoice_amount\": \"6781297961\",\"invoice_date\": \"20180601050551\",\"due_date\": \"20180701000000\"},{\"invoice_amount\": \"6781297961\",\"invoice_date\": \"20180601050551\",\"due_date\": \"20180701000000\"}]}";
	// DashboardResponse dashboardResponse = new DashboardResponse();
	// parsejson(jsonstring, dashboardResponse);
	JSONObject object = new JSONObject(jsonstring);
	JSONObject obj = object.getJSONObject("groupData");
	System.out.println(obj.keys().toString());

    }

}

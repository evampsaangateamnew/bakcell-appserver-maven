/**
 * 
 */
package com.evampsaanga.bakcell.business;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.AppCache;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.DBUtility;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.customerservices.appresume.AppResumeRequest;
import com.evampsaanga.bakcell.models.customerservices.appresume.AppResumeResponse;
import com.evampsaanga.bakcell.models.customerservices.appresume.AppResumeResponseData;
import com.evampsaanga.bakcell.models.customerservices.authenticateuser.AuthenticateUserRequest;
import com.evampsaanga.bakcell.models.customerservices.authenticateuser.AuthenticateUserResponse;
import com.evampsaanga.bakcell.models.customerservices.authenticateuser.AuthenticateUserResponseData;
import com.evampsaanga.bakcell.models.customerservices.authenticateuser.PrimaryOfferings;
import com.evampsaanga.bakcell.models.customerservices.authenticateuser.SupplementaryOfferings;
import com.evampsaanga.bakcell.models.customerservices.changebillinglanguage.ChangeBillingLanguageRequest;
import com.evampsaanga.bakcell.models.customerservices.changebillinglanguage.ChangeBillingLanguageResponse;
import com.evampsaanga.bakcell.models.customerservices.changebillinglanguage.ChangeBillingLanguageResponseData;
import com.evampsaanga.bakcell.models.customerservices.changepassword.ChangePasswordRequest;
import com.evampsaanga.bakcell.models.customerservices.changepassword.ChangePasswordRequestV2;
import com.evampsaanga.bakcell.models.customerservices.changepassword.ChangePasswordResponse;
import com.evampsaanga.bakcell.models.customerservices.changepassword.ChangePasswordResponseData;
import com.evampsaanga.bakcell.models.customerservices.customerinfo.CustomerInfo;
import com.evampsaanga.bakcell.models.customerservices.customerinfo.CustomerInfoRequest;
import com.evampsaanga.bakcell.models.customerservices.customerinfo.CustomerInfoResponseData;
import com.evampsaanga.bakcell.models.customerservices.forgotpassword.ForgotPasswordRequest;
import com.evampsaanga.bakcell.models.customerservices.forgotpassword.ForgotPasswordRequestV2;
import com.evampsaanga.bakcell.models.customerservices.forgotpassword.ForgotPasswordResponse;
import com.evampsaanga.bakcell.models.customerservices.forgotpassword.ForgotPasswordResponseData;
import com.evampsaanga.bakcell.models.customerservices.forgotpassword.ForgotPasswordResponseDataV2;
import com.evampsaanga.bakcell.models.customerservices.forgotpassword.ForgotPasswordResponseV2;
import com.evampsaanga.bakcell.models.customerservices.logout.LogoutRequest;
import com.evampsaanga.bakcell.models.customerservices.logout.LogoutResponse;
import com.evampsaanga.bakcell.models.customerservices.resendpin.ResendPINRequest;
import com.evampsaanga.bakcell.models.customerservices.resendpin.ResendPINResponse;
import com.evampsaanga.bakcell.models.customerservices.resendpin.ResendPINResponseData;
import com.evampsaanga.bakcell.models.customerservices.resendpin.ResendPinRequestV2;
import com.evampsaanga.bakcell.models.customerservices.savecustomer.SaveCustomerRequest;
import com.evampsaanga.bakcell.models.customerservices.savecustomer.SaveCustomerResponse;
import com.evampsaanga.bakcell.models.customerservices.savecustomer.SaveCustomerResponseData;
import com.evampsaanga.bakcell.models.customerservices.sendpinV2.SendPINRequest;
import com.evampsaanga.bakcell.models.customerservices.sendpinV2.SendPINResponse;
import com.evampsaanga.bakcell.models.customerservices.sendpinV2.SendPINResponseData;
import com.evampsaanga.bakcell.models.customerservices.signup.SignupRequest;
import com.evampsaanga.bakcell.models.customerservices.signup.SignupResponse;
import com.evampsaanga.bakcell.models.customerservices.signup.SignupResponseData;
import com.evampsaanga.bakcell.models.customerservices.updatecustomeremail.UpdateCustomerEmailRequest;
import com.evampsaanga.bakcell.models.customerservices.updatecustomeremail.UpdateCustomerEmailResponse;
import com.evampsaanga.bakcell.models.customerservices.updatecustomeremail.UpdateCustomerEmailResponseData;
import com.evampsaanga.bakcell.models.customerservices.verifyotp.VerifyOTPRequest;
import com.evampsaanga.bakcell.models.customerservices.verifyotp.VerifyOTPResponse;
import com.evampsaanga.bakcell.models.customerservices.verifyotp.VerifyOTPResponseData;
import com.evampsaanga.bakcell.models.dashboardservice.DashboardRequest;
import com.evampsaanga.bakcell.models.dashboardservice.LoginData;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.PredefinedDataRequest;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.PredefinedDataResponse;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.TariffMigrationPrices;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.notificationpopup.NotificationPopupConfig;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.topup.CardType;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.topup.PlasticCard;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.topup.TOPUP;
import com.evampsaanga.bakcell.models.notifications.addfcm.AddFCMRequest;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
@Service
public class CustomerServicesBusiness {
    Logger logger = Logger.getLogger(CustomerServicesBusiness.class);

    public SignupResponse signupBusiness(String msisdn, SignupRequest signupRequest, SignupResponse signupResponse)
	    throws JSONException, IOException, SQLException {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SIGNUP_TRANSACTION_NAME + " with data-"
		+ signupRequest.toString(), logger);

	SignupResponseData resData = new SignupResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(signupRequest);

	String path = GetConfigurations.getESBRoute("signup");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	signupResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	signupResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	signupResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, signupResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		String responseData = Utilities.removeParamsFromJSONObject(response, "returnMsg"); // new
												   // JSONObject(response).get("orderList").toString();
		responseData = Utilities.removeParamsFromJSONObject(responseData, "returnCode");

		// String pin = Utilities.getValueFromJSON(response, "pinMsg");
		/* resData = mapper.readValue(responseData, SignupResponseData.class); */
		// resData.setPin(Utilities.getTokens(new
		// JSONObject(response).getString("pinMsg"),
		// ":").get(1).trim());

		// just to send pin inresponse to appp for tessting
		// resData.setPin(pinFromText(pin));
		resData.setPin("");
		signupResponse.setData(resData);
		signupResponse.setCallStatus(Constants.Call_Status_True);
		signupResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		signupResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", signupRequest.getLang()));

	    } else {
		signupResponse.setData(resData);
		signupResponse.setCallStatus(Constants.Call_Status_False);
		signupResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		signupResponse.setResultDesc(Utilities.getErrorMessageFromDB("sign.up", signupRequest.getLang(),
			Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    signupResponse.setCallStatus(Constants.Call_Status_False);
	    signupResponse.setResultCode(Constants.API_FAILURE_CODE);
	    signupResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", signupRequest.getLang()));
	}

	return signupResponse;

    }

    public VerifyOTPResponse verifyOTPBusiness(String msisdn, VerifyOTPRequest verifyOTPRequest,
	    VerifyOTPResponse verifyOTPResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SIGNUP_VERIFY_OTP_TRANSACTION_NAME
		+ " BUSINESS with data-" + verifyOTPRequest.toString(), logger);

	VerifyOTPResponseData resData = new VerifyOTPResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(verifyOTPRequest);

	String path = GetConfigurations.getESBRoute("verifyOTP");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	verifyOTPResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	verifyOTPResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	verifyOTPResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, verifyOTPResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		resData.setMessage(Utilities.getValueFromJSON(response, "returnMsg"));

		verifyOTPResponse.setData(resData);
		verifyOTPResponse.setCallStatus(Constants.Call_Status_True);
		verifyOTPResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		verifyOTPResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", verifyOTPRequest.getLang()));

	    } else {
		verifyOTPResponse.setCallStatus(Constants.Call_Status_False);
		verifyOTPResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		verifyOTPResponse.setResultDesc(Utilities.getErrorMessageFromDB("otp.verification",
			verifyOTPRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    verifyOTPResponse.setCallStatus(Constants.Call_Status_False);
	    verifyOTPResponse.setResultCode(Constants.API_FAILURE_CODE);

	    verifyOTPResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", verifyOTPRequest.getLang()));
	}

	return verifyOTPResponse;

    }

    public SaveCustomerResponse saveCustomerBusiness(String msisdn, String deviceID,
	    SaveCustomerRequest saveCustomerRequest, SaveCustomerResponse saveCustomerResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SAVE_CUSTOMER_TRANSACTION_NAME
		+ " BUSINESS with data-" + saveCustomerRequest.toString(), logger);
	if (saveCustomerRequest.getTemp().length() == 4) {
	    saveCustomerRequest.setTemp(saveCustomerRequest.getTemp());
	    saveCustomerRequest.setPassword(saveCustomerRequest.getPassword());
	    saveCustomerRequest.setConfirm_password(saveCustomerRequest.getConfirm_password());
	} else {
	    saveCustomerRequest.setTemp(Utilities.decryptDataWithCipher(saveCustomerRequest.getTemp()));
	    saveCustomerRequest.setPassword(Utilities.decryptDataWithCipher(saveCustomerRequest.getPassword()));
	    saveCustomerRequest
		    .setConfirm_password(Utilities.decryptDataWithCipher(saveCustomerRequest.getConfirm_password()));
	}
	// preparing customerInfo Request Packet
	CustomerInfoResponseData customerInfoBusiness = new CustomerInfoResponseData();
	CustomerInfoRequest customerInfoRequest = new CustomerInfoRequest();

	customerInfoRequest.setChannel(saveCustomerRequest.getChannel());
	customerInfoRequest.setiP(saveCustomerRequest.getiP());
	customerInfoRequest.setLang(saveCustomerRequest.getLang());
	customerInfoRequest.setMsisdn(saveCustomerRequest.getMsisdn());
	customerInfoRequest.setIsB2B(saveCustomerRequest.getIsB2B());
	
	SaveCustomerResponseData resData = new SaveCustomerResponseData();

	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();
	customerInfoBusiness = getCustomerData(msisdn, deviceID, customerInfoRequest, customerInfoBusiness,
		Constants.TOKEN_CREATION_CALL_TYPE_INTERNAL, null, null);

	if (customerInfoBusiness.getResultCode().equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
	    /*
	     * Setting customer data in request to send to ESB to process the request.
	     */

	    saveCustomerRequest.setCustomerData(customerInfoBusiness.getCustomerData());
	    saveCustomerRequest.getCustomerData().setToken("");
	    String requestJsonESB = mapper.writeValueAsString(saveCustomerRequest);

	    String path = GetConfigurations.getESBRoute("saveCustomer");
	    Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	    Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	    Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	    saveCustomerResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	    String response = rc.getResponseFromESB(path, requestJsonESB);
	    saveCustomerResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	    Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	    // Logging ESB response code and description.
	    saveCustomerResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		    saveCustomerResponse.getLogsReport()));

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		/*
		 * As some internal server calls do not need token recreation, otherwise app's
		 * token can be expired.therefore below check decide on which call should token
		 * be refreshed.
		 */

		// String emailAndPassHash = Utilities.getEmailByEntityId(
		// customerInfoBusiness.getCustomerData().getEntityId(),
		// customerInfoBusiness.getCustomerData().getMsisdn());
		// if (emailAndPassHash != null && emailAndPassHash.length() > 0) {
		// customerInfoBusiness.getCustomerData().setEMAIL(emailAndPassHash.split(";")[0]);
		//
		// customerInfoBusiness.getCustomerData()
		// .setToken(Utilities.prepareToken(msisdn, deviceID,
		// emailAndPassHash.split(";")[1]));
		// }

		resData.setCustomerData(customerInfoBusiness.getCustomerData());
		resData.setPrimaryOffering(customerInfoBusiness.getPrimaryOffering());
		resData.setSupplementaryOfferingList(customerInfoBusiness.getSupplementaryOfferingList());
		resData.setPrimaryOffering(customerInfoBusiness.getPrimaryOffering());
		resData.setPromoMessage(Utilities.getValueFromJSON(response, "promoMessage"));

		resData.setPredefinedData(customerInfoBusiness.getPredefinedData());

		String emailAndPassHash = Utilities.getEmailByEntityId(Utilities.getValueFromJSON(response, "entityId"),
			customerInfoBusiness.getCustomerData().getMsisdn(),
			customerInfoBusiness.getCustomerData().getCustomerId());
		Utilities.printDebugLog(msisdn + "-Magento database response-" + emailAndPassHash, logger);
		if (emailAndPassHash != null && emailAndPassHash.length() > 0) {
		    resData.getCustomerData().setEMAIL(emailAndPassHash.split(";")[0]);

		    resData.getCustomerData()
			    .setToken(Utilities.prepareToken(msisdn, deviceID, emailAndPassHash.split(";")[1]));
		    resData.getCustomerData().setMagCustomerId(emailAndPassHash.split(";")[2]);
		}

		resData.getCustomerData().setEntityId(Utilities.getValueFromJSON(response, "entityId"));

		this.logger.info(customerInfoRequest.getMsisdn() + " - Setting up live chat link");
		String liveChat = setLiveChatLink(customerInfoRequest.getLang(), customerInfoRequest.getChannel(),
			resData.getCustomerData());
		resData.getPredefinedData().setLiveChat(liveChat);

		saveCustomerResponse.setData(resData);
		saveCustomerResponse.setCallStatus(Constants.Call_Status_True);
		saveCustomerResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		saveCustomerResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", saveCustomerRequest.getLang()));

	    } else {

		saveCustomerResponse.setCallStatus(Constants.Call_Status_False);
		saveCustomerResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		saveCustomerResponse.setResultDesc(Utilities.getErrorMessageFromDB("save.customer",
			saveCustomerRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }

	} else {

	    saveCustomerResponse.setCallStatus(Constants.Call_Status_False);
	    saveCustomerResponse.setResultCode(Constants.API_FAILURE_CODE);
	    saveCustomerResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    saveCustomerRequest.getLang()));

	}

	return saveCustomerResponse;
    }

    public SendPINResponse sendPinBusiness(String msisdn, String deviceID, SendPINRequest sendPINRequest,
	    SendPINResponse sendPINResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.SEND_OTP_TRANSACTION_NAME
		+ " BUSINESS with data-" + sendPINRequest.toString(), logger);

	SendPINResponseData resData = new SendPINResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(sendPINRequest);

	String path = GetConfigurations.getESBRoute("sendPin");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	sendPINResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	sendPINResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	sendPINResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, sendPINResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)
		    || Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase("232")) {

		// resData.setPin(Utilities.getTokens(new
		// JSONObject(response).getString("pinMsg"), ":").get(1));
		resData.setResponseMsg(Utilities.getValueFromJSON(response, "responseMsg"));
		resData.setChannel(Utilities.getValueFromJSON(response, "channel"));
		sendPINResponse.setData(resData);
		sendPINResponse.setCallStatus(Constants.Call_Status_True);
		sendPINResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		sendPINResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", sendPINRequest.getLang()));

	    } else {

		sendPINResponse.setCallStatus(Constants.Call_Status_False);
		sendPINResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		sendPINResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
	    }
	} else {

	    sendPINResponse.setCallStatus(Constants.Call_Status_False);
	    sendPINResponse.setResultCode(Constants.API_FAILURE_CODE);
	    sendPINResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", sendPINRequest.getLang()));
	}

	return sendPINResponse;

    }

    public SendPINResponse resendPinBusinessV2(String msisdn, String deviceID, ResendPinRequestV2 resendPINRequest,
	    SendPINResponse resendPINResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.RESEND_OTP_TRANSACTION_NAME
		+ " BUSINESS with data-" + resendPINRequest.toString(), logger);

	SendPINResponseData resData = new SendPINResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(resendPINRequest);

	String path = GetConfigurations.getESBRoute("resendPinV2");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	resendPINResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	resendPINResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	resendPINResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, resendPINResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		// resData.setPin(Utilities.getTokens(new
		// JSONObject(response).getString("pinMsg"), ":").get(1));
		resData.setResponseMsg(Utilities.getValueFromJSON(response, "responseMsg"));
		resendPINResponse.setData(resData);
		resendPINResponse.setCallStatus(Constants.Call_Status_True);
		resendPINResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		resendPINResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", resendPINRequest.getLang()));

	    } else {

		resendPINResponse.setCallStatus(Constants.Call_Status_False);
		resendPINResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		resendPINResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
	    }
	} else {

	    resendPINResponse.setCallStatus(Constants.Call_Status_False);
	    resendPINResponse.setResultCode(Constants.API_FAILURE_CODE);
	    resendPINResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", resendPINRequest.getLang()));
	}

	return resendPINResponse;

    }

    public ResendPINResponse resendPinBusiness(String msisdn, String deviceID, ResendPINRequest resendPINRequest,
	    ResendPINResponse resendPINResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.RESEND_OTP_TRANSACTION_NAME
		+ " BUSINESS with data-" + resendPINRequest.toString(), logger);

	ResendPINResponseData resData = new ResendPINResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(resendPINRequest);

	String path = GetConfigurations.getESBRoute("resendPin");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	resendPINResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	resendPINResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	resendPINResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, resendPINResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		// resData.setPin(Utilities.getTokens(new
		// JSONObject(response).getString("pinMsg"), ":").get(1));
		resendPINResponse.setData(resData);
		resendPINResponse.setCallStatus(Constants.Call_Status_True);
		resendPINResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		resendPINResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", resendPINRequest.getLang()));

	    } else {

		resendPINResponse.setCallStatus(Constants.Call_Status_False);
		resendPINResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		resendPINResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
			Utilities.getValueFromJSON(response, "returnCode"), resendPINRequest.getLang()));
	    }
	} else {

	    resendPINResponse.setCallStatus(Constants.Call_Status_False);
	    resendPINResponse.setResultCode(Constants.API_FAILURE_CODE);
	    resendPINResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", resendPINRequest.getLang()));
	}

	return resendPINResponse;

    }

    public AuthenticateUserResponse authenticateuser(String msisdn, String deviceID,
	    AuthenticateUserRequest authenticateUserRequest, AuthenticateUserResponse authenticateUserResponse)
	    throws JSONException, IOException, ClassNotFoundException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.LOGIN_TRANSACTION_NAME
		+ " BUSINESS with data-" + authenticateUserRequest.toString(), logger);
	PredefinedDataRequest predefinedDataRequest = new PredefinedDataRequest();
	PredefinedDataResponse predefinedDataResponse = new PredefinedDataResponse();

	predefinedDataRequest.setChannel(authenticateUserRequest.getChannel());
	predefinedDataRequest.setiP(authenticateUserRequest.getiP());
	predefinedDataRequest.setLang(authenticateUserRequest.getLang());
	predefinedDataRequest.setMsisdn(authenticateUserRequest.getMsisdn());
	predefinedDataRequest.setIsB2B(authenticateUserRequest.getIsB2B());
	AuthenticateUserResponseData resData = new AuthenticateUserResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();
	try {
	    authenticateUserRequest.setPassword(Utilities.decryptDataWithCipher(authenticateUserRequest.getPassword()));
	} catch (Exception e) {
	    Utilities.printDebugLog("Not Encrypted", logger);
	}

	String requestJsonESB = mapper.writeValueAsString(authenticateUserRequest);

	String path = GetConfigurations.getESBRoute("authenticateUser");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	String response = "";
	authenticateUserResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	response = rc.getResponseFromESB(path, requestJsonESB);
	authenticateUserResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	authenticateUserResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		authenticateUserResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		TypeReference<List<SupplementaryOfferings>> mapType = new TypeReference<List<SupplementaryOfferings>>() {
		};
		resData.setSupplementaryOfferingList(mapper.readValue(Utilities.getValueFromJSON(
			Utilities.getValueFromJSON(response, "customerData"), "supplementaryOfferingList"), mapType));

		resData.setPrimaryOffering(mapper.readValue(Utilities
			.getValueFromJSON(Utilities.getValueFromJSON(response, "customerData"), "PrimaryOffering"),
			PrimaryOfferings.class));

		String customerInfo = Utilities.getValueFromJSON(response, "customerData");
		customerInfo = Utilities.removeParamsFromJSONObject(customerInfo, "supplementaryOfferingList");
		customerInfo = Utilities.removeParamsFromJSONObject(customerInfo, "PrimaryOffering");

		resData.setCustomerData(mapper.readValue(customerInfo, CustomerInfo.class));

		resData.getCustomerData().setStatusDetails(Utilities.getValueFromJSON(customerInfo, "statusCode"));
		resData.getCustomerData().setStatus(GetMessagesMappings.getLabelsFromResourceBundle(
			"status." + resData.getCustomerData().getStatusDetails(), authenticateUserRequest.getLang()));
		resData.getCustomerData().setSIM(Utilities.getValueFromJSON(customerInfo, "simNumber"));
		resData.getCustomerData().setPIN(Utilities.getValueFromJSON(customerInfo, "pinCode"));
		resData.getCustomerData().setPUK(Utilities.getValueFromJSON(customerInfo, "pukCode"));

		resData.getCustomerData()
			.setImageURL(Utilities.getProfileImageURL(authenticateUserRequest.getMsisdn()));

		resData.getCustomerData().setBillingLanguage(Utilities
			.remapBillingLanguageForApp(Utilities.getValueFromJSON(customerInfo, "billingLanguage")));
		resData.getCustomerData()
			.setLoyaltySegment(GetMessagesMappings.getLabelsFromResourceBundle(
				"loyalty." + resData.getCustomerData().getLoyaltySegment().toLowerCase(),
				authenticateUserRequest.getLang()));

		// Fetching Email and Password hash
		String emailAndPassHash = Utilities.getEmailByEntityId(resData.getCustomerData().getEntityId(),
			resData.getCustomerData().getMsisdn(), resData.getCustomerData().getCustomerId());
		resData.getCustomerData().setEMAIL(emailAndPassHash.split(";")[0]);

		// preparing Token
		resData.getCustomerData()
			.setToken(Utilities.prepareToken(msisdn, deviceID, emailAndPassHash.split(";")[1]));

		// Fetching predefined data
		predefinedDataRequest.setOfferingId(resData.getCustomerData().getOfferingId());
		resData.setPredefinedData(getPredefinedData(authenticateUserRequest.getMsisdn(), predefinedDataRequest,
			predefinedDataResponse, resData.getCustomerData().getOfferingId()));
		resData.getPredefinedData().setRoamingVisible(Utilities.getRoamingVisibleFlag(msisdn, resData));
		this.logger.info(authenticateUserRequest.getMsisdn() + " - Setting up live chat link");
		String liveChat = setLiveChatLink(authenticateUserRequest.getLang(),
			authenticateUserRequest.getChannel(), resData.getCustomerData());
		resData.getPredefinedData().setLiveChat(liveChat);

		authenticateUserResponse.setData(resData);
		authenticateUserResponse.setCallStatus(Constants.Call_Status_True);
		authenticateUserResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		authenticateUserResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", authenticateUserRequest.getLang()));
	    } else {
		authenticateUserResponse.setCallStatus(Constants.Call_Status_False);
		authenticateUserResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		authenticateUserResponse.setResultDesc(Utilities.getErrorMessageFromDB("authenticate.user",
			authenticateUserRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    authenticateUserResponse.setCallStatus(Constants.Call_Status_False);
	    authenticateUserResponse.setResultCode(Constants.API_FAILURE_CODE);
	    authenticateUserResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", authenticateUserRequest.getLang()));
	}

	return authenticateUserResponse;
    }

    public ForgotPasswordResponse forgotPasswordBusiness(String msisdn, String deviceID,
	    ForgotPasswordRequest forgotPasswordRequest, ForgotPasswordResponse forgotPasswordResponse)
	    throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.FORGOT_PASSWORD_TRANSACTION_NAME
		+ " BUSINESS with data-" + forgotPasswordRequest.toString(), logger);

	// preparing customerInfo Request Packet
	CustomerInfoResponseData custInfoResponse = new CustomerInfoResponseData();
	CustomerInfoRequest customerInfoRequest = new CustomerInfoRequest();

	customerInfoRequest.setChannel(forgotPasswordRequest.getChannel());
	customerInfoRequest.setiP(forgotPasswordRequest.getiP());
	customerInfoRequest.setLang(forgotPasswordRequest.getLang());
	customerInfoRequest.setMsisdn(forgotPasswordRequest.getMsisdn());
	customerInfoRequest.setIsB2B(forgotPasswordRequest.getIsB2B());
	
	ForgotPasswordResponseData resData = new ForgotPasswordResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	if (forgotPasswordRequest.getTemp().length() == 4) {
	    forgotPasswordRequest.setTemp(forgotPasswordRequest.getTemp());
	    forgotPasswordRequest.setPassword(forgotPasswordRequest.getPassword());
	    forgotPasswordRequest.setConfirmPassword(forgotPasswordRequest.getConfirmPassword());
	} else {
	    forgotPasswordRequest.setTemp(Utilities.decryptDataWithCipher(forgotPasswordRequest.getTemp()));
	    forgotPasswordRequest.setPassword(Utilities.decryptDataWithCipher(forgotPasswordRequest.getPassword()));
	    forgotPasswordRequest
		    .setConfirmPassword(Utilities.decryptDataWithCipher(forgotPasswordRequest.getConfirmPassword()));
	}
	String requestJsonESB = mapper.writeValueAsString(forgotPasswordRequest);
	String path = GetConfigurations.getESBRoute("forgotPassword");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	forgotPasswordResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	forgotPasswordResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	forgotPasswordResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, forgotPasswordResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		/*
		 * Getting last updated email from MAGENTO Database as customerInfo returns
		 * email which is being saved at BAKCELL end.
		 */
		// custInfoResponse.getCustomerData()
		// .setEMAIL(Utilities.getEmailByEntityId(
		// custInfoResponse.getCustomerData().getEntityId().split(";")[0],
		// custInfoResponse.getCustomerData().getMsisdn()));

		custInfoResponse = getCustomerData(msisdn, deviceID, customerInfoRequest, custInfoResponse,
			Constants.TOKEN_CREATION_CALL_TYPE_EXTERNAL, null,
			Utilities.getValueFromJSON(response, "entityId"));

		custInfoResponse.getCustomerData().setEntityId(Utilities.getValueFromJSON(response, "entityId"));

		resData.setCustomerData(custInfoResponse.getCustomerData());

		resData.setPrimaryOffering(custInfoResponse.getPrimaryOffering());
		resData.setSupplementaryOfferingList(custInfoResponse.getSupplementaryOfferingList());
		resData.setPrimaryOffering(custInfoResponse.getPrimaryOffering());

		resData.setPredefinedData(custInfoResponse.getPredefinedData());

		this.logger.info(customerInfoRequest.getMsisdn() + " - Setting up live chat link");
		String liveChat = setLiveChatLink(customerInfoRequest.getLang(), customerInfoRequest.getChannel(),
			resData.getCustomerData());
		resData.getPredefinedData().setLiveChat(liveChat);

		forgotPasswordResponse.setData(resData);
		forgotPasswordResponse.setCallStatus(Constants.Call_Status_True);
		forgotPasswordResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		forgotPasswordResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", forgotPasswordRequest.getLang()));

	    } else {

		forgotPasswordResponse.setCallStatus(Constants.Call_Status_False);
		forgotPasswordResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		forgotPasswordResponse.setResultDesc(Utilities.getErrorMessageFromDB("forgot.password",
			forgotPasswordRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    forgotPasswordResponse.setCallStatus(Constants.Call_Status_False);
	    forgotPasswordResponse.setResultCode(Constants.API_FAILURE_CODE);
	    forgotPasswordResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    forgotPasswordRequest.getLang()));
	}

	return forgotPasswordResponse;

    }

    // for phase 2
    public ForgotPasswordResponseV2 forgotPasswordBusinessV2(String msisdn, String deviceID,
	    ForgotPasswordRequestV2 forgotPasswordRequest, ForgotPasswordResponseV2 forgotPasswordResponse)
	    throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.FORGOT_PASSWORD_TRANSACTION_NAME
		+ " BUSINESS with data-" + forgotPasswordRequest.toString(), logger);

	ForgotPasswordResponseDataV2 resData = new ForgotPasswordResponseDataV2();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	forgotPasswordRequest.setTemp(Utilities.decryptDataWithCipher(forgotPasswordRequest.getTemp()));
	forgotPasswordRequest.setPassword(Utilities.decryptDataWithCipher(forgotPasswordRequest.getPassword()));
	forgotPasswordRequest
		.setConfirmPassword(Utilities.decryptDataWithCipher(forgotPasswordRequest.getConfirmPassword()));

	// preparing login Request Packet
	DashboardRequest loginRequest = new DashboardRequest();
	loginRequest.setChannel(forgotPasswordRequest.getChannel());
	loginRequest.setiP(forgotPasswordRequest.getiP());
	loginRequest.setLang(forgotPasswordRequest.getLang());
	loginRequest.setMsisdn(msisdn);
	loginRequest.setPassword(forgotPasswordRequest.getPassword());
	loginRequest.setIsB2B(forgotPasswordRequest.getIsB2B());

	String requestJsonESB = mapper.writeValueAsString(forgotPasswordRequest);
	String path = GetConfigurations.getESBRoute("forgotPasswordV2");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	forgotPasswordResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	forgotPasswordResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	forgotPasswordResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, forgotPasswordResponse.getLogsReport()));
	String resultDesc = Utilities.getValueFromJSON(response, "msg");
	if (response != null && !response.isEmpty()) {
	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		path = GetConfigurations.getESBRoute("loginUser");
		requestJsonESB = mapper.writeValueAsString(loginRequest);
		Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
		response = rc.getResponseFromESB(path, requestJsonESB);
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
		if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		    LoginData loginData = mapper.readValue(Utilities.getValueFromJSON(response, "loginData"),
			    LoginData.class);
		    // PredefinedDataResponseDataV2 predefinedData =
		    // mapper.readValue(Utilities.getValueFromJSON(response,
		    // "predefineData"), PredefinedDataResponseDataV2.class);
		    String emailAndPassHash = Utilities.getEmailByEntityIdV2(loginData.getEntity_id(),
			    loginData.getMsisdn(), loginData.getCustomer_id());
		    logger.info("<<<<<<<<< Email and password hash is:" + emailAndPassHash);
		    loginData.setToken(Utilities.prepareToken(msisdn, deviceID, emailAndPassHash.split(";")[1]));
		    resData.setLoginData(loginData);
		    forgotPasswordResponse.setCallStatus(Constants.Call_Status_True);
		    forgotPasswordResponse.setResultCode(Constants.VALIDATION_SUCCESS_CODE);
		    forgotPasswordResponse.setResultDesc(resultDesc);
		    forgotPasswordResponse.setData(resData);
		    return forgotPasswordResponse;
		}

	    } else {

		forgotPasswordResponse.setCallStatus(Constants.Call_Status_False);
		forgotPasswordResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		forgotPasswordResponse.setResultDesc(Utilities.getErrorMessageFromDB("forgot.password",
			forgotPasswordRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    forgotPasswordResponse.setCallStatus(Constants.Call_Status_False);
	    forgotPasswordResponse.setResultCode(Constants.API_FAILURE_CODE);
	    forgotPasswordResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    forgotPasswordRequest.getLang()));
	}
	return forgotPasswordResponse;
    }

    public ChangePasswordResponse changePasswordBusiness(String msisdn, ChangePasswordRequest changePasswordRequest,
	    ChangePasswordResponse changePasswordResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.CHANGE_PASSWORD_TRANSACTION_NAME
		+ " BUSINESS with data-" + changePasswordRequest.toString(), logger);

	ChangePasswordResponseData resData = new ChangePasswordResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	changePasswordRequest.setOldPassword(Utilities.decryptDataWithCipher(changePasswordRequest.getOldPassword()));
	changePasswordRequest.setNewPassword(Utilities.decryptDataWithCipher(changePasswordRequest.getNewPassword()));
	changePasswordRequest
		.setConfirmNewPassword(Utilities.decryptDataWithCipher(changePasswordRequest.getConfirmNewPassword()));

	String requestJsonESB = mapper.writeValueAsString(changePasswordRequest);
	String path = GetConfigurations.getESBRoute("changepassword");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	changePasswordResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	changePasswordResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	changePasswordResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, changePasswordResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		resData.setMessage(
			GetMessagesMappings.getMessageFromResourceBundle("success", changePasswordRequest.getLang()));
		changePasswordResponse.setData(resData);
		changePasswordResponse.setCallStatus(Constants.Call_Status_True);
		changePasswordResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		changePasswordResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", changePasswordRequest.getLang()));

	    } else {
		changePasswordResponse.setCallStatus(Constants.Call_Status_False);
		changePasswordResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		changePasswordResponse.setResultDesc(Utilities.getErrorMessageFromDB("change.password",
			changePasswordRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {
	    changePasswordResponse.setCallStatus(Constants.Call_Status_False);
	    changePasswordResponse.setResultCode(Constants.API_FAILURE_CODE);
	    changePasswordResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    changePasswordRequest.getLang()));
	}

	return changePasswordResponse;
    }

    // Phase 2

    public ChangePasswordResponse changePasswordBusinessV2(String msisdn, ChangePasswordRequestV2 changePasswordRequest,
	    ChangePasswordResponse changePasswordResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.CHANGE_PASSWORD_TRANSACTION_NAME
		+ " BUSINESS with data-" + changePasswordRequest.toString(), logger);

	ChangePasswordResponseData resData = new ChangePasswordResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	changePasswordRequest.setOldPassword(Utilities.decryptDataWithCipher(changePasswordRequest.getOldPassword()));
	changePasswordRequest.setNewPassword(Utilities.decryptDataWithCipher(changePasswordRequest.getNewPassword()));
	changePasswordRequest
		.setConfirmNewPassword(Utilities.decryptDataWithCipher(changePasswordRequest.getConfirmNewPassword()));

	String requestJsonESB = mapper.writeValueAsString(changePasswordRequest);
	String path = GetConfigurations.getESBRoute("changepasswordV2");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	changePasswordResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	changePasswordResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	changePasswordResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, changePasswordResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase("204")) {
		resData.setMessage(
			GetMessagesMappings.getMessageFromResourceBundle("success", changePasswordRequest.getLang()));
		changePasswordResponse.setData(resData);
		changePasswordResponse.setCallStatus(Constants.Call_Status_True);
		changePasswordResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		changePasswordResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", changePasswordRequest.getLang()));

	    } else {
		changePasswordResponse.setCallStatus(Constants.Call_Status_False);
		changePasswordResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		changePasswordResponse.setResultDesc(Utilities.getErrorMessageFromDB("change.password",
			changePasswordRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {
	    changePasswordResponse.setCallStatus(Constants.Call_Status_False);
	    changePasswordResponse.setResultCode(Constants.API_FAILURE_CODE);
	    changePasswordResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    changePasswordRequest.getLang()));
	}

	return changePasswordResponse;
    }

    public UpdateCustomerEmailResponse updateCustomerEmailBusiness(String msisdn,
	    UpdateCustomerEmailRequest updateCustomerEmailRequest,
	    UpdateCustomerEmailResponse updateCustomerEmailResponse) throws JSONException, IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.UPDATE_CUSTOMER_EMAIL_TRANSACTION_NAME
		+ " BUSINESS with data-" + updateCustomerEmailRequest.toString(), logger);

	UpdateCustomerEmailResponseData resData = new UpdateCustomerEmailResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(updateCustomerEmailRequest);
	String path = GetConfigurations.getESBRoute("updateCustomerEmail");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	updateCustomerEmailResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	updateCustomerEmailResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	updateCustomerEmailResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		updateCustomerEmailResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		resData.setMessage(Utilities.getErrorMessageFromDB("update.email.customer",
			updateCustomerEmailRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
		updateCustomerEmailResponse.setData(resData);
		updateCustomerEmailResponse.setCallStatus(Constants.Call_Status_True);
		updateCustomerEmailResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		updateCustomerEmailResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle(
			"update.email.customer." + Utilities.getValueFromJSON(response, "returnCode"),
			updateCustomerEmailRequest.getLang()));

	    } else {
		updateCustomerEmailResponse.setCallStatus(Constants.Call_Status_False);
		updateCustomerEmailResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		updateCustomerEmailResponse.setResultDesc(Utilities.getErrorMessageFromDB("update.email.customer",
			updateCustomerEmailRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    updateCustomerEmailResponse.setCallStatus(Constants.Call_Status_False);
	    updateCustomerEmailResponse.setResultCode(Constants.API_FAILURE_CODE);
	    updateCustomerEmailResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", updateCustomerEmailRequest.getLang()));
	}

	return updateCustomerEmailResponse;
    }

    public ChangeBillingLanguageResponse changeBillingLanguageBusiness(String msisdn,
	    ChangeBillingLanguageRequest changeBillingLanguageRequest,
	    ChangeBillingLanguageResponse changeBillingLanguageResponse)
	    throws JSONException, IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.CHANGE_BILLING_LANGUAGE_TRANSACTION_NAME
		+ " BUSINESS with data-" + changeBillingLanguageRequest.toString(), logger);

	ChangeBillingLanguageResponseData resData = new ChangeBillingLanguageResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	// Re-Mapping Billing Language
	changeBillingLanguageRequest
		.setLanguage(Utilities.remapBillingLanguageForESB(changeBillingLanguageRequest.getLanguage()));

	String requestJsonESB = mapper.writeValueAsString(changeBillingLanguageRequest);
	String path = GetConfigurations.getESBRoute("changeBillingLanguage");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	changeBillingLanguageResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	changeBillingLanguageResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	changeBillingLanguageResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		changeBillingLanguageResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		resData.setMessage(GetMessagesMappings.getMessageFromResourceBundle("success.billing",
			changeBillingLanguageRequest.getLang()));
		changeBillingLanguageResponse.setData(resData);
		changeBillingLanguageResponse.setCallStatus(Constants.Call_Status_True);
		changeBillingLanguageResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		changeBillingLanguageResponse.setResultDesc(GetMessagesMappings
			.getMessageFromResourceBundle("success.billing", changeBillingLanguageRequest.getLang()));

	    } else {

		changeBillingLanguageResponse.setCallStatus(Constants.Call_Status_False);
		changeBillingLanguageResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		changeBillingLanguageResponse.setResultDesc(Utilities.getErrorMessageFromDB("change.billing.lang",
			changeBillingLanguageRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    changeBillingLanguageResponse.setCallStatus(Constants.Call_Status_False);
	    changeBillingLanguageResponse.setResultCode(Constants.API_FAILURE_CODE);
	    changeBillingLanguageResponse.setResultDesc(GetMessagesMappings
		    .getMessageFromResourceBundle("connectivity.error", changeBillingLanguageRequest.getLang()));
	}

	return changeBillingLanguageResponse;
    }

    public AppResumeResponse appResumeBusiness(String msisdn, String deviceID, String passHash,
	    AppResumeRequest appResumeRequest, AppResumeResponse appResumeResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.APP_RESUME_TRANSACTION_NAME
		+ " BUSINESS with data-" + appResumeRequest.toString(), logger);

	Utilities.printDebugLog(msisdn + "DEVICE ID-" + deviceID, logger);
	Utilities.printDebugLog(msisdn + "PASSWORD HASH-" + passHash, logger);

	// preparing customerInfo Request Packet
	CustomerInfoResponseData custInfoResponse = new CustomerInfoResponseData();
	CustomerInfoRequest customerInfoRequest = new CustomerInfoRequest();
	AppResumeResponseData resData = new AppResumeResponseData();

	customerInfoRequest.setChannel(appResumeRequest.getChannel());
	customerInfoRequest.setiP(appResumeRequest.getiP());
	customerInfoRequest.setLang(appResumeRequest.getLang());
	customerInfoRequest.setMsisdn(appResumeRequest.getMsisdn());
	customerInfoRequest.setIsB2B(appResumeRequest.getIsB2B());
	
	custInfoResponse = getCustomerData(msisdn, deviceID, customerInfoRequest, custInfoResponse,
		Constants.TOKEN_CREATION_CALL_TYPE_EXTERNAL, appResumeRequest.getCustomerId(),
		appResumeRequest.getEntityId());
	if (custInfoResponse.getResultCode().equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

	    custInfoResponse.getCustomerData().setEntityId(appResumeRequest.getEntityId());
	    resData.setCustomerData(custInfoResponse.getCustomerData());
	    resData.setPrimaryOffering(custInfoResponse.getPrimaryOffering());
	    resData.setSupplementaryOfferingList(custInfoResponse.getSupplementaryOfferingList());
	    resData.setPrimaryOffering(custInfoResponse.getPrimaryOffering());
	    resData.setPredefinedData(custInfoResponse.getPredefinedData());

	    this.logger.info(customerInfoRequest.getMsisdn() + " - Setting up live chat link");
	    String liveChat = setLiveChatLink(customerInfoRequest.getLang(), customerInfoRequest.getChannel(),
		    resData.getCustomerData());
	    resData.getPredefinedData().setLiveChat(liveChat);

	    Utilities.printDebugLog(msisdn + "-Matching passwords", logger);

	    Utilities.printDebugLog(msisdn + "-StatusCustomerDetails :" + resData.getCustomerData().getStatus(),
		    logger);
	    Utilities.printDebugLog(msisdn + "-StatusCustomerStatus :" + resData.getCustomerData().getStatusDetails(),
		    logger);
	    if (resData.getCustomerData().getStatusDetails().equalsIgnoreCase("B01")
		    || resData.getCustomerData().getStatusDetails().equalsIgnoreCase("B04")
		    || resData.getCustomerData().getStatusDetails().equalsIgnoreCase("B03")) {
		if (isHashMatched(msisdn, passHash, resData.getCustomerData())) {
		    appResumeResponse.setData(resData);
		    appResumeResponse.setCallStatus(Constants.Call_Status_True);
		    appResumeResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		    appResumeResponse.setResultDesc(
			    GetMessagesMappings.getMessageFromResourceBundle("success", appResumeRequest.getLang()));
		} else {
		    appResumeResponse.setCallStatus(Constants.Call_Status_False);
		    appResumeResponse.setResultCode(Constants.INVALID_TOKEN);
		    appResumeResponse.setResultDesc(GetMessagesMappings
			    .getMessageFromResourceBundle("token.session.expired", appResumeRequest.getLang()));

		}

	    } // hereshouldbeELSe
	    else {
		appResumeResponse.setCallStatus(Constants.Call_Status_False);
		appResumeResponse.setResultCode(Constants.SESSION_EXPIRED);
		appResumeResponse.setResultDesc(GetMessagesMappings
			.getMessageFromResourceBundle("token.session.expired", appResumeRequest.getLang()));
		return appResumeResponse;
	    }
	} else {
	    appResumeResponse.setCallStatus(Constants.Call_Status_False);
	    appResumeResponse.setResultCode(Constants.API_FAILURE_CODE);
	    appResumeResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", appResumeRequest.getLang()));
	}

	return appResumeResponse;
    }

    private boolean isHashMatched(String msisdn, String passHash, CustomerInfo customerInfo)
	    throws ClassNotFoundException, SQLException, JSONException, UnsupportedEncodingException, SocketException {
	/**
	 * We already called Database to get Email and Password hash at the time of
	 * Customer Info and stored hash into token. Therefore, rather than calling DB
	 * again to get hash, getting updated hash from token to compare it with old
	 * token which we got from requested call.
	 *
	 */

	JSONObject tokenSplitted = Utilities.splitToken(Utilities.decodeString(customerInfo.getToken()));
	if (tokenSplitted.has("passHash")) {
	    String hashFromDB = Utilities.getValueFromJSON(
		    Utilities.splitToken(Utilities.decodeString(customerInfo.getToken())).toString(), "passHash");
	    Utilities.printDebugLog(msisdn + "- OLD PASSWORD HASH OBTAINED FROM TOKEN" + passHash, logger);
	    Utilities.printDebugLog(msisdn + "- NEW PASSWORD HASH FETCHED FROM MAGENTO-" + hashFromDB, logger);
	    if (passHash.equals(hashFromDB))
		Utilities.printDebugLog(msisdn + "- PASSWORD HASH MATCHED SUCCESSFULLY", logger);
	    else
		Utilities.printDebugLog(msisdn + "- PASSWORD HASH MATCHED FAILED", logger);
	    if (customerInfo.getMagCustomerId().equals(customerInfo.getCustomerId()))
		Utilities.printDebugLog(msisdn + "- CUSTOMER ID MATCHED SUCCESSFULLY.", logger);
	    else
		Utilities.printDebugLog(msisdn + "- CUSTOMER ID MATCHED FAILED.", logger);
	    return ((passHash.equals(hashFromDB))
		    && (customerInfo.getMagCustomerId().equals(customerInfo.getCustomerId())));
	}
	return false;
	// return true;
    }

    public LogoutResponse logoutBusiness(String msisdn, String deviceID, LogoutRequest logoutRequest,
	    LogoutResponse logoutResponse) throws ClassNotFoundException, SQLException, IOException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.LOGOUT_TRANSACTION_NAME
		+ " BUSINESS with data-" + logoutRequest.toString(), logger);
	NotificationsBusiness notificationsBusiness = new NotificationsBusiness();
	AddFCMRequest addFCMRequest = new AddFCMRequest();

	addFCMRequest.setMsisdn(logoutRequest.getMsisdn());
	addFCMRequest.setFcmKey("");
	addFCMRequest.setIsEnable("0");
	addFCMRequest.setRingingStatus("");
	Utilities.printDebugLog(msisdn + "-Request to Update FCM Key -" + addFCMRequest.toString(), logger);

	Utilities.printDebugLog(msisdn + "-Response after updating FCM Key-"
		+ notificationsBusiness.updateFCMKey(deviceID, addFCMRequest), logger);

	// Removing session which is stored in hashmap.
	// AppCache.getHashmapSessions().remove(Utilities.getHashmapKeyForSession(msisdn,
	// deviceID));

	logoutResponse.setCallStatus(Constants.Call_Status_True);
	logoutResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
	logoutResponse
		.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("success", logoutRequest.getLang()));
	return logoutResponse;
    }

    public CustomerInfoResponseData getCustomerData(String msisdn, String deviceID,
	    CustomerInfoRequest customerInfoRequest, CustomerInfoResponseData resData, int callType, String customerId,
	    String entityId) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.CUSTOMER_DATA_TRANSACTION_NAME
		+ " BUSINESS with data-" + customerInfoRequest.toString(), logger);
	PredefinedDataRequest predefinedDataRequest = new PredefinedDataRequest();
	PredefinedDataResponse predefinedDataResponse = new PredefinedDataResponse();

	predefinedDataRequest.setChannel(customerInfoRequest.getChannel());
	predefinedDataRequest.setiP(customerInfoRequest.getiP());
	predefinedDataRequest.setLang(customerInfoRequest.getLang());
	predefinedDataRequest.setMsisdn(customerInfoRequest.getMsisdn());
	predefinedDataRequest.setIsB2B(customerInfoRequest.getIsB2B());

	CustomerInfo cInfo = null;
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(customerInfoRequest);

	String path = GetConfigurations.getESBRoute("getCustomerInfo");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB (CUSTOMER INFO)", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	resData.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	resData.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	resData.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response, resData.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		// Parsing Supplementary Offering List
		String responseArraySupplementaryOfferings = Utilities.getValueFromJSON(
			Utilities.getValueFromJSON(response, "customerData"), "supplementaryOfferingList");

		TypeReference<List<SupplementaryOfferings>> mapTypeSupplementaryOfferings = new TypeReference<List<SupplementaryOfferings>>() {
		};
		resData.setSupplementaryOfferingList(
			mapper.readValue(responseArraySupplementaryOfferings, mapTypeSupplementaryOfferings));

		// Parsing Primary Offering
		resData.setPrimaryOffering(mapper.readValue(Utilities
			.getValueFromJSON(Utilities.getValueFromJSON(response, "customerData"), "PrimaryOffering"),
			PrimaryOfferings.class));

		// Parsing Customer Data
		String customerInfo = Utilities.getValueFromJSON(response, "customerData");
		customerInfo = Utilities.removeParamsFromJSONObject(customerInfo, "supplementaryOfferingList");
		customerInfo = Utilities.removeParamsFromJSONObject(customerInfo, "PrimaryOffering");

		cInfo = mapper.readValue(customerInfo, CustomerInfo.class);
		resData.setCustomerData(cInfo);

		resData.getCustomerData()
			.setLoyaltySegment(GetMessagesMappings.getLabelsFromResourceBundle(
				"loyalty." + resData.getCustomerData().getLoyaltySegment().toLowerCase(),
				customerInfoRequest.getLang()));

		// As JSON keys are different in ESB response JSON, therefore
		// setting below values manually.
		resData.getCustomerData().setStatusDetails(Utilities.getValueFromJSON(customerInfo, "statusCode"));
		resData.getCustomerData().setStatus(GetMessagesMappings.getLabelsFromResourceBundle(
			"status." + resData.getCustomerData().getStatusDetails(), customerInfoRequest.getLang()));
		resData.getCustomerData().setSIM(Utilities.getValueFromJSON(customerInfo, "simNumber"));
		resData.getCustomerData().setPIN(Utilities.getValueFromJSON(customerInfo, "pinCode"));
		resData.getCustomerData().setPUK(Utilities.getValueFromJSON(customerInfo, "pukCode"));

		// Re-Mapping Billing Language
		resData.getCustomerData().setBillingLanguage(Utilities
			.remapBillingLanguageForApp(Utilities.getValueFromJSON(customerInfo, "billingLanguage")));

		// Re-Mapping Customer Status
		resData.getCustomerData().setCustomerStatus(
			Utilities.remapCustomerStatus(Utilities.getValueFromJSON(customerInfo, "statusCode")));

		// Setting Profile Image URL
		resData.getCustomerData().setImageURL(Utilities.getProfileImageURL(customerInfoRequest.getMsisdn()));
		// resData.getCustomerData().setHideNumberTariffIds(Utilities.getValueFromJSON(customerInfo,
		// "statusCode"));
		/*
		 * As some internal server calls do not need token recreation, otherwise app's
		 * token can be expired.therefore below check decide on which call should token
		 * be refreshed.
		 */

		if (customerId == null)
		    customerId = resData.getCustomerData().getCustomerId();

		if (callType != Constants.TOKEN_CREATION_CALL_TYPE_INTERNAL) {
		    String emailAndPassHash = Utilities.getEmailByEntityId(entityId,
			    resData.getCustomerData().getMsisdn(), customerId);
		    Utilities.printDebugLog(msisdn + "-Magento database response-" + emailAndPassHash, logger);
		    if (emailAndPassHash != null && emailAndPassHash.length() > 0) {
			resData.getCustomerData().setEMAIL(emailAndPassHash.split(";")[0]);

			resData.getCustomerData()
				.setToken(Utilities.prepareToken(msisdn, deviceID, emailAndPassHash.split(";")[1]));
			resData.getCustomerData().setMagCustomerId(emailAndPassHash.split(";")[2]);
		    }
		}
		// Fetching predefined data
		predefinedDataRequest.setOfferingId(resData.getCustomerData().getOfferingId());
		resData.setPredefinedData(getPredefinedData(customerInfoRequest.getMsisdn(), predefinedDataRequest,
			predefinedDataResponse, resData.getCustomerData().getOfferingId()));
		resData.getPredefinedData().setRoamingVisible(Utilities.getRoamingVisibleFlag(resData.getCustomerData().getMsisdn(), resData.getCustomerData().getSubscriberType(), resData.getCustomerData().getCustomerType()));

		this.logger.info(predefinedDataRequest.getMsisdn() + " - Setting up live chat link");
		String liveChat = setLiveChatLink(predefinedDataRequest.getLang(), predefinedDataRequest.getChannel(),
			resData.getCustomerData());
		resData.getPredefinedData().setLiveChat(liveChat);

		resData.setCallStatus(Constants.Call_Status_True);
		resData.setResultCode(Constants.ESB_SUCCESS_CODE);

	    } else {
		resData.setCallStatus(Constants.Call_Status_False);
		resData.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		resData.setResultDesc(Utilities.getErrorMessageFromDB("customer.info", customerInfoRequest.getLang(),
			Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else

	{
	    resData.setCallStatus(Constants.Call_Status_False);
	    resData.setResultCode(Constants.API_FAILURE_CODE);
	    resData.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    customerInfoRequest.getLang()));
	}
	return resData;
    }

    public PredefinedDataResponse getPredefinedData(String msisdn, PredefinedDataRequest predefinedDataRequest,
	    PredefinedDataResponse predefinedDataResponse, String offeringId)
	    throws JSONException, IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.GET_PREDEFINED_DATA_TRANSACTION_NAME
		+ " BUSINESS with data-" + predefinedDataRequest.toString(), logger);

	// Get key which will be used to store and retrieve data from hash map.
	String predefinedDataKeyCacheKey = this.getKeyForCache(Constants.HASH_KEY_PREDEFINED_DATA, offeringId,
		predefinedDataRequest.getLang());

	if (AppCache.getHashmapPredefinedData().containsKey(predefinedDataKeyCacheKey)) {

	    predefinedDataResponse = AppCache.getHashmapPredefinedData().get(predefinedDataKeyCacheKey);
	    predefinedDataResponse.getLogsReport().setIsCached("true");
	    Utilities.printDebugLog(
		    msisdn + "-PREDEFINED DATA" + Constants.CACHE_EXISTS_DESCRIPTION + "" + predefinedDataKeyCacheKey
			    + "-Data-" + new ObjectMapper().writeValueAsString(predefinedDataResponse),
		    logger);
	    // commenting this code, because now the paymentLinks will be handled by App
	    // team, number,prefix will be added by team
	    /*
	     * String prefix = predefinedDataRequest.getMsisdn().substring(0, 2); String
	     * number = predefinedDataRequest.getMsisdn().substring(2,
	     * predefinedDataRequest.getMsisdn().length());
	     * predefinedDataResponse.getRedirectionLinks().setOnlinePaymentGatewayURL_AZ(
	     * GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.AZ") +
	     * "&prefix=" + prefix + "&number=" + number);
	     * predefinedDataResponse.getRedirectionLinks().setOnlinePaymentGatewayURL_EN(
	     * GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.EN") +
	     * "&prefix=" + prefix + "&number=" + number);
	     * predefinedDataResponse.getRedirectionLinks().setOnlinePaymentGatewayURL_RU(
	     * GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.RU") +
	     * "&prefix=" + prefix + "&number=" + number);
	     */
	    // We will just get baseurl from configurations and send it as it is to the app
	    // team.
	    predefinedDataResponse.getRedirectionLinks().setOnlinePaymentGatewayURL_AZ(
		    GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.AZ"));
	    predefinedDataResponse.getRedirectionLinks().setOnlinePaymentGatewayURL_EN(
		    GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.EN"));
	    predefinedDataResponse.getRedirectionLinks().setOnlinePaymentGatewayURL_RU(
		    GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.RU"));
	    /**
	     * Below are unread notification pop up messages
	     */
	    Utilities.printDebugLog(msisdn + "-Setting up Notification pop up configurations", logger);
	    NotificationPopupConfig notificationPopupConfig = new NotificationPopupConfig();
	    notificationPopupConfig.setPopupMessage(GetConfigurations
		    .getConfigurationFromCache("notification.popup.message." + predefinedDataRequest.getLang()));

	    notificationPopupConfig.setHour(GetConfigurations.getConfigurationFromCache("notification.popup.timebase"));

	    predefinedDataResponse.setNotificationPopupConfig(notificationPopupConfig);
	    
	    GetConfigurations.loadGoldenPayMessages();
	    predefinedDataResponse.setGoldenPayMessages(AppCache.getGoldenPayMessages());
	   

	   // return predefinedDataResponse;

	} else {
	    RestClient rc = new RestClient();
	    ObjectMapper mapper = new ObjectMapper();

	    String requestJsonESB = mapper.writeValueAsString(predefinedDataRequest);

	    String path = GetConfigurations.getESBRoute("getPredefinedData");
	    Utilities.printDebugLog(msisdn + "-Request Call to ESB (CUSTOMER INFO)", logger);
	    Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	    Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	    predefinedDataResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	    String response = rc.getResponseFromESB(path, requestJsonESB);
	    predefinedDataResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	    Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	    // Logging ESB response code and description.
	    predefinedDataResponse.setLogsReport(Utilities.logESBParamsintoReportLog(requestJsonESB, response,
		    predefinedDataResponse.getLogsReport()));

	    if (response != null && !response.isEmpty()) {

		if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		    predefinedDataResponse = mapper.readValue(Utilities.getValueFromJSON(response, "data"),
			    PredefinedDataResponse.class);

		    String prefix = predefinedDataRequest.getMsisdn().substring(0, 2);
		    String number = predefinedDataRequest.getMsisdn().substring(2,
			    predefinedDataRequest.getMsisdn().length());
		    predefinedDataResponse.setTariffMigrationPrices(getTariffMigrationPrices(offeringId,
			    predefinedDataRequest.getMsisdn(), predefinedDataRequest.getLang()));
		    // predefinedDataResponse.setTariffMigrationPrices(getTariffMigrationPrices());
		    predefinedDataResponse.getRedirectionLinks().setOnlinePaymentGatewayURL_AZ(
			    GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.AZ") + "&prefix="
				    + prefix + "&number=" + number);
		    predefinedDataResponse.getRedirectionLinks().setOnlinePaymentGatewayURL_EN(
			    GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.EN") + "&prefix="
				    + prefix + "&number=" + number);
		    predefinedDataResponse.getRedirectionLinks().setOnlinePaymentGatewayURL_RU(
			    GetConfigurations.getConfigurationFromCache("onlinePaymentGatewayURL.RU") + "&prefix="
				    + prefix + "&number=" + number);
		    /**
		     * Below are unread notification pop up messages
		     */
		    Utilities.printDebugLog(msisdn + "-Setting up Notification pop up configurations", logger);
		    NotificationPopupConfig notificationPopupConfig = new NotificationPopupConfig();
		    notificationPopupConfig.setPopupMessage(GetConfigurations.getConfigurationFromCache(
			    "notification.popup.message." + predefinedDataRequest.getLang()));

		    notificationPopupConfig
			    .setHour(GetConfigurations.getConfigurationFromCache("notification.popup.timebase"));

		    predefinedDataResponse.setNotificationPopupConfig(notificationPopupConfig);
		    
		    //set golden pay message map
		    GetConfigurations.loadGoldenPayMessages();
		    predefinedDataResponse.setGoldenPayMessages(AppCache.getGoldenPayMessages());
		    
		    predefinedDataResponse.setCallStatus(Constants.Call_Status_True);
		    predefinedDataResponse.setResultCode(Constants.ESB_SUCCESS_CODE);

		    // Storing response in hashmap.
		    AppCache.getHashmapPredefinedData().put(predefinedDataKeyCacheKey, predefinedDataResponse);

		} else {
		    predefinedDataResponse.setCallStatus(Constants.Call_Status_False);
		    predefinedDataResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		    predefinedDataResponse.setResultDesc(GetMessagesMappings
			    .getMessageFromResourceBundle("connectivity.error", predefinedDataRequest.getLang()));
		}
	    } else {
		predefinedDataResponse.setCallStatus(Constants.Call_Status_False);
		predefinedDataResponse.setResultCode(Constants.API_FAILURE_CODE);
		predefinedDataResponse.setResultDesc(GetMessagesMappings
			.getMessageFromResourceBundle("connectivity.error", predefinedDataRequest.getLang()));
	    }
	    
	}
	
	//Adding plastic cards to predefined data
    PlasticCard plasticCard = new PlasticCard();
    plasticCard.setCardTypes(Arrays.asList
    		(new CardType(Constants.PLASTIC_CARD_TYPE_MASTER_ID, Constants.PLASTIC_CARD_TYPE_MASTER),
    		 new CardType(Constants.PLASTIC_CARD_TYPE_VISA_ID, Constants.PLASTIC_CARD_TYPE_VISA)));
    predefinedDataResponse.getTopup().setPlasticCard(plasticCard);
    
	return predefinedDataResponse;
    }

    private String getKeyForCache(String hashKeyPredefinedData, String offeringId, String lang) {

	return hashKeyPredefinedData + "." + offeringId + "." + lang;
    }

    public List<TariffMigrationPrices> getTariffMigrationPrices(String msisdn, String offeringId)
	    throws SocketException, SQLException {
	List<TariffMigrationPrices> listMigrationPrices = new ArrayList<>();
	if (AppCache.getHashmapTariffMigration().get(offeringId) != null) {
	    listMigrationPrices = AppCache.getHashmapTariffMigration().get(offeringId);
	    return listMigrationPrices;
	}
	Utilities.printDebugLog(msisdn + "- GET TARIFF MIGRATION PRICES WITH OFFERING ID-" + offeringId, logger);
	String query = "select * from tariff_migration_prices where `from`=?";
	Utilities.printDebugLog(msisdn + "- QUERY-" + query, logger);
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	preparedStatement = DBUtility.getMAGConnection().prepareStatement(query);
	preparedStatement.setString(1, offeringId);
	resultSet = preparedStatement.executeQuery();
	while (resultSet.next()) {
	    TariffMigrationPrices t = new TariffMigrationPrices();
	    t.setKey(resultSet.getString("to"));
	    t.setValue(resultSet.getString("price"));
	    listMigrationPrices.add(t);
	}

	resultSet.close();
	preparedStatement.close();

	AppCache.getHashmapTariffMigration().put(offeringId, listMigrationPrices);

	Utilities.printDebugLog(msisdn + "-List MIGRATION PRICES SIZE-" + listMigrationPrices.size(), logger);
	return listMigrationPrices;
    }

    public List<TariffMigrationPrices> getTariffMigrationPrices() {

	ArrayList<String> listofKeys = new ArrayList<>();
	listofKeys.add("cin@cin@prepaid@en");
	listofKeys.add("cin@cin@prepaid@az");
	listofKeys.add("cin@cin@prepaid@ru");
	listofKeys.add("cin@klass@prepaid@en");
	listofKeys.add("cin@klass@prepaid@az");
	listofKeys.add("cin@klass@prepaid@ru");
	listofKeys.add("klass@cin@prepaid@en");
	listofKeys.add("klass@cin@prepaid@az");
	listofKeys.add("klass@cin@prepaid@ru");
	listofKeys.add("klass@klass@prepaid@en");
	listofKeys.add("klass@klass@prepaid@az");
	listofKeys.add("klass@klass@prepaid@ru");
	listofKeys.add("klass@klass@postpaid@en");
	listofKeys.add("klass@klass@postpaid@az");
	listofKeys.add("klass@klass@postpaid@ru");

	List<TariffMigrationPrices> listMigrationPrices = new ArrayList<>();

	for (int i = 0; i < listofKeys.size(); i++)
	    try {
		listMigrationPrices.add(new TariffMigrationPrices(listofKeys.get(i),
			GetConfigurations.getConfigurationFromCache(listofKeys.get(i))));
	    } catch (SocketException | SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }

	return listMigrationPrices;
    }

    public List<TariffMigrationPrices> getTariffMigrationPrices(String offeringId, String msisdn, String language)
	    throws SocketException, SQLException {

	String cacheKey = offeringId + "." + language;
	List<TariffMigrationPrices> listMigrationPrices = new ArrayList<>();
	if (AppCache.getHashmapTariffMigration().get(cacheKey) != null) {
	    listMigrationPrices = AppCache.getHashmapTariffMigration().get(cacheKey);
	    return listMigrationPrices;
	}
	Utilities.printDebugLog(msisdn + "- GET TARIFF MIGRATION PRICES WITH OFFERING ID-" + offeringId, logger);
	String query = "select * from tariff_migration_prices WHERE type='' and migrate_from=?";
	Utilities.printDebugLog(msisdn + "- QUERY-" + query, logger);
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	preparedStatement = DBUtility.getMAGConnection().prepareStatement(query);
	preparedStatement.setString(1, offeringId);
	resultSet = preparedStatement.executeQuery();
	while (resultSet.next()) {
	    TariffMigrationPrices t = new TariffMigrationPrices();
	    t.setKey(resultSet.getString("migrate_to"));
	    if (language.equals("2")) {
		t.setValue(resultSet.getString("message_ru"));
	    }
	    if (language.equals("3")) {
		t.setValue(resultSet.getString("message_en"));
	    }
	    if (language.equals("4")) {
		t.setValue(resultSet.getString("message_az"));
	    }
	    listMigrationPrices.add(t);
	}
	listMigrationPrices.add(new TariffMigrationPrices("default",
		GetConfigurations.getConfigurationFromCache("default.tariffmigration.price." + language)));
	resultSet.close();
	preparedStatement.close();

	AppCache.getHashmapTariffMigration().put(cacheKey, listMigrationPrices);

	Utilities.printDebugLog(msisdn + "-List MIGRATION PRICES SIZE-" + listMigrationPrices.size(), logger);
	return listMigrationPrices;
    }

    private String setLiveChatLink(String lang, String channel, CustomerInfo customerInfo)
	    throws SocketException, SQLException {
	StringBuilder sBuilder = new StringBuilder();
	try {
	    sBuilder.append(GetConfigurations.getConfigurationFromCache("livechat.baseurl"));
	    if (customerInfo != null) {
		sBuilder.append(customerInfo.getFirstName()).append(" ");
		sBuilder.append(customerInfo.getLastName() == null ? "" : customerInfo.getLastName());
		if (!customerInfo.getEMAIL().contains(customerInfo.getMsisdn())) {
		    sBuilder.append("&pte=").append(customerInfo.getEMAIL() == null ? "" : customerInfo.getEMAIL());
		}
		String language = lang;
		if (language.equals("2")) {
		    language = "ru";
		} else if (language.equals("3")) {
		    language = "en";
		} else if (language.equals("4")) {
		    language = "az";
		}
		sBuilder.append("&ptl=").append(language).append("&ptp=").append(customerInfo.getMsisdn());
		sBuilder.append("&hcgs=MQ__&htgs=MQ__&hfk=MQ__");
		if (channel.equals("ios")) {
		    sBuilder.append("&epc=I2U4MjM0Mg__&esc=I2U4MjM0Mg__");
		} else {
		    sBuilder.append("&epc=I0UwMDAzNA__&esc=I0UwMDAzNA__");
		}
		return sBuilder.toString();
	    }
	    this.logger.info("Live Chat link " + sBuilder.toString());
	} catch (Exception e) {
	    this.logger.error(e);
	}
	return sBuilder.toString();
    }

    public String pinFromText(String pinMsg) {
	StringBuffer sBuffer = new StringBuffer();
	Pattern p = Pattern.compile("-?\\d+");
	Matcher m = p.matcher(pinMsg);
	while (m.find()) {

	    sBuffer.append(m.group());
	}

	return sBuffer.toString();
    }

}

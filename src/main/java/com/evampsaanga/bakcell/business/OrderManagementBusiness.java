package com.evampsaanga.bakcell.business;

import java.net.SocketException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.cancelpending.CancelPendingRequest;
import com.evampsaanga.bakcell.models.cancelpending.CancelPendingResponse;
import com.evampsaanga.bakcell.models.cancelpending.CancelPendingResponseData;
import com.evampsaanga.bakcell.models.orderdetails.OrderDetailsAppServerResponse;
import com.evampsaanga.bakcell.models.orderdetails.OrderDetailsData;
import com.evampsaanga.bakcell.models.orderdetails.OrderDetailsEsbResponse;
import com.evampsaanga.bakcell.models.orderdetails.OrderDetailsRequest;
import com.evampsaanga.bakcell.models.ordermanagementinsert.ChangeLimitInitialMaximumRequest;
import com.evampsaanga.bakcell.models.ordermanagementinsert.OrderManagementInsertRequest;
import com.evampsaanga.bakcell.models.ordermanagementinsert.OrderManagementInsertResponse;
import com.evampsaanga.bakcell.models.ordermanagementinsert.OrderManagementInsertResponseData;
import com.evampsaanga.bakcell.models.ordermanagementinsert.changeLimitInitialMaximumData;
import com.evampsaanga.bakcell.models.ordermanagementinsert.changeLimitInitialMaximumResponse;
import com.evampsaanga.bakcell.models.retryfailed.RetryFailedRequest;
import com.evampsaanga.bakcell.models.retryfailed.RetryFailedResponse;
import com.evampsaanga.bakcell.models.retryfailed.RetryFailedResponseData;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Aqeel Abbas
 *
 */
@Service
public class OrderManagementBusiness {
	Logger logger = Logger.getLogger(OrderManagementBusiness.class);

	public RetryFailedResponse retryFailed(String msisdn, RetryFailedRequest retryFailedRequest,
			RetryFailedResponse retryFailedResponse)
			throws SocketException, JsonProcessingException, JSONException, SQLException {

		retryFailedRequest.setOrderKey("R004");
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.RETRY_FAILED_TRANSECTION_NAME
				+ " BUSINESS with data-" + retryFailedRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();
		RetryFailedResponseData responseData = new RetryFailedResponseData();
		String requestJsonESB = mapper.writeValueAsString(retryFailedRequest);

		String path = GetConfigurations.getESBRoute("insertorders");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		retryFailedResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		retryFailedResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				responseData.setResponseMsg(Utilities.getValueFromJSON(response, "responseMsg"));
				retryFailedResponse.setData(responseData);
				retryFailedResponse.setCallStatus(Constants.Call_Status_True);
				retryFailedResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				retryFailedResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", retryFailedRequest.getLang()));
				return retryFailedResponse;
			} else {
				retryFailedResponse.setCallStatus(Constants.Call_Status_False);
				retryFailedResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				retryFailedResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
				return retryFailedResponse;
			}
		} else {
			retryFailedResponse.setCallStatus(Constants.Call_Status_False);
			retryFailedResponse.setResultCode(Constants.API_FAILURE_CODE);
			retryFailedResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					retryFailedRequest.getLang()));
			return retryFailedResponse;
		}
	}

	public CancelPendingResponse cancelPending(String msisdn, CancelPendingRequest cancelPendingRequest,
			CancelPendingResponse cancelPendingResponse)
			throws SocketException, SQLException, JSONException, JsonProcessingException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.RETRY_FAILED_TRANSECTION_NAME
				+ " BUSINESS with data-" + cancelPendingRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();
		CancelPendingResponseData responseData = new CancelPendingResponseData();
		String requestJsonESB = mapper.writeValueAsString(cancelPendingRequest);

		String path = GetConfigurations.getESBRoute("insertorders");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		cancelPendingResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		cancelPendingResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				responseData.setResponseMsg(Utilities.getValueFromJSON(response, "responseMsg"));
				cancelPendingResponse.setData(responseData);
				cancelPendingResponse.setCallStatus(Constants.Call_Status_True);
				cancelPendingResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				cancelPendingResponse.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", cancelPendingRequest.getLang()));
				return cancelPendingResponse;
			} else {
				cancelPendingResponse.setCallStatus(Constants.Call_Status_False);
				cancelPendingResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				cancelPendingResponse.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
				return cancelPendingResponse;
			}
		} else {
			cancelPendingResponse.setCallStatus(Constants.Call_Status_False);
			cancelPendingResponse.setResultCode(Constants.API_FAILURE_CODE);
			cancelPendingResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					cancelPendingRequest.getLang()));
			return cancelPendingResponse;
		}

	}

	// start of OrderDetails
	public OrderDetailsAppServerResponse orderDetails(String msisdn, OrderDetailsRequest cancelPendingRequest,
			OrderDetailsAppServerResponse responseOrderDetails)
			throws SocketException, SQLException, JSONException, JsonProcessingException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.ORDER_DETAILS_TRANSECTION_NAME
				+ " BUSINESS with data-" + cancelPendingRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		OrderDetailsEsbResponse responseData = new OrderDetailsEsbResponse();

		String requestJsonESB = mapper.writeValueAsString(cancelPendingRequest);

		String path = GetConfigurations.getESBRoute("orderdetails");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		responseOrderDetails.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		responseOrderDetails.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
		try {
			responseData = mapper.readValue(response, OrderDetailsEsbResponse.class);
		}
		// TODO Auto-generated catch block
		catch (Exception e) {
			logger.error("ERROR:", e);
			responseOrderDetails.setCallStatus(Constants.Call_Status_False);
			responseOrderDetails.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
			responseOrderDetails.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
			return responseOrderDetails;

		}
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				// responseData.setResponseMsg(Utilities.getValueFromJSON(response,
				// "responseMsg"));
				OrderDetailsData orderdetail = new OrderDetailsData();
				orderdetail.setOrderdetails(responseData.getOrderDetailsResponse());
				responseOrderDetails.setData(orderdetail);
				responseOrderDetails.setCallStatus(Constants.Call_Status_True);
				responseOrderDetails.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				responseOrderDetails.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", cancelPendingRequest.getLang()));
				return responseOrderDetails;
			} else {
				responseOrderDetails.setCallStatus(Constants.Call_Status_False);
				responseOrderDetails.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				responseOrderDetails.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
				return responseOrderDetails;
			}
		} else {
			responseOrderDetails.setCallStatus(Constants.Call_Status_False);
			responseOrderDetails.setResultCode(Constants.API_FAILURE_CODE);
			responseOrderDetails.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
					cancelPendingRequest.getLang()));
			return responseOrderDetails;
		}

	}

	// end of OrderDetails

	// start of ordermanagementInsert
	public OrderManagementInsertResponse orderMangementInsert(String msisdn, OrderManagementInsertRequest insertRequest,
			OrderManagementInsertResponse responseInsert)
			throws SocketException, SQLException, JSONException, JsonProcessingException {
		Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.INSERT_BULK_ORDER_TRANSACTION_NAME
				+ " BUSINESS with data-" + insertRequest.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		// OrderManagementInsertResponseEsbData responseData=new
		// OrderManagementInsertResponseEsbData();

		String requestJsonESB = mapper.writeValueAsString(insertRequest);

		String path = GetConfigurations.getESBRoute("insertbulkorder");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		responseInsert.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		responseInsert.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());
		/*
		 * try { responseData=mapper.readValue(response,
		 * OrderManagementInsertResponseEsbData.class); } // TODO Auto-generated catch
		 * block catch (Exception e) { logger.error("ERROR:", e);
		 * responseInsert.setCallStatus(Constants.Call_Status_False);
		 * responseInsert.setResultCode(Utilities.getValueFromJSON(response,
		 * "returnCode"));
		 * responseInsert.setResultDesc(Utilities.getValueFromJSON(response,
		 * "returnMsg")); return responseInsert;
		 * 
		 * }
		 */
		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				// responseData.setResponseMsg(Utilities.getValueFromJSON(response,
				// "responseMsg"));

				OrderManagementInsertResponseData apserverData = new OrderManagementInsertResponseData();
				apserverData.setResponseMsg(
						GetMessagesMappings.getMessageFromResourceBundle("success", insertRequest.getLang()));
				responseInsert.setData(apserverData);
				responseInsert.setCallStatus(Constants.Call_Status_True);
				responseInsert.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				responseInsert.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", insertRequest.getLang()));
				return responseInsert;
			} else {
				responseInsert.setCallStatus(Constants.Call_Status_False);
				responseInsert.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				responseInsert.setResultDesc(Utilities.getValueFromJSON(response, "returnMsg"));
				return responseInsert;
			}
		} else {

			responseInsert.setCallStatus(Constants.Call_Status_False);
			responseInsert.setResultCode(Constants.API_FAILURE_CODE);
			responseInsert.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", insertRequest.getLang()));
			return responseInsert;
		}

	}
	// end of ordermanagementInsert

	// start of ChangeLimitMinMax
	public changeLimitInitialMaximumResponse changeLimitMinMax(String msisdn,
			ChangeLimitInitialMaximumRequest requestminmax, changeLimitInitialMaximumResponse responsechangeLimitMinMax)
			throws SocketException, SQLException, JSONException, JsonProcessingException {
		Utilities.printDebugLog(msisdn + "-Request received in  changeLimitMinMax"
				+ Transactions.CHANGE_LIMIT_BULK_GET_MAX_MIN_LIMIT_TRANSACTION_NAME + " BUSINESS with data-"
				+ requestminmax.toString(), logger);

		RestClient rc = new RestClient();
		ObjectMapper mapper = new ObjectMapper();

		// OrderManagementInsertResponseEsbData responseData=new
		// OrderManagementInsertResponseEsbData();

		String requestJsonESB = mapper.writeValueAsString(requestminmax);

		String path = GetConfigurations.getESBRoute("limitpaymentrelation");
		Utilities.printDebugLog(msisdn + "<<<<<<<<< Request Call to ESB >>>>>>>>>", logger);
		Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
		Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

		responsechangeLimitMinMax.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
		String response = rc.getResponseFromESB(path, requestJsonESB);
		responsechangeLimitMinMax.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

		Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);
		if (response != null && !response.isEmpty()) {
			if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
				// responseData.setResponseMsg(Utilities.getValueFromJSON(response,
				// "responseMsg"));

				changeLimitInitialMaximumData apserverData = new changeLimitInitialMaximumData();
				apserverData.setInitialLimit(Utilities.getValueFromJSON(response, "initialLimit"));
				apserverData.setMaxLimit(Utilities.getValueFromJSON(response, "maxLimit"));
				responsechangeLimitMinMax.setData(apserverData);
				responsechangeLimitMinMax.setCallStatus(Constants.Call_Status_True);
				responsechangeLimitMinMax.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
				responsechangeLimitMinMax.setResultDesc(
						GetMessagesMappings.getMessageFromResourceBundle("success", requestminmax.getLang()));
				return responsechangeLimitMinMax;
			} else {
				responsechangeLimitMinMax.setCallStatus(Constants.Call_Status_False);
				responsechangeLimitMinMax.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
				responsechangeLimitMinMax.setResultDesc(GetMessagesMappings
						.getMessageFromResourceBundle("connectivity.error", requestminmax.getLang()));
				return responsechangeLimitMinMax;
			}
		} else {

			responsechangeLimitMinMax.setCallStatus(Constants.Call_Status_False);
			responsechangeLimitMinMax.setResultCode(Constants.API_FAILURE_CODE);
			responsechangeLimitMinMax.setResultDesc(
					GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", requestminmax.getLang()));
			return responsechangeLimitMinMax;
		}

	}

	// end o ChangeLimitMinMAx

}

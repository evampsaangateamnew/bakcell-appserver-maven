/**
 * 
 */
package com.evampsaanga.bakcell.business;

import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Service;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Transactions;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.financialservices.getloan.GetLoanRequest;
import com.evampsaanga.bakcell.models.financialservices.getloan.GetLoanResponse;
import com.evampsaanga.bakcell.models.financialservices.getloan.GetLoanResponseData;
import com.evampsaanga.bakcell.models.financialservices.loanhistory.Loan;
import com.evampsaanga.bakcell.models.financialservices.loanhistory.LoanHistoryRequest;
import com.evampsaanga.bakcell.models.financialservices.loanhistory.LoanHistoryResponse;
import com.evampsaanga.bakcell.models.financialservices.loanhistory.LoanHistoryResponseData;
import com.evampsaanga.bakcell.models.financialservices.moneytransfer.MoneyTransferRequest;
import com.evampsaanga.bakcell.models.financialservices.moneytransfer.MoneyTransferResponse;
import com.evampsaanga.bakcell.models.financialservices.moneytransfer.MoneyTransferResponseData;
import com.evampsaanga.bakcell.models.financialservices.paymenthistory.Payment;
import com.evampsaanga.bakcell.models.financialservices.paymenthistory.PaymentHistoryRequest;
import com.evampsaanga.bakcell.models.financialservices.paymenthistory.PaymentHistoryResponse;
import com.evampsaanga.bakcell.models.financialservices.paymenthistory.PaymentHistoryResponseData;
import com.evampsaanga.bakcell.models.financialservices.topups.TopupRequest;
import com.evampsaanga.bakcell.models.financialservices.topups.TopupResponse;
import com.evampsaanga.bakcell.models.financialservices.topups.TopupResponseData;
import com.evampsaanga.bakcell.restclient.RestClient;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
@Service
public class FinancialServicesBusiness {
    Logger logger = Logger.getLogger(FinancialServicesBusiness.class);

    public TopupResponse getTopupBusiness(String msisdn, TopupRequest topupRequest, TopupResponse topupResponse)
	    throws JSONException, IOException, SQLException {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.TOPUP_TRANSACTION_NAME
		+ " BUSINESS with data-" + topupRequest.toString(), logger);

	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();
	TopupResponseData resData = new TopupResponseData();

	String requestJsonESB = mapper.writeValueAsString(topupRequest);
	String path = GetConfigurations.getESBRoute("requesttopups");

	/*
	 * Redefining request parameters for ESB Request as , we have different params
	 * for request at Mobile and ESB end.
	 */
	String subscriberType = Utilities.getValueFromJSON(requestJsonESB, "subscriberType");
	requestJsonESB = Utilities.removeParamsFromJSONObject(requestJsonESB, "subscriberType");
	requestJsonESB = Utilities.addParamsToJSONObject(requestJsonESB, "customerType", subscriberType);

	/*
	 * Below JSON modification is done just to mask scratch card number. Purpose is
	 * not to print scratch card number in logs.
	 */

	String tempData = requestJsonESB;
	String pinCard = Utilities.getValueFromJSON(tempData, "cardPinNumber");
	pinCard = Utilities.maskString(pinCard, Constants.SCRATCH_CARD_MASKING_COUNT);
	tempData = Utilities.removeParamsFromJSONObject(tempData, "cardPinNumber");
	tempData = Utilities.addParamsToJSONObject(tempData, "cardPinNumber", pinCard);

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + tempData, logger);

	topupResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	topupResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	// String response = HardCodedResponses.TOP_UP;

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	topupResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, topupResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		resData = mapper.readValue(Utilities.getValueFromJSON(response, "balance"), TopupResponseData.class);
		topupResponse.setData(resData);
		topupResponse.setCallStatus(Constants.Call_Status_True);
		topupResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		topupResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", topupRequest.getLang()));

	    } else {
		topupResponse.setCallStatus(Constants.Call_Status_False);
		topupResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		topupResponse.setResultDesc(Utilities.getErrorMessageFromDB("top.up", topupRequest.getLang(),
			Utilities.getValueFromJSON(response, "returnCode")));

	    }
	} else {
	    topupResponse.setCallStatus(Constants.Call_Status_False);
	    topupResponse.setResultCode(Constants.API_FAILURE_CODE);
	    topupResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", topupRequest.getLang()));
	}

	return topupResponse;
    }

    public MoneyTransferResponse moneyTransferBusiness(String msisdn, MoneyTransferRequest moneyTransferRequest,
	    MoneyTransferResponse moneyTransferResponse) throws Exception {
	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.TRANSFER_MONEY_TRANSACTION_NAME
		+ "  BUSINESS with data-" + moneyTransferRequest.toString(), logger);

	RestClient rc = new RestClient();
	ObjectMapper objectMapper = new ObjectMapper();
	MoneyTransferResponseData resData = new MoneyTransferResponseData();

	String requestJsonESB = objectMapper.writeValueAsString(moneyTransferRequest);
	String path = GetConfigurations.getESBRoute("moneyTransfer");

	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	moneyTransferResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	moneyTransferResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	// String response = HardCodedResponses.TRANSFER_BALANCE;
	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	moneyTransferResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, moneyTransferResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {
		resData.setOldBalance(Utilities.getValueFromJSON(response, "oldBalance"));
		resData.setNewBalance(Utilities.getValueFromJSON(response, "newBalance"));
		moneyTransferResponse.setData(resData);
		moneyTransferResponse.setCallStatus(Constants.Call_Status_True);
		moneyTransferResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		moneyTransferResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", moneyTransferRequest.getLang()));

	    } else {
		moneyTransferResponse.setCallStatus(Constants.Call_Status_False);
		moneyTransferResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));
		moneyTransferResponse.setResultDesc(Utilities.getErrorMessageFromDB("money.transfer",
			moneyTransferRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }

	} else {
	    moneyTransferResponse.setCallStatus(Constants.Call_Status_False);
	    moneyTransferResponse.setResultCode(Constants.API_FAILURE_CODE);

	    moneyTransferResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    moneyTransferRequest.getLang()));
	}

	return moneyTransferResponse;
    }

    public GetLoanResponse getLoanBusiness(String msisdn, GetLoanRequest getLoanRequest,
	    GetLoanResponse getLoanResponse) throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.LOAN_REQUEST_TRANSACTION_NAME
		+ " BUSINESS with data-" + getLoanRequest.toString(), logger);

	GetLoanResponseData resData = new GetLoanResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(getLoanRequest);

	String path = GetConfigurations.getESBRoute("getLoan");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);

	getLoanResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	getLoanResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	// String response = HardCodedResponses.GET_LOAN;
	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	getLoanResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, getLoanResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		resData.setNewBalance(Utilities.getValueFromJSON(response, "newBalance"));
		resData.setNewCredit(Utilities.getValueFromJSON(response, "newCredit"));
		getLoanResponse.setData(resData);
		getLoanResponse.setCallStatus(Constants.Call_Status_True);
		getLoanResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);
		getLoanResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", getLoanRequest.getLang()));

	    } else {
		resData.setMessage(Utilities.getErrorMessageFromDB("get.loan", getLoanRequest.getLang(),
			Utilities.getValueFromJSON(response, "returnCode")));
		getLoanResponse.setData(resData);
		getLoanResponse.setCallStatus(Constants.Call_Status_False);
		getLoanResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));

		getLoanResponse.setResultDesc(Utilities.getErrorMessageFromDB("get.loan", getLoanRequest.getLang(),
			Utilities.getValueFromJSON(response, "returnCode")));
	    }

	} else {
	    getLoanResponse.setCallStatus(Constants.Call_Status_False);
	    getLoanResponse.setResultCode(Constants.API_FAILURE_CODE);
	    getLoanResponse.setResultDesc(
		    GetMessagesMappings.getMessageFromResourceBundle("connectivity.error", getLoanRequest.getLang()));
	}

	return getLoanResponse;
    }

    public LoanHistoryResponse getLoanHistory(String msisdn, LoanHistoryRequest loanHistoryRequest,
	    LoanHistoryResponse loanHistoryResponse) throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.LOAN_REQUEST_HISTORY_TRANSACTION_NAME
		+ " BUSINESS with data-" + loanHistoryRequest.toString(), logger);

	LoanHistoryResponseData resData = new LoanHistoryResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(loanHistoryRequest);

	String path = GetConfigurations.getESBRoute("getLoanHistory");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	// String response = HardCodedResponses.LOAN_HISTORY;

	loanHistoryResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	loanHistoryResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	loanHistoryResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, loanHistoryResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		TypeReference<List<Loan>> mapType = new TypeReference<List<Loan>>() {
		};
		resData.setLoanHistory(mapper.readValue(Utilities.getValueFromJSON(response, "loanRequest"), mapType));
		resData.setLoanHistory(this.re_mapMessages(resData.getLoanHistory(), loanHistoryRequest.getLang()));

		loanHistoryResponse.setData(resData);
		loanHistoryResponse.setCallStatus(Constants.Call_Status_True);
		loanHistoryResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		loanHistoryResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", loanHistoryRequest.getLang()));
	    } else {

		loanHistoryResponse.setCallStatus(Constants.Call_Status_False);
		loanHistoryResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));

		loanHistoryResponse.setResultDesc(Utilities.getErrorMessageFromDB("loan.history",
			loanHistoryRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    loanHistoryResponse.setCallStatus(Constants.Call_Status_False);
	    loanHistoryResponse.setResultCode(Constants.API_FAILURE_CODE);

	    loanHistoryResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    loanHistoryRequest.getLang()));
	}

	return loanHistoryResponse;
    }

    public PaymentHistoryResponse getPaymentHistory(String msisdn, PaymentHistoryRequest paymentHistoryRequest,
	    PaymentHistoryResponse paymentHistoryResponse) throws Exception {

	Utilities.printDebugLog(msisdn + "-Request received in " + Transactions.LOAN_PAYMENT_HISTORY_TRANSACTION_NAME
		+ " BUSINESS with data-" + paymentHistoryRequest.toString(), logger);

	PaymentHistoryResponseData resData = new PaymentHistoryResponseData();
	RestClient rc = new RestClient();
	ObjectMapper mapper = new ObjectMapper();

	String requestJsonESB = mapper.writeValueAsString(paymentHistoryRequest);

	String path = GetConfigurations.getESBRoute("getPaymentHistory");
	Utilities.printDebugLog(msisdn + "-Request Call to ESB", logger);
	Utilities.printDebugLog(msisdn + "-Path-" + path, logger);
	Utilities.printDebugLog(msisdn + "-Request Packet-" + requestJsonESB, logger);
	// String response = HardCodedResponses.PAYMENT_HISTORY;

	paymentHistoryResponse.getLogsReport().setEsbRequestTime(Utilities.getReportDateTime());
	String response = rc.getResponseFromESB(path, requestJsonESB);
	paymentHistoryResponse.getLogsReport().setEsbResponseTime(Utilities.getReportDateTime());

	Utilities.printDebugLog(msisdn + "-Received response from ESB-" + response, logger);

	// Logging ESB response code and description.
	paymentHistoryResponse.setLogsReport(
		Utilities.logESBParamsintoReportLog(requestJsonESB, response, paymentHistoryResponse.getLogsReport()));

	if (response != null && !response.isEmpty()) {

	    if (Utilities.getValueFromJSON(response, "returnCode").equalsIgnoreCase(Constants.ESB_SUCCESS_CODE)) {

		TypeReference<List<Payment>> mapType = new TypeReference<List<Payment>>() {
		};
		resData.setPaymentHistory(
			mapper.readValue(Utilities.getValueFromJSON(response, "loanPayment"), mapType));

		paymentHistoryResponse.setData(resData);
		paymentHistoryResponse.setCallStatus(Constants.Call_Status_True);
		paymentHistoryResponse.setResultCode(Constants.APP_SERVER_SUCCESS_CODE);

		paymentHistoryResponse.setResultDesc(
			GetMessagesMappings.getMessageFromResourceBundle("success", paymentHistoryRequest.getLang()));

	    } else {
		paymentHistoryResponse.setCallStatus(Constants.Call_Status_False);
		paymentHistoryResponse.setResultCode(Utilities.getValueFromJSON(response, "returnCode"));

		paymentHistoryResponse.setResultDesc(Utilities.getErrorMessageFromDB("payment.history",
			paymentHistoryRequest.getLang(), Utilities.getValueFromJSON(response, "returnCode")));
	    }
	} else {

	    paymentHistoryResponse.setCallStatus(Constants.Call_Status_False);
	    paymentHistoryResponse.setResultCode(Constants.API_FAILURE_CODE);

	    paymentHistoryResponse.setResultDesc(GetMessagesMappings.getMessageFromResourceBundle("connectivity.error",
		    paymentHistoryRequest.getLang()));
	}

	return paymentHistoryResponse;
    }

    private List<Loan> re_mapMessages(List<Loan> loanHistory, String lang) throws SocketException, SQLException {

	for (int i = 0; i < loanHistory.size(); i++) {
	    Loan loan = new Loan();
	    loan = loanHistory.get(i);
	    if (loan.getStatus().equalsIgnoreCase(Constants.LOAN_STATUS_OPEN)) {
		loan.setStatus(GetMessagesMappings.getLabelsFromResourceBundle(Constants.LOAN_STATUS_INPROGRESS, lang));
	    } else if (loan.getStatus().equalsIgnoreCase(Constants.LOAN_STATUS_CLOSED)) {
		loan.setStatus(GetMessagesMappings.getLabelsFromResourceBundle(Constants.LOAN_STATUS_PAID, lang));
	    }
	    loanHistory.set(i, loan);
	}
	return loanHistory;
    }
}

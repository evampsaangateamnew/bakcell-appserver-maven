/**
 * 
 */
package com.evampsaanga.bakcell.interceptor;

import java.io.IOException;
import java.net.SocketException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.evampsaanga.bakcell.common.messagesmappings.GetMessagesMappings;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Utilities;

/**
 * @author Evamp & Saanga
 *
 */
public class Interceptor implements HandlerInterceptor {

    Logger logger = Logger.getLogger(Interceptor.class);

    @Override
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
	    throws Exception {
	// TODO Auto-generated method stub

    }

    @Override
    public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
	    throws Exception {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2) throws Exception {
	Utilities.printDebugLog("-----------------Interceptor Fire wall-----------------", logger);
	String lang = arg0.getHeader("lang");
	/*
	 * By force, turning flag status to "TRUE" means by passing all token
	 * validations.
	 * 
	 */
	Boolean flag = false;
	if (flag)
	    return flag;

	if (GetConfigurations.getConfigurationFromCache("isServerUnderMaintenance").trim().equalsIgnoreCase("true")) {
	    Utilities.printDebugLog("----------------- SERVER IS CURRENTLY UNDERGONE FOR MAINTENANCE -----------------",
		    logger);
	    arg1 = prepareResponseObject(arg1, Constants.SERVER_UNDER_MAINTENANCE,
		    GetMessagesMappings.getMessageFromResourceBundle("serverUnderMaintenance", lang));
	    return false;
	} else {
	    // arg0 to check if it is b2b or b2c

	    printRequestHeaders(arg0);
	    String token = arg0.getHeader("token");

	    Utilities.printDebugLog(arg0.getHeader("msisdn") + "SECURITY INTERCEPTOR RECEIVED TOKEN-" + token, logger);
	    Utilities.printDebugLog("HEADER ATTRIBUTEs:\nMSISDN-" + arg0.getHeader("msisdn") + "\nDevice ID-"
		    + arg0.getHeader("deviceID") + "\nAgent: " + arg0.getHeader("UserAgent") + "\nPath: "
		    + arg0.getPathInfo() + "\nLANGUAGE: " + lang, logger);

	    if (arg0.getHeader("UserAgent").equalsIgnoreCase(Constants.ALLOWABLE_USER_AGENTS.android.toString())
		    || arg0.getHeader("UserAgent").equalsIgnoreCase(Constants.ALLOWABLE_USER_AGENTS.iPhone.toString())
		    || arg0.getHeader("UserAgent")
			    .equalsIgnoreCase(Constants.ALLOWABLE_USER_AGENTS.portal.toString())) {
		Utilities.printDebugLog("----------------- AGENT MATCHED SUCCESSFULLY -----------------", logger);

		if (arg0.getHeader("token") != null && !arg0.getHeader("token").isEmpty()) {
		    JSONObject tokenObj = Utilities.splitToken(Utilities.decodeString(token));
		    Utilities.printDebugLog(Utilities.decodeString(token), logger);
		    if (tokenObj.getString("msisdn").equalsIgnoreCase(arg0.getHeader("msisdn"))
			    && tokenObj.getString("deviceID").equalsIgnoreCase(arg0.getHeader("deviceID"))) {
			Utilities.printDebugLog("----------------- TOKEN VALIDATED SUCCESSFULLY-----------------",
				logger);
			flag = true;
		    } else {
			Utilities.printDebugLog(
				"----------------- TOKEN INVALID (MSISDN || DEVICE ID NOT MATCHED) -----------------",
				logger);
			Utilities.printDebugLog("----------------- TOKEN MSISDN: " + tokenObj.getString("msisdn"),
				logger);
			Utilities.printDebugLog("----------------- TOKEN DEVICE ID: " + tokenObj.getString("deviceID"),
				logger);

			arg1 = prepareResponseObject(arg1, Constants.INVALID_TOKEN,
				GetMessagesMappings.getMessageFromResourceBundle("token.invalid", lang));
		    }

		} else {
		    Utilities.printDebugLog("----------------- TOKEN NOT FOUND -----------------", logger);
		    arg1 = prepareResponseObject(arg1, Constants.INVALID_TOKEN,
			    GetMessagesMappings.getMessageFromResourceBundle("token.invalid", lang));
		}

	    } else {
		Utilities.printDebugLog(
			"-----------------REQUEST AGEND UNKNOW : REQUEST CANNOT BE PROCEEDED -----------------",
			logger);
		arg1 = prepareResponseObject(arg1, Constants.UNKNOWN_AGENT,
			GetMessagesMappings.getMessageFromResourceBundle("unknown.agent", lang));

	    }
	    return flag;
	}
    }

    private void printRequestHeaders(HttpServletRequest arg0) throws SocketException {
	Utilities.printTraceLog(
		"*********************************** ALL REQUEST HEADERS ******************************* ", logger);
	Utilities.printDebugLog(arg0.getHeader("msisdn") + "-Request For " + arg0.getRequestURL(), logger);
	Enumeration<String> headerNames = arg0.getHeaderNames();
	while (headerNames.hasMoreElements()) {
	    String headerName = headerNames.nextElement();
	    Utilities.printTraceLog(arg0.getHeader("msisdn") + "-" + headerName + ":" + arg0.getHeader(headerName),
		    logger);

	}
	Utilities.printTraceLog(
		"*********************************** ALL REQUEST HEADERS ******************************* ", logger);

    }

    private HttpServletResponse prepareResponseObject(HttpServletResponse arg1, String resultCode, String resultDesc)
	    throws JSONException, IOException {
	JSONObject obj = new JSONObject();
	arg1.setStatus(200);

	obj.put("resultCode", resultCode);
	obj.put("resultDesc", resultDesc);
	obj.put("callStatus", Constants.Call_Status_False);
	arg1.setCharacterEncoding("UTF-8");
	arg1.getWriter().write(obj.toString());
	return arg1;
    }

}

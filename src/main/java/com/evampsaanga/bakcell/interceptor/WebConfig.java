/**
 * 
 */
package com.evampsaanga.bakcell.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.evampsaanga.bakcell.common.utilities.Constants;

/**
 * @author Evamp & Saanga
 *
 */

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
	registry.addMapping("/**");// .allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE",
				   // "PATCH");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

	registry.addInterceptor(new Interceptor()).addPathPatterns(Constants.SERVLET_URL + "/**").excludePathPatterns(
		Constants.SERVLET_URL + "/customerservices/signup",
		Constants.SERVLET_URL + "/customerservices/verifyotp",
		Constants.SERVLET_URL + "/customerservices/savecustomer",
		Constants.SERVLET_URL + "/customerservices/resendpin",
		Constants.SERVLET_URL + "/customerservices/authenticateuser",
		Constants.SERVLET_URL + "/customerservices/forgotpassword",
		Constants.SERVLET_URL + "/generalservices/verifyappversion",
		Constants.SERVLET_URL + "/dashboardservices/gethomepage", Constants.SERVLET_URL + "/historyV2/sendpin",
		Constants.SERVLET_URL + "/historyV2/historyresendpin", Constants.SERVLET_URL + "/historyV2/verifypin",
		Constants.SERVLET_URL + "/customerservicesV2/forgotpassword",
		Constants.SERVLET_URL + "/generalservicesV2/verifyappversion",
		Constants.SERVLET_URL + "/trigger/refreshcache",
		Constants.SERVLET_URL + "/mysubscriptions/getsubscriptionsforportal");

    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
	registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");

	registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

}

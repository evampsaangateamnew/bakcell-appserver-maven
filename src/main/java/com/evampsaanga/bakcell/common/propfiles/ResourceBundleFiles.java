/**
 * 
 */
package com.evampsaanga.bakcell.common.propfiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.SocketException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Utilities;

/**
 * @author Evamp & Saanga
 *
 */
public class ResourceBundleFiles {
    static Logger logger = Logger.getLogger(ResourceBundleFiles.class);
    private static Properties propsForConfig = null;
    private static Properties propsForRoutes = null;
    private static Properties propsForDBQueries = null;
    private static Properties propsForValidations = null;

    public static synchronized void loadConfigProperties() throws SocketException {
	if (propsForConfig == null) {
	    FileInputStream fileInput = null;
	    try {
		File file = new File(Constants.GET_BASE_CONFIG_PATH + "Config.properties");
		fileInput = new FileInputStream(file);
		Reader reader = new InputStreamReader(fileInput, "UTF-8");
		propsForConfig = new Properties();
		propsForConfig.load(reader);
	    } catch (Exception e) {
		Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    } finally {
		try {
		    fileInput.close();
		} catch (Exception e) {
		    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		}
	    }
	}
    }

    public static synchronized void loadESBRoutesProperties() throws SocketException {
	if (propsForRoutes == null) {

	    FileInputStream fileInput = null;
	    try {
		File file = new File(Constants.GET_BASE_CONFIG_PATH + "ESBRoutes.properties");

		fileInput = new FileInputStream(file);
		Reader reader = new InputStreamReader(fileInput, "UTF-8");
		propsForRoutes = new Properties();
		propsForRoutes.load(reader);
		fileInput.close();
	    } catch (Exception e) {
		Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    } finally {
		try {
		    fileInput.close();
		} catch (Exception e) {
		    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		}
	    }
	}
    }

    public static synchronized void loadDBQueriesProperties() throws SocketException {
	if (propsForDBQueries == null) {
	    FileInputStream fileInput = null;
	    try {
		File file = new File(Constants.GET_BASE_CONFIG_PATH + "DBQueries.properties");
		fileInput = new FileInputStream(file);
		Reader reader = new InputStreamReader(fileInput, "UTF-8");
		propsForDBQueries = new Properties();
		propsForDBQueries.load(reader);
		fileInput.close();
	    } catch (Exception e) {
		Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    } finally {
		try {
		    fileInput.close();
		} catch (Exception e) {
		    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		}
	    }
	}

    }

    public static synchronized void loadValidationsProperties() {
	if (propsForValidations == null) {
	    FileInputStream fileInput = null;
	    try {
		File file = new File(Constants.GET_BASE_CONFIG_PATH + "validations.properties");
		fileInput = new FileInputStream(file);
		Reader reader = new InputStreamReader(fileInput, "UTF-8");
		propsForValidations = new Properties();
		propsForValidations.load(reader);
		fileInput.close();
	    } catch (Exception e) {
		try {
		    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		} catch (SocketException e1) {
		    e1.printStackTrace();
		}
	    } finally {
		try {
		    fileInput.close();
		} catch (Exception e) {
		    try {
			Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
		    } catch (SocketException e1) {
			e1.printStackTrace();
		    }
		}
	    }
	}
    }

    public static Properties getResourceBundleForConfig() {
	try {
	    loadConfigProperties();
	    return propsForConfig;
	} catch (Exception e) {
	    try {
		Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    } catch (SocketException e1) {

		e1.printStackTrace();
	    }
	}
	return propsForConfig;
    }

    public static Properties getResourceBundleForESBRoutes() {
	try {
	    loadESBRoutesProperties();
	    return propsForRoutes;
	} catch (Exception e) {
	    try {
		Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    } catch (SocketException e1) {

		e1.printStackTrace();
	    }
	}
	return propsForConfig;
    }

    public static Properties getResourceBundleForDBQueries() {
	try {
	    loadDBQueriesProperties();
	    return propsForDBQueries;
	} catch (Exception e) {
	    try {
		Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	    } catch (SocketException e1) {

		e1.printStackTrace();
	    }
	}
	return propsForConfig;
    }

    public static Properties getResourceBundleForValidations() throws SocketException {
	loadValidationsProperties();
	return propsForValidations;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.common.utilities;

import java.net.SocketException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.evampsaanga.bakcell.common.propfiles.ResourceBundleFiles;

/**
 * @author Evamp & Saanga
 *
 */
public class GetConfigurations {
    static Logger logger = Logger.getLogger(GetConfigurations.class);

    static Properties propsConfig = ResourceBundleFiles.getResourceBundleForConfig();
    static Properties propsESBRoutes = ResourceBundleFiles.getResourceBundleForESBRoutes();
    static Properties propsDB = ResourceBundleFiles.getResourceBundleForDBQueries();
    static String baseURL = "conf_baseURL";

    public static String getConfigurationFromCache(String key) throws SocketException, SQLException {

	Utilities.printDebugLog("GET CONFIGURATIONS FOR KEY-" + key, logger);

	if (AppCache.getHashmapConfigurations().containsKey(key)) {
	    Utilities.printDebugLog("CONFIGURATION FETCHED FROM CACHE WITH KEY-" + key + ":"
		    + AppCache.getHashmapConfigurations().get(key), logger);
	    return AppCache.getHashmapConfigurations().get(key);
	} else {
	    Utilities.printDebugLog("CONFIGURATIONS NOT FOUND IN CACHE", logger);
	    reloadConfigurationsCache(key);
	    Utilities.printDebugLog("SEARCHING KEY (" + key + ") IN CACHE AFTER RELOAD-"
		    + AppCache.getHashmapConfigurations().containsKey(key), logger);
	    if (AppCache.getHashmapConfigurations().containsKey(key)) {
		Utilities.printDebugLog("CONFIGURATION FETCHED FROM CACHE WITH KEY-" + key + ":"
			+ AppCache.getHashmapConfigurations().get(key), logger);
		return AppCache.getHashmapConfigurations().get(key);
	    } else {
		Utilities.printDebugLog("CONFIGURATION KEY(" + key + ") NOT ADDED INTO CONFIGURATION DATABASE.",
			logger);
		return AppCache.getHashmapConfigurations().get("unexpected.error." + getLangFromKey(key));
	    }
	}
    }

    public static void reloadConfigurationsCache(String key) throws SocketException, SQLException {
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	Utilities.printDebugLog("READING CONFIGURATIONS FOR KEY FROM DB-" + key, logger);
	String queryGetConfigurations = getDBConfig("getConfigurations");
	Utilities.printDebugLog("QUERY FOR CONFIGURATIONS-" + queryGetConfigurations, logger);
	preparedStatement = DBUtility.getMAGConnection().prepareStatement(queryGetConfigurations);
	resultSet = preparedStatement.executeQuery();
	while (resultSet.next())
	    AppCache.getHashmapConfigurations().put(resultSet.getString("key"), resultSet.getString("value"));
	if (resultSet != null)
	    resultSet.close();
	if (preparedStatement != null)
	    preparedStatement.close();
    }
    
    public static void loadGoldenPayMessages() throws SocketException, SQLException {
    	Utilities.printDebugLog("Message map "+AppCache.getGoldenPayMessages(), logger);
    	Utilities.printDebugLog("Message map size -"+AppCache.getGoldenPayMessages().size(), logger);
    	if(AppCache.getGoldenPayMessages() == null || AppCache.getGoldenPayMessages().size() == 0)
    	{
    	PreparedStatement preparedStatement = null;
    	ResultSet resultSet = null;
    	Utilities.printDebugLog("Loading Golden pay messages from DB-", logger);
    	String queryGetConfigurations = getDBConfig("getGoldenPayMessages");
    	Utilities.printDebugLog("QUERY FOR GET GOLDEN PAY MESSAGES-" + queryGetConfigurations, logger);
    	preparedStatement = DBUtility.getMAGConnection().prepareStatement(queryGetConfigurations);
    	resultSet = preparedStatement.executeQuery();
    	while (resultSet.next())
    	    AppCache.getGoldenPayMessages().put(resultSet.getString("key"), resultSet.getString("value"));
    	if (resultSet != null)
    	    resultSet.close();
    	if (preparedStatement != null)
    	    preparedStatement.close();
    	}
      }

    public static String getESBRoute(String endPoint) {
	return propsConfig.getProperty(baseURL).trim() + propsESBRoutes.getProperty(endPoint).trim();
    }

    public static String getDBConfig(String key) {
	return propsDB.getProperty(key, "").trim();
    }

    public static String getConfig(String key) {
	return propsConfig.getProperty(key, "").trim();
    }

    public static String getLangFromKey(String key) {
	if (key.contains("en")) {
	    return "en";
	} else if (key.contains("az")) {
	    return "az";
	} else if (key.contains("ru")) {
	    return "ru";
	} else
	    return "en";
    }
}

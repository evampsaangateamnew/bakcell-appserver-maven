package com.evampsaanga.bakcell.common.utilities;

import java.net.SocketException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class DBUtility {
    static Logger logger = Logger.getLogger(DBUtility.class);

    private static Connection connection = null;
    private static Connection connectionMagento = null;
    private static Connection connectionESBDB = null;

    public static synchronized Connection getConnection() throws SQLException, SocketException {

	if (connection == null || connection.isClosed() || !connection.isValid(0)) {
	    Utilities.printDebugLog("Connecting to Database...", logger);

	    DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());

	    String dburl = GetConfigurations.getConfig("url");
	    String username = GetConfigurations.getConfig("username");
	    String password = GetConfigurations.getConfig("password");

	    connection = DriverManager.getConnection(dburl, username, password);
	    if (connection == null || connection.isClosed() || !connection.isValid(0)) {
		Utilities.printDebugLog("Failed to connect to database.", logger);
	    } else {
		Utilities.printDebugLog("Database connected successfully!", logger);
	    }
	}

	return connection;

    }

    public static synchronized Connection getESBDBConnection() throws SQLException, SocketException {

	if (connectionESBDB == null || connectionESBDB.isClosed() || !connectionESBDB.isValid(0)) {
	    Utilities.printDebugLog("Connecting to Database...", logger);

	    DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());

	    String dburl = GetConfigurations.getConfig("transaction.url");
	    String username = GetConfigurations.getConfig("transaction.username");
	    String password = GetConfigurations.getConfig("transaction.password");

	    connectionESBDB = DriverManager.getConnection(dburl, username, password);
	    if (connectionESBDB == null || connectionESBDB.isClosed() || !connectionESBDB.isValid(0)) {
		Utilities.printDebugLog("Failed to connect to database.", logger);
	    } else {
		Utilities.printDebugLog("Database connected successfully!", logger);
	    }
	}

	return connectionESBDB;

    }

    public static synchronized Connection getMAGConnection() throws SQLException, SocketException {

	if (connectionMagento == null || connectionMagento.isClosed() || !connectionMagento.isValid(0)) {
	    Utilities.printDebugLog("Connecting to MAGENTO Database...", logger);

	    DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());

	    String dburl = GetConfigurations.getConfig("urlMAG");
	    String username = GetConfigurations.getConfig("usernameMAG");
	    String password = GetConfigurations.getConfig("passwordMAG");

	    System.out.println(dburl + "-" + username + "-" + password);
	    connectionMagento = DriverManager.getConnection(dburl, username, password);
	    if (connectionMagento == null || connectionMagento.isClosed() || !connectionMagento.isValid(0)) {
		Utilities.printDebugLog("Failed to connect to MAGENTO database.", logger);
	    } else {
		Utilities.printDebugLog("MAGENTO Database connected successfully!", logger);
	    }
	}

	return connectionMagento;

    }

}

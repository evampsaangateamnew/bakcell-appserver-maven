/**
 * 
 */
package com.evampsaanga.bakcell.common.utilities;

/**
 * @author Evamp & Saanga
 *
 */
public class Constants {
	public enum ALLOWABLE_USER_AGENTS {
		android, iPhone, portal
	}

	public static final String SERVLET_URL = "/api";
	// Configurations Path
	public static final String GET_BASE_CONFIG_PATH = "//opt//tomcat//conf//bakcellappconfigurations//";

	// public static final String GET_BASE_CONFIG_PATH = "C:\\Users\\Muneeb
	// Rehman\\Desktop\\propfiles\\";

	// General Mappings
	public static final String CREDENTIALS_FOR_INTERNAL_CALLS = "RQ/Rjcf8Gn2+7srAiXNO0ZGuRnJORSir";
	public static final String UNAUTHORIZED_ACCESS = "401";

	public static final String SUPPLEMENTARY_TARIFF_DEFAULT_DISABLE_BUTTON = "2";
	public static final String Call_Status_True = "true";
	public static final String ESB_SUCCESS_CODE = "200";
	public static final String MAG_SUCCESS_CODE_SUPPLEMENTARY = "80";
	public static final String MAG_NO_CONTENT_CODE_SUPPLEMENTARY = "81";
	public static final String MAG_SUCCESS_CODE_TARIFF_1 = "62";
	public static final String MAG_SUCCESS_CODE_TARIFF_2 = "63";
	public static final String APP_SERVER_SUCCESS_CODE = "00";
	public static final String VALIDATION_SUCCESS_CODE = "00";
	public static final String VALIDATION_FAILURE_CODE = "10";

	public static final String Call_Status_False = "false";
	public static final String EXCEPTION_CODE = "500";
	public static final String API_FAILURE_CODE = "1";
	public static final String UNKNOWN_AGENT = "7";
	public static final String INVALID_TOKEN = "7";
	public static final String SESSION_EXPIRED = "7";
	public static final String APP_FORCE_UPDATE = "8";
	public static final String APP_OPTIONAL_UPDATE = "9";
	public static final String SERVER_UNDER_MAINTENANCE = "10";

	public static final int SCRATCH_CARD_MASKING_COUNT = 4;

	// Token creation call type
	public static final int TOKEN_CREATION_CALL_TYPE_EXTERNAL = 1;
	public static final int TOKEN_CREATION_CALL_TYPE_INTERNAL = 0;

	// Profile Image Extension
	public static final String PROFILE_IMAGE_EXT = ".jpg";

	// Cache description messages for logs.
	public static final String CACHE_EXISTS_DESCRIPTION = "-Response returned from cache against key-";
	public static final String CACHE_DOES_NOT_EXIST_DESCRIPTION = "-Response not found in cache against key-";

	/*
	 * Below cache keys are used for hash-map key pre-constant and in Time-stamp as
	 * key to time-stamp value.
	 */

	// Warning! Do not modify below constants.
	public static final String HASH_KEY_APP_MENU = "appmenu";
	public static final String HASH_KEY_FAQs = "faqs";
	public static final String HASH_KEY_CONTACT_US = "contactus";
	public static final String HASH_KEY_SUPPLEMENTARY_OFFERINGS = "supplementaryofferings";
	public static final String HASH_KEY_My_SUBSCRIPTIONS = "mysubscriptions";
	public static final String HASH_KEY_TARIFFS = "tariffs";
	public static final String HASH_KEY_STORE_LOCATOR = "storelocator";
	public static final String HASH_KEY_PREDEFINED_DATA = "predefined";
	public static final String HASH_KEY_SURVEY = "survey";

	// Languages Mappings
	public static final String LANGUAGE_RUSSIAN_MAPPING = "2";
	public static final String LANGUAGE_ENGLISH_MAPPING = "3";
	public static final String LANGUAGE_AZERI_MAPPING = "4";

	public static final String LANGUAGE_RUSSIAN_MAPPING_DESC = "ru";
	public static final String LANGUAGE_ENGLISH_MAPPING_DESC = "en";
	public static final String LANGUAGE_AZERI_MAPPING_DESC = "az";

	// ESB common attributes keys for Request body.
	public static final String SOURCE_IP_KEY = "iP";
	public static final String SOURCE_CHANNEL_KEY = "channel";

	// ESB common attributes values for Request body.
	public static final String REQUEST_CHANNEL_VALUE = "Default Value";

	// Headers Keys
	public static final String MSISDN_KEY = "msisdn";
	public static final String TOKEN_KEY = "token";
	public static final String CONTENT_TYPE_KEY = "Content-Type";
	public static final String USER_AGENT_KEY = "UserAgent";
	public static final String LANGUAGE_KEY = "lang";
	public static final String DEVICE_ID_KEY = "deviceID";
	public static final String SUBSCRIBER_TYPE_KEY = "subscriberType";
	public static final String TARIFF_TYPE_KEY = "tariffType";
	public static final String ACCOUNT_TYPE_KEY = "accountType";
	public static final String GROUP_TYPE_KEY = "groupType";

	// Headers Default Values
	public static final String MSISDN_DEFAULT_VALUE = "Unknow Msisdn";
	public static final String TOKEN_DEFAULT_VALUE = "Default Token";
	public static final String CONTENT_TYPE_DEFAULT_VALUE = "Accept=application/json";
	public static final String USER_AGENT_DEFAULT_VALUE = "Unknown Agent";
	public static final String LANGUAGE_DEFAULT_VALUE = "3";
	public static final String DEVICE_ID_DEFAULT_VALUE = "Default deviceid";
	public static final String SUBSCRIBER_TYPE_DEFAULT_VALUE = "Default Type";
	public static final String TARIFF_TYPE_DEFAULT_VALUE = "Default Type";
	public static final String ACCOUNT_TYPE_DEFAULT_VALUE = "Default Type";
	public static final String GROUP_TYPE_DEFAULT_VALUE = "Default Type";

	// Specific Failure Codes
	public static final String FILE_NOT_FOUND_EXCEPTION_CODE = "400";
	public static final String INVALID_STORE_ID = "91";

	// Sign up Failure Codes
	public static final String USER_ALREADY_EXISTS_SIGNUP = "03";
	public static final String PROBLEM_SIGNUP_SEND_OTP = "05";

	// Verify OTP Failure Codes
	public static final String USER_ALREADY_EXISTS_VERIFY_OTP = "13";
	public static final String INVALID_OTP = "14";
	public static final String PROBLEM_OTP_VERIFICATION = "16";

	// RESEND OTP Failure Codes
	public static final String USER_ALREADY_EXISTS_RESEND_OTP = "23";
	public static final String PROBLEM_RESEND_OTP = "25";

	// Save Customer Failure Codes
	public static final String TERMS_AND_CONDITIONS_NOT_MATCHED = "33";
	public static final String PASSWORD_CONFIRM_PASSWORD_MISMATCH = "34";
	public static final String PASSWORD_CONDITIONS_NOT_MATCHED = "35";
	public static final String USER_ALREADY_EXISTS_SAVE_CUSTOMER = "36";

	// Authenticate User Failure Codes
	public static final String INVALID_LOGIN_PASSWORD = "43";
	public static final String USER_NOT_FOUND = "44";
	public static final String USER_BLOCK = "45";

	// Change Password Failure Codes
	public static final String PASSWORD_CONFIRM_PASSWORD_MISMATCH_CHANGE_PASSWORD = "103";
	public static final String INVALID_LOGIN_PASSWORD_CHANGE_PASSWORD = "104";
	public static final String NO_USER_FOUND_CHANGE_PASSWORD = "107";
	public static final String PASSWORD_CONDITIONS_NOT_MATCHED_CHANGE_PASSWORD = "108";

	// Forgot Password
	public static final String PASSWORD_CONFIRM_PASSWORD_MISMATCH_FORGOT_PASSWORD = "52";
	public static final String PASSWORD_CONDITIONS_NOT_MATCHED_FORGOT_PASSWORD = "54";
	public static final String MSISDN_NOT_VALID_FORGOT_PASSWORD = "53";

	// Get Loan Failure Codes
	public static final String LOAN_LIMIT_EXCEEDED = "4707";
	public static final String LOAN_NOT_EXISTS = "4715";

	// Supplementary Offerings Tabs
	public static final String FILTERS = "filters";
	public static final String OFFERS = "offers";

	public static final String SUPPLEMENTARY_TAB_INTERNET = "Internet";
	public static final String SUPPLEMENTARY_TAB_CALL = "Call";
	public static final String SUPPLEMENTARY_TAB_SMS = "SMS";
	public static final String SUPPLEMENTARY_TAB_TM = "TM";
	public static final String SUPPLEMENTARY_TAB_CAMPAIGN = "Campaign";
	public static final String SUPPLEMENTARY_TAB_HYBRID = "Hybrid";
	public static final String SUPPLEMENTARY_TAB_ROAMING = "roaming";

	public static final String SUPPLEMENTARY_HEADER_SECTION = "header";
	public static final String SUPPLEMENTARY_DETAILS_SECTION = "details";
	public static final String SUPPLEMENTARY_DESCRIPTION_SECTION = "description";
	public static final String GET_BULLETS_BREAK_KEY = "<\\n>";
	public static final String ROAMING_DESC_POSITION_TOP = "1";
	public static final String ROAMING_DESC_POSITION_BOTTOM = "0";

	// Below Date Time is used in Home page
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	// Below format is also used in cache time stamps and supplementary Batch
	// dates
	public static final String DATE_TIME_FORMAT_FOR_TOKEN = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_TIME_FORMAT_FOR_REPORT_LOGGER = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String DATE_TIME_FORMAT_FOR_FNF_NUMBERS = "dd MMM yyyy";

	// Free Resources Types
	public static final String FREE_RESOURCE_TYPE_DATA = "data";
	public static final String FREE_RESOURCE_TYPE_VOICE = "voice";
	public static final String FREE_RESOURCE_TYPE_SMS = "sms";

	// Free Resources Roaming Types
	public static final String FREE_RESOURCE_ROAMING_TYPE_DATA = "data";
	public static final String FREE_RESOURCE_ROAMING_TYPE_VOICE = "voice";
	public static final String FREE_RESOURCE_ROAMING_TYPE_SMS = "sms";

	// Loan History Status Messages
	public static final String LOAN_STATUS_OPEN = "Open";
	public static final String LOAN_STATUS_INPROGRESS = "Unpaid";
	public static final String LOAN_STATUS_CLOSED = "Close";
	public static final String LOAN_STATUS_PAID = "Paid";

	// TOP UP
	public static final String INVALID_RECHARGE_CARD = "118100140";

	// Money Transfer
	public static final String MONEY_TRANSFER_LOW_BALANCE = "4565";
	public static final String MONEY_TRANSFER_NUMBER_NOT_EXISTS = "118109004";
	public static final String MSISDN_NOT_ACTIVE_FORGOT_PASSWORD = "-1";

	// Usage History Verify PIN
	public static final String USAGE_HISTORY_VERIFY_PIN_NOT_MATCHED = "118";

	// FNF Settings.
	public static final String ADD_FNF_ACTION_TYPE = "1";
	public static final String DELETE_FNF_ACTION_TYPE = "3";
	public static final String ADD_FCM_CAUSE_SETTINGS = "settings";
	public static final String ADD_FCM_CAUSE_LOGIN = "login";

	// TRIGGERS CONFIGURATIONS
	public static final String TRIGGERS_CACHE_REFRESH_SUCCESS_MESSAGE = "Cache refreshed successfully.";
	public static final String TRIGGERS_CACHE_AFTER_REFRESH = "Trigger cache size After refresh:";
	public static final String TRIGGERS_CACHE_REFRESH_FAIL_MESSAGE = "Failed to refresh cache.";

	// HOME PAGE CREDIT EXPIRATION LABELS
	public static final String CREDIT_EXPIRATION_LABEL_BLOCK_ONE = "Block one way";
	public static final String CREDIT_EXPIRATION_LABEL_BLOCK_TWO = "Block two way";
	public static final String CREDIT_EXPIRATION_LABEL_EXPIRED = "To be expired";

	// My Subscriptions Free Resource Usage Type
	public static final String MY_SUBSCRIPTIONS_FREE_USAGE_INTERNET = "511";
	public static final String MY_SUBSCRIPTIONS_FREE_USAGE_HYBRID = "512";
	public static final String MY_SUBSCRIPTIONS_FREE_USAGE_CALL = "513";
	public static final String MY_SUBSCRIPTIONS_FREE_USAGE_SMS = "514";
	public static final String MY_SUBSCRIPTIONS_FREE_USAGE_TM = "515";
	public static final String MY_SUBSCRIPTIONS_FREE_USAGE_CAMPAIGN = "516";
	public static final String MY_SUBSCRIPTIONS_FREE_USAGE_ROAMING = "517";
	public static final String APP_PHASE_KEY = "isFromB2B";
	public static final String APP_PHASE_DEFAULT_VALUE = "false";
	public static final int MAX_INT = 99999;
	public static final String APP_VERSION_PHASE2 = "B2B";

	// Card Type Constants
	public static final String PLASTIC_CARD_TYPE_MASTER = "master";
	public static final String PLASTIC_CARD_TYPE_VISA = "visa";

	public static final int PLASTIC_CARD_TYPE_MASTER_ID = 1;
	public static final int PLASTIC_CARD_TYPE_VISA_ID = 2;

	public static final String DAILY_SCHEDULER = "1";
	public static final String WEEKLY_SCHEDULER = "2";
	public static final String MONTHLY_SCHEDULER = "3";
}
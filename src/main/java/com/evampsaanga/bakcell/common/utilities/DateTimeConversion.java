/**
 * 
 */
package com.evampsaanga.bakcell.common.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Evamp & Saanga
 *
 */
public class DateTimeConversion {
    public static String getTimeSecMinutesHoursDays(String dateString) throws ParseException {

	SimpleDateFormat endDateFormat = new SimpleDateFormat(Constants.DATE_TIME_FORMAT_FOR_TOKEN);
	String currentDateAndTime = endDateFormat.format(new Date());
	Date endDate = endDateFormat.parse(currentDateAndTime);

	SimpleDateFormat startDateFormat = new SimpleDateFormat(Constants.DATE_TIME_FORMAT_FOR_TOKEN);
	Date startDate = startDateFormat.parse(dateString);
	// format the java.util.Date object to the desired format

	long difference = endDate.getTime() - startDate.getTime();

	String minutesHoursDays = calculateTime(difference);

	return minutesHoursDays; // minutes:Hours:days
    }

    public static String calculateTime(long milis) {

	String time = "";
	long day = milis / (1000 * 60 * 60 * 24);
	long hours = (milis - (1000 * 60 * 60 * 24 * day)) / (1000 * 60 * 60);
	long minute = (milis - (1000 * 60 * 60 * 24 * day) - (1000 * 60 * 60 * hours)) / (1000 * 60);
	long second = (milis / 1000) % 60;

	if (day > 0) {
	    if (day == 1) {
		time = day + " day";
	    } else {
		time = day + " days";
	    }
	} else if (day < 1 && hours > 0) {

	    if (hours == 1) {
		time = hours + " hr";
	    } else {
		time = hours + " hrs";
	    }
	} else if (hours < 1 && minute > 0) {
	    if (minute == 1) {
		time = minute + " min";
	    } else {
		time = minute + " mins";
	    }
	} else if (minute < 1 && second > 0) {

	    if (second == 1) {
		time = second + " sec";
	    } else {
		time = second + " secs";
	    }
	} else {

	    if (second <= 0) {
		time = "0 sec";
	    }
	}

	return time;
    }
}

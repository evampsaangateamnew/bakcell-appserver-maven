package com.evampsaanga.bakcell.common.utilities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.evampsaanga.bakcell.models.generalservices.contactus.ContactUsResponse;
import com.evampsaanga.bakcell.models.generalservices.faqs.FAQSResponse;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.PredefinedDataResponse;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.TariffMigrationPrices;
import com.evampsaanga.bakcell.models.generalservices.storelocator.StoreLocatorResponse;
import com.evampsaanga.bakcell.models.menues.appmenu.AppMenuResponse;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponse;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponseData;
import com.evampsaanga.bakcell.models.survey.GetSurveyResponse;
import com.evampsaanga.bakcell.models.survey.Survey;
import com.evampsaanga.bakcell.models.tariffdetails.TariffResponse;
import com.evampsaanga.bakcell.models.tariffdetails.TariffResponseV2;
import com.evampsaanga.bakcell.models.verifyappversion.TimeStamps;

public class AppCache {
	private static HashMap<String, String> hashmapConfigurations = new HashMap<>();

	private static Map<String, String> hashmapSessions = new HashMap<String, String>();

	private static Map<String, TimeStamps> hashmapTimestamps = new HashMap<String, TimeStamps>();

	private static Map<String, FAQSResponse> hashmapFAQs = new HashMap<String, FAQSResponse>();

	private static Map<String, AppMenuResponse> hashmapAppMenu = new HashMap<String, AppMenuResponse>();

	private static Map<String, ContactUsResponse> hashmapContactUs = new HashMap<String, ContactUsResponse>();

	private static Map<String, StoreLocatorResponse> hashmapStoreLocator = new HashMap<String, StoreLocatorResponse>();

	private static Map<String, SupplementaryOfferingsResponse> hashmapSupplementaryOfferings = new HashMap<String, SupplementaryOfferingsResponse>();

	private static Map<String, SupplementaryOfferingsResponseData> hashmapMySubscriptions = new HashMap<String, SupplementaryOfferingsResponseData>();

	private static Map<String, TariffResponse> hashmapTariffs = new HashMap<String, TariffResponse>();
	private static Map<String, TariffResponseV2> hashmapTariffsV2 = new HashMap<String, TariffResponseV2>();

	private static Map<String, PredefinedDataResponse> hashmapPredefinedData = new HashMap<String, PredefinedDataResponse>();

	private static Map<String, String> hashmapTransactionNames = new HashMap<String, String>();

	private static HashMap<String, String> goldenPayMessages = new HashMap<String, String>();

	private static HashMap<String, GetSurveyResponse> hashMapSurveys = new HashMap<String, GetSurveyResponse>();

	public static Map<String, StoreLocatorResponse> getHashmapStoreLocator() {
		return hashmapStoreLocator;
	}

	public static void setHashmapStoreLocator(Map<String, StoreLocatorResponse> hashmapStoreLocator) {
		AppCache.hashmapStoreLocator = hashmapStoreLocator;
	}

	public static Map<String, AppMenuResponse> getHashmapAppMenu() {
		return hashmapAppMenu;
	}

	public static void setHashmapAppMenu(Map<String, AppMenuResponse> hashmapAppMenu) {
		AppCache.hashmapAppMenu = hashmapAppMenu;
	}

	public static Map<String, TimeStamps> getHashmapTimestamps() {
		return hashmapTimestamps;
	}

	public static void setHashmapTimestamps(Map<String, TimeStamps> hashmapTimestamps) {
		AppCache.hashmapTimestamps = hashmapTimestamps;
	}

	public static Map<String, String> getHashmapSessions() {
		return hashmapSessions;
	}

	public static void setHashmapSessions(Map<String, String> hashmapSessions) {
		AppCache.hashmapSessions = hashmapSessions;
	}

	public static Map<String, FAQSResponse> getHashmapFAQs() {
		return hashmapFAQs;
	}

	public static void setHashmapFAQs(Map<String, FAQSResponse> hashmapFAQs) {
		AppCache.hashmapFAQs = hashmapFAQs;
	}

	public static Map<String, ContactUsResponse> getHashmapContactUs() {
		return hashmapContactUs;
	}

	public static void setHashmapContactUs(Map<String, ContactUsResponse> hashmapContactUs) {
		AppCache.hashmapContactUs = hashmapContactUs;
	}

	public static Map<String, SupplementaryOfferingsResponse> getHashmapSupplementaryOfferings() {
		return hashmapSupplementaryOfferings;
	}

	public static void setHashmapSupplementaryOfferings(
			Map<String, SupplementaryOfferingsResponse> hashmapSupplementaryOfferings) {
		AppCache.hashmapSupplementaryOfferings = hashmapSupplementaryOfferings;
	}

	public static Map<String, TariffResponse> getHashmapTariffs() {
		return hashmapTariffs;
	}

	public static Map<String, TariffResponseV2> getHashmapTariffsV2() {
		return hashmapTariffsV2;
	}

	public static void setHashmapTariffs(Map<String, TariffResponse> hashmapTariffs) {
		AppCache.hashmapTariffs = hashmapTariffs;
	}

	public static Map<String, PredefinedDataResponse> getHashmapPredefinedData() {
		return hashmapPredefinedData;
	}

	public static void setHashmapPredefinedData(Map<String, PredefinedDataResponse> hashmapPredefinedData) {
		AppCache.hashmapPredefinedData = hashmapPredefinedData;
	}

	public static Map<String, SupplementaryOfferingsResponseData> getHashmapMySubscriptions() {
		return hashmapMySubscriptions;
	}

	public static void setHashmapMySubscriptions(
			Map<String, SupplementaryOfferingsResponseData> hashmapMySubscriptions) {
		AppCache.hashmapMySubscriptions = hashmapMySubscriptions;
	}

	public static Map<String, String> getHashmapTransactionNames() {
		return hashmapTransactionNames;
	}

	public static void setHashmapTransactionNames(Map<String, String> hashmapTransactionNames) {
		AppCache.hashmapTransactionNames = hashmapTransactionNames;
	}

	public static HashMap<String, String> getHashmapConfigurations() {
		return hashmapConfigurations;
	}

	public static void setHashmapConfigurations(HashMap<String, String> hashmapConfigurations) {
		AppCache.hashmapConfigurations = hashmapConfigurations;
	}

	public static Map<String, List<TariffMigrationPrices>> getHashmapTariffMigration() {
		return hashmapTariffMigration;
	}

	public static void setHashmapTariffMigration(Map<String, List<TariffMigrationPrices>> hashmapTariffMigration) {
		AppCache.hashmapTariffMigration = hashmapTariffMigration;
	}

	public static HashMap<String, String> getGoldenPayMessages() {
		return goldenPayMessages;
	}

	public static void setGoldenPayMessages(HashMap<String, String> goldenPayMessages) {
		AppCache.goldenPayMessages = goldenPayMessages;
	}

	public static HashMap<String, GetSurveyResponse> getHashMapSurveys() {
		return hashMapSurveys;
	}

	public static void setHashMapSurveys(HashMap<String, GetSurveyResponse> hashMapSurveys) {
		AppCache.hashMapSurveys = hashMapSurveys;
	}

	private static Map<String, List<TariffMigrationPrices>> hashmapTariffMigration = new HashMap<String, List<TariffMigrationPrices>>();
}

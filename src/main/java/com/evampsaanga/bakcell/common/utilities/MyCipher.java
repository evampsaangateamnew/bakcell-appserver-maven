package com.evampsaanga.bakcell.common.utilities;

import java.net.SocketException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;

import sun.misc.BASE64Encoder;

/*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will Google be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, as long as the origin is not misrepresented.
* 
* @author: Ricardo Champa
* 
*/

@SuppressWarnings("restriction")
public class MyCipher {

    static Logger logger = Logger.getLogger(MyCipher.class);
    static SecretKeySpec secretkey = null;

    static SecretKeySpec masterKey = null;

    public MyCipher(byte[] masterbytes, byte[] secretbytes) {

	masterKey = new SecretKeySpec(masterbytes, "AES");
	secretkey = new SecretKeySpec(secretbytes, "AES");

    }

    public static String encryptsecretkey(byte[] secretkey1) throws NoSuchAlgorithmException, NoSuchProviderException,
	    NoSuchPaddingException, InvalidKeyException, SocketException {
	Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

	Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
	cipher.init(Cipher.ENCRYPT_MODE, masterKey);

	byte[] cipherText = new byte[cipher.getOutputSize(secretkey1.length)];
	try {
	    int ctLength = cipher.update(secretkey1, 0, secretkey1.length, cipherText, 0);
	    ctLength += cipher.doFinal(cipherText, ctLength);

	} catch (Exception e) {

	    e.printStackTrace();
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	}
	BASE64Encoder base64Encoder = new BASE64Encoder();
	return new String(base64Encoder.encode(cipherText));
    }

    public static String encryptdata(byte[] secretkey1) throws NoSuchAlgorithmException, NoSuchProviderException,
	    NoSuchPaddingException, InvalidKeyException, SocketException {
	Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

	Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
	cipher.init(Cipher.ENCRYPT_MODE, secretkey);

	byte[] cipherText = new byte[cipher.getOutputSize(secretkey1.length)];
	try {
	    int ctLength = cipher.update(secretkey1, 0, secretkey1.length, cipherText, 0);
	    ctLength += cipher.doFinal(cipherText, ctLength);

	} catch (Exception e) {

	    e.printStackTrace();
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	}
	BASE64Encoder base64Encoder = new BASE64Encoder();
	return base64Encoder.encode(cipherText);

    }

    public static String decryptthrsecretkey(String encryptedData) throws Exception {
	Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
	if (encryptedData.contains("\\")) {
	    encryptedData = encryptedData.replaceAll("\\", "");
	}
	// BASE64Decoder base64Decoder = new BASE64Decoder();
	// byte[] input = base64Decoder.decodeBuffer(encryptedData);
	byte[] input = java.util.Base64.getMimeDecoder().decode(encryptedData);
	Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
	cipher.init(Cipher.DECRYPT_MODE, secretkey);

	byte[] plainText = new byte[cipher.getOutputSize(input.length)];
	int ptLength = cipher.update(input, 0, input.length, plainText, 0);
	ptLength += cipher.doFinal(plainText, ptLength);

	String simplText = new String(plainText);

	return simplText.trim();
    }

    public static String masterdecryptt(String encryptedData) throws Exception {

	byte[] input = java.util.Base64.getMimeDecoder().decode(encryptedData);
	Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
	cipher.init(Cipher.DECRYPT_MODE, masterKey);

	byte[] plainText = new byte[cipher.getOutputSize(input.length)];
	int ptLength = cipher.update(input, 0, input.length, plainText, 0);
	ptLength += cipher.doFinal(plainText, ptLength);

	String simplText = new String(plainText);

	return simplText.trim();
    }

    public static String encrpyt(byte[] input)
	    throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException,
	    ShortBufferException, IllegalBlockSizeException, BadPaddingException {

	Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");

	cipher.init(Cipher.ENCRYPT_MODE, secretkey);

	byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
	int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
	ctLength += cipher.doFinal(cipherText, ctLength);
	return new String(java.util.Base64.getEncoder().encodeToString(cipherText));
    }

}
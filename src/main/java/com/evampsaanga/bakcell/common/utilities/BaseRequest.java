/**
 * 
 */
package com.evampsaanga.bakcell.common.utilities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseRequest {
    private String lang;
    private String iP;
    private String channel;
    private String msisdn;

    @JsonInclude(Include.NON_NULL)

    private String isB2B = "";

    public String getIsB2B() {
	return isB2B;
    }

    public void setIsB2B(String isB2B) {
	this.isB2B = isB2B;
    }

    public String getLang() {
	return lang;
    }

    public void setLang(String lang) {
	this.lang = lang;
    }

    public String getiP() {
	return iP;
    }

    public void setiP(String iP) {
	this.iP = iP;
    }

    public String getChannel() {
	return channel;
    }

    public void setChannel(String channel) {
	this.channel = channel;
    }

    public String getMsisdn() {
	return msisdn;
    }

    public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
    }

    @Override
    public String toString() {
	return "BaseRequest [lang=" + lang + ", iP=" + iP + ", channel=" + channel + ", msisdn=" + msisdn + ", isB2B="
		+ isB2B + "]";
    }

}

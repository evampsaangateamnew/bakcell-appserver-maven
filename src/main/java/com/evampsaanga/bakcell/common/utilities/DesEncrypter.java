package com.evampsaanga.bakcell.common.utilities;

import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.apache.log4j.Logger;

public class DesEncrypter {
    Cipher ecipher;
    Cipher dcipher;

    Logger logger = Logger.getLogger(DesEncrypter.class);

    public DesEncrypter() throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException,
	    InvalidKeySpecException, NoSuchPaddingException {
	String keyString = "euldlmdc";
	SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
	byte[] utf8 = keyString.getBytes("UTF8");
	DESKeySpec dks = new DESKeySpec(utf8);
	SecretKey key = skf.generateSecret(dks);
	ecipher = Cipher.getInstance("DES");
	dcipher = Cipher.getInstance("DES");
	ecipher.init(Cipher.ENCRYPT_MODE, key);
	dcipher.init(Cipher.DECRYPT_MODE, key);
    }

    @SuppressWarnings("restriction")
    public String encrypt(String str) throws SocketException {
	try {
	    // Encode the string into bytes using utf-8
	    byte[] utf8 = str.getBytes("UTF8");
	    // Encrypt
	    byte[] enc = ecipher.doFinal(utf8);
	    // Encode bytes to base64 to get a string
	    return new sun.misc.BASE64Encoder().encode(enc);
	} catch (javax.crypto.BadPaddingException e) {
	    e.printStackTrace();
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	} catch (IllegalBlockSizeException e) {
	    e.printStackTrace();
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	} catch (UnsupportedEncodingException e) {
	    e.printStackTrace();
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	}
	return null;
    }

    public String decrypt(String str) throws SocketException {
	try {
	    // Decode base64 to get bytes
	    @SuppressWarnings("restriction")
	    byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);

	    // Decrypt
	    byte[] utf8 = dcipher.doFinal(dec);

	    // Decode using utf-8
	    return new String(utf8, "UTF8");
	} catch (javax.crypto.BadPaddingException e) {
	    e.printStackTrace();
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	} catch (IllegalBlockSizeException e) {
	    e.printStackTrace();
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	} catch (UnsupportedEncodingException e) {
	    e.printStackTrace();
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	} catch (java.io.IOException e) {
	    e.printStackTrace();
	    Utilities.printErrorLog(Utilities.convertStackTraceToString(e), logger);
	}
	return null;
    }

    // private static SecretKey getKeyFromFile() throws IOException,
    // ClassNotFoundException {
    // try {
    // SecretKey key = null;
    // ObjectInputStream ois = new ObjectInputStream(new
    // FileInputStream("/key.des"));
    // key = (SecretKey) ois.readObject();
    // ois.close();
    //
    // return key;
    // } catch (Exception e) {
    // e.printStackTrace();
    // return null;
    // }
    // }
    //
    // private static void saveKeyToFile(SecretKey key) throws
    // FileNotFoundException, IOException {
    // try {
    // ObjectOutputStream oos = new ObjectOutputStream(new
    // FileOutputStream("key.txt"));
    // oos.writeObject(key);
    // oos.close();
    // } catch (Exception e) {
    // e.printStackTrace();
    //
    // }
    // }

    public static String encodeString(String data) {
	try {
	    DesEncrypter encrypter = new DesEncrypter();
	    return encrypter.encrypt(data);
	} catch (Exception e) {
	    e.printStackTrace();
	    return "";
	}
    }

    public static String decodeString(String data) {
	try {
	    DesEncrypter encrypter = new DesEncrypter();
	    return encrypter.decrypt(data);
	} catch (Exception e) {
	    e.printStackTrace();
	    return "";
	}
    }
}

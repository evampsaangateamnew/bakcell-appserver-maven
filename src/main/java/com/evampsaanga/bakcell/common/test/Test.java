package com.evampsaanga.bakcell.common.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Test {

    public static void main(String[] args) {

	// new Test().removeFreeSMSTables();

    }

    public void removeFreeSMSTables() {

	Connection conn = null;
	Statement stmt = null;
	try { // STEP 2: Register JDBC driver
	    Class.forName("com.mysql.jdbc.Driver");
	    String dburl = "jdbc:mysql://10.220.48.202:3306/stg_esb?autoReconnect=true&useUnicode=true&characterEncoding=utf8";
	    String username = "navicate";
	    String password = "navicate123";
	    // STEP 3: Open a connection
	    System.out.println("Connecting to a selected database...");
	    conn = DriverManager.getConnection(dburl, username, password);
	    System.out.println("Connected database successfully...");

	    // STEP 4: Execute a query
	    System.out.println("Deleting table in given database...");
	    stmt = conn.createStatement();
	    String tableName = "sendfreesms202008";
	    String counter = "";

	    for (int i = 1; i < 32; i++) {
		if (i < 10)
		    counter = "0" + i;
		else
		    counter = i + "";
		String sql = "DROP TABLE IF EXISTS " + tableName + counter;

		stmt.executeUpdate(sql);
		System.out.println("Table deleted in given database..." + tableName + counter);
		counter = "";
	    }

	} catch (SQLException se) { // Handle errors for JDBC
	    se.printStackTrace();
	} catch (Exception e) { // Handle errors for
	    e.printStackTrace();
	} finally { // finally block used to close resources
	    try {
		if (stmt != null)
		    conn.close();
	    } catch (SQLException se) {
	    } // do nothing
	    try {
		if (conn != null)
		    conn.close();
	    } catch (SQLException se) {
	    }

	}
    }
}
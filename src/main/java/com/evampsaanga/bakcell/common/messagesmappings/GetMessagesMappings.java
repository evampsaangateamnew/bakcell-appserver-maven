package com.evampsaanga.bakcell.common.messagesmappings;

import java.net.SocketException;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Utilities;

public class GetMessagesMappings {

    static Logger logger = Logger.getLogger(GetMessagesMappings.class);

    public static HashMap<String, String> labelsNdMessagesCacheHashMap = new HashMap<>();

    public static String getMessageFromResourceBundle(String keyname, String lang)
	    throws SocketException, SQLException {

	Utilities.printDebugLog("GET MESSAGE (FROM CONFIGURATION CACHE) FOR KEY-" + keyname + "." + getLang(lang),
		logger);

	return GetConfigurations.getConfigurationFromCache(keyname + "." + getLang(lang));

    }

    public static String getLabelsFromResourceBundle(String keyname, String lang) throws SocketException, SQLException {

	Utilities.printDebugLog("GET LABEL (FROM CONFIGURATION CACHE) FOR KEY-" + keyname + "." + getLang(lang),
		logger);
	return GetConfigurations.getConfigurationFromCache(keyname + "." + getLang(lang));

    }

    public static String getLang(String lang) {
	String language = "";
	if (lang != null && lang.equals(Constants.LANGUAGE_AZERI_MAPPING)) {
	    language = Constants.LANGUAGE_AZERI_MAPPING_DESC;
	} else if (lang != null && lang.equals(Constants.LANGUAGE_ENGLISH_MAPPING)) {
	    language = Constants.LANGUAGE_ENGLISH_MAPPING_DESC;
	} else if (lang != null && lang.equals(Constants.LANGUAGE_RUSSIAN_MAPPING)) {
	    language = Constants.LANGUAGE_RUSSIAN_MAPPING_DESC;
	} else {
	    language = Constants.LANGUAGE_ENGLISH_MAPPING_DESC;
	}
	return language;
    }

}

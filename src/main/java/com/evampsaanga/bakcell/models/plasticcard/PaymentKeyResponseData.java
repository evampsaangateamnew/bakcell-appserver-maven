package com.evampsaanga.bakcell.models.plasticcard;

public class PaymentKeyResponseData {

	private String paymentKey;

	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	@Override
	public String toString() {
		return "PaymentKeyData [paymentKey=" + paymentKey + ", toString()=" + super.toString() + "]";
	}

	
	
}

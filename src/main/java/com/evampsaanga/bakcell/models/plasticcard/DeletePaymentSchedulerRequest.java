package com.evampsaanga.bakcell.models.plasticcard;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class DeletePaymentSchedulerRequest extends BaseRequest {
	private String paymentSchedulerId;

	public String getId() {
		return paymentSchedulerId;
	}

	public void setId(String paymentSchedulerId) {
		this.paymentSchedulerId = paymentSchedulerId;
	}

	@Override
	public String toString() {
		return "DeletePaymentSchedulerRequest [paymentSchedulerId=" + paymentSchedulerId + ", toString()=" + super.toString() + "]";
	}

}

package com.evampsaanga.bakcell.models.plasticcard;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class PaymentKeyResponse extends BaseResponse{

	private PaymentKeyResponseData data;

	public PaymentKeyResponseData getData() {
		return data;
	}

	public void setData(PaymentKeyResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "PaymentKeyResponse [data=" + data + ", toString()=" + super.toString() + "]";
	}


	
}

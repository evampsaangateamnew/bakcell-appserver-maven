package com.evampsaanga.bakcell.models.plasticcard;

public class VerifyCardResponseData {

	private String returnCode;

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	@Override
	public String toString() {
		return "VerifyCardResponseData [returnCode=" + returnCode + ", toString()=" + super.toString() + "]";
	}

}

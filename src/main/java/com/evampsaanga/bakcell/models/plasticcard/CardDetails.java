package com.evampsaanga.bakcell.models.plasticcard;

public class CardDetails {

	private String id;
	private String cardMaskNumber;
	private String cardType;
	private String paymentKey;

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "CardDetails [id=" + id + ", number=" + cardMaskNumber + ", cardType=" + cardType + ", paymentKey=" + paymentKey
				+ "]";
	}

	public String getCardMaskNumber() {
		return cardMaskNumber;
	}

	public void setCardMaskNumber(String cardMaskNumber) {
		this.cardMaskNumber = cardMaskNumber;
	}

}

package com.evampsaanga.bakcell.models.plasticcard;

import java.util.List;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class FastPaymentResponse extends BaseResponse {

	private FastPaymentResponseData data;

	public FastPaymentResponseData getData() {
		return data;
	}

	public void setData(FastPaymentResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "FastPaymentResponse [data=" + data + ", toString()=" + super.toString() + "]";
	}


}

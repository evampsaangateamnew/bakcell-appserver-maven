package com.evampsaanga.bakcell.models.plasticcard;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class ScheduledPaymentsResponse extends BaseResponse{

		private ScheduledPaymentsResponseData data;

		public ScheduledPaymentsResponseData getData() {
			return data;
		}

		public void setData(ScheduledPaymentsResponseData data) {
			this.data = data;
		}

		@Override
		public String toString() {
			return "ScheduledPaymentsResponse [data=" + data + ", toString()=" + super.toString() + "]";
		}
		
		
}

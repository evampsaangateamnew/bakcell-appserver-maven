package com.evampsaanga.bakcell.models.plasticcard;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class VerifyCardResponse extends BaseResponse {

	private VerifyCardResponseData data;

	public VerifyCardResponseData getData() {
		return data;
	}

	public void setData(VerifyCardResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "VerifyCardResponse [data=" + data + ", toString()=" + super.toString() + "]";
	}

}

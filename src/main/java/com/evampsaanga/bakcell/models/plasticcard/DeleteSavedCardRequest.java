package com.evampsaanga.bakcell.models.plasticcard;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class DeleteSavedCardRequest extends BaseRequest{
	private String savedCardId;

	public String getId() {
		return savedCardId;
	}

	public void setId(String id) {
		this.savedCardId = id;
	}

	@Override
	public String toString() {
		return "DeleteSavedCardRequest [savedCardId=" + savedCardId + ", toString()=" + super.toString() + "]";
	}
}

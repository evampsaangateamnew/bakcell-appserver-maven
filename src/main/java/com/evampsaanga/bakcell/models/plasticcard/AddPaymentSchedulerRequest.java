package com.evampsaanga.bakcell.models.plasticcard;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class AddPaymentSchedulerRequest extends BaseRequest{
	
//	private PaymentSchedulerData data;
	
	private String amount;
	private String billingCycle; // 1.daily, 2. weekly, 3. monthly
	private String startDate;
	private String recurrenceNumber;
	private String recurrenceFrequency;
	private String savedCardId;
	
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getBillingCycle() {
		return billingCycle;
	}
	public void setBillingCycle(String billingCycle) {
		this.billingCycle = billingCycle;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getRecurrenceNumber() {
		return recurrenceNumber;
	}
	public void setRecurrenceNumber(String recurrenceNumber) {
		this.recurrenceNumber = recurrenceNumber;
	}
	public String getSavedCardId() {
		return savedCardId;
	}
	public void setSavedCardId(String savedCardId) {
		this.savedCardId = savedCardId;
	}
	public String getRecurrenceFrequency() {
		return recurrenceFrequency;
	}
	public void setRecurrenceFrequency(String recurrenceFrequency) {
		this.recurrenceFrequency = recurrenceFrequency;
	}

//	public PaymentSchedulerData getData() {
//		return data;
//	}
//
//	public void setData(PaymentSchedulerData data) {
//		this.data = data;
//	}

//	@Override
//	public String toString() {
//		return "AddPaymentSchedulerRequest [data=" + data + ", toString()=" + super.toString() + "]";
//	}
}

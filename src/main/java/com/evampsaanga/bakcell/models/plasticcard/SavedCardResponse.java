package com.evampsaanga.bakcell.models.plasticcard;


import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class SavedCardResponse extends BaseResponse {

	private SavedCardResponseData data;

	public SavedCardResponseData getData() {
		return data;
	}

	public void setData(SavedCardResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "SavedCardResponse [data=" + data + ", toString()=" + super.toString() + "]";
	}	
	
}

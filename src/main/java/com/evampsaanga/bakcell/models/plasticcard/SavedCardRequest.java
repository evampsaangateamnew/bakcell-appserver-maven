package com.evampsaanga.bakcell.models.plasticcard;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class SavedCardRequest extends BaseRequest {

	@Override
	public String toString() {
		return "SavedCardRequest [toString()=" + super.toString() + "]";
	}

}

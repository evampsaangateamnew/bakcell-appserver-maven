package com.evampsaanga.bakcell.models.plasticcard;

import java.util.List;

public class SavedCardResponseData {
	private List<CardDetails> cardDetails;
	private String lastAmount; 

	public String getLastAmount() {
		return lastAmount;
	}

	public void setLastAmount(String lastAmount) {
		this.lastAmount = lastAmount;
	}

	public List<CardDetails> getCardDetails() {
		return cardDetails;
	}

	public void setCardDetails(List<CardDetails> cardDetails) {
		this.cardDetails = cardDetails;
	}

	@Override
	public String toString() {
		return "SavedCardResponseData [cardDetails=" + cardDetails + "]";
	}
}

package com.evampsaanga.bakcell.models.plasticcard;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class DeleteFastPaymentRequest extends BaseRequest{
	private String fastPaymentId;

	public String getId() {
		return fastPaymentId;
	}

	public void setId(String id) {
		this.fastPaymentId = id;
	}

	@Override
	public String toString() {
		return "DeleteFastPaymentRequest [fastPaymentId=" + fastPaymentId + ", toString()=" + super.toString() + "]";
	}
	
}

package com.evampsaanga.bakcell.models.plasticcard;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class MakePaymentResponse extends BaseResponse {

	private MakePaymentResponseData data;

	public MakePaymentResponseData getData() {
		return data;
	}

	public void setData(MakePaymentResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "MakePaymentResponse [data=" + data + ", toString()=" + super.toString() + "]";
	}

}

package com.evampsaanga.bakcell.models.plasticcard;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class MakePaymentRequest extends BaseRequest {

	private String paymentKey;
	private String amount;
	private String topupNumber;

	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTopupNumber() {
		return topupNumber;
	}

	public void setTopupNumber(String topupNumber) {
		this.topupNumber = topupNumber;
	}

	@Override
	public String toString() {
		return "MakePaymentRequest [paymentKey=" + paymentKey + ", amount=" + amount + ", topupNumber=" + topupNumber
				+ ", toString()=" + super.toString() + "]";
	}

}

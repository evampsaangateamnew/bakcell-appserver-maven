package com.evampsaanga.bakcell.models.plasticcard;

public class MakePaymentResponseData {

	private String responseData;

	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}

	@Override
	public String toString() {
		return "MakePaymentResponseData [responseData=" + responseData + ", toString()=" + super.toString() + "]";
	}
	
}

package com.evampsaanga.bakcell.models.plasticcard;

public class FastPaymentDetails {

	private String id;
	private String cardType;
	private String amount;
	private String topupNumber;
	private String paymentKey;

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTopupNumber() {
		return topupNumber;
	}

	public void setTopupNumber(String topupNumber) {
		this.topupNumber = topupNumber;
	}

	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "FastPaymentDetails [id=" + id + ", cardType=" + cardType + ", amount=" + amount + ", topupNumber="
				+ topupNumber + ", paymentKey=" + paymentKey + "]";
	}

}

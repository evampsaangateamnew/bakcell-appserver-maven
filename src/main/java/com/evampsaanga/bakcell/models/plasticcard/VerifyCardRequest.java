package com.evampsaanga.bakcell.models.plasticcard;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class VerifyCardRequest extends BaseRequest {

	private String cardToken;
	private String paymentKey;
	private String amount;

	public String getCardToken() {
		return cardToken;
	}

	public void setCardToken(String cardToken) {
		this.cardToken = cardToken;
	}

	public String getPaymentKey() {
		return paymentKey;
	}

	public void setPaymentKey(String paymentKey) {
		this.paymentKey = paymentKey;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "VerifyCardDataRequest [cardToken=" + cardToken + ", paymentKey=" + paymentKey + ", amount=" + amount
				+ "]";
	}

}

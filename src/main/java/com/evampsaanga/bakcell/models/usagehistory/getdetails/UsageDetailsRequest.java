/**
 * 
 */
package com.evampsaanga.bakcell.models.usagehistory.getdetails;

/**
 * @author Evamp & Saanga
 *
 */
public class UsageDetailsRequest {

	private String lang;
	private String iP;
	private String channel;
	private String msisdn;
	private String startDate;
	private String endDate;

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getiP() {
		return iP;
	}

	public void setiP(String iP) {
		this.iP = iP;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "UsageDetailsRequest [lang=" + lang + ", iP=" + iP + ", channel=" + channel + ", msisdn=" + msisdn
				+ ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}

}

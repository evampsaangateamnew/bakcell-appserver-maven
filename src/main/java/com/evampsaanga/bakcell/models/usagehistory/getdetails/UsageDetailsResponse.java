/**
 * 
 */
package com.evampsaanga.bakcell.models.usagehistory.getdetails;

/**
 * @author Evamp & Saanga
 *
 */

public class UsageDetailsResponse {
	private String callStatus;
	private String resultCode;
	private String resultDesc;
	private String exception;
	private UsageDetailsResponseData data;

	public String getCallStatus() {
		return callStatus;
	}

	public void setCallStatus(String callStatus) {
		this.callStatus = callStatus;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultDesc() {
		return resultDesc;
	}

	public void setResultDesc(String resultDesc) {
		this.resultDesc = resultDesc;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public UsageDetailsResponseData getData() {
		return data;
	}

	public void setData(UsageDetailsResponseData data) {
		this.data = data;
	}
}

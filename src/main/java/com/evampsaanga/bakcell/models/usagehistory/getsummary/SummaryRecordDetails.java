/**
 * 
 */
package com.evampsaanga.bakcell.models.usagehistory.getsummary;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class SummaryRecordDetails {
	private String totalUsage;
	private String totalCharge;
	private List<SummaryRecord> records;

	public String getTotalUsage() {
		return totalUsage;
	}

	public void setTotalUsage(String totalUsage) {
		this.totalUsage = totalUsage;
	}

	public String getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(String totalCharge) {
		this.totalCharge = totalCharge;
	}

	public List<SummaryRecord> getRecords() {
		return records;
	}

	public void setRecords(List<SummaryRecord> records) {
		this.records = records;
	}

}

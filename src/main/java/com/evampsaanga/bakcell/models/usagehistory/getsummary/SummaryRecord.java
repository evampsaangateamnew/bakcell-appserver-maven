/**
 * 
 */
package com.evampsaanga.bakcell.models.usagehistory.getsummary;

/**
 * @author Evamp & Saanga
 *
 */
public class SummaryRecord {
	private String service_type;
	private String unit;
	private String total_usage;
	private String chargeable_amount;
	private String count;

	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getTotal_usage() {
		return total_usage;
	}

	public void setTotal_usage(String total_usage) {
		this.total_usage = total_usage;
	}

	public String getChargeable_amount() {
		return chargeable_amount;
	}

	public void setChargeable_amount(String chargeable_amount) {
		this.chargeable_amount = chargeable_amount;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

}

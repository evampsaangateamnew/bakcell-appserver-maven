package com.evampsaanga.bakcell.models.broadcastsms;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class BroadcastSMSRequest extends BaseRequest {
    private String recieverMsisdn;
    private String textmsg;
    private String msgLang;
    private String orderKey;
    private String senderName;

    public String getSenderName() {
	return senderName;
    }

    public void setSenderName(String senderName) {
	this.senderName = senderName;
    }

    public String getRecieverMsisdn() {
	return recieverMsisdn;
    }

    public void setRecieverMsisdn(String recieverMsisdn) {
	this.recieverMsisdn = recieverMsisdn;
    }

    public String getTextmsg() {
	return textmsg;
    }

    public void setTextmsg(String textmsg) {
	this.textmsg = textmsg;
    }

    public String getMsgLang() {
	return msgLang;
    }

    public void setMsgLang(String msgLang) {
	this.msgLang = msgLang;
    }

    public String getOrderKey() {
	return orderKey;
    }

    public void setOrderKey(String orderKey) {
	this.orderKey = orderKey;
    }

}

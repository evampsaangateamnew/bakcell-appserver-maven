package com.evampsaanga.bakcell.models.getreports;

public class SaveFileRequest {
	private String fileName;
	private String file;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	@Override
	public String toString() {
		return "SaveFileRequest [fileName=" + fileName + ", file=" + file + "]";
	}

}

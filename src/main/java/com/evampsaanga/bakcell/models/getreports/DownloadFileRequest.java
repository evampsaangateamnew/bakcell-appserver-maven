package com.evampsaanga.bakcell.models.getreports;

public class DownloadFileRequest {
	private String fileName;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String toString() {
		return "DownloadFileRequest [fileName=" + fileName + "]";
	}
}

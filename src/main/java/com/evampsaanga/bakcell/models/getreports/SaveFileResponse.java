package com.evampsaanga.bakcell.models.getreports;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class SaveFileResponse extends BaseResponse {
	private String fileName;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String toString() {
		return "SaveFileResponse [fileName=" + fileName + ", toString()=" + super.toString() + "]";
	}

}

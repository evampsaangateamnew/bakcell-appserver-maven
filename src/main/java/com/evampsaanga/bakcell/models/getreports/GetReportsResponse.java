package com.evampsaanga.bakcell.models.getreports;

import java.util.List;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class GetReportsResponse extends BaseResponse {
	private List<ReportData> reportData;

	public List<ReportData> getReportData() {
		return reportData;
	}

	public void setReportData(List<ReportData> reportData) {
		this.reportData = reportData;
	}

	@Override
	public String toString() {
		return "GetReportsResponse [reportData=" + reportData + ", toString()=" + super.toString() + "]";
	}
}

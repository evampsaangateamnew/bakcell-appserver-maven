
package com.evampsaanga.bakcell.models.companyinvoice.companyinvoiceresponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "companySummary",
    "companyDetail",
    "totalDiscountLable",
    "totalDiscountValue"
})
public class Data {

	@JsonProperty("companySummary")
	private List<CompanySummary> companySummary = null;

	@JsonProperty("totalDiscountLable")
    private String totalDiscountLable;
	@JsonProperty("totalDiscountValue")
    private String totalDiscountValue;
	
	
    @JsonProperty("companyDetail")
    private CompanyDetail companyDetail;

    @JsonProperty("companySummary")
    public List<CompanySummary> getCompanySummary() {
    return companySummary;
    }

    @JsonProperty("companySummary")
    public void setCompanySummary(List<CompanySummary> companySummary) {
    this.companySummary = companySummary;
    }
    @JsonProperty("companyDetail")
    public CompanyDetail getCompanyDetail() {
        return companyDetail;
    }
    
    
    
    @JsonProperty("totalDiscountLable")
    public String getTotalDiscountLable() {
		return totalDiscountLable;
	}
    @JsonProperty("totalDiscountLable")
	public void setTotalDiscountLable(String totalDiscountLable) {
		this.totalDiscountLable = totalDiscountLable;
	}
    @JsonProperty("totalDiscountValue")
	public String getTotalDiscountValue() {
		return totalDiscountValue;
	}
    @JsonProperty("totalDiscountValue")
	public void setTotalDiscountValue(String totalDiscountValue) {
		this.totalDiscountValue = totalDiscountValue;
	}

	@JsonProperty("companyDetail")
    public void setCompanyDetail(CompanyDetail companyDetail) {
        this.companyDetail = companyDetail;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("companySummary", companySummary).append("companyDetail", companyDetail).toString();
    }

}

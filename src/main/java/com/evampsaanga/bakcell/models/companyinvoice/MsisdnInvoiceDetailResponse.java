package com.evampsaanga.bakcell.models.companyinvoice;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class MsisdnInvoiceDetailResponse extends BaseResponse {
    private Data data;

    public Data getData() {
	return data;
    }

    public void setData(Data data) {
	this.data = data;
    }
}


package com.evampsaanga.bakcell.models.companyinvoice;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "detailInvoice",
    "detailDesc",
    "detailTotal",
    "detailUsage",
    "subitemTitle",
    "detailUnit"
})
public class Details {

	@JsonProperty("detailUnit")
	private String detailUnit;
    @JsonProperty("detailInvoice")
    private String detailInvoice;
    @JsonProperty("detailDesc")
    private String detailDesc;
    @JsonProperty("detailTotal")
    private String detailTotal;
    @JsonProperty("detailUsage")
    private String detailUsage;
    @JsonProperty("subitemTitle")
    private String subitemTitle;

    
    @JsonProperty("detailUnit")
    public String getDetailUnit() {
		return detailUnit;
	}
    @JsonProperty("detailUnit")
	public void setDetailUnit(String detailUnit) {
		this.detailUnit = detailUnit;
	}

	@JsonProperty("detailInvoice")
    public String getDetailInvoice() {
        return detailInvoice;
    }

    @JsonProperty("detailInvoice")
    public void setDetailInvoice(String detailInvoice) {
        this.detailInvoice = detailInvoice;
    }

    @JsonProperty("detailDesc")
    public String getDetailDesc() {
        return detailDesc;
    }

    @JsonProperty("detailDesc")
    public void setDetailDesc(String detailDesc) {
        this.detailDesc = detailDesc;
    }

    @JsonProperty("detailTotal")
    public String getDetailTotal() {
        return detailTotal;
    }

    @JsonProperty("detailTotal")
    public void setDetailTotal(String detailTotal) {
        this.detailTotal = detailTotal;
    }

    @JsonProperty("detailUsage")
    public String getDetailUsage() {
        return detailUsage;
    }

    @JsonProperty("detailUsage")
    public void setDetailUsage(String detailUsage) {
        this.detailUsage = detailUsage;
    }

    @JsonProperty("subitemTitle")
    public String getSubitemTitle() {
        return subitemTitle;
    }

    @JsonProperty("subitemTitle")
    public void setSubitemTitle(String subitemTitle) {
        this.subitemTitle = subitemTitle;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("detailInvoice", detailInvoice).append("detailDesc", detailDesc).append("detailTotal", detailTotal).append("detailUsage", detailUsage).append("subitemTitle", subitemTitle).toString();
    }

}

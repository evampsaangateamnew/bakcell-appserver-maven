
package com.evampsaanga.bakcell.models.companyinvoice;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "msisdnSummary", "msisdnDetail", "totalLabel", "totalValue" })
public class Data {

    @JsonProperty("totalLabel")
    private String totalLabel;

    @JsonProperty("totalValue")
    private String totalValue;

    @JsonProperty("msisdnSummary")
    private List<MsisdnSummary> msisdnSummary = null;
    @JsonProperty("msisdnDetail")
    private MsisdnDetail msisdnDetail;

    @JsonProperty("msisdnSummary")
    public List<MsisdnSummary> getMsisdnSummary() {
	return msisdnSummary;
    }

    @JsonProperty("msisdnSummary")
    public void setMsisdnSummary(List<MsisdnSummary> msisdnSummary) {
	this.msisdnSummary = msisdnSummary;
    }

    @JsonProperty("msisdnDetail")
    public MsisdnDetail getMsisdnDetail() {
	return msisdnDetail;
    }

    @JsonProperty("msisdnDetail")
    public void setMsisdnDetail(MsisdnDetail msisdnDetail) {
	this.msisdnDetail = msisdnDetail;
    }

    @JsonProperty("totalLabel")
    public String getTotalLabel() {
	return totalLabel;
    }

    @JsonProperty("totalLabel")
    public void setTotalLabel(String totalLabel) {
	this.totalLabel = totalLabel;
    }

    @JsonProperty("totalValue")
    public String getTotalValue() {
	return totalValue;
    }

    @JsonProperty("totalValue")
    public void setTotalValue(String totalValue) {
	this.totalValue = totalValue;
    }

    @Override
    public String toString() {
	return new ToStringBuilder(this).append("msisdnSummary", msisdnSummary).append("msisdnDetail", msisdnDetail)
		.toString();
    }

}

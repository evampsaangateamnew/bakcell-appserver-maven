package com.evampsaanga.bakcell.models.companyinvoice;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class MsisdnInvoiceResponse extends BaseResponse {
    private com.evampsaanga.bakcell.models.companyinvoice.Data data;

    public com.evampsaanga.bakcell.models.companyinvoice.Data getData() {
	return data;
    }

    public void setData(com.evampsaanga.bakcell.models.companyinvoice.Data data) {
	this.data = data;
    }

}

package com.evampsaanga.bakcell.models.companyinvoice;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "acctCode", "billMonth", "customerId" })
public class CompanyInvoiceRequest extends BaseRequest {

    @JsonProperty("acctCode")
    private String acctCode;
    @JsonProperty("billMonth")
    private String billMonth;
    @JsonProperty("customerId")
    private String customerId;

    @JsonProperty("customerId")
    public String getCustomerId() {
	return customerId;
    }

    @JsonProperty("customerId")
    public void setCustomerId(String customerId) {
	this.customerId = customerId;
    }

    @JsonProperty("acctCode")
    public String getAcctCode() {
	return acctCode;
    }

    @JsonProperty("acctCode")
    public void setAcctCode(String acctCode) {
	this.acctCode = acctCode;
    }

    @JsonProperty("billMonth")
    public String getBillMonth() {
	return billMonth;
    }

    @JsonProperty("billMonth")
    public void setBillMonth(String billMonth) {
	this.billMonth = billMonth;
    }

    @Override
    public String toString() {
	return new ToStringBuilder(this).append("acctCode", acctCode).append("billMonth", billMonth).toString();
    }

}
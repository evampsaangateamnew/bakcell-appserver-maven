
package com.evampsaanga.bakcell.models.companyinvoice;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "misdn", "limit", "invoiceAmount" })
public class MsisdnSummary {

    @JsonProperty("misdn")
    private String msisdn;
    @JsonProperty("limit")
    private String limit;
    @JsonProperty("invoiceAmount")
    private String invoiceAmount;

    @JsonProperty("misdn")
    public String getMsidn() {
	return msisdn;
    }

    @JsonProperty("misdn")
    public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
    }

    @JsonProperty("limit")
    public String getLimit() {
	return limit;
    }

    @JsonProperty("limit")
    public void setLimit(String limit) {
	this.limit = limit;
    }

    @JsonProperty("invoiceAmount")
    public String getInvoiceAmount() {
	return invoiceAmount;
    }

    @JsonProperty("invoiceAmount")
    public void setInvoiceAmount(String invoiceAmount) {
	this.invoiceAmount = invoiceAmount;
    }

}

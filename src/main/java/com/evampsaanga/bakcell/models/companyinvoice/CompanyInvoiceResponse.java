package com.evampsaanga.bakcell.models.companyinvoice;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class CompanyInvoiceResponse extends BaseResponse {
    private com.evampsaanga.bakcell.models.companyinvoice.companyinvoiceresponse.Data data;

    public com.evampsaanga.bakcell.models.companyinvoice.companyinvoiceresponse.Data getData() {
	return data;
    }

    public void setData(com.evampsaanga.bakcell.models.companyinvoice.companyinvoiceresponse.Data data) {
	this.data = data;
    }

}

package com.evampsaanga.bakcell.models.orderdetails;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class OrderDetailsAppServerResponse extends BaseResponse {

    OrderDetailsData data = null;

    public OrderDetailsData getData() {
	return data;
    }

    public void setData(OrderDetailsData data) {
	this.data = data;
    }

}

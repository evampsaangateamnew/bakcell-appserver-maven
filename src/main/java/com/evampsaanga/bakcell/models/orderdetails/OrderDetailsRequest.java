package com.evampsaanga.bakcell.models.orderdetails;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class OrderDetailsRequest extends BaseRequest {

    private String orderId = "";

    public String getOrderId() {
	return orderId;
    }

    public void setOrderId(String orderId) {
	this.orderId = orderId;
    }

}

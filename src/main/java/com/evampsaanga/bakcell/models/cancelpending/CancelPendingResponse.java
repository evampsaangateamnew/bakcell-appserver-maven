package com.evampsaanga.bakcell.models.cancelpending;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class CancelPendingResponse extends BaseResponse {

    private CancelPendingResponseData data;

    public CancelPendingResponseData getData() {
	return data;
    }

    public void setData(CancelPendingResponseData data) {
	this.data = data;
    }

}

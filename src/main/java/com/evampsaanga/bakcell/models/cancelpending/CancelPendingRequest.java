package com.evampsaanga.bakcell.models.cancelpending;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class CancelPendingRequest extends BaseRequest {

    private String orderId;
    private String orderKey;

    public String getOrderId() {
	return orderId;
    }

    public void setOrderId(String orderId) {
	this.orderId = orderId;
    }

    public String getOrderKey() {
	return orderKey;
    }

    public void setOrderKey(String orderKey) {
	this.orderKey = orderKey;
    }

}

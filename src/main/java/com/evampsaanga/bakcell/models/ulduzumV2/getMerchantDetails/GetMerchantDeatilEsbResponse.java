package com.evampsaanga.bakcell.models.ulduzumV2.getMerchantDetails;

public class GetMerchantDeatilEsbResponse {
	
    private String returnMsg;
    private String returnCode;
	private Merchant data;

	public String getReturnMsg() {
		return returnMsg;
	}
	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public Merchant getData() {
		return data;
	}

	public void setData(Merchant data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "GetMerchantsEsbResponse [returnMsg=" + returnMsg + ", returnCode=" + returnCode + ", data=" + data
				+ "]";
	}
    
    

}

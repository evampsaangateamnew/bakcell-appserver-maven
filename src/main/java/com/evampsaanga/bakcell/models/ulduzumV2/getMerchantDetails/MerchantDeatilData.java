package com.evampsaanga.bakcell.models.ulduzumV2.getMerchantDetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MerchantDeatilData {

	Merchant data;

	@JsonProperty("merchantDetail")
	public Merchant getData() {
		return data;
	}

	@JsonProperty("data")
	public void setData(Merchant data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Data [data=" + data + "]";
	}


}

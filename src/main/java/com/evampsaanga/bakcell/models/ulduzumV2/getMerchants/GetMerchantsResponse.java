package com.evampsaanga.bakcell.models.ulduzumV2.getMerchants;

import java.util.List;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class GetMerchantsResponse extends BaseResponse{
	Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetMerchantsResponse [data=" + data + "]";
	}

}

package com.evampsaanga.bakcell.models.ulduzumV2.getMerchantDetails;


import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class GetMerchantDetailResponse extends BaseResponse{
	MerchantDeatilData data;

	public MerchantDeatilData getData() {
		return data;
	}

	public void setData(MerchantDeatilData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetMerchantsResponse [data=" + data + "]";
	}

}

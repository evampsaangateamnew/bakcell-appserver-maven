package com.evampsaanga.bakcell.models.ulduzumV2.getMerchants;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Data {
	List<Merchant> data;
	List<Category> categories;

	@JsonProperty("merchants")
	public List<Merchant> getData() {
		return data;
	}

	@JsonProperty("data")
	public void setData(List<Merchant> data) {
		this.data = data;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	@Override
	public String toString() {
		return "GetMerchantsResponse [data=" + data + ", categories=" + categories + "]";
	}

}

package com.evampsaanga.bakcell.models.ulduzumV2.getMerchants;

import java.util.List;


public class GetMerchantsEsbResponse {
	
    private String returnMsg;
    private String returnCode;
    private List<Merchant> data;
    
	public String getReturnMsg() {
		return returnMsg;
	}
	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	public List<Merchant> getData() {
		return data;
	}
	public void setData(List<Merchant> data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "GetMerchantsEsbResponse [returnMsg=" + returnMsg + ", returnCode=" + returnCode + ", data=" + data
				+ "]";
	}
    
    
}

package com.evampsaanga.bakcell.models.ulduzumV2.getMerchantDetails;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class GetMerchantDetailRequest extends BaseRequest{
	private String merchantid;

	public String getMerchantid() {
		return merchantid;
	}

	public void setMerchantid(String merchantid) {
		this.merchantid = merchantid;
	}

	@Override
	public String toString() {
		return "GetMerchantDetailsRequest [merchantid=" + merchantid + "]";
	}
}

package com.evampsaanga.bakcell.models.ulduzumV2.getMerchantDetails;


public class Branches {
	private String name;
	private String coord_lat;
	private String coord_lng;
	private String address;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCoord_lat() {
		return coord_lat;
	}
	public void setCoord_lat(String coord_lat) {
		this.coord_lat = coord_lat;
	}
	public String getCoord_lng() {
		return coord_lng;
	}
	public void setCoord_lng(String coord_lng) {
		this.coord_lng = coord_lng;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "Branches [name=" + name + ", coord_lat=" + coord_lat + ", coord_lng=" + coord_lng + ", address="
				+ address + "]";
	}
	
	
}

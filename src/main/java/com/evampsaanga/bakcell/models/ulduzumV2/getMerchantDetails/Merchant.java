package com.evampsaanga.bakcell.models.ulduzumV2.getMerchantDetails;

import java.util.List;

public class Merchant {
	private int id;
	private String name;
	private int category_id;
	private String category_name;
	private String logo;
	private String coord_lat;
	private String coord_lng;
	private boolean isspecialdisc;
	private String discount;
	private List<Branches> branches;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getCoord_lat() {
		return coord_lat;
	}

	public void setCoord_lat(String coord_lat) {
		this.coord_lat = coord_lat;
	}

	public String getCoord_lng() {
		return coord_lng;
	}

	public void setCoord_lng(String coord_lng) {
		this.coord_lng = coord_lng;
	}

	public boolean isIsspecialdisc() {
		return isspecialdisc;
	}

	public void setIsspecialdisc(boolean isspecialdisc) {
		this.isspecialdisc = isspecialdisc;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public List<Branches> getBranches() {
		return branches;
	}

	public void setBranches(List<Branches> branches) {
		this.branches = branches;
	}

	@Override
	public String toString() {
		return "Data [id=" + id + ", name=" + name + ", category_id=" + category_id + ", category_name=" + category_name
				+ ", logo=" + logo + ", coord_lat=" + coord_lat + ", coord_lng=" + coord_lng + ", isspecialdisc="
				+ isspecialdisc + ", discount=" + discount + ", branches=" + branches + "]";
	}

}

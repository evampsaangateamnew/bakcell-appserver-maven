package com.evampsaanga.bakcell.models.ulduzumV2.getUsageTotals;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class GetUsageTotalsResponse extends BaseResponse {

    private DataUsageTotals data = null;

    public DataUsageTotals getData() {
	return data;
    }

    public void setData(DataUsageTotals data) {
	this.data = data;
    }

}

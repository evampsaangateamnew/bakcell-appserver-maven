/**
 * 
 */
package com.evampsaanga.bakcell.models.verifyappversion;

/**
 * @author Evamp & Saanga
 *
 */
public class TimeStamps {
	private String cacheType;
	private String timeStamp;
	public String getCacheType() {
		return cacheType;
	}
	public void setCacheType(String cacheType) {
		this.cacheType = cacheType;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
}

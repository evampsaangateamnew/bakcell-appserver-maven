/**
 * 
 */
package com.evampsaanga.bakcell.models.verifyappversion;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class VerifyAppVersionRequest extends BaseRequest {
    private String appversion;

    public String getAppversion() {
	return appversion;
    }

    public void setAppversion(String appversion) {
	this.appversion = appversion;
    }

    @Override
    public String toString() {
	return "VerifyAppVersionRequest [appversion=" + appversion + ", getLang()=" + getLang() + ", getiP()=" + getiP()
		+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

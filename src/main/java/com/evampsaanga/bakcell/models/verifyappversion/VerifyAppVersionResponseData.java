/**
 * 
 */
package com.evampsaanga.bakcell.models.verifyappversion;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class VerifyAppVersionResponseData {
	private String versionConfig;
	private String message;
	private List<TimeStamps> timeStamps;
	private String appStore;
	private String playStore;
	
	public List<TimeStamps> getTimeStamps() {
		return timeStamps;
	}

	public void setTimeStamps(List<TimeStamps> timeStamps) {
		this.timeStamps = timeStamps;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getVersionConfig() {
		return versionConfig;
	}

	public void setVersionConfig(String versionConfig) {
		this.versionConfig = versionConfig;
	}

	public String getAppStore() {
		return appStore;
	}

	public void setAppStore(String appStore) {
		this.appStore = appStore;
	}

	public String getPlayStore() {
		return playStore;
	}

	public void setPlayStore(String playStore) {
		this.playStore = playStore;
	}
	
}

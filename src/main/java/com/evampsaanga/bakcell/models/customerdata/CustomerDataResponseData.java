package com.evampsaanga.bakcell.models.customerdata;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerDataResponseData {
	public static final Logger logger = Logger.getLogger("bakcellLogs-V2");

	private String accountId;

    private String entityId;

    private String simNumber;

    private String brandName;

    private String brandId;

    private String statusCode;

    private String pinCode;

    private String title;

    private String customerId;

    private String offeringNameDisplay;

    private String gender;

    private String pukCode;

    private String effectiveDate;

    private String firstName;

    private String middleName;

    private String subscriberType;

    private String lastName;

    private String customerType;

    private String status;

    private String msisdn;

    private String expiryDate;

    private String loyaltySegment;

    private String billingLanguage;

    private String offeringName;

    private String offeringId;

    private String pin2Code;

    private String email;

    private String dob;

    private String language;

    private String statusDetails;

    private String puk2Code;
    
    private String groupType;
    
    private String imageURL;
    private String cinTariff;
    
    public String getCinTariff() {
		return cinTariff;
	}

	public void setCinTariff(String cinTariff) {
		this.cinTariff = cinTariff;
	}

	public ArrayList<String> getHideNumberTariffIds() {
		return hideNumberTariffIds;
	}

	public void setHideNumberTariffIds(ArrayList<String> hideNumberTariffIds) {
		this.hideNumberTariffIds = hideNumberTariffIds;
	}

	private ArrayList<String> hideNumberTariffIds=null;
    
    

    public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getAccountId ()
    {
        return accountId;
    }

    public void setAccountId (String accountId)
    {
        this.accountId = accountId;
    }

    public String getEntityId ()
    {
        return entityId;
    }

    public void setEntityId (String entityId)
    {
        this.entityId = entityId;
    }

    public String getSimNumber ()
    {
        return simNumber;
    }

    public void setSimNumber (String simNumber)
    {
        this.simNumber = simNumber;
    }

    public String getBrandName ()
    {
        return brandName;
    }

    public void setBrandName (String brandName)
    {
        this.brandName = brandName;
    }

    public String getBrandId ()
    {
        return brandId;
    }

    public void setBrandId (String brandId)
    {
        this.brandId = brandId;
    }

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getPinCode ()
    {
        return pinCode;
    }

    public void setPinCode (String pinCode)
    {
        this.pinCode = pinCode;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getCustomerId ()
    {
        return customerId;
    }

    public void setCustomerId (String customerId)
    {
        this.customerId = customerId;
    }

    public String getOfferingNameDisplay ()
    {
        return offeringNameDisplay;
    }

    public void setOfferingNameDisplay (String offeringNameDisplay)
    {
        this.offeringNameDisplay = offeringNameDisplay;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getPukCode ()
    {
        return pukCode;
    }

    public void setPukCode (String pukCode)
    {
        this.pukCode = pukCode;
    }

    public String getEffectiveDate ()
    {
        return effectiveDate;
    }

    public void setEffectiveDate (String effectiveDate)
    {
        this.effectiveDate = effectiveDate;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    public String getMiddleName ()
    {
        return middleName;
    }

    public void setMiddleName (String middleName)
    {
        this.middleName = middleName;
    }

    public String getSubscriberType ()
    {
        return subscriberType;
    }

    public void setSubscriberType (String subscriberType)
    {
        this.subscriberType = subscriberType;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public String getCustomerType ()
    {
        return customerType;
    }

    public void setCustomerType (String customerType)
    {
        this.customerType = customerType;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getMsisdn ()
    {
        return msisdn;
    }

    public void setMsisdn (String msisdn)
    {
        this.msisdn = msisdn;
    }

    public String getExpiryDate ()
    {
        return expiryDate;
    }

    public void setExpiryDate (String expiryDate)
    {
        this.expiryDate = expiryDate;
    }

    public String getLoyaltySegment ()
    {
        return loyaltySegment;
    }

    public void setLoyaltySegment (String loyaltySegment)
    {
        this.loyaltySegment = loyaltySegment;
    }

    public String getBillingLanguage ()
    {
        return billingLanguage;
    }

    public void setBillingLanguage (String billingLanguage)
    {
        this.billingLanguage = billingLanguage;
    }

    public String getOfferingName ()
    {
        return offeringName;
    }

    public void setOfferingName (String offeringName)
    {
        this.offeringName = offeringName;
    }

    public String getOfferingId ()
    {
        return offeringId;
    }

    public void setOfferingId (String offeringId)
    {
        this.offeringId = offeringId;
    }

    public String getPin2Code ()
    {
        return pin2Code;
    }

    public void setPin2Code (String pin2Code)
    {
        this.pin2Code = pin2Code;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getLanguage ()
    {
        return language;
    }

    public void setLanguage (String language)
    {
        this.language = language;
    }

    public String getStatusDetails ()
    {
        return statusDetails;
    }

    public void setStatusDetails (String statusDetails)
    {
        this.statusDetails = statusDetails;
    }

    public String getPuk2Code ()
    {
        return puk2Code;
    }

    public void setPuk2Code (String puk2Code)
    {
        this.puk2Code = puk2Code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [accountId = "+accountId+", entityId = "+entityId+", simNumber = "+simNumber+", brandName = "+brandName+", brandId = "+brandId+", statusCode = "+statusCode+", pinCode = "+pinCode+", title = "+title+", customerId = "+customerId+", offeringNameDisplay = "+offeringNameDisplay+", gender = "+gender+", pukCode = "+pukCode+", effectiveDate = "+effectiveDate+", firstName = "+firstName+", middleName = "+middleName+", subscriberType = "+subscriberType+", lastName = "+lastName+", customerType = "+customerType+", status = "+status+", msisdn = "+msisdn+", expiryDate = "+expiryDate+", loyaltySegment = "+loyaltySegment+", billingLanguage = "+billingLanguage+", offeringName = "+offeringName+", offeringId = "+offeringId+", pin2Code = "+pin2Code+", email = "+email+", dob = "+dob+", language = "+language+", statusDetails = "+statusDetails+", puk2Code = "+puk2Code+"]";
    }

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

}

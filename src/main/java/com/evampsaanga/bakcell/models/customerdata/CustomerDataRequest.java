package com.evampsaanga.bakcell.models.customerdata;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class CustomerDataRequest extends BaseRequest {

    @Override
    public String toString() {
	return "CustomerDataRequest [getIsB2B()=" + getIsB2B() + ", getLang()=" + getLang() + ", getiP()=" + getiP()
		+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

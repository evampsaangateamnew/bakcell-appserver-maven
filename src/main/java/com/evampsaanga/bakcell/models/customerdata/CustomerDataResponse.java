package com.evampsaanga.bakcell.models.customerdata;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class CustomerDataResponse extends BaseResponse {
    CustomerDataResponseData data;

    public CustomerDataResponseData getData() {
	return data;
    }

    public void setData(CustomerDataResponseData data) {
	this.data = data;
    }

}

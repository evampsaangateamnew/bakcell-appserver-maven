package com.evampsaanga.bakcell.models.customerdata;

import com.evampsaanga.bakcell.models.generalservices.predefineddata.PredefinedDataResponse;
import com.evampsaanga.bakcell.models.homepageservices.HomePageResponseData;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HomePageResponseDataV2 {
	private HomePageResponseData homePageData;
	private CustomerDataResponseData customerInfo;
	private PredefinedDataResponse predefinedData;
	

	public PredefinedDataResponse getPredefinedData() {
		return predefinedData;
	}

	public void setPredefinedData(PredefinedDataResponse predefinedData) {
		this.predefinedData = predefinedData;
	}

	public CustomerDataResponseData getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerDataResponseData customerInfo) {
		this.customerInfo = customerInfo;
	}

	public HomePageResponseData getHomePageData() {
		return homePageData;
	}

	public void setHomePageData(HomePageResponseData homePageData) {
		this.homePageData = homePageData;
	}
	
}


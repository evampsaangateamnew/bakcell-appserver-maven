package com.evampsaanga.bakcell.models.customerdata;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class HomePageResponseV2 extends BaseResponse {
    private HomePageResponseDataV2 data;

    public HomePageResponseDataV2 getData() {
	return data;
    }

    public void setData(HomePageResponseDataV2 data) {
	this.data = data;
    }

}
package com.evampsaanga.bakcell.models.rateus;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class RateUsResponse extends BaseResponse {
    private RateUsResponseData data;

    public RateUsResponseData getData() {
	return data;
    }

    public void setData(RateUsResponseData data) {
	this.data = data;
    }

    @Override
    public String toString() {
	return "RateUsResponse [data=" + data + "]";
    }

}

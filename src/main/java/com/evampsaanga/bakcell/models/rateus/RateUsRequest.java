package com.evampsaanga.bakcell.models.rateus;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class RateUsRequest extends BaseRequest {

    @Override
    public String toString() {
	return "RateUsRequest [getIsB2B()=" + getIsB2B() + ", getLang()=" + getLang() + ", getiP()=" + getiP()
		+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

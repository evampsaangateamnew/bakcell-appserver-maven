/**
 * 
 */
package com.evampsaanga.bakcell.models.mysubscriptions.getsubscriptions;

/**
 * @author Evamp & Saanga
 *
 */
public class HeaderAction {
	private String title;
	private String value;
	private String iconName;
	private String desc;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}

/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.prepaid;

import java.util.List;

import com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.Cin;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass.Klass;

/**
 * @author Evamp & Saanga
 *
 */
public class PrepaidTariff {
	private List<Klass> klass;
	private List<Cin> cin;
	private List<Klass> cin_new;

	public List<Klass> getCin_new() {
		return cin_new;
	}

	public void setCin_new(List<Klass> cin_new) {
		this.cin_new = cin_new;
	}

	public List<Klass> getKlass() {
		return klass;
	}

	public void setKlass(List<Klass> klass) {
		this.klass = klass;
	}

	public List<Cin> getCin() {
		return cin;
	}

	public void setCin(List<Cin> cin) {
		this.cin = cin;
	}

}

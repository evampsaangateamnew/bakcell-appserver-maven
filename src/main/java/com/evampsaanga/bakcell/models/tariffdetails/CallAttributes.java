/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails;

/**
 * @author Evamp & Saanga
 *
 */
public class CallAttributes {
	private String title;
	private String valueLeft;
	private String valueRight;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getValueLeft() {
		return valueLeft;
	}

	public void setValueLeft(String valueLeft) {
		this.valueLeft = valueLeft;
	}

	public String getValueRight() {
		return valueRight;
	}

	public void setValueRight(String valueRight) {
		this.valueRight = valueRight;
	}

}

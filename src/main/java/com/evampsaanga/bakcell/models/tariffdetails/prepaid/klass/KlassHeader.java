/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class KlassHeader {

	private String id;
	private String offeringId;
	private String currency;
	private String name;
	private String priceLabel;
	private String priceValue;
	private String subscribable;
	private Integer sortOrder;
	private String tariffType;
	
	public String getTariffType() {
		return tariffType;
	}

	public void setTariffType(String tariffType) {
		this.tariffType = tariffType;
	}


	List<KlassHeaderAttributes> attributes;
	
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPriceLabel() {
		return priceLabel;
	}

	public void setPriceLabel(String priceLabel) {
		this.priceLabel = priceLabel;
	}

	public String getPriceValue() {
		return priceValue;
	}

	public void setPriceValue(String priceValue) {
		this.priceValue = priceValue;
	}

	public String getSubscribable() {
		return subscribable;
	}

	public void setSubscribable(String subscribable) {
		this.subscribable = subscribable;
	}

	public List<KlassHeaderAttributes> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<KlassHeaderAttributes> attributes) {
		this.attributes = attributes;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	

}

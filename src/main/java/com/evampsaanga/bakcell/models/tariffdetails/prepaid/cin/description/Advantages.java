/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.description;

/**
 * @author Evamp & Saanga
 *
 */
public class Advantages {
	private String title;
	private String description;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}

package com.evampsaanga.bakcell.models.tariffdetails.changetariff;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class ChangeTariffResponseBulk extends BaseResponse {

    ChangeTariffResponseData data = new ChangeTariffResponseData();

    public ChangeTariffResponseData getData() {
	return data;
    }

    public void setData(ChangeTariffResponseData data) {
	this.data = data;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.changetariff;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangeTariffResponse extends BaseResponse {

    private ChangeTariffResponseData data;

    public ChangeTariffResponseData getData() {
	return data;
    }

    public void setData(ChangeTariffResponseData data) {
	this.data = data;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.postpaid.corporate.details;

import com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass.details.InternationalPeak;

/**
 * @author Evamp & Saanga
 *
 */
public class CorporateDetails {
    private String detailsLabel;
    private CorporateDestination destination;
    private InternationalPeak internationalOnPeak;
    private InternationalPeak internationalOffPeak;

    public CorporateDetails() {
	this.destination = new CorporateDestination();
	this.internationalOnPeak = new InternationalPeak();
	this.internationalOffPeak = new InternationalPeak();

    }

    public CorporateDestination getDestination() {
	return destination;
    }

    public void setDestination(CorporateDestination destination) {
	this.destination = destination;
    }

    public InternationalPeak getInternationalOnPeak() {
	return internationalOnPeak;
    }

    public void setInternationalOnPeak(InternationalPeak internationalOnPeak) {
	this.internationalOnPeak = internationalOnPeak;
    }

    public InternationalPeak getInternationalOffPeak() {
	return internationalOffPeak;
    }

    public void setInternationalOffPeak(InternationalPeak internationalOffPeak) {
	this.internationalOffPeak = internationalOffPeak;
    }

    public String getDetailsLabel() {
	return detailsLabel;
    }

    public void setDetailsLabel(String detailsLabel) {
	this.detailsLabel = detailsLabel;
    }

}

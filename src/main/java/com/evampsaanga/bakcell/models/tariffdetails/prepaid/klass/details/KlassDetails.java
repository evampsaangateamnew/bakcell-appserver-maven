/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass.details;

/**
 * @author Evamp & Saanga
 *
 */
public class KlassDetails {

	private String detailsLabel;
	private DestinationDetails destination;
	private InternationalPeak internationalOnPeak;
	private InternationalPeak internationalOffPeak;

	public KlassDetails() {
		this.destination = new DestinationDetails();
		this.internationalOnPeak = new InternationalPeak();
		this.internationalOffPeak = new InternationalPeak();

	}

	public DestinationDetails getDestination() {
		return destination;
	}

	public void setDestination(DestinationDetails destination) {
		this.destination = destination;
	}

	public InternationalPeak getInternationalOnPeak() {
		return internationalOnPeak;
	}

	public void setInternationalOnPeak(InternationalPeak internationalOnPeak) {
		this.internationalOnPeak = internationalOnPeak;
	}

	public InternationalPeak getInternationalOffPeak() {
		return internationalOffPeak;
	}

	public void setInternationalOffPeak(InternationalPeak internationalOffPeak) {
		this.internationalOffPeak = internationalOffPeak;
	}

	public String getDetailsLabel() {
		return detailsLabel;
	}

	public void setDetailsLabel(String detailsLabel) {
		this.detailsLabel = detailsLabel;
	}

}

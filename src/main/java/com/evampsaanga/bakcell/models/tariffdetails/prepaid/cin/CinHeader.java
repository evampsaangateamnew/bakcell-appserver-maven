/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin;

/**
 * @author Evamp & Saanga
 *
 */
public class CinHeader {
	private String id;
	private String offeringId;
	private String bonusDescription;
	private String bonusIconName;
	private String bonusTitle;
	private String name;
	private String subscribable;
	private HeaderCall call;
	private CinSMS sms;
	private HeaderInternet internet;

	private Integer sortOrder;
	private String tariffType;
	
	public String getTariffType() {
		return tariffType;
	}

	public void setTariffType(String tariffType) {
		this.tariffType = tariffType;
	}
	
	public CinHeader() {
		this.call = new HeaderCall();
		this.sms = new CinSMS();
		this.internet = new HeaderInternet();
	}

	public String getBonusDescription() {
		return bonusDescription;
	}

	public void setBonusDescription(String bonusDescription) {
		this.bonusDescription = bonusDescription;
	}

	public String getBonusIconName() {
		return bonusIconName;
	}

	public void setBonusIconName(String bonusIconName) {
		this.bonusIconName = bonusIconName;
	}

	public String getBonusTitle() {
		return bonusTitle;
	}

	public void setBonusTitle(String bonusTitle) {
		this.bonusTitle = bonusTitle;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubscribable() {
		return subscribable;
	}

	public void setSubscribable(String subscribable) {
		this.subscribable = subscribable;
	}

	public HeaderCall getCall() {
		return call;
	}

	public void setCall(HeaderCall call) {
		this.call = call;
	}

	public CinSMS getSms() {
		return sms;
	}

	public void setSms(CinSMS sms) {
		this.sms = sms;
	}

	public HeaderInternet getInternet() {
		return internet;
	}

	public void setInternet(HeaderInternet internet) {
		this.internet = internet;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(String offeringId) {
		this.offeringId = offeringId;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	
}

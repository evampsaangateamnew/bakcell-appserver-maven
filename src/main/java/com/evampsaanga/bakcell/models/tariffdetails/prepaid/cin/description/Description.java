/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.description;

/**
 * @author Evamp & Saanga
 *
 */
public class Description {

	private String descLabel;
	private Advantages advantages;
	private Classification classification;

	public Advantages getAdvantages() {
		return advantages;
	}

	public void setAdvantages(Advantages advantages) {
		this.advantages = advantages;
	}

	public Classification getClassification() {
		return classification;
	}

	public void setClassification(Classification classification) {
		this.classification = classification;
	}

	public String getDescLabel() {
		return descLabel;
	}

	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.postpaid.corporate;

import com.evampsaanga.bakcell.models.tariffdetails.TariffDetailsSection;
import com.evampsaanga.bakcell.models.tariffdetails.postpaid.corporate.price.CorporatePrice;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.description.Description;

/**
 * @author Evamp & Saanga
 *
 */
public class Corporate {
    private CorporateHeader header;
    private CorporatePrice price;
    private TariffDetailsSection details;
    private Description description;

    /**
     * @param header
     * @param price
     * @param details
     * @param description
     */
    public Corporate() {
	this.header = new CorporateHeader();
	this.price = new CorporatePrice();
	this.details = new TariffDetailsSection();
	this.description = new Description();
    }

    public CorporateHeader getHeader() {
	return header;
    }

    public void setHeader(CorporateHeader header) {
	this.header = header;
    }

    public CorporatePrice getPrice() {
	return price;
    }

    public void setPrice(CorporatePrice price) {
	this.price = price;
    }

    public TariffDetailsSection getDetails() {
	return details;
    }

    public void setDetails(TariffDetailsSection details) {
	this.details = details;
    }

    public Description getDescription() {
	return description;
    }

    public void setDescription(Description description) {
	this.description = description;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.postpaid.individual;

import com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.HeaderCall;

/**
 * @author Evamp & Saanga
 *
 */
public class IndividualHeader {

    private String id;
    private String offeringId;
    private String name;
    private String priceLabel;
    private String priceValue;
    private String subscribable;
    private HeaderCall call;
    private IndividualSMS sms;
    private IndividualInternet internet;
    private Integer sortOrder;

    public IndividualHeader() {
	this.call = new HeaderCall();
	this.sms = new IndividualSMS();
	this.internet = new IndividualInternet();
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getPriceLabel() {
	return priceLabel;
    }

    public void setPriceLabel(String priceLabel) {
	this.priceLabel = priceLabel;
    }

    public String getPriceValue() {
	return priceValue;
    }

    public void setPriceValue(String priceValue) {
	this.priceValue = priceValue;
    }

    public String getSubscribable() {
	return subscribable;
    }

    public void setSubscribable(String subscribable) {
	this.subscribable = subscribable;
    }

    public HeaderCall getCall() {
	return call;
    }

    public void setCall(HeaderCall call) {
	this.call = call;
    }

    public IndividualSMS getSms() {
	return sms;
    }

    public void setSms(IndividualSMS sms) {
	this.sms = sms;
    }

    public IndividualInternet getInternet() {
	return internet;
    }

    public void setInternet(IndividualInternet internet) {
	this.internet = internet;
    }

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public String getOfferingId() {
	return offeringId;
    }

    public void setOfferingId(String offeringId) {
	this.offeringId = offeringId;
    }

    public Integer getSortOrder() {
	return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
	this.sortOrder = sortOrder;
    }

}

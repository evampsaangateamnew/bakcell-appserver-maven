package com.evampsaanga.bakcell.models.tariffdetails.postpaid.individual;

import com.evampsaanga.bakcell.models.tariffdetails.TariffDetailsSection;
import com.evampsaanga.bakcell.models.tariffdetails.postpaid.individual.description.IndividualDescription;

public class IndividualV2 {

    private IndividualHeader header;

    private TariffDetailsSection details;
    private IndividualDescription description;

    public IndividualV2() {
	this.header = new IndividualHeader();
	this.setDetails(new TariffDetailsSection());
	this.description = new IndividualDescription();

    }

    public IndividualDescription getDescription() {
	return description;
    }

    public void setDescription(IndividualDescription description) {
	this.description = description;
    }

    public TariffDetailsSection getDetails() {
	return details;
    }

    public void setDetails(TariffDetailsSection details) {
	this.details = details;
    }

    public IndividualHeader getHeader() {
	return header;
    }

    public void setHeader(IndividualHeader header) {
	this.header = header;
    }

}
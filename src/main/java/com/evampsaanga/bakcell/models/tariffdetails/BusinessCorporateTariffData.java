/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails;

import com.evampsaanga.bakcell.models.tariffdetails.postpaid.corporate.Corporate;

/**
 * @author Muneeb Rehman
 *
 */
public class BusinessCorporateTariffData {
	int sortOrder;
	String tariffType;
	Corporate corporate;

	public BusinessCorporateTariffData() {
	corporate = new Corporate();
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getTariffType() {
		return tariffType;
	}

	public void setTariffType(String tariffType) {
		this.tariffType = tariffType;
	}

	public Corporate getCorporate() {
		return corporate;
	}

	public void setCorporate(Corporate corporate) {
		this.corporate = corporate;
	}
	
	
}

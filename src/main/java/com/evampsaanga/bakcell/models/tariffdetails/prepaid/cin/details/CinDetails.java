/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.details;

/**
 * @author Evamp & Saanga
 *
 */
public class CinDetails {

	private String detailLabel;
	private DetailsModel mms;
	private DetailsModel call;
	private DetailsModel destination;

	public DetailsModel getMms() {
		return mms;
	}

	public void setMms(DetailsModel mms) {
		this.mms = mms;
	}

	public DetailsModel getCall() {
		return call;
	}

	public void setCall(DetailsModel call) {
		this.call = call;
	}

	public DetailsModel getDestination() {
		return destination;
	}

	public void setDestination(DetailsModel destination) {
		this.destination = destination;
	}

	public String getDetailLabel() {
		return detailLabel;
	}

	public void setDetailLabel(String detailLabel) {
		this.detailLabel = detailLabel;
	}

}

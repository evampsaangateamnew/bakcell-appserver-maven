/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.postpaid.corporate.price;

/**
 * @author Evamp & Saanga
 *
 */
public class CorporateInternet {
	private String iconName;
	private String title;
	private String titleValue;
	private String subTitle;
	private String subTitleValue;

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleValue() {
		return titleValue;
	}

	public void setTitleValue(String titleValue) {
		this.titleValue = titleValue;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getSubTitleValue() {
		return subTitleValue;
	}

	public void setSubTitleValue(String subTitleValue) {
		this.subTitleValue = subTitleValue;
	}

}

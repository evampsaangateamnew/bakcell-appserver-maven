/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.changetariff;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangeTariffResponseData {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

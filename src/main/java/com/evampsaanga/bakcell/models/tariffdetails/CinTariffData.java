/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails;

import com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.Cin;

/**
 * @author Muneeb Rehman
 *
 */
public class CinTariffData {
	int sortOrder;
	String tariffType;
	Cin cin;

	public CinTariffData() {
		cin = new Cin();
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getTariffType() {
		return tariffType;
	}

	public void setTariffType(String tariffType) {
		this.tariffType = tariffType;
	}

	public Cin getCin() {
		return cin;
	}

	public void setCin(Cin cin) {
		this.cin = cin;
	}

}

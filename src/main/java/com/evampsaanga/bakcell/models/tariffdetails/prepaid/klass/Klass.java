/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass;

import com.evampsaanga.bakcell.models.tariffdetails.TariffDetailsSection;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass.packageprice.PackagePrice;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass.paygprice.PaygPrice;

/**
 * @author Evamp & Saanga
 *
 */
public class Klass {

    private KlassHeader header;
    private PackagePrice packagePrice;
    private PaygPrice paygPrice;
    private TariffDetailsSection details;

    public Klass() {
	this.header = new KlassHeader();
	this.packagePrice = new PackagePrice();
	this.paygPrice = new PaygPrice();
	this.details = new TariffDetailsSection();

    }

    public KlassHeader getHeader() {
	return header;
    }

    public void setHeader(KlassHeader header) {
	this.header = header;
    }

    public PackagePrice getPackagePrice() {
	return packagePrice;
    }

    public void setPackagePrice(PackagePrice packagePrice) {
	this.packagePrice = packagePrice;
    }

    public PaygPrice getPaygPrice() {
	return paygPrice;
    }

    public void setPaygPrice(PaygPrice paygPrice) {
	this.paygPrice = paygPrice;
    }

    public TariffDetailsSection getDetails() {
	return details;
    }

    public void setDetails(TariffDetailsSection details) {
	this.details = details;
    }

}

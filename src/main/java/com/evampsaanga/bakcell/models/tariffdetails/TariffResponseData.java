/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails;

import com.evampsaanga.bakcell.models.tariffdetails.postpaid.PostpaidTariff;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.PrepaidTariff;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffResponseData {

	private PrepaidTariff prepaid;
	private PostpaidTariff postpaid;

	public TariffResponseData() {
		this.prepaid = new PrepaidTariff();
		this.postpaid = new PostpaidTariff();
	}

	public PrepaidTariff getPrepaid() {
		return prepaid;
	}

	public void setPrepaid(PrepaidTariff prepaid) {
		this.prepaid = prepaid;
	}

	public PostpaidTariff getPostpaid() {
		return postpaid;
	}

	public void setPostpaid(PostpaidTariff postpaid) {
		this.postpaid = postpaid;
	}

}

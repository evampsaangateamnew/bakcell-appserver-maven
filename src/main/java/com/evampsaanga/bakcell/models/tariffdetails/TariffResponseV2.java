/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffResponseV2 extends BaseResponse {
    TariffResponseDataV2 data;

    public TariffResponseDataV2 getData() {
	return data;
    }

    public void setData(TariffResponseDataV2 data) {
	this.data = data;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails;

import java.util.ArrayList;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffRequestV2 extends BaseRequest {

    ArrayList<String> offeringId;

    public ArrayList<String> getOfferingId() {
	return offeringId;
    }

    public void setOfferingId(ArrayList<String> offeringId) {
	this.offeringId = offeringId;
    }

    @Override
    public String toString() {
	return "TariffRequestV2 [offeringId=" + offeringId + ", getOfferingId()=" + getOfferingId() + ", getIsB2B()="
		+ getIsB2B() + ", getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
		+ ", getMsisdn()=" + getMsisdn() + "]";
    }

}

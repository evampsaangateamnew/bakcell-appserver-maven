/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass.packageprice;

import com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.HeaderCall;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.HeaderInternet;

/**
 * @author Evamp & Saanga
 *
 */
public class PackagePrice {

    private String packagePriceLabel;
    private HeaderCall call;
    private PackagePriceModel sms;
    private HeaderInternet internet;

    public PackagePrice() {
	this.call = new HeaderCall();
	this.sms = new PackagePriceModel();
	this.internet = new HeaderInternet();
    }

    public HeaderCall getCall() {
	return call;
    }

    public void setCall(HeaderCall call) {
	this.call = call;
    }

    public PackagePriceModel getSms() {
	return sms;
    }

    public void setSms(PackagePriceModel sms) {
	this.sms = sms;
    }

    public HeaderInternet getInternet() {
	return internet;
    }

    public void setInternet(HeaderInternet internet) {
	this.internet = internet;
    }

    public String getPackagePriceLabel() {
	return packagePriceLabel;
    }

    public void setPackagePriceLabel(String packagePriceLabel) {
	this.packagePriceLabel = packagePriceLabel;
    }

}

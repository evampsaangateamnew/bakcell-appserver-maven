/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.postpaid.individual;

import com.evampsaanga.bakcell.models.tariffdetails.TariffDetailsSection;
import com.evampsaanga.bakcell.models.tariffdetails.postpaid.corporate.CorporateHeader;
import com.evampsaanga.bakcell.models.tariffdetails.postpaid.corporate.price.CorporatePrice;
import com.evampsaanga.bakcell.models.tariffdetails.postpaid.individual.description.IndividualDescription;

/**
 * @author Evamp & Saanga
 *
 */
public class Individual {
    /* private IndividualHeader header; */

    private CorporateHeader header;

    private CorporatePrice price;
    private TariffDetailsSection details;
    private IndividualDescription description;

    public Individual() {
	this.header = new CorporateHeader();
	this.setDetails(new TariffDetailsSection());
	this.description = new IndividualDescription();
	this.price = new CorporatePrice();
    }

    public CorporateHeader getHeader() {
	return header;
    }

    public void setHeader(CorporateHeader header) {
	this.header = header;
    }

    public IndividualDescription getDescription() {
	return description;
    }

    public void setDescription(IndividualDescription description) {
	this.description = description;
    }

    public TariffDetailsSection getDetails() {
	return details;
    }

    public void setDetails(TariffDetailsSection details) {
	this.details = details;
    }

    public CorporatePrice getPrice() {
	return price;
    }

    public void setPrice(CorporatePrice price) {
	this.price = price;
    }
}

/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffRequest extends BaseRequest {

    private String storeId;
    private String subscribedOfferingName;
    private String subscriberType;
    private String customerType;
    private String offeringId;

    public String getStoreId() {
	return storeId;
    }

    public void setStoreId(String storeId) {
	this.storeId = storeId;
    }

    public String getSubscribedOfferingName() {
	return subscribedOfferingName;
    }

    public void setSubscribedOfferingName(String subscribedOfferingName) {
	this.subscribedOfferingName = subscribedOfferingName;
    }

    public String getSubscriberType() {
	return subscriberType;
    }

    public void setSubscriberType(String subscriberType) {
	this.subscriberType = subscriberType;
    }

    public String getCustomerType() {
	return customerType;
    }

    public void setCustomerType(String customerType) {
	this.customerType = customerType;
    }

    public String getOfferingId() {
	return offeringId;
    }

    public void setOfferingId(String offeringId) {
	this.offeringId = offeringId;
    }

    @Override
    public String toString() {
	return "TariffRequest [storeId=" + storeId + ", subscribedOfferingName=" + subscribedOfferingName
		+ ", subscriberType=" + subscriberType + ", customerType=" + customerType + ", offeringId=" + offeringId
		+ "]";
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails;

import com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass.Klass;

/**
 * @author Muneeb Rehman
 *
 */
public class KlassTariffData {

	int sortOrder;
	String tariffType;
	Klass klass;

	public KlassTariffData() {
		klass = new Klass();
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getTariffType() {
		return tariffType;
	}

	public void setTariffType(String tariffType) {
		this.tariffType = tariffType;
	}

	public Klass getKlass() {
		return klass;
	}

	public void setKlass(Klass klass) {
		this.klass = klass;
	}

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffResponse extends BaseResponse {
    TariffResponseData data;

    public TariffResponseData getData() {
	return data;
    }

    public void setData(TariffResponseData data) {
	this.data = data;
    }

}

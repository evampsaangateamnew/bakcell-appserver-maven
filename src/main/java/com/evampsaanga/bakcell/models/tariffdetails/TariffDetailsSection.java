/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.DateTemplate;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.FreeResourceValidity;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.Rounding;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithOutTitle;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithPoints;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithTitle;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TimeTemplate;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description.Price;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.roamingoffers.RoamingDetails;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass.details.InternationalPeak;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffDetailsSection {
	private String detailLabel;
	private List<Price> price;
	private Rounding rounding;
	private TextWithTitle textWithTitle;
	private TextWithOutTitle textWithOutTitle;
	private TextWithPoints textWithPoints;
	private TitleSubTitleTariff titleSubTitleValueAndDesc;
	private DateTemplate date;
	private TimeTemplate time;
	private RoamingDetails roamingDetails;
	private FreeResourceValidity freeResourceValidity;
	private InternationalPeak internationalOnPeak;
	private InternationalPeak internationalOffPeak;

	/**
	 * @param detailLabel
	 * @param call
	 * @param mms
	 * @param destination
	 * @param textWithTitle
	 * @param textWithOutTitle
	 * @param textWithPoints
	 * @param titleSubTitle
	 * @param date
	 * @param time
	 * @param freeResourceValidity
	 */
	public TariffDetailsSection() {
		this.detailLabel = "";
		this.price = new ArrayList<>();
		this.rounding = new Rounding();
		this.textWithTitle = new TextWithTitle();
		this.textWithOutTitle = new TextWithOutTitle();
		this.textWithPoints = new TextWithPoints();
		this.titleSubTitleValueAndDesc = new TitleSubTitleTariff();
		this.date = new DateTemplate();
		this.time = new TimeTemplate();
		this.freeResourceValidity = new FreeResourceValidity();
	}

	public String getDetailLabel() {
		return detailLabel;
	}

	public void setDetailLabel(String detailLabel) {
		this.detailLabel = detailLabel;
	}

	public TextWithTitle getTextWithTitle() {
		return textWithTitle;
	}

	public void setTextWithTitle(TextWithTitle textWithTitle) {
		this.textWithTitle = textWithTitle;
	}

	public TextWithOutTitle getTextWithOutTitle() {
		return textWithOutTitle;
	}

	public void setTextWithOutTitle(TextWithOutTitle textWithOutTitle) {
		this.textWithOutTitle = textWithOutTitle;
	}

	public TextWithPoints getTextWithPoints() {
		return textWithPoints;
	}

	public void setTextWithPoints(TextWithPoints textWithPoints) {
		this.textWithPoints = textWithPoints;
	}

	public TitleSubTitleTariff getTitleSubTitleValueAndDesc() {
		return titleSubTitleValueAndDesc;
	}

	public void setTitleSubTitleValueAndDesc(TitleSubTitleTariff titleSubTitleValueAndDesc) {
		this.titleSubTitleValueAndDesc = titleSubTitleValueAndDesc;
	}

	public DateTemplate getDate() {
		return date;
	}

	public void setDate(DateTemplate date) {
		this.date = date;
	}

	public TimeTemplate getTime() {
		return time;
	}

	public void setTime(TimeTemplate time) {
		this.time = time;
	}

	public RoamingDetails getRoamingDetails() {
		return roamingDetails;
	}

	public void setRoamingDetails(RoamingDetails roamingDetails) {
		this.roamingDetails = roamingDetails;
	}

	public FreeResourceValidity getFreeResourceValidity() {
		return freeResourceValidity;
	}

	public void setFreeResourceValidity(FreeResourceValidity freeResourceValidity) {
		this.freeResourceValidity = freeResourceValidity;
	}

	public InternationalPeak getInternationalOnPeak() {
		return internationalOnPeak;
	}

	public void setInternationalOnPeak(InternationalPeak internationalOnPeak) {
		this.internationalOnPeak = internationalOnPeak;
	}

	public InternationalPeak getInternationalOffPeak() {
		return internationalOffPeak;
	}

	public void setInternationalOffPeak(InternationalPeak internationalOffPeak) {
		this.internationalOffPeak = internationalOffPeak;
	}

	public List<Price> getPrice() {
		return price;
	}

	public void setPrice(List<Price> price) {
		this.price = price;
	}

	public Rounding getRounding() {
		return rounding;
	}

	public void setRounding(Rounding rounding) {
		this.rounding = rounding;
	}

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass.paygprice;

import com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.HeaderCall;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.HeaderInternet;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass.packageprice.PackagePriceModel;

/**
 * @author Evamp & Saanga
 *
 */
public class PaygPrice {
    private String paygPriceLabel;
    private HeaderCall call;
    private PackagePriceModel sms;
    private HeaderInternet internet;

    public PaygPrice() {
	this.call = new HeaderCall();
	this.sms = new PackagePriceModel();
	this.internet = new HeaderInternet();
    }

    public PackagePriceModel getSms() {
	return sms;
    }

    public HeaderCall getCall() {
	return call;
    }

    public void setCall(HeaderCall call) {
	this.call = call;
    }

    public void setSms(PackagePriceModel sms) {
	this.sms = sms;
    }

    public HeaderInternet getInternet() {
	return internet;
    }

    public void setInternet(HeaderInternet internet) {
	this.internet = internet;
    }

    public String getPaygPriceLabel() {
	return paygPriceLabel;
    }

    public void setPaygPriceLabel(String paygPriceLabel) {
	this.paygPriceLabel = paygPriceLabel;
    }

}

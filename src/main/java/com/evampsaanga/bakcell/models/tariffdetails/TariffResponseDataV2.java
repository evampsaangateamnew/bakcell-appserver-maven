/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffResponseDataV2 {
	private List<KlassTariffData> klassTariffData;
	private List<CinTariffData> cinTariffData;
	private List<BusinessCorporateTariffData> businessCorporateTariffData;
	private List<BusinessIndividualTariffData> businessIndividualTariffData;
	private List<KlassPostpaidTariffData> klassPostpaidTariffData;

	public TariffResponseDataV2() {
		this.klassTariffData = new ArrayList<>();
		this.cinTariffData = new ArrayList<>();
		this.businessCorporateTariffData = new ArrayList<>();
		this.businessIndividualTariffData = new ArrayList<>();
		this.klassPostpaidTariffData = new ArrayList<>();
	}

	public List<KlassTariffData> getKlassTariffData() {
		return klassTariffData;
	}

	public void setKlassTariffData(List<KlassTariffData> klassTariffData) {
		this.klassTariffData = klassTariffData;
	}

	public List<CinTariffData> getCinTariffData() {
		return cinTariffData;
	}

	public void setCinTariffData(List<CinTariffData> cinTariffData) {
		this.cinTariffData = cinTariffData;
	}

	public List<BusinessCorporateTariffData> getBusinessCorporateTariffData() {
		return businessCorporateTariffData;
	}

	public void setBusinessCorporateTariffData(List<BusinessCorporateTariffData> businessCorporateTariffData) {
		this.businessCorporateTariffData = businessCorporateTariffData;
	}

	public List<BusinessIndividualTariffData> getBusinessIndividualTariffData() {
		return businessIndividualTariffData;
	}

	public void setBusinessIndividualTariffData(List<BusinessIndividualTariffData> businessIndividualTariffData) {
		this.businessIndividualTariffData = businessIndividualTariffData;
	}

	public List<KlassPostpaidTariffData> getKlassPostpaidTariffData() {
		return klassPostpaidTariffData;
	}

	public void setKlassPostpaidTariffData(List<KlassPostpaidTariffData> klassPostpaidTariffData) {
		this.klassPostpaidTariffData = klassPostpaidTariffData;
	}

}

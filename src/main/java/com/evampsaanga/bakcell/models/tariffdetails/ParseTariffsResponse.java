/**
 * 
 */
package com.evampsaanga.bakcell.models.tariffdetails;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.ParseSupplementaryOfferingsResponse;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithOutTitle;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithPoints;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithTitle;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description.DetailsAttributes;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description.Price;
import com.evampsaanga.bakcell.models.tariffdetails.postpaid.corporate.Corporate;
import com.evampsaanga.bakcell.models.tariffdetails.postpaid.corporate.HeaderAttribute;
import com.evampsaanga.bakcell.models.tariffdetails.postpaid.corporate.price.CorporateInternet;
import com.evampsaanga.bakcell.models.tariffdetails.postpaid.corporate.price.CorporateSMS;
import com.evampsaanga.bakcell.models.tariffdetails.postpaid.individual.Individual;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.Cin;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.HeaderCall;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.description.Advantages;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.cin.description.Classification;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass.Klass;
import com.evampsaanga.bakcell.models.tariffdetails.prepaid.klass.KlassHeaderAttributes;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author Evamp & Saanga
 *
 */
public class ParseTariffsResponse {

    static Logger logger = Logger.getLogger(ParseTariffsResponse.class);

    public static TariffResponseData parseTariffResponseData(String msisdn, String valueFromJSON,
	    TariffResponseData resData) throws JSONException, IOException {
	// Complete JSON
	Utilities.printDebugLog(msisdn + "-Parser has received JSON: " + valueFromJSON, logger);

	if (!valueFromJSON.isEmpty()) {

	    /*
	     * If subscriber type in request of tariff API is prepaid, below section will be
	     * parsed.
	     */

	    if (new JSONObject(valueFromJSON).get("Prepaid") instanceof JSONObject) {

		resData.setPostpaid(null);
		JSONObject prepaidObj = new JSONObject(valueFromJSON).getJSONObject("Prepaid");
		// CIN JSON
		if (prepaidObj.get("Cin") instanceof JSONArray) {
		    JSONArray cinArrayList = prepaidObj.getJSONArray("Cin");
		    Utilities.printDebugLog(msisdn + "-Parser has CIN Array List: " + cinArrayList, logger);
		    resData = parseCINData(msisdn, cinArrayList, resData);
		}
		// KLASS JSON
		if (prepaidObj.get("Klass") instanceof JSONArray) {
		    JSONArray klassArrayList = prepaidObj.getJSONArray("Klass");
		    Utilities.printDebugLog(msisdn + "-Parser has KLASS Array List: " + klassArrayList, logger);
		    resData = parseKlassData(msisdn, klassArrayList, resData);
		}
		if (prepaidObj.get("Cin_new") instanceof JSONArray) {
		    JSONArray cinNewArrayList = prepaidObj.getJSONArray("Cin_new");
		    Utilities.printDebugLog(msisdn + "-Parser has cin_new Array List: " + cinNewArrayList, logger);
		    resData = parseCinNewData(msisdn, cinNewArrayList, resData);
		}
		// Start of cin_new
		/*
		 * If subscriber type in request of tariff API is postpaid, below section will
		 * be parsed.
		 */
	    } else if (new JSONObject(valueFromJSON).get("Postpaid") instanceof JSONObject) {
		resData.setPrepaid(null);

		JSONObject postpaid = new JSONObject(valueFromJSON).getJSONObject("Postpaid");
		// CORPORATE JSON
		if (postpaid.get("BusinessCorporate") instanceof JSONArray) {

		    JSONArray corporateArrayList = postpaid.getJSONArray("BusinessCorporate");
		    Utilities.printDebugLog(msisdn + "-Parser has CORPORATE Array List: " + corporateArrayList, logger);
		    resData = parseCorporateData(msisdn, corporateArrayList, resData);
		}
		// INDIVIDUAL JSON
		if (postpaid.get("BusinessIndividual") instanceof JSONArray) {

		    JSONArray individualArrayList = postpaid.getJSONArray("BusinessIndividual");
		    Utilities.printDebugLog(msisdn + "-Parser has INDIVIDUAL Array List: " + individualArrayList,
			    logger);
		    resData = parseIndividualData(msisdn, individualArrayList, resData);
		}
		// KLASS POSTPAID JSON
		if (postpaid.get("KlassPostpaid") instanceof JSONArray) {

		    JSONArray klassPostpaidArrayList = postpaid.getJSONArray("KlassPostpaid");
		    Utilities.printDebugLog(msisdn + "-Parser has KLASS POSTPAID Array List: " + klassPostpaidArrayList,
			    logger);

		    resData = parseKlassPostpaidData(msisdn, klassPostpaidArrayList, resData);
		}

	    } else {
		Utilities.printDebugLog(
			msisdn + "-Parser dont have either CORPORATRE or INDIVIDUAL or KLASS BUSINESS Data.", logger);
	    }
	} else {
	    Utilities.printDebugLog(msisdn + "-Parser has received EMPTY JSON. " + valueFromJSON, logger);
	}

	return resData;
    }

    // Parse CIN Section
    private static TariffResponseData parseCINData(String msisdn, JSONArray cinArrayList, TariffResponseData resData)
	    throws JSONException, IOException {
	Utilities.printDebugLog(msisdn + "-CIN Array List is going to be parsed." + cinArrayList, logger);
	List<Cin> cinDataList = new ArrayList<>();
	for (int i = 0; i < cinArrayList.length(); i++) {
	    Cin cin = new Cin();
	    GenericAttributes attribute = new GenericAttributes();
	    /*
	     * As we have different templates for Call in header section and SMS|
	     * Destination therefore we have different call attributes.
	     */

	    // Header section starts
	    JSONObject header = cinArrayList.getJSONObject(i).getJSONObject("header");
	    cin.getHeader().setId(header.getString("id"));
	    cin.getHeader().setName(header.getString("name"));
	    cin.getHeader().setBonusTitle(header.getString("tag"));
	    cin.getHeader().setBonusIconName(header.getString("tagIcon"));
	    cin.getHeader().setBonusDescription(header.getString("shortDescription"));
	    cin.getHeader().setOfferingId(header.getString("offeringId"));
	    cin.getHeader().setTariffType("cin");
	    cin.getHeader().setSortOrder(header.getInt("sortOrder"));

	    /*
	     * If tariff does not has any offering id then activation button will be
	     * disabled.
	     */

	    if (Utilities.isOfferingIdExists(cin.getHeader().getOfferingId())) {
		cin.getHeader().setSubscribable(cinArrayList.getJSONObject(i).getString("subscribable"));
	    } else {
		cin.getHeader().setSubscribable(Constants.SUPPLEMENTARY_TARIFF_DEFAULT_DISABLE_BUTTON);
	    }

	    // Parsing CIN Call response in a separate method.
	    cin.getHeader().setCall(parseHeaderCallSection(header.getJSONObject("Call")));

	    JSONObject headerSmsObj = header.getJSONObject("SMS");

	    cin.getHeader().getSms().setTitle(headerSmsObj.getString("smsLabel"));
	    cin.getHeader().getSms().setTitleValue(headerSmsObj.getString("smsValue"));
	    // cin.getHeader().getSms().setIconName(headerSmsObj.getString("smsIcon"));

	    // Temporarily Fix Hardcode icon for all CIN tariffs.
	    cin.getHeader().getSms().setIconName("countrywideSMS");

	    attribute.setTitle(headerSmsObj.getString("smsCountryWideLabel"));
	    attribute.setValue(headerSmsObj.getString("smsCountryWideValue"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		cin.getHeader().getSms().getAttributes().add(attribute);

	    attribute = new GenericAttributes();
	    attribute.setTitle(headerSmsObj.getString("smsInternationalLabel"));
	    attribute.setValue(headerSmsObj.getString("smsInternationalValue"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		cin.getHeader().getSms().getAttributes().add(attribute);

	    JSONObject headerInternetObj = header.getJSONObject("Internet");
	    cin.getHeader().getInternet().setTitle(headerInternetObj.getString("internetLabel"));
	    cin.getHeader().getInternet().setTitleValue(headerInternetObj.getString("internetValue"));

	    // cin.getHeader().getInternet().setIconName(headerInternetObj.getString("internetIcon"));
	    // Temporarily Fix Hardcode icon for all CIN tariffs.
	    cin.getHeader().getInternet().setIconName("countrywideInternet");

	    cin.getHeader().getInternet().setSubTitle(headerInternetObj.getString("internetDownloadAndUploadLabel"));
	    cin.getHeader().getInternet()
		    .setSubTitleValue(headerInternetObj.getString("internetDownloadAndUploadValue"));

	    // ------------------ Headers section ends ------------------//

	    // ------------------ Details section starts ------------------ //
	    JSONObject detailsObj = cinArrayList.getJSONObject(i).getJSONObject("details");
	    TariffDetailsSection detailSection = new TariffDetailsSection();
	    detailSection = parseDetailSection(msisdn, detailsObj, detailSection);
	    cin.setDetails(detailSection);
	    // ------------------ Details section ends ------------------//

	    // Description sections starts
	    JSONObject descriptionObj = cinArrayList.getJSONObject(i).getJSONObject("description");
	    cin.getDescription().setDescLabel(descriptionObj.getString("descriptionSectionLabel"));

	    cin.getDescription().setAdvantages(parseDescriptionAdvantages(descriptionObj));

	    cin.getDescription().setClassification(parseDescriptionClassification(descriptionObj));

	    cinDataList.add(cin);
	    // --------- ------ Description section ends---------------------//

	} // for loop ends.

	resData.getPrepaid().setCin(cinDataList);

	return resData;
    }

    // Parse KLASS Section
    private static TariffResponseData parseKlassData(String msisdn, JSONArray klassArrayList,
	    TariffResponseData resData) throws JSONException, JsonParseException, JsonMappingException, IOException {
	Utilities.printDebugLog(msisdn + "-KLASS Array List is going to be parsed." + klassArrayList, logger);
	List<Klass> klassDataList = new ArrayList<>();
	for (int i = 0; i < klassArrayList.length(); i++) {
	    Klass klass = new Klass();

	    List<KlassHeaderAttributes> klassHeaderAttributesList = new ArrayList<>();

	    // Header section starts
	    JSONObject header = klassArrayList.getJSONObject(i).getJSONObject("header");
	    klass.getHeader().setId(header.getString("id"));
	    klass.getHeader().setName(header.getString("name"));
	    klass.getHeader().setPriceLabel(header.getString("mrcLabel"));
	    klass.getHeader().setPriceValue(header.getString("mrcValue"));
	    klass.getHeader().setCurrency(header.getString("currency"));
	    klass.getHeader().setOfferingId(header.getString("offeringId"));
	    klass.getHeader().setTariffType("klass");
	    klass.getHeader().setSortOrder(header.getInt("sortOrder"));
	    /*
	     * If tariff does not has any offering id then activation button will be
	     * disabled.
	     */

	    if (Utilities.isOfferingIdExists(klass.getHeader().getOfferingId())) {
		klass.getHeader().setSubscribable(klassArrayList.getJSONObject(i).getString("subscribable"));
	    } else {
		klass.getHeader().setSubscribable(Constants.SUPPLEMENTARY_TARIFF_DEFAULT_DISABLE_BUTTON);
	    }

	    // Header Call Attribute List
	    JSONObject headerCall = header.getJSONObject("Call");
	    KlassHeaderAttributes klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerCall.getString("callLabel"));
	    klassHeaderAttributes.setValue(headerCall.getString("callValue"));
	    klassHeaderAttributes.setIconName(headerCall.getString("callIcon"));
	    klassHeaderAttributes.setMetrics(headerCall.getString("callMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    // Header SMS Attribute List
	    JSONObject headerSMS = header.getJSONObject("SMS");
	    klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerSMS.getString("smsLabel"));
	    klassHeaderAttributes.setValue(headerSMS.getString("smsValue"));
	    klassHeaderAttributes.setIconName(headerSMS.getString("smsIcon"));
	    klassHeaderAttributes.setMetrics(headerSMS.getString("smsMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    // Header Internet Attribute List
	    JSONObject headerInternet = header.getJSONObject("Internet");
	    klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerInternet.getString("internetLabel"));
	    klassHeaderAttributes.setValue(headerInternet.getString("internetValue"));
	    klassHeaderAttributes.setIconName(headerInternet.getString("internetIcon"));
	    klassHeaderAttributes.setMetrics(headerInternet.getString("internetMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    // Header Country Wide Attribute List
	    JSONObject headerCountryWide = header.getJSONObject("countryWide");
	    klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerCountryWide.getString("countryWideLabel"));
	    klassHeaderAttributes.setValue(headerCountryWide.getString("countryWideValue"));
	    klassHeaderAttributes.setIconName(headerCountryWide.getString("countryWideIcon"));
	    klassHeaderAttributes.setMetrics(headerCountryWide.getString("countryWideMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    // Header Whatsapp Attribute List
	    JSONObject headerWhatsapp = header.getJSONObject("Whatsapp");
	    klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerWhatsapp.getString("whatsappLabel"));
	    klassHeaderAttributes.setValue(headerWhatsapp.getString("whatsappValue"));
	    klassHeaderAttributes.setIconName(headerWhatsapp.getString("whatsappIcon"));
	    klassHeaderAttributes.setMetrics(headerWhatsapp.getString("whatsappMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    // Same structure and keys of bonuses objects parsing in a single
	    // method.
	    KlassHeaderAttributes klassHeaderAttributesBonus = parseBonusesObjects(header.getJSONObject("bonusSix"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributesBonus.getTitle(), klassHeaderAttributesBonus.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributesBonus);

	    klassHeaderAttributesBonus = parseBonusesObjects(header.getJSONObject("bonusSeven"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributesBonus.getTitle(), klassHeaderAttributesBonus.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributesBonus);

	    klassHeaderAttributesBonus = parseBonusesObjects(header.getJSONObject("bonusEight"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributesBonus.getTitle(), klassHeaderAttributesBonus.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributesBonus);

	    klass.getHeader().setAttributes(klassHeaderAttributesList);
	    // -------------------- Headers section ends --------------------

	    // Package Price Section starts
	    JSONObject packagePrice = klassArrayList.getJSONObject(i).getJSONObject("packagePrices");
	    logger.info("SMS Icon Pakcage Prices: " + packagePrice.toString());

	    klass.getPackagePrice().setPackagePriceLabel(packagePrice.getString("packagePricesSectionLabel"));

	    // Package Price Call
	    List<GenericAttributes> genericAttributesList = new ArrayList<>();

	    // Parsing KLASS CALL response in a separate method.
	    klass.getPackagePrice().setCall(parseHeaderCallSection(packagePrice.getJSONObject("Call")));

	    // Package Price SMS
	    genericAttributesList = new ArrayList<>();
	    JSONObject smsObj = packagePrice.getJSONObject("SMS");
	    klass.getPackagePrice().getSms().setTitle(smsObj.getString("smsLabel"));
	    klass.getPackagePrice().getSms().setTitleValue(smsObj.getString("smsValue"));
	    klass.getPackagePrice().getSms().setIconName(smsObj.getString("smsIcon"));
	    logger.info("SMS Icon: " + smsObj.getString("smsIcon") + " ID: "
		    + klassArrayList.getJSONObject(i).getJSONObject("header").getString("id"));

	    GenericAttributes attribute = new GenericAttributes();
	    attribute.setTitle(smsObj.getString("smsCountryWideLabel"));
	    attribute.setValue(smsObj.getString("smsCountryWideValue"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		genericAttributesList.add(attribute);

	    attribute = new GenericAttributes();
	    attribute.setTitle(smsObj.getString("smsInternationalLabel"));
	    attribute.setValue(smsObj.getString("smsInternationalValue"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		genericAttributesList.add(attribute);

	    klass.getPackagePrice().getSms().setAttributes(genericAttributesList);

	    // Package Price Internet
	    JSONObject internetObj = packagePrice.getJSONObject("Internet");
	    klass.getPackagePrice().getInternet().setTitle(internetObj.getString("internetLabel"));
	    klass.getPackagePrice().getInternet().setTitleValue(internetObj.getString("internetValue"));
	    klass.getPackagePrice().getInternet().setIconName(internetObj.getString("internetIcon"));
	    klass.getPackagePrice().getInternet().setSubTitle(internetObj.getString("internetDownloadAndUploadLabel"));
	    klass.getPackagePrice().getInternet()
		    .setSubTitleValue(internetObj.getString("internetDownloadAndUploadValue"));
	    // ----- ----------- Package Price Section ends -----------------

	    // Payg Section starts
	    JSONObject payg = klassArrayList.getJSONObject(i).getJSONObject("payg");

	    klass.getPaygPrice().setPaygPriceLabel(payg.getString("paygSectionLabel"));

	    // Parsing Payg CALL response in a separate method.
	    klass.getPaygPrice().setCall(parseHeaderCallSection(payg.getJSONObject("Call")));

	    // Payg SMS
	    genericAttributesList = new ArrayList<>();
	    smsObj = payg.getJSONObject("SMS");
	    klass.getPaygPrice().getSms().setTitle(smsObj.getString("smsLabel"));
	    klass.getPaygPrice().getSms().setTitleValue(smsObj.getString("smsValue"));
	    klass.getPaygPrice().getSms().setIconName(smsObj.getString("smsIcon"));

	    attribute = new GenericAttributes();
	    attribute.setTitle(smsObj.getString("smsCountryWideLabel"));
	    attribute.setValue(smsObj.getString("smsCountryWideValue"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		genericAttributesList.add(attribute);

	    attribute = new GenericAttributes();
	    attribute.setTitle(smsObj.getString("smsInternationalLabel"));
	    attribute.setValue(smsObj.getString("smsInternationalValue"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		genericAttributesList.add(attribute);

	    klass.getPaygPrice().getSms().setAttributes(genericAttributesList);

	    // Payg Internet
	    internetObj = payg.getJSONObject("Internet");
	    klass.getPaygPrice().getInternet().setTitle(internetObj.getString("internetLabel"));
	    klass.getPaygPrice().getInternet().setTitleValue(internetObj.getString("internetValue"));
	    klass.getPaygPrice().getInternet().setIconName(internetObj.getString("internetIcon"));
	    klass.getPaygPrice().getInternet().setSubTitle(internetObj.getString("internetDownloadAndUploadLabel"));
	    klass.getPaygPrice().getInternet()
		    .setSubTitleValue(internetObj.getString("internetDownloadAndUploadValue"));
	    // ---------------- Payg Section ends -----------------

	    // ---------------- Details section starts -----------------

	    JSONObject detailsObj = klassArrayList.getJSONObject(i).getJSONObject("details");
	    TariffDetailsSection detailSection = new TariffDetailsSection();
	    klass.setDetails(parseDetailSection(msisdn, detailsObj, detailSection));

	    // -------------- Details Section ends -----------------

	    klassDataList.add(klass);
	}
	resData.getPrepaid().setKlass(klassDataList);

	return resData;
    }

    // Parse CIn_new. as it will have the same data as Klass so we will use the same
    private static TariffResponseData parseCinNewData(String msisdn, JSONArray klassArrayList,
	    TariffResponseData resData) throws JSONException, JsonParseException, JsonMappingException, IOException {
	Utilities.printDebugLog(msisdn + "-KLASS Array List is going to be parsed." + klassArrayList, logger);
	List<Klass> klassDataList = new ArrayList<>();
	for (int i = 0; i < klassArrayList.length(); i++) {
	    Klass klass = new Klass();

	    List<KlassHeaderAttributes> klassHeaderAttributesList = new ArrayList<>();

	    // Header section starts
	    JSONObject header = klassArrayList.getJSONObject(i).getJSONObject("header");
	    klass.getHeader().setId(header.getString("id"));
	    klass.getHeader().setName(header.getString("name"));
	    klass.getHeader().setPriceLabel(header.getString("mrcLabel"));
	    klass.getHeader().setPriceValue(header.getString("mrcValue"));
	    klass.getHeader().setCurrency(header.getString("currency"));
	    klass.getHeader().setOfferingId(header.getString("offeringId"));
	    klass.getHeader().setTariffType("cinNew");
	    klass.getHeader().setSortOrder(header.getInt("sortOrder"));
	    /*
	     * If tariff does not has any offering id then activation button will be
	     * disabled.
	     */

	    if (Utilities.isOfferingIdExists(klass.getHeader().getOfferingId())) {
		klass.getHeader().setSubscribable(klassArrayList.getJSONObject(i).getString("subscribable"));
	    } else {
		klass.getHeader().setSubscribable(Constants.SUPPLEMENTARY_TARIFF_DEFAULT_DISABLE_BUTTON);
	    }

	    // Header Call Attribute List
	    JSONObject headerCall = header.getJSONObject("Call");
	    KlassHeaderAttributes klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerCall.getString("callLabel"));
	    klassHeaderAttributes.setValue(headerCall.getString("callValue"));
	    klassHeaderAttributes.setIconName(headerCall.getString("callIcon"));
	    klassHeaderAttributes.setMetrics(headerCall.getString("callMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    // Header SMS Attribute List
	    JSONObject headerSMS = header.getJSONObject("SMS");
	    klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerSMS.getString("smsLabel"));
	    klassHeaderAttributes.setValue(headerSMS.getString("smsValue"));
	    klassHeaderAttributes.setIconName(headerSMS.getString("smsIcon"));
	    klassHeaderAttributes.setMetrics(headerSMS.getString("smsMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    // Header Internet Attribute List
	    JSONObject headerInternet = header.getJSONObject("Internet");
	    klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerInternet.getString("internetLabel"));
	    klassHeaderAttributes.setValue(headerInternet.getString("internetValue"));
	    klassHeaderAttributes.setIconName(headerInternet.getString("internetIcon"));
	    klassHeaderAttributes.setMetrics(headerInternet.getString("internetMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    // Header Country Wide Attribute List
	    JSONObject headerCountryWide = header.getJSONObject("countryWide");
	    klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerCountryWide.getString("countryWideLabel"));
	    klassHeaderAttributes.setValue(headerCountryWide.getString("countryWideValue"));
	    klassHeaderAttributes.setIconName(headerCountryWide.getString("countryWideIcon"));
	    klassHeaderAttributes.setMetrics(headerCountryWide.getString("countryWideMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    // Header Whatsapp Attribute List
	    JSONObject headerWhatsapp = header.getJSONObject("Whatsapp");
	    klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerWhatsapp.getString("whatsappLabel"));
	    klassHeaderAttributes.setValue(headerWhatsapp.getString("whatsappValue"));
	    klassHeaderAttributes.setIconName(headerWhatsapp.getString("whatsappIcon"));
	    klassHeaderAttributes.setMetrics(headerWhatsapp.getString("whatsappMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    // Same structure and keys of bonuses objects parsing in a single
	    // method.
	    KlassHeaderAttributes klassHeaderAttributesBonus = parseBonusesObjects(header.getJSONObject("bonusSix"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributesBonus.getTitle(), klassHeaderAttributesBonus.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributesBonus);

	    klassHeaderAttributesBonus = parseBonusesObjects(header.getJSONObject("bonusSeven"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributesBonus.getTitle(), klassHeaderAttributesBonus.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributesBonus);

	    klassHeaderAttributesBonus = parseBonusesObjects(header.getJSONObject("bonusEight"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributesBonus.getTitle(), klassHeaderAttributesBonus.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributesBonus);

	    klass.getHeader().setAttributes(klassHeaderAttributesList);
	    // -------------------- Headers section ends --------------------

	    // Package Price Section starts
	    JSONObject packagePrice = klassArrayList.getJSONObject(i).getJSONObject("packagePrices");
	    logger.info("SMS Icon Pakcage Prices: " + packagePrice.toString());

	    klass.getPackagePrice().setPackagePriceLabel(packagePrice.getString("packagePricesSectionLabel"));

	    // Package Price Call
	    List<GenericAttributes> genericAttributesList = new ArrayList<>();

	    // Parsing KLASS CALL response in a separate method.
	    klass.getPackagePrice().setCall(parseHeaderCallSection(packagePrice.getJSONObject("Call")));

	    // Package Price SMS
	    genericAttributesList = new ArrayList<>();
	    JSONObject smsObj = packagePrice.getJSONObject("SMS");
	    klass.getPackagePrice().getSms().setTitle(smsObj.getString("smsLabel"));
	    klass.getPackagePrice().getSms().setTitleValue(smsObj.getString("smsValue"));
	    klass.getPackagePrice().getSms().setIconName(smsObj.getString("smsIcon"));
	    logger.info("SMS Icon: " + smsObj.getString("smsIcon") + " ID: "
		    + klassArrayList.getJSONObject(i).getJSONObject("header").getString("id"));

	    GenericAttributes attribute = new GenericAttributes();
	    attribute.setTitle(smsObj.getString("smsCountryWideLabel"));
	    attribute.setValue(smsObj.getString("smsCountryWideValue"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		genericAttributesList.add(attribute);

	    attribute = new GenericAttributes();
	    attribute.setTitle(smsObj.getString("smsInternationalLabel"));
	    attribute.setValue(smsObj.getString("smsInternationalValue"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		genericAttributesList.add(attribute);

	    klass.getPackagePrice().getSms().setAttributes(genericAttributesList);

	    // Package Price Internet
	    JSONObject internetObj = packagePrice.getJSONObject("Internet");
	    klass.getPackagePrice().getInternet().setTitle(internetObj.getString("internetLabel"));
	    klass.getPackagePrice().getInternet().setTitleValue(internetObj.getString("internetValue"));
	    klass.getPackagePrice().getInternet().setIconName(internetObj.getString("internetIcon"));
	    klass.getPackagePrice().getInternet().setSubTitle(internetObj.getString("internetDownloadAndUploadLabel"));
	    klass.getPackagePrice().getInternet()
		    .setSubTitleValue(internetObj.getString("internetDownloadAndUploadValue"));
	    // ----- ----------- Package Price Section ends -----------------

	    // Payg Section starts
	    JSONObject payg = klassArrayList.getJSONObject(i).getJSONObject("payg");

	    klass.getPaygPrice().setPaygPriceLabel(payg.getString("paygSectionLabel"));

	    // Parsing Payg CALL response in a separate method.
	    klass.getPaygPrice().setCall(parseHeaderCallSection(payg.getJSONObject("Call")));

	    // Payg SMS
	    genericAttributesList = new ArrayList<>();
	    smsObj = payg.getJSONObject("SMS");
	    klass.getPaygPrice().getSms().setTitle(smsObj.getString("smsLabel"));
	    klass.getPaygPrice().getSms().setTitleValue(smsObj.getString("smsValue"));
	    klass.getPaygPrice().getSms().setIconName(smsObj.getString("smsIcon"));

	    attribute = new GenericAttributes();
	    attribute.setTitle(smsObj.getString("smsCountryWideLabel"));
	    attribute.setValue(smsObj.getString("smsCountryWideValue"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		genericAttributesList.add(attribute);

	    attribute = new GenericAttributes();
	    attribute.setTitle(smsObj.getString("smsInternationalLabel"));
	    attribute.setValue(smsObj.getString("smsInternationalValue"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		genericAttributesList.add(attribute);

	    klass.getPaygPrice().getSms().setAttributes(genericAttributesList);

	    // Payg Internet
	    internetObj = payg.getJSONObject("Internet");
	    klass.getPaygPrice().getInternet().setTitle(internetObj.getString("internetLabel"));
	    klass.getPaygPrice().getInternet().setTitleValue(internetObj.getString("internetValue"));
	    klass.getPaygPrice().getInternet().setIconName(internetObj.getString("internetIcon"));
	    klass.getPaygPrice().getInternet().setSubTitle(internetObj.getString("internetDownloadAndUploadLabel"));
	    klass.getPaygPrice().getInternet()
		    .setSubTitleValue(internetObj.getString("internetDownloadAndUploadValue"));
	    // ---------------- Payg Section ends -----------------

	    // ---------------- Details section starts -----------------

	    JSONObject detailsObj = klassArrayList.getJSONObject(i).getJSONObject("details");
	    TariffDetailsSection detailSection = new TariffDetailsSection();
	    klass.setDetails(parseDetailSection(msisdn, detailsObj, detailSection));

	    // -------------- Details Section ends -----------------

	    klassDataList.add(klass);
	}
	resData.getPrepaid().setCin_new(klassDataList);

	return resData;
    }

    private static KlassHeaderAttributes parseBonusesObjects(JSONObject headerObj) throws JSONException {
	KlassHeaderAttributes klassHeaderAttributes = new KlassHeaderAttributes();
	klassHeaderAttributes.setTitle(headerObj.getString("label"));
	klassHeaderAttributes.setValue(headerObj.getString("value"));
	klassHeaderAttributes.setIconName(headerObj.getString("icon"));
	klassHeaderAttributes.setMetrics(headerObj.getString("metrics"));
	return klassHeaderAttributes;
    }

    private static HeaderAttribute parseBonusesObjectsCorporate(JSONObject headerObj) throws JSONException {
	HeaderAttribute headerAttributes = new HeaderAttribute();
	headerAttributes.setTitle(headerObj.getString("label"));
	headerAttributes.setValue(headerObj.getString("value"));
	headerAttributes.setIconName(headerObj.getString("icon"));
	headerAttributes.setMetrics(headerObj.getString("metrics"));
	return headerAttributes;
    }

    // Parsing Business Corporate Section
    private static TariffResponseData parseCorporateData(String msisdn, JSONArray corporateArrayList,
	    TariffResponseData resData) throws JSONException, IOException {
	Utilities.printDebugLog(msisdn + "-Corporate Array List is going to be parsed." + corporateArrayList, logger);
	List<Corporate> corportateDataList = new ArrayList<>();
	for (int i = 0; i < corporateArrayList.length(); i++) {
	    Corporate corporate = new Corporate();

	    // Header section starts
	    List<HeaderAttribute> attributesList = new ArrayList<>();
	    JSONObject header = corporateArrayList.getJSONObject(i).getJSONObject("header");

	    corporate.getHeader().setId(header.getString("id"));
	    corporate.getHeader().setName(header.getString("name"));
	    corporate.getHeader().setPriceLabel(header.getString("mrcLabel"));
	    corporate.getHeader().setPriceValue(header.getString("mrcValue"));
	    corporate.getHeader().setOfferingId(header.getString("offeringId"));

	    /*
	     * If tariff does not has any offering id then activation button will be
	     * disabled.
	     */

	    if (Utilities.isOfferingIdExists(corporate.getHeader().getOfferingId())) {
		corporate.getHeader().setSubscribable(corporateArrayList.getJSONObject(i).getString("subscribable"));
	    } else {
		corporate.getHeader().setSubscribable(Constants.SUPPLEMENTARY_TARIFF_DEFAULT_DISABLE_BUTTON);
	    }

	    HeaderAttribute attribute = new HeaderAttribute();
	    attribute.setTitle(header.getJSONObject("Call").getString("callLabel"));
	    attribute.setValue(header.getJSONObject("Call").getString("callValue"));
	    attribute.setIconName(header.getJSONObject("Call").getString("callIcon"));
	    attribute.setMetrics(header.getJSONObject("Call").getString("callMetrics"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    attribute = new HeaderAttribute();
	    attribute.setTitle(header.getJSONObject("SMS").getString("smsLabel"));
	    attribute.setValue(header.getJSONObject("SMS").getString("smsValue"));
	    attribute.setIconName(header.getJSONObject("SMS").getString("smsIcon"));
	    attribute.setMetrics(header.getJSONObject("SMS").getString("smsMetrics"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    attribute = new HeaderAttribute();
	    attribute.setTitle(header.getJSONObject("Internet").getString("internetLabel"));
	    attribute.setValue(header.getJSONObject("Internet").getString("internetValue"));
	    attribute.setIconName(header.getJSONObject("Internet").getString("internetIcon"));
	    attribute.setMetrics(header.getJSONObject("Internet").getString("internetMetrics"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    attribute = new HeaderAttribute();
	    attribute.setTitle(header.getJSONObject("countryWide").getString("countryWideLabel"));
	    attribute.setValue(header.getJSONObject("countryWide").getString("countryWideValue"));
	    attribute.setIconName(header.getJSONObject("countryWide").getString("countryWideIcon"));
	    attribute.setIconName(header.getJSONObject("countryWide").getString("countryWideMetrics"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    attribute = new HeaderAttribute();
	    attribute.setTitle(header.getJSONObject("Whatsapp").getString("whatsappLabel"));
	    attribute.setValue(header.getJSONObject("Whatsapp").getString("whatsappValue"));
	    attribute.setIconName(header.getJSONObject("Whatsapp").getString("whatsappIcon"));
	    attribute.setIconName(header.getJSONObject("Whatsapp").getString("whatsappMetrics"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    // Same structure and keys of bonuses objects parsing in a single
	    // method.
	    attribute = new HeaderAttribute();
	    attribute = parseBonusesObjectsCorporate(header.getJSONObject("bonusSix"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    attribute = new HeaderAttribute();
	    attribute = parseBonusesObjectsCorporate(header.getJSONObject("bonusSeven"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    attribute = new HeaderAttribute();
	    attribute = parseBonusesObjectsCorporate(header.getJSONObject("bonusEight"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    corporate.getHeader().setAttributes(attributesList);
	    // ------------------- Header Section ends------------------

	    // Corporate Price section starts
	    List<GenericAttributes> genericAttributesList = new ArrayList<>();
	    JSONObject corporatePrice = corporateArrayList.getJSONObject(i).getJSONObject("prices");
	    corporate.getPrice().setPriceLabel(corporatePrice.getString("pricesSectionLabel"));

	    // Prices Call
	    corporate.getPrice().setCall(parseHeaderCallSection(corporatePrice.getJSONObject("Call")));

	    // Prices SMS
	    CorporateSMS corporateSMS = new CorporateSMS();
	    JSONObject smsObj = corporatePrice.getJSONObject("SMS");

	    corporateSMS.setTitle(smsObj.getString("smsLabel"));
	    corporateSMS.setTitleValue(smsObj.getString("smsValue"));
	    corporateSMS.setIconName(smsObj.getString("smsIcon"));

	    GenericAttributes genericAttribute = new GenericAttributes();
	    genericAttribute.setTitle(smsObj.getString("smsCountryWideLabel"));
	    genericAttribute.setValue(smsObj.getString("smsCountryWideValue"));
	    if (Utilities.ifNotEmpty(genericAttribute.getTitle(), genericAttribute.getValue()))
		genericAttributesList.add(genericAttribute);

	    genericAttribute = new GenericAttributes();
	    genericAttribute.setTitle(smsObj.getString("smsInternationalLabel"));
	    genericAttribute.setValue(smsObj.getString("smsInternationalValue"));
	    if (Utilities.ifNotEmpty(genericAttribute.getTitle(), genericAttribute.getValue()))
		genericAttributesList.add(genericAttribute);

	    corporateSMS.setAttributes(genericAttributesList);
	    corporate.getPrice().setSms(corporateSMS);

	    // Prices Internet
	    CorporateInternet corporateInternet = new CorporateInternet();
	    JSONObject internetObj = corporatePrice.getJSONObject("Internet");

	    corporateInternet.setTitle(internetObj.getString("internetLabel"));
	    corporateInternet.setTitleValue(internetObj.getString("internetValue"));
	    corporateInternet.setIconName(internetObj.getString("internetIcon"));
	    corporateInternet.setSubTitle(internetObj.getString("internetDownloadAndUploadLabel"));
	    corporateInternet.setSubTitleValue(internetObj.getString("internetDownloadAndUploadValue"));

	    corporate.getPrice().setInternet(corporateInternet);
	    // ----------------- Prices Section ends--------------------------

	    // Corporate Details section starts
	    JSONObject corporateDetails = corporateArrayList.getJSONObject(i).getJSONObject("details");
	    corporate.getDetails().setDetailLabel(corporateDetails.getString("detailSectionLabel"));
	    TariffDetailsSection detailsSection = new TariffDetailsSection();

	    corporate.setDetails(parseDetailSection(msisdn, corporateDetails, detailsSection));

	    // Corporate Details Destination
	    List<DetailsAttributes> destinationAttributesList = new ArrayList<>();
	    JSONObject destination = corporateDetails.getJSONObject("destination");

	    corporate.getDetails().getRounding().setTitle(destination.getString("destinationLabel"));
	    corporate.getDetails().getRounding().setValue(destination.getString("destinationValue"));
	    corporate.getDetails().getRounding().setIconName(destination.getString("destinationIcon"));

	    DetailsAttributes destinationAttributes = new DetailsAttributes();
	    destinationAttributes.setTitle(destination.getString("destinationOnnetLabel"));
	    destinationAttributes.setValue(destination.getString("destinationOnnetValue"));
	    if (Utilities.ifNotEmpty(destinationAttributes.getTitle(), destinationAttributes.getValue()))
		destinationAttributesList.add(destinationAttributes);

	    destinationAttributes = new DetailsAttributes();
	    destinationAttributes.setTitle(destination.getString("destinationOffnetLabel"));
	    destinationAttributes.setValue(destination.getString("destinationOffnetValue"));
	    if (Utilities.ifNotEmpty(destinationAttributes.getTitle(), destinationAttributes.getValue()))
		destinationAttributesList.add(destinationAttributes);

	    destinationAttributes = new DetailsAttributes();
	    destinationAttributes.setTitle(destination.getString("destinationInternationalLabel"));
	    destinationAttributes.setValue(destination.getString("destinationInternationalValue"));
	    if (Utilities.ifNotEmpty(destinationAttributes.getTitle(), destinationAttributes.getValue()))
		destinationAttributesList.add(destinationAttributes);

	    destinationAttributes = new DetailsAttributes();
	    destinationAttributes.setTitle(destination.getString("destinationInternetLabel"));
	    destinationAttributes.setValue(destination.getString("destinationInternetValue"));
	    if (Utilities.ifNotEmpty(destinationAttributes.getTitle(), destinationAttributes.getValue()))
		destinationAttributesList.add(destinationAttributes);

	    corporate.getDetails().getRounding().setAttributeList(destinationAttributesList);
	    // --------------- Corporate Details section ends-------------------

	    // Corporate Description sections starts
	    JSONObject corporateDescription = corporateArrayList.getJSONObject(i).getJSONObject("description");
	    corporate.getDescription().setDescLabel(corporateDescription.getString("descriptionSectionLabel"));

	    // Description Advantages
	    corporate.getDescription().setAdvantages(parseDescriptionAdvantages(corporateDescription));
	    // Description Classification
	    corporate.getDescription().setClassification(parseDescriptionClassification(corporateDescription));

	    // If description label is empty then object will be null.
	    if (corporate.getDescription().getDescLabel().isEmpty())
		corporate.setDescription(null);

	    corportateDataList.add(corporate);
	}

	resData.getPostpaid().setCorporate(corportateDataList);
	return resData;
    }

    // Parsing Business Individual Section
    private static TariffResponseData parseIndividualData(String msisdn, JSONArray individualArrayList,
	    TariffResponseData resData) throws JSONException, JsonParseException, JsonMappingException, IOException {
	Utilities.printDebugLog(msisdn + "-Individual Array List is going to be parsed." + individualArrayList, logger);
	List<Individual> individualDataList = new ArrayList<>();
	for (int i = 0; i < individualArrayList.length(); i++) {
	    Individual individual = new Individual();

	    // Header section starts
	    List<HeaderAttribute> attributesList = new ArrayList<>();
	    JSONObject header = individualArrayList.getJSONObject(i).getJSONObject("header");

	    individual.getHeader().setId(header.getString("id"));
	    individual.getHeader().setName(header.getString("name"));
	    individual.getHeader().setPriceLabel(header.getString("mrcLabel"));
	    individual.getHeader().setPriceValue(header.getString("mrcValue"));
	    individual.getHeader().setOfferingId(header.getString("offeringId"));

	    /*
	     * If tariff does not has any offering id then activation button will be
	     * disabled.
	     */

	    if (Utilities.isOfferingIdExists(individual.getHeader().getOfferingId())) {
		individual.getHeader().setSubscribable(individualArrayList.getJSONObject(i).getString("subscribable"));
	    } else {
		individual.getHeader().setSubscribable(Constants.SUPPLEMENTARY_TARIFF_DEFAULT_DISABLE_BUTTON);
	    }

	    HeaderAttribute attribute = new HeaderAttribute();
	    attribute.setTitle(header.getJSONObject("Call").getString("callLabel"));
	    attribute.setValue(header.getJSONObject("Call").getString("callValue"));
	    attribute.setIconName(header.getJSONObject("Call").getString("callIcon"));
	    attribute.setMetrics(header.getJSONObject("Call").getString("callMetrics"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    attribute = new HeaderAttribute();
	    attribute.setTitle(header.getJSONObject("SMS").getString("smsLabel"));
	    attribute.setValue(header.getJSONObject("SMS").getString("smsValue"));
	    attribute.setIconName(header.getJSONObject("SMS").getString("smsIcon"));
	    attribute.setMetrics(header.getJSONObject("SMS").getString("smsMetrics"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    attribute = new HeaderAttribute();
	    attribute.setTitle(header.getJSONObject("Internet").getString("internetLabel"));
	    attribute.setValue(header.getJSONObject("Internet").getString("internetValue"));
	    attribute.setIconName(header.getJSONObject("Internet").getString("internetIcon"));
	    attribute.setMetrics(header.getJSONObject("Internet").getString("internetMetrics"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    attribute = new HeaderAttribute();
	    attribute.setTitle(header.getJSONObject("countryWide").getString("countryWideLabel"));
	    attribute.setValue(header.getJSONObject("countryWide").getString("countryWideValue"));
	    attribute.setIconName(header.getJSONObject("countryWide").getString("countryWideIcon"));
	    attribute.setIconName(header.getJSONObject("countryWide").getString("countryWideMetrics"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    attribute = new HeaderAttribute();
	    attribute.setTitle(header.getJSONObject("Whatsapp").getString("whatsappLabel"));
	    attribute.setValue(header.getJSONObject("Whatsapp").getString("whatsappValue"));
	    attribute.setIconName(header.getJSONObject("Whatsapp").getString("whatsappIcon"));
	    attribute.setIconName(header.getJSONObject("Whatsapp").getString("whatsappMetrics"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    // Same structure and keys of bonuses objects parsing in a single
	    // method.
	    attribute = new HeaderAttribute();
	    attribute = parseBonusesObjectsCorporate(header.getJSONObject("bonusSix"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    attribute = new HeaderAttribute();
	    attribute = parseBonusesObjectsCorporate(header.getJSONObject("bonusSeven"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    attribute = new HeaderAttribute();
	    attribute = parseBonusesObjectsCorporate(header.getJSONObject("bonusEight"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		attributesList.add(attribute);

	    individual.getHeader().setAttributes(attributesList);
	    // ------------------- Header Section ends------------------

	    // Corporate Price section starts
	    List<GenericAttributes> genericAttributesList = new ArrayList<>();
	    JSONObject indididualPrice = individualArrayList.getJSONObject(i).getJSONObject("prices");
	    individual.getPrice().setPriceLabel(indididualPrice.getString("pricesSectionLabel"));

	    // Prices Call
	    individual.getPrice().setCall(parseHeaderCallSection(indididualPrice.getJSONObject("Call")));

	    // Prices SMS
	    CorporateSMS corporateSMS = new CorporateSMS();
	    JSONObject smsObj = indididualPrice.getJSONObject("SMS");

	    corporateSMS.setTitle(smsObj.getString("smsLabel"));
	    corporateSMS.setTitleValue(smsObj.getString("smsValue"));
	    corporateSMS.setIconName(smsObj.getString("smsIcon"));

	    GenericAttributes genericAttribute = new GenericAttributes();
	    genericAttribute.setTitle(smsObj.getString("smsCountryWideLabel"));
	    genericAttribute.setValue(smsObj.getString("smsCountryWideValue"));
	    if (Utilities.ifNotEmpty(genericAttribute.getTitle(), genericAttribute.getValue()))
		genericAttributesList.add(genericAttribute);

	    genericAttribute = new GenericAttributes();
	    genericAttribute.setTitle(smsObj.getString("smsInternationalLabel"));
	    genericAttribute.setValue(smsObj.getString("smsInternationalValue"));
	    if (Utilities.ifNotEmpty(genericAttribute.getTitle(), genericAttribute.getValue()))
		genericAttributesList.add(genericAttribute);

	    corporateSMS.setAttributes(genericAttributesList);
	    individual.getPrice().setSms(corporateSMS);

	    // Prices Internet
	    CorporateInternet corporateInternet = new CorporateInternet();
	    JSONObject internetObj = indididualPrice.getJSONObject("Internet");

	    corporateInternet.setTitle(internetObj.getString("internetLabel"));
	    corporateInternet.setTitleValue(internetObj.getString("internetValue"));
	    corporateInternet.setIconName(internetObj.getString("internetIcon"));
	    corporateInternet.setSubTitle(internetObj.getString("internetDownloadAndUploadLabel"));
	    corporateInternet.setSubTitleValue(internetObj.getString("internetDownloadAndUploadValue"));

	    individual.getPrice().setInternet(corporateInternet);
	    // ----------------- Prices Section ends--------------------------

	    // ------------ Individual Details section starts ------------
	    JSONObject details = individualArrayList.getJSONObject(i).getJSONObject("details");

	    TariffDetailsSection detailsSection = new TariffDetailsSection();

	    individual.setDetails(parseDetailSection(msisdn, details, detailsSection));

	    // Individual Description starts
	    JSONObject description = individualArrayList.getJSONObject(i).getJSONObject("description");
	    individual.getDescription().setDescriptionTitle(description.getString("descriptionSectionLabel"));

	    individual.getDescription().setAdvantages(parseDescriptionAdvantages(description));

	    individual.getDescription().setClassification(parseDescriptionClassification(description));
	    // ----------------- Individual Description ends --------------

	    individualDataList.add(individual);
	}
	resData.getPostpaid().setIndividual(individualDataList);
	return resData;
    }

    // Parsing Business KLASS Section
    private static TariffResponseData parseKlassPostpaidData(String msisdn, JSONArray klassArrayList,
	    TariffResponseData resData) throws JSONException, IOException {
	Utilities.printDebugLog(msisdn + "-BUSINESS KLASS Array List is going to be parsed." + klassArrayList, logger);
	List<Klass> klassDataList = new ArrayList<>();
	for (int i = 0; i < klassArrayList.length(); i++) {
	    Klass klass = new Klass();

	    List<KlassHeaderAttributes> klassHeaderAttributesList = new ArrayList<>();

	    // Header section starts
	    JSONObject header = klassArrayList.getJSONObject(i).getJSONObject("header");
	    klass.getHeader().setId(header.getString("id"));
	    klass.getHeader().setName(header.getString("name"));
	    klass.getHeader().setPriceLabel(header.getString("mrcLabel"));
	    klass.getHeader().setPriceValue(header.getString("mrcValue"));
	    klass.getHeader().setCurrency(header.getString("currency"));
	    klass.getHeader().setOfferingId(header.getString("offeringId"));

	    /*
	     * If tariff does not has any offering id then activation button will be
	     * disabled.
	     */

	    if (Utilities.isOfferingIdExists(klass.getHeader().getOfferingId())) {
		klass.getHeader().setSubscribable(klassArrayList.getJSONObject(i).getString("subscribable"));
	    } else {
		klass.getHeader().setSubscribable(Constants.SUPPLEMENTARY_TARIFF_DEFAULT_DISABLE_BUTTON);
	    }

	    // Header Call Attribute List
	    JSONObject headerCall = header.getJSONObject("Call");
	    KlassHeaderAttributes klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerCall.getString("callLabel"));
	    klassHeaderAttributes.setValue(headerCall.getString("callValue"));
	    klassHeaderAttributes.setIconName(headerCall.getString("callIcon"));
	    klassHeaderAttributes.setMetrics(headerCall.getString("callMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    // Header SMS Attribute List
	    JSONObject headerSMS = header.getJSONObject("SMS");
	    klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerSMS.getString("smsLabel"));
	    klassHeaderAttributes.setValue(headerSMS.getString("smsValue"));
	    klassHeaderAttributes.setIconName(headerSMS.getString("smsIcon"));
	    klassHeaderAttributes.setMetrics(headerSMS.getString("smsMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    // Header Internet Attribute List
	    JSONObject headerInternet = header.getJSONObject("Internet");
	    klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerInternet.getString("internetLabel"));
	    klassHeaderAttributes.setValue(headerInternet.getString("internetValue"));
	    klassHeaderAttributes.setIconName(headerInternet.getString("internetIcon"));
	    klassHeaderAttributes.setMetrics(headerInternet.getString("internetMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    // Header Country Wide Attribute List
	    JSONObject headerCuntryWide = header.getJSONObject("countryWide");
	    klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerCuntryWide.getString("countryWideLabel"));
	    klassHeaderAttributes.setValue(headerCuntryWide.getString("countryWideValue"));
	    klassHeaderAttributes.setIconName(headerCuntryWide.getString("countryWideIcon"));
	    klassHeaderAttributes.setMetrics(headerCuntryWide.getString("countryWideMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    // Header Whatsapp Attribute List
	    JSONObject headerWhatsapp = header.getJSONObject("Whatsapp");
	    klassHeaderAttributes = new KlassHeaderAttributes();
	    klassHeaderAttributes.setTitle(headerWhatsapp.getString("whatsappLabel"));
	    klassHeaderAttributes.setValue(headerWhatsapp.getString("whatsappValue"));
	    klassHeaderAttributes.setIconName(headerWhatsapp.getString("whatsappIcon"));
	    klassHeaderAttributes.setMetrics(headerWhatsapp.getString("whatsappMetrics"));
	    if (Utilities.ifNotEmpty(klassHeaderAttributes.getTitle(), klassHeaderAttributes.getValue()))
		klassHeaderAttributesList.add(klassHeaderAttributes);

	    klass.getHeader().setAttributes(klassHeaderAttributesList);
	    // -------------------- Headers section ends --------------------

	    // Package Price Section starts
	    JSONObject packagePrice = klassArrayList.getJSONObject(i).getJSONObject("packagePrices");

	    klass.getPackagePrice().setPackagePriceLabel(packagePrice.getString("packagePricesSectionLabel"));

	    // Package Price Call
	    List<GenericAttributes> genericAttributesList = new ArrayList<>();

	    // Parsing KLASS CALL response in a separate method.
	    klass.getPackagePrice().setCall(parseHeaderCallSection(packagePrice.getJSONObject("Call")));

	    // Package Price SMS
	    genericAttributesList = new ArrayList<>();
	    JSONObject smsObj = packagePrice.getJSONObject("SMS");
	    klass.getPackagePrice().getSms().setTitle(smsObj.getString("smsLabel"));
	    klass.getPackagePrice().getSms().setTitleValue(smsObj.getString("smsValue"));
	    klass.getPackagePrice().getSms().setIconName(smsObj.getString("smsIcon"));

	    GenericAttributes attribute = new GenericAttributes();
	    attribute.setTitle(smsObj.getString("smsCountryWideLabel"));
	    attribute.setValue(smsObj.getString("smsCountryWideValue"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		genericAttributesList.add(attribute);

	    attribute = new GenericAttributes();
	    attribute.setTitle(smsObj.getString("smsInternationalLabel"));
	    attribute.setValue(smsObj.getString("smsInternationalValue"));
	    if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
		genericAttributesList.add(attribute);

	    klass.getPackagePrice().getSms().setAttributes(genericAttributesList);

	    // Package Price Internet
	    JSONObject internetObj = packagePrice.getJSONObject("Internet");

	    klass.getPackagePrice().getInternet().setTitle(internetObj.getString("internetLabel"));
	    klass.getPackagePrice().getInternet().setTitleValue(internetObj.getString("internetValue"));
	    klass.getPackagePrice().getInternet().setIconName(internetObj.getString("internetIcon"));
	    klass.getPackagePrice().getInternet().setSubTitle(internetObj.getString("internetDownloadAndUploadLabel"));
	    klass.getPackagePrice().getInternet()
		    .setSubTitleValue(internetObj.getString("internetDownloadAndUploadValue"));
	    if (!Utilities.ifNotEmpty(klass.getPackagePrice().getInternet().getTitle(),
		    klass.getPackagePrice().getInternet().getTitleValue())) {
		klass.getPackagePrice().setInternet(null);
	    }

	    // ------------- Package Price Section ends -----------------

	    // Klass Details section starts
	    JSONObject klassDetails = klassArrayList.getJSONObject(i).getJSONObject("details");
	    klass.getDetails().setDetailLabel(klassDetails.getString("detailSectionLabel"));
	    TariffDetailsSection detailsSection = new TariffDetailsSection();

	    klass.setDetails(parseDetailSection(msisdn, klassDetails, detailsSection));

	    klass.setPaygPrice(null);

	    klassDataList.add(klass);
	}
	resData.getPostpaid().setKlassPostpaid(klassDataList);

	return resData;
    }

    // Tariff Details Section Parsing
    private static TariffDetailsSection parseDetailSection(String msisdn, JSONObject detailsObj,
	    TariffDetailsSection detailsSection) throws JSONException, IOException {
	Utilities.printDebugLog(
		msisdn + "-Tariff Details- Parsing tariff details section with data-" + detailsObj.toString(), logger);
	detailsSection.setDetailLabel(detailsObj.getString("detailSectionLabel"));

	// ---------------- Details- Call Section ----------------

	// It is as same as of price in supplementary offerings.

	Price price = parseDetailsCallAndFragmentObjs(detailsObj.getJSONObject("Call"));
	if (Utilities.isPriceEmpty(price))
	    detailsSection.getPrice().add(price);
	price = parseDetailsCallAndFragmentObjs(detailsObj.getJSONObject("fragmentB"));
	if (Utilities.isPriceEmpty(price))
	    detailsSection.getPrice().add(price);
	price = parseDetailsCallAndFragmentObjs(detailsObj.getJSONObject("fragmentC"));
	if (Utilities.isPriceEmpty(price))
	    detailsSection.getPrice().add(price);

	price = parseDetailsCallAndFragmentObjs(detailsObj.getJSONObject("fragmentD"));
	if (Utilities.isPriceEmpty(price))
	    detailsSection.getPrice().add(price);

	// ---------------- Details- Destination Section ----------------
	// It is same as of rounding in supplementary offerings.
	JSONObject detailDestinationObj = detailsObj.getJSONObject("destination");

	detailsSection.getRounding().setTitle(detailDestinationObj.getString("destinationLabel"));
	detailsSection.getRounding().setValue(detailDestinationObj.getString("destinationValue"));
	detailsSection.getRounding().setIconName(detailDestinationObj.getString("destinationIcon"));

	DetailsAttributes attribute = new DetailsAttributes();
	attribute.setTitle(detailDestinationObj.getString("destinationInternationalLabel"));
	attribute.setValue(detailDestinationObj.getString("destinationInternationalValue"));
	if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
	    detailsSection.getRounding().getAttributeList().add(attribute);

	attribute = new DetailsAttributes();
	attribute.setTitle(detailDestinationObj.getString("destinationInternetLabel"));
	attribute.setValue(detailDestinationObj.getString("destinationInternetValue"));
	if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
	    detailsSection.getRounding().getAttributeList().add(attribute);

	attribute = new DetailsAttributes();
	attribute.setTitle(detailDestinationObj.getString("destinationOffnetLabel"));
	attribute.setValue(detailDestinationObj.getString("destinationOffnetValue"));
	if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
	    detailsSection.getRounding().getAttributeList().add(attribute);

	attribute = new DetailsAttributes();
	attribute.setTitle(detailDestinationObj.getString("destinationOnnetLabel"));
	attribute.setValue(detailDestinationObj.getString("destinationOnnetValue"));
	if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
	    detailsSection.getRounding().getAttributeList().add(attribute);

	// ----- Details- Title Sub Title And Description Section -----

	JSONObject detailTitleSubTitleObj = detailsObj.getJSONObject("titleSubtitle");
	detailsSection.getTitleSubTitleValueAndDesc().setTitle(detailTitleSubTitleObj.getString("titleLabel"));
	if (!detailsSection.getTitleSubTitleValueAndDesc().getTitle().isEmpty()) {

	    detailsSection.getTitleSubTitleValueAndDesc()
		    .setShortDesc(detailTitleSubTitleObj.getString("shortDescription"));

	    GenericAttributes attributeSubTitle = new GenericAttributes();
	    attributeSubTitle.setTitle(detailTitleSubTitleObj.getString("subtitle1Label"));
	    attributeSubTitle.setValue(detailTitleSubTitleObj.getString("subtitle1Value"));
	    if (Utilities.ifNotEmpty(attributeSubTitle.getTitle(), attributeSubTitle.getValue()))
		detailsSection.getTitleSubTitleValueAndDesc().getAttributesList().add(attributeSubTitle);

	    attributeSubTitle = new GenericAttributes();
	    attributeSubTitle.setTitle(detailTitleSubTitleObj.getString("subtitle2Label"));
	    attributeSubTitle.setValue(detailTitleSubTitleObj.getString("subtitle2Value"));
	    if (Utilities.ifNotEmpty(attributeSubTitle.getTitle(), attributeSubTitle.getValue()))
		detailsSection.getTitleSubTitleValueAndDesc().getAttributesList().add(attributeSubTitle);

	    attributeSubTitle = new GenericAttributes();
	    attributeSubTitle.setTitle(detailTitleSubTitleObj.getString("subtitle3Label"));
	    attributeSubTitle.setValue(detailTitleSubTitleObj.getString("subtitle3Value"));
	    if (Utilities.ifNotEmpty(attributeSubTitle.getTitle(), attributeSubTitle.getValue()))
		detailsSection.getTitleSubTitleValueAndDesc().getAttributesList().add(attributeSubTitle);

	    attributeSubTitle = new GenericAttributes();
	    attributeSubTitle.setTitle(detailTitleSubTitleObj.getString("subtitle4Label"));
	    attributeSubTitle.setValue(detailTitleSubTitleObj.getString("subtitle4Value"));
	    if (Utilities.ifNotEmpty(attributeSubTitle.getTitle(), attributeSubTitle.getValue()))
		detailsSection.getTitleSubTitleValueAndDesc().getAttributesList().add(attributeSubTitle);
	} else {
	    detailsSection.setTitleSubTitleValueAndDesc(null);
	}
	// ------------------ Details- Advantages Section ------------------
	JSONObject detailAdvantagesObj = detailsObj.getJSONObject("advantages");
	detailsSection = parseAdvantagesSection(detailAdvantagesObj, detailsSection);

	// ------------------ Details- Time Section ------------------
	detailsSection.setTime(ParseSupplementaryOfferingsResponse.prepareTimeObject(detailsObj.getJSONObject("time")));

	// ------------------ Details- Date Section ------------------
	detailsSection.setDate(ParseSupplementaryOfferingsResponse.prepareDateObject(detailsObj.getJSONObject("date")));

	// ------------------ Details- Free Resources Section ------------------
	detailsSection.setFreeResourceValidity(ParseSupplementaryOfferingsResponse
		.prepareFreeResourceValidity(detailsObj.getJSONObject("freeResource")));

	// ------------------ Details- Roaming Section ------------------
	detailsSection.setRoamingDetails(
		ParseSupplementaryOfferingsResponse.parseRoamingData(msisdn, detailsObj.getJSONObject("roaming")));

	return detailsSection;
    }

    private static Price parseDetailsCallAndFragmentObjs(JSONObject detailCallObj) throws JSONException {
	Price priceObj = new Price();
	priceObj.setTitle(detailCallObj.getString("callLabel"));
	priceObj.setValue(detailCallObj.getString("callValue"));
	priceObj.setIconName(detailCallObj.getString("callIcon"));

	DetailsAttributes attribute = new DetailsAttributes();
	attribute.setTitle(detailCallObj.getString("callOffnetOutOfRegionLabel"));
	attribute.setValue(detailCallObj.getString("callOffnetOutOfRegionValue"));
	if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
	    priceObj.getAttributeList().add(attribute);

	attribute = new DetailsAttributes();
	attribute.setTitle(detailCallObj.getString("callOnnetOutOfRegionLabel"));
	attribute.setValue(detailCallObj.getString("callOnnetOutOfRegionValue"));
	if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
	    priceObj.getAttributeList().add(attribute);

	attribute = new DetailsAttributes();
	attribute.setTitle(detailCallObj.getString("titleThree"));
	attribute.setValue(detailCallObj.getString("valueThree"));
	if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
	    priceObj.getAttributeList().add(attribute);

	attribute = new DetailsAttributes();
	attribute.setTitle(detailCallObj.getString("titleFour"));
	attribute.setValue(detailCallObj.getString("valueFour"));
	if (Utilities.ifNotEmpty(attribute.getTitle(), attribute.getValue()))
	    priceObj.getAttributeList().add(attribute);

	return priceObj;

    }

    public static TariffDetailsSection parseAdvantagesSection(JSONObject advantagesObj,
	    TariffDetailsSection detailsSection) throws JSONException, SocketException {
	String textWithTitleLabel = advantagesObj.getString("advantagesLabel");
	String textWithTitleDescription = advantagesObj.getString("description");

	Utilities.printDebugLog("Description Text-" + textWithTitleDescription, logger);

	Utilities.printDebugLog(Boolean.toString(textWithTitleDescription.contains(Constants.GET_BULLETS_BREAK_KEY)),
		logger);

	if (textWithTitleDescription.contains(Constants.GET_BULLETS_BREAK_KEY)) {
	    Utilities.printDebugLog("Process Bullets-", logger);
	    /*
	     * As per template samples, below parsing is for bullets( Text with points
	     * templates). In this template we show description in bullet points rather than
	     * a paragraph.
	     */

	    TextWithPoints textWithPoints = new TextWithPoints();

	    textWithPoints.setPointsList(Utilities.processBullets(textWithTitleDescription));
	    detailsSection.setTextWithPoints(textWithPoints);
	    detailsSection.setTextWithTitle(null);
	    detailsSection.setTextWithOutTitle(null);
	} else if (!textWithTitleLabel.trim().isEmpty()) {
	    /*
	     * As per template samples, below parsing is for text with title template. In
	     * this template we show title and description.
	     */
	    TextWithTitle textWithTitle = new TextWithTitle();
	    textWithTitle.setTitle(textWithTitleLabel);
	    textWithTitle.setText(textWithTitleDescription);
	    detailsSection.setTextWithTitle(textWithTitle);

	    detailsSection.setTextWithPoints(null);
	    detailsSection.setTextWithOutTitle(null);
	} else if (textWithTitleLabel.trim().isEmpty()
		&& !textWithTitleDescription.contains(Constants.GET_BULLETS_BREAK_KEY)) {
	    /*
	     * As per template samples, below parsing is for text without title template. In
	     * this template we only show description.
	     */
	    TextWithOutTitle textWithOutTitle = new TextWithOutTitle();
	    textWithOutTitle.setDescription(textWithTitleDescription);
	    if (!textWithOutTitle.getDescription().isEmpty()) {
		detailsSection.setTextWithOutTitle(textWithOutTitle);
	    } else {
		detailsSection.setTextWithOutTitle(null);
	    }

	    detailsSection.setTextWithTitle(null);
	    detailsSection.setTextWithPoints(null);
	}
	return detailsSection;
    }

    public static HeaderCall parseHeaderCallSection(JSONObject headerCallObj) throws JSONException {
	HeaderCall callSectionObj = new HeaderCall();
	CallAttributes callAttributes = new CallAttributes();

	callSectionObj.setPriceTemplate(headerCallObj.getString("priceTemplate"));
	callSectionObj.setTitle(headerCallObj.getString("callLabel"));
	callSectionObj.setTitleValueLeft(headerCallObj.getString("callValueA"));
	callSectionObj.setTitleValueRight(headerCallObj.getString("callValueB"));
	callSectionObj.setIconName(headerCallObj.getString("callIcon"));

	callAttributes.setTitle(headerCallObj.getString("onnetLabel"));
	callAttributes.setValueLeft(headerCallObj.getString("onnetValueA"));
	callAttributes.setValueRight(headerCallObj.getString("onnetValueB"));

	callSectionObj.getAttributes().add(callAttributes);

	callAttributes = new CallAttributes();
	callAttributes.setTitle(headerCallObj.getString("offnetLabel"));
	callAttributes.setValueLeft(headerCallObj.getString("offnetValueA"));
	callAttributes.setValueRight(headerCallObj.getString("offnetValueB"));

	callSectionObj.getAttributes().add(callAttributes);
	return callSectionObj;
    }

    private static Advantages parseDescriptionAdvantages(JSONObject descriptionObj) throws JSONException {

	Advantages advantages = new Advantages();
	advantages.setTitle(descriptionObj.getString("advantagesLabel"));
	advantages.setDescription(descriptionObj.getString("advantagesValue"));
	if (Utilities.ifNotEmpty(advantages.getTitle(), advantages.getDescription()))
	    return advantages;
	else
	    return null;
    }

    private static Classification parseDescriptionClassification(JSONObject descriptionObj) throws JSONException {

	Classification classification = new Classification();
	classification.setTitle(descriptionObj.getString("clarificationLabel"));
	classification.setDescription(descriptionObj.getString("clarificationValue"));
	if (Utilities.ifNotEmpty(classification.getTitle(), classification.getDescription()))
	    return classification;
	else
	    return null;
    }
}

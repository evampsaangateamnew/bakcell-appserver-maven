package com.evampsaanga.bakcell.models.tariffdetails;

import com.evampsaanga.bakcell.models.tariffdetails.postpaid.individual.IndividualV2;

public class BusinessIndividualTariffData {
    int sortOrder;
    String tariffType;
    IndividualV2 individual;

    public BusinessIndividualTariffData() {
	individual = new IndividualV2();
    }

    public int getSortOrder() {
	return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
	this.sortOrder = sortOrder;
    }

    public String getTariffType() {
	return tariffType;
    }

    public void setTariffType(String tariffType) {
	this.tariffType = tariffType;
    }

    public IndividualV2 getIndividual() {
	return individual;
    }

    public void setIndividual(IndividualV2 individual) {
	this.individual = individual;
    }

}

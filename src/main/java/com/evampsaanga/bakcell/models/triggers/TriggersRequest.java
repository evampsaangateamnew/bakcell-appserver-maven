/**
 * 
 */
package com.evampsaanga.bakcell.models.triggers;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author Evamp & Saanga
 *
 */
public class TriggersRequest {

    @ApiModelProperty(notes = "Cache Type:{FAQ:1| Contact Us:2| App Menu:3| Store Locator:4| Supplementary Offerings:5| Tariffs:6| Predefined Data:7| My Subscriptions:8| Configurations:9| Transaction Names:10")
    private String cacheType;

    public String getCacheType() {
	return cacheType;
    }

    public void setCacheType(String cacheType) {
	this.cacheType = cacheType;
    }

    @Override
    public String toString() {
	return "TriggersRequest [cacheType=" + cacheType + "]";
    }

}

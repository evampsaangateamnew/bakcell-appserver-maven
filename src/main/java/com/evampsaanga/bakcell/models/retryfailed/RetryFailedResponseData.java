package com.evampsaanga.bakcell.models.retryfailed;

public class RetryFailedResponseData {
	private String responseMsg;

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}
}

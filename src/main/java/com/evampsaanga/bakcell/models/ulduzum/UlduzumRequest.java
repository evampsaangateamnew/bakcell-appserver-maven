package com.evampsaanga.bakcell.models.ulduzum;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UlduzumRequest extends BaseRequest {

}


package com.evampsaanga.bakcell.models.ulduzum;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "returnMsg",
    "returnCode",
    "data",
    "categoryNamelist"
})
public class GetMerchantsEsbResponse {

    @JsonProperty("returnMsg")
    private String returnMsg;
    @JsonProperty("returnCode")
    private String returnCode;
    @JsonProperty("data")
    private List<GetMerchantsData> data = null;
    @JsonProperty("categoryNamelist")
    private List<CategoryNamelist> categoryNamelist = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("returnMsg")
    public String getReturnMsg() {
        return returnMsg;
    }

    @JsonProperty("returnMsg")
    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    @JsonProperty("returnCode")
    public String getReturnCode() {
        return returnCode;
    }

    @JsonProperty("returnCode")
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    @JsonProperty("data")
    public List<GetMerchantsData> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<GetMerchantsData> data) {
        this.data = data;
    }

    @JsonProperty("categoryNamelist")
    public List<CategoryNamelist> getCategoryNamelist() {
        return categoryNamelist;
    }

    @JsonProperty("categoryNamelist")
    public void setCategoryNamelist(List<CategoryNamelist> categoryNamelist) {
        this.categoryNamelist = categoryNamelist;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

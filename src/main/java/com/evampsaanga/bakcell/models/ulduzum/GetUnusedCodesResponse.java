package com.evampsaanga.bakcell.models.ulduzum;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class GetUnusedCodesResponse extends BaseResponse {

    public GetUnusedCodesDataAppServer getData() {
	return data;
    }

    public void setData(GetUnusedCodesDataAppServer data) {
	this.data = data;
    }

    private GetUnusedCodesDataAppServer data = new GetUnusedCodesDataAppServer();

}

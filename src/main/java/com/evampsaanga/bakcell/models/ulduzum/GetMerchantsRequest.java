package com.evampsaanga.bakcell.models.ulduzum;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class GetMerchantsRequest extends BaseRequest {

    private String loyaltySegment = "";

    public String getLoyaltySegment() {
	return loyaltySegment;
    }

    public void setLoyaltySegment(String loyaltySegment) {
	this.loyaltySegment = loyaltySegment;
    }

}

package com.evampsaanga.bakcell.models.ulduzum;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class GetMerchantsResponse extends BaseResponse {

    private GetMerchantsDataAppServer data = new GetMerchantsDataAppServer();

    public GetMerchantsDataAppServer getData() {
	return data;
    }

    public void setData(GetMerchantsDataAppServer data) {
	this.data = data;
    }

}

package com.evampsaanga.bakcell.models.ulduzum;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class GetUsageHistoryResponse extends BaseResponse {

    private GetUsageHistoryDataAppServer data = new GetUsageHistoryDataAppServer();

    public GetUsageHistoryDataAppServer getData() {
	return data;
    }

    public void setData(GetUsageHistoryDataAppServer data) {
	this.data = data;
    }

}

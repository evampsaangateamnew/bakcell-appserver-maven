package com.evampsaanga.bakcell.models.ulduzum;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class GetCodesResponse extends BaseResponse {

    private GetCodesData data = null;

    public GetCodesData getData() {
	return data;
    }

    public void setData(GetCodesData data) {
	this.data = data;
    }

}

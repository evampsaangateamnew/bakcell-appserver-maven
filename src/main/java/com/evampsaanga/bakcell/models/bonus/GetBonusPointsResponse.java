package com.evampsaanga.bakcell.models.bonus;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class GetBonusPointsResponse extends BaseResponse {
	private String bonusPoints;

	public String getBonusPoints() {
		return bonusPoints;
	}

	public void setBonusPoints(String bonusPoints) {
		this.bonusPoints = bonusPoints;
	}

	@Override
	public String toString() {
		return "GetBonusPointsResponse [bonusPoints=" + bonusPoints + ", toString()=" + super.toString() + "]";
	}

}

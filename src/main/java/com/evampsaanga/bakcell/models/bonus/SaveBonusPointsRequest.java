package com.evampsaanga.bakcell.models.bonus;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class SaveBonusPointsRequest extends BaseRequest {
	private String bonusPoints;
	private String action;
	private String dateTime;
	private String reasonSpent;
	private String reasonGain;

	public String getBonusPoints() {
		return bonusPoints;
	}

	public void setBonusPoints(String bonusPoints) {
		this.bonusPoints = bonusPoints;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getReasonSpent() {
		return reasonSpent;
	}

	public void setReasonSpent(String reasonSpent) {
		this.reasonSpent = reasonSpent;
	}

	public String getReasonGain() {
		return reasonGain;
	}

	public void setReasonGain(String reasonGain) {
		this.reasonGain = reasonGain;
	}

	@Override
	public String toString() {
		return "SaveBonusPointsRequest [bonusPoints=" + bonusPoints + ", action=" + action + ", dateTime=" + dateTime
				+ ", reasonSpent=" + reasonSpent + ", reasonGain=" + reasonGain + ", toString()=" + super.toString()
				+ "]";
	}

}

package com.evampsaanga.bakcell.models.survey;

import java.util.List;

public class GetSurveyResponseData {
	private List<Survey> surveys;
	private List<UserSurveyData> userSurveys;

	public List<Survey> getSurveys() {
		return surveys;
	}

	public void setSurveys(List<Survey> surveys) {
		this.surveys = surveys;
	}

	public List<UserSurveyData> getUserSurveys() {
		return userSurveys;
	}

	public void setUserSurveys(List<UserSurveyData> userSurveys) {
		this.userSurveys = userSurveys;
	}

	@Override
	public String toString() {
		return "GetSurveyResponseData [surveys=" + surveys + ", userSurveys=" + userSurveys + "]";
	}

}

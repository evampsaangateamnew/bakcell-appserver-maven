package com.evampsaanga.bakcell.models.survey;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class GetSurveyResponse extends BaseResponse {
	private GetSurveyResponseData data;

	public GetSurveyResponseData getData() {
		return data;
	}

	public void setData(GetSurveyResponseData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GetSurveyResponse [data=" + data + ", toString()=" + super.toString() + "]";
	}

}

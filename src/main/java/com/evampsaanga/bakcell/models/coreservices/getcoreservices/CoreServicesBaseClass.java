/**
 * 
 */
package com.evampsaanga.bakcell.models.coreservices.getcoreservices;

import java.util.ArrayList;

/**
 * @author Evamp & Saanga
 *
 */
public class CoreServicesBaseClass {
	private String coreServiceCategory;
	private ArrayList<CoreServicesCategoryItem> coreServicesList = new ArrayList<CoreServicesCategoryItem>();

	
	public CoreServicesBaseClass() {
	}
	
	public CoreServicesBaseClass(String coreServiceCategory) {
		this.coreServiceCategory = coreServiceCategory;
	}
	
	public CoreServicesBaseClass(String coreServiceCategory, ArrayList<CoreServicesCategoryItem> coreServicesList) {
		this.coreServiceCategory = coreServiceCategory;
		this.coreServicesList = coreServicesList;
	}
	
	
	
	public ArrayList<CoreServicesCategoryItem> getCoreServicesList() {
		return coreServicesList;
	}





	public void setCoreServicesList(ArrayList<CoreServicesCategoryItem> coreServicesList) {
		this.coreServicesList = coreServicesList;
	}






	public String getCoreServiceCategory() {
		return coreServiceCategory;
	}

	public void setCoreServiceCategory(String coreServiceCategory) {
		this.coreServiceCategory = coreServiceCategory;
	}

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.coreservices.processcoreservices;

/**
 * @author Evamp & Saanga
 *
 */
public class ProcessCoreServicesResponseData {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

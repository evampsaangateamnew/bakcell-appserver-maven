/**
 * 
 */
package com.evampsaanga.bakcell.models.coreservices.getcoreservices;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Aqeel Abbas
 *
 */
public class GetCoreServicesRequestV2 extends BaseRequest {

    private String accountType;
    private String groupType;

    public String getAccountType() {
	return accountType;
    }

    public void setAccountType(String accountType) {
	this.accountType = accountType;
    }

    public String getGroupType() {
	return groupType;
    }

    public void setGroupType(String groupType) {
	this.groupType = groupType;
    }

    @Override
    public String toString() {
	return "GetCoreServicesRequestV2 [accountType=" + accountType + ", groupType=" + groupType + "]";
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.coreservices.getcoreservices;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class GetCoreServicesResponse extends BaseResponse {
    GetCoreServicesResponseData data;

    public GetCoreServicesResponseData getData() {
	return data;
    }

    public void setData(GetCoreServicesResponseData data) {
	this.data = data;
    }

}

package com.evampsaanga.bakcell.models.coreservices.processcoreservices;

public class ProcessCoreServicesRequestDataV2 {
	private String msisdn;
	private String groupType;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

}

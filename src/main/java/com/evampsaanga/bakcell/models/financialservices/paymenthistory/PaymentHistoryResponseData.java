/**
 * 
 */
package com.evampsaanga.bakcell.models.financialservices.paymenthistory;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class PaymentHistoryResponseData {

	List<Payment> paymentHistory;

	public List<Payment> getPaymentHistory() {
		return paymentHistory;
	}

	public void setPaymentHistory(List<Payment> paymentHistory) {
		this.paymentHistory = paymentHistory;
	}

}

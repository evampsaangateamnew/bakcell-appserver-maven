/**
 * 
 */
package com.evampsaanga.bakcell.models.financialservices.loanhistory;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class LoanHistoryResponseData {

	private List<Loan> loanHistory;

	public List<Loan> getLoanHistory() {
		return loanHistory;
	}

	public void setLoanHistory(List<Loan> loanHistory) {
		this.loanHistory = loanHistory;
	}
}

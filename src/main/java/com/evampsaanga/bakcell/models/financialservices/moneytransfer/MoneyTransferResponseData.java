package com.evampsaanga.bakcell.models.financialservices.moneytransfer;

public class MoneyTransferResponseData {

	private String oldBalance;
	private String newBalance;

	public String getOldBalance() {
		return oldBalance;
	}

	public void setOldBalance(String oldBalance) {
		this.oldBalance = oldBalance;
	}

	public String getNewBalance() {
		return newBalance;
	}

	public void setNewBalance(String newBalance) {
		this.newBalance = newBalance;
	}

}

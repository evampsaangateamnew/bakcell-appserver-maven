/**
 * 
 */
package com.evampsaanga.bakcell.models.financialservices.paymenthistory;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class PaymentHistoryResponse extends BaseResponse {
    private PaymentHistoryResponseData data;

    public PaymentHistoryResponseData getData() {
	return data;
    }

    public void setData(PaymentHistoryResponseData data) {
	this.data = data;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.financialservices.getloan;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class GetLoanRequest extends BaseRequest {

    private String loanAmount;
    private String brandId;
    private String subscriberType;

    public String getSubscriberType() {
	return subscriberType;
    }

    public void setSubscriberType(String subscriberType) {
	this.subscriberType = subscriberType;
    }

    public String getLoanAmount() {
	return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
	this.loanAmount = loanAmount;
    }

    @Override
    public String toString() {
	return "GetLoanRequest [loanAmount=" + loanAmount + ", getLang()=" + getLang() + ", getiP()=" + getiP()
		+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

    public String getBrandId() {
	return brandId;
    }

    public void setBrandId(String brandId) {
	this.brandId = brandId;
    }

}

package com.evampsaanga.bakcell.models.financialservices.moneytransfer;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class MoneyTransferRequest extends BaseRequest {

    private String transferee;
    private String amount;

    public String getTransferee() {
	return transferee;
    }

    public void setTransferee(String transferee) {
	this.transferee = transferee;
    }

    public String getAmount() {
	return amount;
    }

    public void setAmount(String amount) {
	this.amount = amount;
    }

    @Override
    public String toString() {
	return "MoneyTransferRequest [transferee=" + transferee + ", amount=" + amount + ", getLang()=" + getLang()
		+ ", getiP()=" + getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

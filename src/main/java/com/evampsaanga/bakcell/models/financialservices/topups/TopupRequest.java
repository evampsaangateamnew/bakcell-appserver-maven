package com.evampsaanga.bakcell.models.financialservices.topups;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;
import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.Utilities;

public class TopupRequest extends BaseRequest {

    private String cardPinNumber;
    private String subscriberType;
    private String topupnum;

    public String getCardPinNumber() {
	return cardPinNumber;
    }

    public void setCardPinNumber(String cardPinNumber) {
	this.cardPinNumber = cardPinNumber;
    }

    public String getSubscriberType() {
	return subscriberType;
    }

    public void setSubscriberType(String subscriberType) {
	this.subscriberType = subscriberType;
    }

    public String getTopupnum() {
	return topupnum;
    }

    public void setTopupnum(String topupnum) {
	this.topupnum = topupnum;
    }

    @Override
    public String toString() {
	return "TopupRequest [cardPinNumber="
		+ Utilities.maskString(cardPinNumber, Constants.SCRATCH_CARD_MASKING_COUNT) + ", subscriberType="
		+ subscriberType + ", topupnum=" + topupnum + ", getLang()=" + getLang() + ", getiP()=" + getiP()
		+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

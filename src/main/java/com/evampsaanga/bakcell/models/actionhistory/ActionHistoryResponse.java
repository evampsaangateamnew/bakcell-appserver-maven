package com.evampsaanga.bakcell.models.actionhistory;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class ActionHistoryResponse extends BaseResponse {
    private ActionHistoryOrderList data;

    public ActionHistoryOrderList getData() {
	return data;
    }

    public void setData(ActionHistoryOrderList data) {
	this.data = data;
    }

}

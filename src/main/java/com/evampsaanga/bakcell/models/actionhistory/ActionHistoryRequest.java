package com.evampsaanga.bakcell.models.actionhistory;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class ActionHistoryRequest extends BaseRequest {
    private String startDate;
    private String endDate;
    private String orderType;

    public String getStartDate() {
	return startDate;
    }

    public void setStartDate(String startDate) {
	this.startDate = startDate;
    }

    public String getEndDate() {
	return endDate;
    }

    public void setEndDate(String endDate) {
	this.endDate = endDate;
    }

    public String getOrderType() {
	return orderType;
    }

    public void setOrderType(String orderType) {
	this.orderType = orderType;
    }

}

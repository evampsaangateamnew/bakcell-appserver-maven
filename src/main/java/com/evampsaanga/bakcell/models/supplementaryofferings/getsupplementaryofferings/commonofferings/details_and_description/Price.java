/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class Price {
	private String title;
	private String value;
	private String iconName;
	private String description;
	private String offersCurrency;
	private List<DetailsAttributes> attributeList;

	public Price() {
		attributeList = new ArrayList<>();
		this.title = "";
		this.value = "";
		this.iconName = "";
		this.description = "";
		this.offersCurrency = "";
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<DetailsAttributes> getAttributeList() {
		return attributeList;
	}

	public void setAttributeList(List<DetailsAttributes> attributeList) {
		this.attributeList = attributeList;
	}

	public String getOffersCurrency() {
		return offersCurrency;
	}

	public void setOffersCurrency(String offersCurrency) {
		this.offersCurrency = offersCurrency;
	}

}

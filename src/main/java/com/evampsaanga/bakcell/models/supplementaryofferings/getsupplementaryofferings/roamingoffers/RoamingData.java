/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.roamingoffers;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.Countries;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Filters;

/**
 * @author Evamp & Saanga
 *
 */
public class RoamingData {
    private List<Countries> countries;
    private List<SupplementryOfferingsData> offers;
    private Filters filters;

    public RoamingData() {
	this.offers = new ArrayList<>();
	this.countries = new ArrayList<Countries>();
    }

    public List<Countries> getCountries() {
	return countries;
    }

    public void setCountries(List<Countries> countries) {
	this.countries = countries;
    }

    public Filters getFilters() {
	return filters;
    }

    public void setFilters(Filters filters) {
	this.filters = filters;
    }

    public List<SupplementryOfferingsData> getOffers() {
	return offers;
    }

    public void setOffers(List<SupplementryOfferingsData> list) {
	this.offers = list;
    }

    @Override
    public String toString() {
	return "RoamingData [countries=" + countries + ", offers=" + offers + ", filters=" + filters + "]";
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class TextWithPoints {
	List<String> pointsList;

	public List<String> getPointsList() {
		return pointsList;
	}

	public void setPointsList(List<String> pointsList) {
		this.pointsList = pointsList;
	}

}

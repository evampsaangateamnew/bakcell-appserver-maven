/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.header;

/**
 * @author Evamp & Saanga
 *
 */
public class HeaderAttributes {
	private String title;
	private String value;
	private String description;
	private String iconMap;
	private String unit;
	private String onnetLabel;
	private String onnetValue;
	private String offnetLabel;
	private String offnetValue;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIconMap() {
		return iconMap;
	}

	public void setIconMap(String iconMap) {
		this.iconMap = iconMap;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getOnnetLabel() {
		return onnetLabel;
	}

	public void setOnnetLabel(String onnetLabel) {
		this.onnetLabel = onnetLabel;
	}

	public String getOnnetValue() {
		return onnetValue;
	}

	public void setOnnetValue(String onnetValue) {
		this.onnetValue = onnetValue;
	}

	public String getOffnetLabel() {
		return offnetLabel;
	}

	public void setOffnetLabel(String offnetLabel) {
		this.offnetLabel = offnetLabel;
	}

	public String getOffnetValue() {
		return offnetValue;
	}

	public void setOffnetValue(String offnetValue) {
		this.offnetValue = offnetValue;
	}

}

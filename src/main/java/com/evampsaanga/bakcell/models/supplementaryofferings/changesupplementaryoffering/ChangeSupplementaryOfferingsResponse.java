/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.changesupplementaryoffering;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangeSupplementaryOfferingsResponse extends BaseResponse {
    private ChangeSupplementaryOfferingsResponseData data;

    public ChangeSupplementaryOfferingsResponseData getData() {
	return data;
    }

    public void setData(ChangeSupplementaryOfferingsResponseData data) {
	this.data = data;
    }

}

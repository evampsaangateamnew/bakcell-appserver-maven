/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings;

/**
 * @author Evamp & Saanga
 *
 */
public class SupplementryOfferingsData {
	/*
	 * As per app designs. Follow information will cater in Header {-Offer Name,
	 * -Price, -Type as group - Batch Details validity and Attributes
	 */
	private SupplementryOffersHeaders Header;

	/*
	 * As per design Following information will be catered in Details {sub
	 * attributes,-roaming- advantages,- destination- points,- date,- time and
	 * free resource validity}
	 */
	private SupplementryOffersDetailsAndDescription Details;
	/*
	 * As per design Following information will be catered in Description {sub
	 * attributes,-roaming- advantages,- destination- points,- date,- time and
	 * free resource validity}
	 */
	private SupplementryOffersDetailsAndDescription Description;

	public SupplementryOffersHeaders getHeader() {
		return Header;
	}

	public void setHeader(SupplementryOffersHeaders header) {
		Header = header;
	}

	public SupplementryOffersDetailsAndDescription getDetails() {
		return Details;
	}

	public void setDetails(SupplementryOffersDetailsAndDescription details) {
		Details = details;
	}

	public SupplementryOffersDetailsAndDescription getDescription() {
		return Description;
	}

	public void setDescription(SupplementryOffersDetailsAndDescription description) {
		Description = description;
	}

}

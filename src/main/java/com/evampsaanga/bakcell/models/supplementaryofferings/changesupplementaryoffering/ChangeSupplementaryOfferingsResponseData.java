/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.changesupplementaryoffering;

import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.SupplementaryOfferingsResponseData;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangeSupplementaryOfferingsResponseData {
	private String message;
	private SupplementaryOfferingsResponseData mySubscriptionsData;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SupplementaryOfferingsResponseData getMySubscriptionsData() {
		return mySubscriptionsData;
	}

	public void setMySubscriptionsData(SupplementaryOfferingsResponseData mySubscriptionsData) {
		this.mySubscriptionsData = mySubscriptionsData;
	}

}

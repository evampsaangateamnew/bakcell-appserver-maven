/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.changesupplementaryoffering;

/**
 * @author Aqeel Abbas
 *
 */
public class ChangeSupplementaryOfferingsResponseDatabulk {
	private String responseMsg;

	public String getMessage() {
		return responseMsg;
	}

	public void setMessage(String responseMsg) {
		this.responseMsg = responseMsg;
	}

}

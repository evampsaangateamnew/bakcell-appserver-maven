/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings;

import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description.Price;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.roamingoffers.RoamingDetails;

/**
 * @author Evamp & Saanga
 *
 */
public class SupplementryOffersDetailsAndDescription {

	private Price price;
	private Rounding rounding;
	private TextWithTitle textWithTitle;
	private TextWithOutTitle textWithOutTitle;
	private TextWithPoints textWithPoints;
	private TitleSubTitleListAndDesc titleSubTitleListAndDesc;
	private DateTemplate date;
	private TimeTemplate time;
	private RoamingDetails roamingDetails;
	private FreeResourceValidity freeResourceValidity;

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Rounding getRounding() {
		return rounding;
	}

	public void setRounding(Rounding rounding) {
		this.rounding = rounding;
	}

	public TextWithTitle getTextWithTitle() {
		return textWithTitle;
	}

	public void setTextWithTitle(TextWithTitle textWithTitle) {
		this.textWithTitle = textWithTitle;
	}

	public TextWithOutTitle getTextWithOutTitle() {
		return textWithOutTitle;
	}

	public void setTextWithOutTitle(TextWithOutTitle textWithOutTitle) {
		this.textWithOutTitle = textWithOutTitle;
	}

	public TextWithPoints getTextWithPoints() {
		return textWithPoints;
	}

	public void setTextWithPoints(TextWithPoints textWithPoints) {
		this.textWithPoints = textWithPoints;
	}

	public TitleSubTitleListAndDesc getTitleSubTitleValueAndDesc() {
		return titleSubTitleListAndDesc;
	}

	public void setTitleSubTitleValueAndDesc(TitleSubTitleListAndDesc titleSubTitleValueAndDesc) {
		this.titleSubTitleListAndDesc = titleSubTitleValueAndDesc;
	}

	public DateTemplate getDate() {
		return date;
	}

	public void setDate(DateTemplate date) {
		this.date = date;
	}

	public TimeTemplate getTime() {
		return time;
	}

	public void setTime(TimeTemplate time) {
		this.time = time;
	}

	public FreeResourceValidity getFreeResourceValidity() {
		return freeResourceValidity;
	}

	public void setFreeResourceValidity(FreeResourceValidity freeResourceValidity) {
		this.freeResourceValidity = freeResourceValidity;
	}

	public RoamingDetails getRoamingDetails() {
		return roamingDetails;
	}

	public void setRoamingDetails(RoamingDetails roamingDetails) {
		this.roamingDetails = roamingDetails;
	}

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.changesupplementaryoffering;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangeSupplementaryOfferingsResponsebulk extends BaseResponse {
    private ChangeSupplementaryOfferingsResponseDatabulk data;

    public ChangeSupplementaryOfferingsResponseDatabulk getData() {
	return data;
    }

    public void setData(ChangeSupplementaryOfferingsResponseDatabulk data) {
	this.data = data;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings;

import java.util.List;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class SupplementaryOfferingsByIDsRequest extends BaseRequest {

    private List<String> offeringIds;

    public List<String> getOfferingIds() {
	return offeringIds;
    }

    public void setOfferingIds(List<String> offeringIds) {
	this.offeringIds = offeringIds;
    }

    @Override
    public String toString() {
	return "SupplementaryOfferingsByIDsRequest [offeringIds=" + offeringIds + ", getLang()=" + getLang()
		+ ", getiP()=" + getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

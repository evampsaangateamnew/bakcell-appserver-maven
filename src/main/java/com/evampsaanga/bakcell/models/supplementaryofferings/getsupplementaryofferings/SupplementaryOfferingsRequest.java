/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class SupplementaryOfferingsRequest extends BaseRequest {
    private String offeringName;
    private String brandName;

    public String getOfferingName() {
	return offeringName;
    }

    public void setOfferingName(String offeringName) {
	this.offeringName = offeringName;
    }

    public String getBrandName() {
	return brandName;
    }

    public void setBrandName(String brandName) {
	this.brandName = brandName;
    }

    @Override
    public String toString() {
	return "SupplementaryOfferingsRequest [offeringName=" + offeringName + ", brandName=" + brandName + "]";
    }

}

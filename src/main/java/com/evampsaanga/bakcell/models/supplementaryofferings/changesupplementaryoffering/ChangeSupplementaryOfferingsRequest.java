/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.changesupplementaryoffering;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangeSupplementaryOfferingsRequest extends BaseRequest {

    private String offerName;
    private String offeringId;
    private String actionType;
    @JsonIgnore
    private String offeringName;

    public String getOfferingId() {
	return offeringId;
    }

    public void setOfferingId(String offeringId) {
	this.offeringId = offeringId;
    }

    public String getActionType() {
	return actionType;
    }

    public void setActionType(String actionType) {
	this.actionType = actionType;
    }

    public String getOfferName() {
	return offerName;
    }

    public void setOfferName(String offerName) {
	this.offerName = offerName;
    }

    @Override
    public String toString() {
	return "ChangeSupplementaryOfferingsRequest [offerName=" + offerName + ", offeringId=" + offeringId
		+ ", actionType=" + actionType + ", getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()="
		+ getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

    public String getOfferingName() {
	return offeringName;
    }

    public void setOfferingName(String offeringName) {
	this.offeringName = offeringName;
    }

}

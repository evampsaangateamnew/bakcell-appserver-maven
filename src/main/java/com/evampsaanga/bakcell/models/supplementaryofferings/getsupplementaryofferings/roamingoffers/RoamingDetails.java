/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.roamingoffers;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class RoamingDetails {

	private List<RoamingDetailsCountries> roamingDetailsCountriesList;
	private String descriptionAbove;
	private String descriptionBelow;

	public String getDescriptionAbove() {
		return descriptionAbove;
	}

	public void setDescriptionAbove(String descriptionAbove) {
		this.descriptionAbove = descriptionAbove;
	}

	public String getDescriptionBelow() {
		return descriptionBelow;
	}

	public void setDescriptionBelow(String descriptionBelow) {
		this.descriptionBelow = descriptionBelow;
	}

	public List<RoamingDetailsCountries> getRoamingDetailsCountriesList() {
		return roamingDetailsCountriesList;
	}

	public void setRoamingDetailsCountriesList(List<RoamingDetailsCountries> roamingDetailsCountriesList) {
		this.roamingDetailsCountriesList = roamingDetailsCountriesList;
	}

}

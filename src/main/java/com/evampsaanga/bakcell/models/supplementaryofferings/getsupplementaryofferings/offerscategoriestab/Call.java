/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;

/**
 * @author Evamp & Saanga
 *
 */
public class Call {
	private Filters filters;
	private List<SupplementryOfferingsData> offers;

	public Call() {
		offers = new ArrayList<>();
	}

	public Filters getFilters() {
		return filters;
	}

	public void setFilters(Filters filters) {
		this.filters = filters;
	}

	public List<SupplementryOfferingsData> getOffers() {
		return offers;
	}

	public void setOffers(List<SupplementryOfferingsData> offers) {
		this.offers = offers;
	}
}

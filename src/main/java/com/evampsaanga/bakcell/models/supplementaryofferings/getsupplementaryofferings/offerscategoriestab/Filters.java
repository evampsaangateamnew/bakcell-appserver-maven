/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class Filters {
	List<FilterData> app;
	List<FilterData> desktop;
	List<FilterData> tab;

	/**
	 * @param app
	 * @param desktop
	 * @param tab
	 */
	public Filters() {
		super();
		this.app = new ArrayList<FilterData>();
		this.desktop = new ArrayList<FilterData>();
		this.tab = new ArrayList<FilterData>();
	}

	public List<FilterData> getApp() {
		return app;
	}

	public void setApp(List<FilterData> app) {
		this.app = app;
	}

	public List<FilterData> getDesktop() {
		return desktop;
	}

	public void setDesktop(List<FilterData> desktop) {
		this.desktop = desktop;
	}

	public List<FilterData> getTab() {
		return tab;
	}

	public void setTab(List<FilterData> tab) {
		this.tab = tab;
	}

}

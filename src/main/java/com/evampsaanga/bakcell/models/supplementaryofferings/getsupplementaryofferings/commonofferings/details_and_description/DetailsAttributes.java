/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description;

/**
 * @author Evamp & Saanga
 *
 */
public class DetailsAttributes {
	private String title;
	private String value;
	private String iconMap;
	private String description;
	private String unit;

	public DetailsAttributes() {
		this.title = "";
		this.value = "";
		this.iconMap = "";
		this.description = "";
		this.unit = "";

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIconMap() {
		return iconMap;
	}

	public void setIconMap(String iconMap) {
		this.iconMap = iconMap;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings;

import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Call;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Compaign;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Hybrid;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Internet;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.SMS;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.TM;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.roamingoffers.RoamingData;

/**
 * @author Evamp & Saanga
 *
 */
public class SupplementaryOfferingsResponseData {
    private Internet internet;
    private Compaign campaign;
    private SMS sms;
    private Call call;
    private TM tm;
    private Hybrid hybrid;
    private RoamingData roaming;
    private InternetInclusiveOffers internetInclusiveOffers;
    private VoiceInclusiveOffers voiceInclusiveOffers;
    private SMSInclusiveOffers smsInclusiveOffers;

    /**
     * @param internet
     * @param campaign
     * @param sms
     * @param call
     * @param tm
     * @param hybrid
     * @param roaming
     * @param internetInclusiveOffers
     * @param voiceInclusiveOffers
     * @param smsInclusiveOffers
     */
    public SupplementaryOfferingsResponseData() {
	super();
	this.internet = new Internet();
	this.campaign = new Compaign();
	this.sms = new SMS();
	this.call = new Call();
	this.tm = new TM();
	this.hybrid = new Hybrid();
	this.roaming = new RoamingData();
	this.internetInclusiveOffers = new InternetInclusiveOffers();
	this.voiceInclusiveOffers = new VoiceInclusiveOffers();
	this.smsInclusiveOffers = new SMSInclusiveOffers();
    }
    
    public SupplementaryOfferingsResponseData(SupplementaryOfferingsResponseData data) {
    	super();
    	this.internet = data.getInternet();
    	this.campaign = data.getCampaign();
    	this.sms = data.getSms();
    	this.call = data.getCall();
    	this.tm = new TM();
    	this.tm.getOffers().addAll(data.getTm().getOffers());
    	this.hybrid = data.getHybrid();
    	this.roaming = data.getRoaming();
    	this.internetInclusiveOffers = data.getInternetInclusiveOffers();
    	this.voiceInclusiveOffers = data.getVoiceInclusiveOffers();
    	this.smsInclusiveOffers = data.getSmsInclusiveOffers();
        }

    public Internet getInternet() {
	return internet;
    }

    public void setInternet(Internet internet) {
	this.internet = internet;
    }

    public Compaign getCampaign() {
	return campaign;
    }

    public void setCampaign(Compaign campaign) {
	this.campaign = campaign;
    }

    public SMS getSms() {
	return sms;
    }

    public void setSms(SMS sms) {
	this.sms = sms;
    }

    public Call getCall() {
	return call;
    }

    public void setCall(Call call) {
	this.call = call;
    }

    public TM getTm() {
	return tm;
    }

    public void setTm(TM tm) {
	this.tm = tm;
    }

    public Hybrid getHybrid() {
	return hybrid;
    }

    public void setHybrid(Hybrid hybrid) {
	this.hybrid = hybrid;
    }

    public RoamingData getRoaming() {
	return roaming;
    }

    public void setRoaming(RoamingData roaming) {
	this.roaming = roaming;
    }

    public InternetInclusiveOffers getInternetInclusiveOffers() {
	return internetInclusiveOffers;
    }

    public void setInternetInclusiveOffers(InternetInclusiveOffers internetInclusiveOffers) {
	this.internetInclusiveOffers = internetInclusiveOffers;
    }

    public VoiceInclusiveOffers getVoiceInclusiveOffers() {
	return voiceInclusiveOffers;
    }

    public void setVoiceInclusiveOffers(VoiceInclusiveOffers voiceInclusiveOffers) {
	this.voiceInclusiveOffers = voiceInclusiveOffers;
    }

    public SMSInclusiveOffers getSmsInclusiveOffers() {
	return smsInclusiveOffers;
    }

    public void setSmsInclusiveOffers(SMSInclusiveOffers smsInclusiveOffers) {
	this.smsInclusiveOffers = smsInclusiveOffers;
    }
}

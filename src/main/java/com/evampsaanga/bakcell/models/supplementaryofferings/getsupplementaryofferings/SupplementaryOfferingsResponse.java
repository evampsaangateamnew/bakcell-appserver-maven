/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class SupplementaryOfferingsResponse extends BaseResponse implements Cloneable {

    public SupplementaryOfferingsResponse(SupplementaryOfferingsResponse offeringsResponse) {
	this.data = new SupplementaryOfferingsResponseData(offeringsResponse.getData());
	this.setCallStatus(offeringsResponse.getCallStatus());
	this.setLogsReport(offeringsResponse.getLogsReport());
	this.setResultCode(offeringsResponse.getResultCode());
	this.setResultDesc(offeringsResponse.getResultDesc());
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
	// TODO Auto-generated method stub
	return super.clone();
    }

    public SupplementaryOfferingsResponse() {
    }

    private SupplementaryOfferingsResponseData data;

    public SupplementaryOfferingsResponseData getData() {
	return data;
    }

    public void setData(SupplementaryOfferingsResponseData data) {
	this.data = data;
    }
}

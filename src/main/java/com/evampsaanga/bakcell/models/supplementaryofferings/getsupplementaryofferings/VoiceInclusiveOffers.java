package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings;

import java.util.ArrayList;
import java.util.List;

import com.evampsaanga.bakcell.models.mysubscriptions.getsubscriptions.HeaderUsage;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;

public class VoiceInclusiveOffers {
    private List<SupplementryOfferingsData> offers;
    List<HeaderUsage> inclusiveUsageList;

    public VoiceInclusiveOffers() {
	this.setOffers(new ArrayList<>());
	this.inclusiveUsageList = new ArrayList<>();
    }

    public List<SupplementryOfferingsData> getOffers() {
	return offers;
    }

    public void setOffers(List<SupplementryOfferingsData> offers) {
	this.offers = offers;
    }

    public List<HeaderUsage> getInclusiveUsageList() {
	return inclusiveUsageList;
    }

    public void setInclusiveUsageList(List<HeaderUsage> inclusiveUsageList) {
	this.inclusiveUsageList = inclusiveUsageList;
    }

}

package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings;

public class Countries {

    private String name;
    private String flag;

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getFlag() {
	return flag;
    }

    public void setFlag(String flag) {
	this.flag = flag;
    }

    @Override
    public String toString() {
	return "Countries [name=" + name + ", flag=" + flag + "]";
    }

}

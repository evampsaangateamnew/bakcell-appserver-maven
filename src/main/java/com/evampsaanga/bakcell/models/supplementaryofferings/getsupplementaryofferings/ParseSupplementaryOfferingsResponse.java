/**
 * 
 */
package com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings;

import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.evampsaanga.bakcell.common.utilities.Constants;
import com.evampsaanga.bakcell.common.utilities.GetConfigurations;
import com.evampsaanga.bakcell.common.utilities.Utilities;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.DateTemplate;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.FreeResourceValidity;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.Rounding;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOfferingsData;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOffersDetailsAndDescription;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.SupplementryOffersHeaders;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithOutTitle;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithPoints;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TextWithTitle;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TimeTemplate;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.TitleSubTitleListAndDesc;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description.DetailsAttributes;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.details_and_description.Price;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.header.GroupedOfferAttributes;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.commonofferings.header.HeaderAttributes;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.FilterData;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.offerscategoriestab.Filters;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.roamingoffers.RoamingData;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.roamingoffers.RoamingDetails;
import com.evampsaanga.bakcell.models.supplementaryofferings.getsupplementaryofferings.roamingoffers.RoamingDetailsCountries;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
public class ParseSupplementaryOfferingsResponse {

    static Logger logger = Logger.getLogger(ParseSupplementaryOfferingsResponse.class);

    /**
     * 
     */
    private ParseSupplementaryOfferingsResponse() {
	super();

    }

    /**
     * This method also checks JSON String either is object or an empty string as we
     * are receiving empty string instead of object when there is no filter.
     * 
     * @param valueFromJSON
     * @param resData
     * @return
     * @throws JSONException
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws ParseException
     * @throws SQLException
     */

    public static SupplementaryOfferingsResponseData parseSupplementaryOfferingsData(String msisdn,
	    String valueFromJSON, SupplementaryOfferingsResponseData resData)
	    throws JSONException, IOException, ParseException, SQLException {

	Utilities.printDebugLog(msisdn + "-Parser has received Supplementary Offerings Data-" + valueFromJSON, logger);
	JSONObject offersMainObject = new JSONObject(valueFromJSON);
	Utilities.printDebugLog(msisdn + "-Parser parsing data for " + Constants.FILTERS, logger);

	// Parse Filters and Offerings
	if (offersMainObject.get(Constants.SUPPLEMENTARY_TAB_INTERNET) instanceof JSONObject) {
	    if (offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_INTERNET).has(Constants.FILTERS)
		    && offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_INTERNET)
			    .get(Constants.FILTERS) instanceof JSONObject) {
		resData.getInternet().setFilters(parseFiltersData(msisdn, offersMainObject
			.getJSONObject(Constants.SUPPLEMENTARY_TAB_INTERNET).getJSONObject(Constants.FILTERS)));
	    }

	    if (offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_INTERNET).has(Constants.OFFERS)
		    && offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_INTERNET)
			    .get(Constants.OFFERS) instanceof JSONArray) {
		resData.getInternet()
			.setOffers(
				parseOfferingList(msisdn,
					offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_INTERNET)
						.getJSONArray(Constants.OFFERS),
					Constants.SUPPLEMENTARY_TAB_INTERNET, false));
	    }
	}

	if (offersMainObject.get(Constants.SUPPLEMENTARY_TAB_CALL) instanceof JSONObject) {
	    if (offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_CALL).has(Constants.FILTERS)
		    && offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_CALL)
			    .get(Constants.FILTERS) instanceof JSONObject) {
		resData.getCall().setFilters(parseFiltersData(msisdn, offersMainObject
			.getJSONObject(Constants.SUPPLEMENTARY_TAB_CALL).getJSONObject(Constants.FILTERS)));
	    }

	    if (offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_CALL).has(Constants.OFFERS)
		    && offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_CALL)
			    .get(Constants.OFFERS) instanceof JSONArray) {
		resData.getCall().setOffers(parseOfferingList(msisdn,
			offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_CALL).getJSONArray(Constants.OFFERS),
			Constants.SUPPLEMENTARY_TAB_CALL, false));
	    }
	}

	if (offersMainObject.get(Constants.SUPPLEMENTARY_TAB_CAMPAIGN) instanceof JSONObject) {
	    if (offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_CAMPAIGN).has(Constants.FILTERS)
		    && offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_CAMPAIGN)
			    .get(Constants.FILTERS) instanceof JSONObject) {
		resData.getCampaign().setFilters(parseFiltersData(msisdn, offersMainObject
			.getJSONObject(Constants.SUPPLEMENTARY_TAB_CAMPAIGN).getJSONObject(Constants.FILTERS)));
	    }
	    if (offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_CAMPAIGN).has(Constants.OFFERS)
		    && offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_CAMPAIGN)
			    .get(Constants.OFFERS) instanceof JSONArray) {
		resData.getCampaign()
			.setOffers(
				parseOfferingList(msisdn,
					offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_CAMPAIGN)
						.getJSONArray(Constants.OFFERS),
					Constants.SUPPLEMENTARY_TAB_CAMPAIGN, false));
	    }
	}

	if (offersMainObject.get(Constants.SUPPLEMENTARY_TAB_SMS) instanceof JSONObject) {
	    if (offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_SMS).has(Constants.FILTERS)
		    && offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_SMS)
			    .get(Constants.FILTERS) instanceof JSONObject) {
		resData.getSms().setFilters(parseFiltersData(msisdn, offersMainObject
			.getJSONObject(Constants.SUPPLEMENTARY_TAB_SMS).getJSONObject(Constants.FILTERS)));
	    }

	    if (offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_SMS).has(Constants.OFFERS)
		    && offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_SMS)
			    .get(Constants.OFFERS) instanceof JSONArray) {
		resData.getSms().setOffers(parseOfferingList(msisdn,
			offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_SMS).getJSONArray(Constants.OFFERS),
			Constants.SUPPLEMENTARY_TAB_SMS, false));
	    }
	}

	if (offersMainObject.get(Constants.SUPPLEMENTARY_TAB_HYBRID) instanceof JSONObject) {
	    if (offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_HYBRID).has(Constants.FILTERS)
		    && offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_HYBRID)
			    .get(Constants.FILTERS) instanceof JSONObject) {
		resData.getHybrid().setFilters(parseFiltersData(msisdn, offersMainObject
			.getJSONObject(Constants.SUPPLEMENTARY_TAB_HYBRID).getJSONObject(Constants.FILTERS)));
	    }

	    if (offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_HYBRID).has(Constants.OFFERS)
		    && offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_HYBRID)
			    .get(Constants.OFFERS) instanceof JSONArray) {
		resData.getHybrid().setOffers(
			parseOfferingList(msisdn, offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_HYBRID)
				.getJSONArray(Constants.OFFERS), Constants.SUPPLEMENTARY_TAB_HYBRID, false));
	    }
	}

	if (offersMainObject.get(Constants.SUPPLEMENTARY_TAB_TM) instanceof JSONObject) {
	    if (offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_TM).has(Constants.FILTERS)
		    && offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_TM)
			    .get(Constants.FILTERS) instanceof JSONObject) {
		resData.getTm().setFilters(parseFiltersData(msisdn, offersMainObject
			.getJSONObject(Constants.SUPPLEMENTARY_TAB_TM).getJSONObject(Constants.FILTERS)));
	    }

	    if (offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_TM).has(Constants.OFFERS) && offersMainObject
		    .getJSONObject(Constants.SUPPLEMENTARY_TAB_TM).get(Constants.OFFERS) instanceof JSONArray) {
		resData.getTm().setOffers(parseOfferingList(msisdn,
			offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_TM).getJSONArray(Constants.OFFERS),
			Constants.SUPPLEMENTARY_TAB_TM, false));
	    }
	}

	/*
	 * Roaming JSON Structure is different from rest of the Offering Lists.
	 * Therefore we have separate parser for roaming.
	 */
	resData.setRoaming(
		parseRoamingObject(msisdn, offersMainObject.getJSONObject(Constants.SUPPLEMENTARY_TAB_ROAMING),
			Constants.SUPPLEMENTARY_TAB_ROAMING));
	return resData;
    }

    private static Filters parseFiltersData(String msisdn, JSONObject jsonObject)
	    throws JsonParseException, JsonMappingException, IOException, JSONException {
	Utilities.printDebugLog(msisdn + "-Parser parsing data  for Filters-" + jsonObject.toString(), logger);

	ObjectMapper mapper = new ObjectMapper();
	Filters filters = new Filters();

	if (jsonObject.has("app")) {

	    String filtersData = Utilities.getValueFromJSON(jsonObject.toString(), "app");

	    TypeReference<List<FilterData>> mapTypeFilters = new TypeReference<List<FilterData>>() {
	    };

	    filters.setApp(mapper.readValue(filtersData, mapTypeFilters));
	}
	if (jsonObject.has("desktop")) {
	    String filtersData = Utilities.getValueFromJSON(jsonObject.toString(), "desktop");

	    TypeReference<List<FilterData>> mapTypeFilters = new TypeReference<List<FilterData>>() {
	    };

	    filters.setDesktop(mapper.readValue(filtersData, mapTypeFilters));
	}
	if (jsonObject.has("tab"))

	{
	    String filtersData = Utilities.getValueFromJSON(jsonObject.toString(), "tab");

	    TypeReference<List<FilterData>> mapTypeFilters = new TypeReference<List<FilterData>>() {
	    };

	    filters.setTab(mapper.readValue(filtersData, mapTypeFilters));
	}

	return filters;
    }

    private static List<SupplementryOfferingsData> parseOfferingList(String msisdn, JSONArray tabData, String tabName,
	    boolean isRoaming)
	    throws JSONException, JsonParseException, JsonMappingException, IOException, ParseException, SQLException {
	Utilities.printDebugLog(msisdn + "-" + tabName + "-Tab Data-" + tabData, logger);

	List<SupplementryOfferingsData> offersList = new ArrayList<>();

	for (int i = 0; i < tabData.length(); i++) {
	    SupplementryOfferingsData supplementryOfferingsData = new SupplementryOfferingsData();

	    JSONObject sectionObject = (JSONObject) tabData.get(i);
	    if (!sectionObject.isNull(Constants.SUPPLEMENTARY_HEADER_SECTION)) {
		List<HeaderAttributes> attributeList = new ArrayList<>();
		SupplementryOffersHeaders supplementaryOfferingsHeader = new SupplementryOffersHeaders();
		GroupedOfferAttributes groupedOfferAttributes = new GroupedOfferAttributes();

		JSONObject sectionHeaderData = sectionObject.getJSONObject("header");
		if (!isRoaming) {
		    groupedOfferAttributes.setGroupName(sectionHeaderData.getString("offerGroupNameLabel"));
		    groupedOfferAttributes.setGroupValue(sectionHeaderData.getString("offerGroupNameValue"));
		    supplementaryOfferingsHeader.setOfferGroup(groupedOfferAttributes);
		}
		boolean flag = false;
		if (sectionHeaderData.has("tagValidToDate")) {
		    flag = checkBatchValidity(msisdn, sectionHeaderData.getString("tagValidToDate"));
		}
		Utilities.printDebugLog(msisdn + "- Batch Validity Check: " + flag, logger);
		if (flag) {
		    Utilities.printDebugLog(msisdn + "- Batch Validity not expired. ", logger);
		    if (sectionHeaderData.getString("tag") == null || sectionHeaderData.getString("tag").isEmpty()) {
			supplementaryOfferingsHeader.setStickerLabel("new");
			Utilities.printDebugLog(
				msisdn + "- Batch not received from MAGENTO therefore sending default batch: " + flag,
				logger);
		    } else {

			supplementaryOfferingsHeader.setStickerLabel(sectionHeaderData.getString("tag"));
		    }
		    /*
		     * As sticker/batch color configuration are on APP Server. Therefore reading
		     * color code from property file.
		     */
		    supplementaryOfferingsHeader.setStickerColorCode(GetConfigurations.getConfigurationFromCache(
			    "sticker.color." + supplementaryOfferingsHeader.getStickerLabel().toLowerCase()));

		}

		supplementaryOfferingsHeader.setId(sectionHeaderData.getString("id"));
		supplementaryOfferingsHeader.setOfferName(sectionHeaderData.getString("name"));
		supplementaryOfferingsHeader.setPrice(sectionHeaderData.getString("price"));
		supplementaryOfferingsHeader.setType(sectionHeaderData.getString("type"));
		supplementaryOfferingsHeader.setValidityTitle(sectionHeaderData.getString("validityLabel"));
		supplementaryOfferingsHeader.setValidityInformation(sectionHeaderData.getString("validityDate"));
		supplementaryOfferingsHeader.setValidityValue(sectionHeaderData.getString("validityInformation"));
		supplementaryOfferingsHeader.setAppOfferFilter(sectionHeaderData.getString("appOfferFilter"));
		supplementaryOfferingsHeader.setOfferingId(sectionHeaderData.getString("offeringId"));
		supplementaryOfferingsHeader.setOfferLevel(sectionHeaderData.getString("offerLevel"));
		supplementaryOfferingsHeader.setBtnDeactivate(sectionHeaderData.getString("deactivate"));
		supplementaryOfferingsHeader.setBtnRenew(sectionHeaderData.getString("renew"));
		supplementaryOfferingsHeader.setIsTopUp(sectionHeaderData.getString("isTopUp"));
		supplementaryOfferingsHeader.setPreReqOfferId(sectionHeaderData.getString("preReqOfferId"));
		attributeList = setAttributesArray(sectionHeaderData);
		supplementaryOfferingsHeader.setAttributeList(attributeList);

		supplementryOfferingsData.setHeader(supplementaryOfferingsHeader);

	    }
	    if (!sectionObject.isNull(Constants.SUPPLEMENTARY_DETAILS_SECTION)) {
		JSONObject sectionDetailsObjData = sectionObject.getJSONObject(Constants.SUPPLEMENTARY_DETAILS_SECTION);
		SupplementryOffersDetailsAndDescription supplementryOffersDetailsAndDescription = new SupplementryOffersDetailsAndDescription();

		if (sectionDetailsObjData.has("price") && !sectionDetailsObjData.isNull("price")) {

		    supplementryOffersDetailsAndDescription
			    .setPrice(preparePriceObject(sectionDetailsObjData.getJSONObject("price")));
		}

		if (sectionDetailsObjData.has("rounding") && !sectionDetailsObjData.isNull("rounding")) {
		    supplementryOffersDetailsAndDescription
			    .setRounding(prepareRoundingObject(sectionDetailsObjData.getJSONObject("rounding")));
		}

		if (sectionDetailsObjData.has("titleSubtitle") && !sectionDetailsObjData.isNull("titleSubtitle")) {
		    supplementryOffersDetailsAndDescription.setTitleSubTitleValueAndDesc(
			    prepareTitleSubTitleValueDesc(sectionDetailsObjData.getJSONObject("titleSubtitle")));
		}

		if (sectionDetailsObjData.has("date") && !sectionDetailsObjData.isNull("date")) {
		    supplementryOffersDetailsAndDescription
			    .setDate(prepareDateObject(sectionDetailsObjData.getJSONObject("date")));
		}

		if (sectionDetailsObjData.has("time") && !sectionDetailsObjData.isNull("time")) {
		    supplementryOffersDetailsAndDescription
			    .setTime(prepareTimeObject(sectionDetailsObjData.getJSONObject("time")));
		}

		if (sectionDetailsObjData.has("freeResource") && !sectionDetailsObjData.isNull("freeResource")) {
		    supplementryOffersDetailsAndDescription.setFreeResourceValidity(
			    prepareFreeResourceValidity(sectionDetailsObjData.getJSONObject("freeResource")));
		}

		/*
		 * Below "textTitle" parser will be used for 3 templates therefore, let the
		 * parser decide which object suits for received JSON from MAGENTO.
		 */
		if (sectionDetailsObjData.has("textTitle") && !sectionDetailsObjData.isNull("textTitle")) {
		    supplementryOffersDetailsAndDescription = parseTextTitle(supplementryOffersDetailsAndDescription,
			    sectionDetailsObjData.getJSONObject("textTitle"));

		}
		if (sectionDetailsObjData.has("roaming") && !sectionDetailsObjData.isNull("roaming")) {
		    supplementryOffersDetailsAndDescription.setRoamingDetails(
			    parseRoamingData(msisdn, sectionDetailsObjData.getJSONObject("roaming")));
		}
		supplementryOfferingsData.setDetails(supplementryOffersDetailsAndDescription);
	    }
	    if (!sectionObject.isNull(Constants.SUPPLEMENTARY_DESCRIPTION_SECTION)) {
		JSONObject sectionDescriptionObjData = sectionObject
			.getJSONObject(Constants.SUPPLEMENTARY_DESCRIPTION_SECTION);
		SupplementryOffersDetailsAndDescription supplementryOffersDetailsAndDescription = new SupplementryOffersDetailsAndDescription();
		if (sectionDescriptionObjData.has("price") && !sectionDescriptionObjData.isNull("price")) {

		    supplementryOffersDetailsAndDescription
			    .setPrice(preparePriceObject(sectionDescriptionObjData.getJSONObject("price")));
		}
		if (sectionDescriptionObjData.has("rounding") && !sectionDescriptionObjData.isNull("rounding")) {
		    supplementryOffersDetailsAndDescription
			    .setRounding(prepareRoundingObject(sectionDescriptionObjData.getJSONObject("rounding")));
		}
		if (sectionDescriptionObjData.has("titleSubtitle")
			&& !sectionDescriptionObjData.isNull("titleSubtitle")) {
		    supplementryOffersDetailsAndDescription.setTitleSubTitleValueAndDesc(
			    prepareTitleSubTitleValueDesc(sectionDescriptionObjData.getJSONObject("titleSubtitle")));
		}

		if (sectionDescriptionObjData.has("date") && !sectionDescriptionObjData.isNull("date")) {
		    supplementryOffersDetailsAndDescription
			    .setDate(prepareDateObject(sectionDescriptionObjData.getJSONObject("date")));
		}

		if (sectionDescriptionObjData.has("time") && !sectionDescriptionObjData.isNull("time")) {
		    supplementryOffersDetailsAndDescription
			    .setTime(prepareTimeObject(sectionDescriptionObjData.getJSONObject("time")));
		}

		if (sectionDescriptionObjData.has("freeResource")
			&& !sectionDescriptionObjData.isNull("freeResource")) {
		    supplementryOffersDetailsAndDescription.setFreeResourceValidity(
			    prepareFreeResourceValidity(sectionDescriptionObjData.getJSONObject("freeResource")));
		}

		/*
		 * Below "textTitle" parser will be used for 3 templates therefore, let the
		 * parser decide which object suits for received JSON from MAGENTO.
		 */
		if (sectionDescriptionObjData.has("textTitle") && !sectionDescriptionObjData.isNull("textTitle")) {
		    supplementryOffersDetailsAndDescription = parseTextTitle(supplementryOffersDetailsAndDescription,
			    sectionDescriptionObjData.getJSONObject("textTitle"));

		}

		if (sectionDescriptionObjData.has("roaming") && !sectionDescriptionObjData.isNull("roaming")) {
		    supplementryOffersDetailsAndDescription.setRoamingDetails(
			    parseRoamingData(msisdn, sectionDescriptionObjData.getJSONObject("roaming")));
		}
		supplementryOfferingsData.setDescription(supplementryOffersDetailsAndDescription);
	    }

	    offersList.add(supplementryOfferingsData);
	}
	return offersList;
    }

    public static boolean checkBatchValidity(String msisdn, String expiryDateStr)
	    throws ParseException, SocketException {
	Utilities.printDebugLog(msisdn + "-Validating batch expiry. ", logger);
	Utilities.printDebugLog(msisdn + "-Expiry Date: " + expiryDateStr, logger);
	if (!expiryDateStr.isEmpty()) {
	    SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_TIME_FORMAT_FOR_TOKEN);
	    Date currentDate = sdf.parse(Utilities.getCurrentDateTime());
	    Date expiryDate = sdf.parse(expiryDateStr);

	    if (expiryDate.compareTo(currentDate) > 0) {
		Utilities.printDebugLog(msisdn + "-Batch is not yet Expired.", logger);

		return true;
	    } else {
		Utilities.printDebugLog(msisdn + "-Batch has Expired.", logger);
		return false;
	    }
	} else {
	    Utilities.printDebugLog(msisdn + "-Batch expiry date is empty. ", logger);
	    return false;
	}

    }

    public static RoamingDetails parseRoamingData(String msisdn, JSONObject jsonObject)
	    throws JSONException, JsonParseException, JsonMappingException, IOException {
	Utilities.printDebugLog(msisdn + "-Roaming Data-" + jsonObject, logger);
	ObjectMapper mapper = new ObjectMapper();
	RoamingDetails roamingData = new RoamingDetails();
	List<RoamingDetailsCountries> roamingDetailsCountriesList = new ArrayList<>();

	if (jsonObject.get("country") instanceof JSONArray) {
	    JSONArray countriesList = jsonObject.getJSONArray("country");
	    for (int i = 0; i < countriesList.length(); i++) {
		RoamingDetailsCountries roamingDetailsCountries = new RoamingDetailsCountries();

		JSONObject countryObj = countriesList.getJSONObject(i);
		roamingDetailsCountries.setCountryName(countryObj.getString("name"));
		roamingDetailsCountries.setFlag(countryObj.getString("flag"));

		String operator = Utilities.getValueFromJSON(countryObj.toString(), "operator");

		TypeReference<List<String>> mapTypeOperator = new TypeReference<List<String>>() {
		};

		roamingDetailsCountries.setOperatorList(mapper.readValue(operator, mapTypeOperator));
		roamingDetailsCountriesList.add(roamingDetailsCountries);
	    }
	    if (!roamingDetailsCountriesList.isEmpty())
		roamingData.setRoamingDetailsCountriesList(roamingDetailsCountriesList);
	    else {
		roamingData.setRoamingDetailsCountriesList(null);
	    }
	}

	if (Utilities.getValueFromJSON(jsonObject.toString(), "descriptionPosition")
		.equalsIgnoreCase(Constants.ROAMING_DESC_POSITION_TOP)) {
	    roamingData.setDescriptionAbove(Utilities.getValueFromJSON(jsonObject.toString(), "description"));
	    roamingData.setDescriptionBelow("");
	} else if (Utilities.getValueFromJSON(jsonObject.toString(), "descriptionPosition")
		.equalsIgnoreCase(Constants.ROAMING_DESC_POSITION_BOTTOM)) {
	    roamingData.setDescriptionBelow(Utilities.getValueFromJSON(jsonObject.toString(), "description"));
	    roamingData.setDescriptionAbove("");
	} else {
	    return null;
	}

	if (roamingData.getDescriptionAbove().isEmpty() && roamingData.getDescriptionBelow().isEmpty()
		&& roamingData.getRoamingDetailsCountriesList() == null) {
	    return null;
	} else
	    return roamingData;
    }

    public static FreeResourceValidity prepareFreeResourceValidity(JSONObject jsonObject) throws JSONException {
	FreeResourceValidity freeResourceValidity = new FreeResourceValidity();
	if (Utilities.ifNotEmpty(jsonObject.getString("freeResourceLabel"),
		jsonObject.getString("freeResourceValue"))) {
	    freeResourceValidity.setTitle(jsonObject.getString("freeResourceLabel"));
	    freeResourceValidity.setTitleValue(jsonObject.getString("freeResourceValue"));
	    freeResourceValidity.setSubTitle(jsonObject.getString("onnetFreeResourceLabel"));
	    freeResourceValidity.setSubTitleValue(jsonObject.getString("onnetFreeResourceValue"));
	    freeResourceValidity.setDescription(jsonObject.getString("descriptionFreeResource"));

	    return freeResourceValidity;
	} else {
	    return null;
	}
    }

    public static DateTemplate prepareDateObject(JSONObject jsonObject) throws JSONException {
	DateTemplate date = new DateTemplate();
	if (Utilities.ifNotEmpty(jsonObject.getString("fromDateLabel"), jsonObject.getString("fromDateValue"))) {
	    date.setFromTitle(jsonObject.getString("fromDateLabel"));
	    date.setFromValue(jsonObject.getString("fromDateValue"));
	    date.setToTitle(jsonObject.getString("toDateLabel"));
	    date.setToValue(jsonObject.getString("toDateValue"));
	    date.setDescription(jsonObject.getString("dateDescription"));
	    return date;
	} else {
	    return null;
	}

    }

    public static TimeTemplate prepareTimeObject(JSONObject jsonObject) throws JSONException {
	TimeTemplate time = new TimeTemplate();
	if (Utilities.ifNotEmpty(jsonObject.getString("fromTimeLabel"), jsonObject.getString("fromTimeValue"))) {
	    time.setFromTitle(jsonObject.getString("fromTimeLabel"));
	    time.setFromValue(jsonObject.getString("fromTimeValue"));
	    time.setToTitle(jsonObject.getString("toTimeLabel"));
	    time.setToValue(jsonObject.getString("toTimeValue"));
	    time.setDescription(jsonObject.getString("timeDescription"));
	    return time;
	} else {
	    return null;
	}
    }

    private static TitleSubTitleListAndDesc prepareTitleSubTitleValueDesc(JSONObject jsonObject) throws JSONException {
	TitleSubTitleListAndDesc titleSubTitleListAndDesc = new TitleSubTitleListAndDesc();
	List<DetailsAttributes> attributeList = new ArrayList<>();
	DetailsAttributes detailsAttributes = null;

	titleSubTitleListAndDesc.setTitle(jsonObject.getString("titleLabel"));
	if (!titleSubTitleListAndDesc.getTitle().isEmpty()) {
	    if (!jsonObject.getString("subtitleALabel").isEmpty()) {
		detailsAttributes = new DetailsAttributes();
		detailsAttributes.setTitle(jsonObject.getString("subtitleALabel"));
		detailsAttributes.setValue(jsonObject.getString("subtitleAValue"));
		detailsAttributes.setIconMap(jsonObject.getString("subtitleAIcon"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleADescription"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleAUnit"));
		attributeList.add(detailsAttributes);
	    }
	    if (!jsonObject.getString("subtitleBLabel").isEmpty()) {
		detailsAttributes = new DetailsAttributes();
		detailsAttributes.setTitle(jsonObject.getString("subtitleBLabel"));
		detailsAttributes.setValue(jsonObject.getString("subtitleBValue"));
		detailsAttributes.setIconMap(jsonObject.getString("subtitleBIcon"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleBDescription"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleBUnit"));
		attributeList.add(detailsAttributes);
	    }
	    if (!jsonObject.getString("subtitleCLabel").isEmpty()) {
		detailsAttributes = new DetailsAttributes();
		detailsAttributes.setTitle(jsonObject.getString("subtitleCLabel"));
		detailsAttributes.setValue(jsonObject.getString("subtitleCValue"));
		detailsAttributes.setIconMap(jsonObject.getString("subtitleCIcon"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleCDescription"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleCUnit"));
		attributeList.add(detailsAttributes);
	    }
	    if (!jsonObject.getString("subtitleDLabel").isEmpty()) {
		detailsAttributes = new DetailsAttributes();
		detailsAttributes.setTitle(jsonObject.getString("subtitleDLabel"));
		detailsAttributes.setValue(jsonObject.getString("subtitleDvalue"));
		detailsAttributes.setIconMap(jsonObject.getString("subtitleDIcon"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleDDescription"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleDUnit"));
		attributeList.add(detailsAttributes);
	    }

	    if (!jsonObject.getString("subtitleELabel").isEmpty()) {
		detailsAttributes = new DetailsAttributes();
		detailsAttributes.setTitle(jsonObject.getString("subtitleELabel"));
		detailsAttributes.setValue(jsonObject.getString("subtitleEvalue"));
		detailsAttributes.setIconMap(jsonObject.getString("subtitleEIcon"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleEDescription"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleEUnit"));
		attributeList.add(detailsAttributes);
	    }

	    if (!jsonObject.getString("subtitleFLabel").isEmpty()) {
		detailsAttributes = new DetailsAttributes();
		detailsAttributes.setTitle(jsonObject.getString("subtitleFLabel"));
		detailsAttributes.setValue(jsonObject.getString("subtitleFvalue"));
		detailsAttributes.setIconMap(jsonObject.getString("subtitleFIcon"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleFDescription"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleFUnit"));
		attributeList.add(detailsAttributes);
	    }

	    if (!jsonObject.getString("subtitleGLabel").isEmpty()) {
		detailsAttributes = new DetailsAttributes();
		detailsAttributes.setTitle(jsonObject.getString("subtitleGLabel"));
		detailsAttributes.setValue(jsonObject.getString("subtitleGvalue"));
		detailsAttributes.setIconMap(jsonObject.getString("subtitleGIcon"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleGDescription"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleGUnit"));
		attributeList.add(detailsAttributes);
	    }

	    if (!jsonObject.getString("subtitleHLabel").isEmpty()) {
		detailsAttributes = new DetailsAttributes();
		detailsAttributes.setTitle(jsonObject.getString("subtitleHLabel"));
		detailsAttributes.setValue(jsonObject.getString("subtitleHvalue"));
		detailsAttributes.setIconMap(jsonObject.getString("subtitleHIcon"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleHDescription"));
		detailsAttributes.setDescription(jsonObject.getString("subtitleHUnit"));
		attributeList.add(detailsAttributes);
	    }

	    titleSubTitleListAndDesc.setAttributeList(attributeList);
	    return titleSubTitleListAndDesc;
	} else {
	    return null;
	}
    }

    private static Price preparePriceObject(JSONObject jsonObject) throws JSONException {
	Price price = new Price();
	DetailsAttributes detailsAttributes = new DetailsAttributes();
	List<DetailsAttributes> attributeList = new ArrayList<>();
	price.setTitle(jsonObject.getString("callPriceLabel"));
	price.setValue(jsonObject.getString("callPriceValue"));
	price.setDescription(jsonObject.getString("descriptionPrice"));
	price.setIconName(jsonObject.getString("callPriceIcon"));
	price.setOffersCurrency(jsonObject.getString("offersCurrency"));

	// ListAttributes
	detailsAttributes.setTitle(jsonObject.getString("onnetPriceLabel"));
	detailsAttributes.setValue(jsonObject.getString("onnetPriceValue"));
	if (Utilities.ifNotEmpty(detailsAttributes.getTitle(), detailsAttributes.getValue()))
	    attributeList.add(detailsAttributes);

	detailsAttributes = new DetailsAttributes();
	detailsAttributes.setTitle(jsonObject.getString("offnetPriceLabel"));
	detailsAttributes.setValue(jsonObject.getString("offnetPriceValue"));
	if (Utilities.ifNotEmpty(detailsAttributes.getTitle(), detailsAttributes.getValue()))
	    attributeList.add(detailsAttributes);

	detailsAttributes = new DetailsAttributes();
	detailsAttributes.setTitle(jsonObject.getString("priceLabelC"));
	detailsAttributes.setValue(jsonObject.getString("priceValueC"));
	if (Utilities.ifNotEmpty(detailsAttributes.getTitle(), detailsAttributes.getValue()))
	    attributeList.add(detailsAttributes);

	detailsAttributes = new DetailsAttributes();
	detailsAttributes.setTitle(jsonObject.getString("priceLabelD"));
	detailsAttributes.setValue(jsonObject.getString("priceValueD"));
	if (Utilities.ifNotEmpty(detailsAttributes.getTitle(), detailsAttributes.getValue()))
	    attributeList.add(detailsAttributes);

	price.setAttributeList(attributeList);
	return price;
    }

    private static Rounding prepareRoundingObject(JSONObject jsonObject) throws JSONException {
	Rounding rounding = new Rounding();
	DetailsAttributes detailsAttributes = null;
	List<DetailsAttributes> attributeList = new ArrayList<>();
	rounding.setTitle(jsonObject.getString("destinationRoundingLabel"));
	rounding.setValue(jsonObject.getString("destinationRoundingValue"));
	rounding.setIconName(jsonObject.getString("destinationRoundingIcon"));
	rounding.setDescription(jsonObject.getString("descriptionRounding"));

	detailsAttributes = new DetailsAttributes();
	detailsAttributes.setTitle(jsonObject.getString("onnetRoundingLabel"));
	detailsAttributes.setValue(jsonObject.getString("onnetRoundingValue"));
	if (Utilities.ifNotEmpty(detailsAttributes.getTitle(), detailsAttributes.getValue()))
	    attributeList.add(detailsAttributes);

	detailsAttributes = new DetailsAttributes();
	detailsAttributes.setTitle(jsonObject.getString("offnetRoundingLabel"));
	detailsAttributes.setValue(jsonObject.getString("offnetRoundingValue"));
	if (Utilities.ifNotEmpty(detailsAttributes.getTitle(), detailsAttributes.getValue()))
	    attributeList.add(detailsAttributes);

	detailsAttributes = new DetailsAttributes();
	detailsAttributes.setTitle(jsonObject.getString("internationalRoundingLabel"));
	detailsAttributes.setValue(jsonObject.getString("internationalRoundingValue"));
	if (Utilities.ifNotEmpty(detailsAttributes.getTitle(), detailsAttributes.getValue()))
	    attributeList.add(detailsAttributes);

	detailsAttributes = new DetailsAttributes();
	detailsAttributes.setTitle(jsonObject.getString("internetRoundingLabel"));
	detailsAttributes.setValue(jsonObject.getString("internetRoundingValue"));
	if (Utilities.ifNotEmpty(detailsAttributes.getTitle(), detailsAttributes.getValue()))
	    attributeList.add(detailsAttributes);

	rounding.setAttributeList(attributeList);
	return rounding;
    }

    private static List<HeaderAttributes> setAttributesArray(JSONObject sectionObjData) throws JSONException {
	HeaderAttributes headerAttributes = new HeaderAttributes();
	List<HeaderAttributes> attributeList = new ArrayList<>();
	if (Utilities.ifNotEmpty(sectionObjData.getString("callLable"), sectionObjData.getString("callValue"))) {
	    headerAttributes.setTitle(sectionObjData.getString("callLable"));
	    headerAttributes.setValue(sectionObjData.getString("callValue"));
	    headerAttributes.setDescription(sectionObjData.getString("callDesc"));
	    headerAttributes.setIconMap(sectionObjData.getString("callIcon"));
	    headerAttributes.setUnit(sectionObjData.getString("callUnit"));
	    headerAttributes.setOnnetLabel(sectionObjData.getString("callDestinationLabel1"));
	    headerAttributes.setOnnetValue(sectionObjData.getString("callDestinationValue1"));
	    headerAttributes.setOffnetLabel(sectionObjData.getString("callDestinationLabel2"));
	    headerAttributes.setOffnetValue(sectionObjData.getString("callDestinationValue2"));
	    attributeList.add(headerAttributes);
	}
	if (Utilities.ifNotEmpty(sectionObjData.getString("smsLable"), sectionObjData.getString("smsValue"))) {
	    headerAttributes = new HeaderAttributes();
	    headerAttributes.setTitle(sectionObjData.getString("smsLable"));
	    headerAttributes.setValue(sectionObjData.getString("smsValue"));
	    headerAttributes.setDescription(sectionObjData.getString("smsDesc"));
	    headerAttributes.setIconMap(sectionObjData.getString("smsIcon"));
	    headerAttributes.setUnit(sectionObjData.getString("smsUnit"));
	    headerAttributes.setOnnetLabel(sectionObjData.getString("smsDestinationLabel1"));
	    headerAttributes.setOnnetValue(sectionObjData.getString("smsDestinationValue1"));
	    headerAttributes.setOffnetLabel(sectionObjData.getString("smsDestinationLabel2"));
	    headerAttributes.setOffnetValue(sectionObjData.getString("smsDestinationValue2"));
	    attributeList.add(headerAttributes);
	}

	if (Utilities.ifNotEmpty(sectionObjData.getString("internetLabel"),
		sectionObjData.getString("internetValue"))) {
	    headerAttributes = new HeaderAttributes();
	    headerAttributes.setTitle(sectionObjData.getString("internetLabel"));
	    headerAttributes.setValue(sectionObjData.getString("internetValue"));
	    headerAttributes.setDescription(sectionObjData.getString("internetDesc"));
	    headerAttributes.setIconMap(sectionObjData.getString("internetIcon"));
	    headerAttributes.setUnit(sectionObjData.getString("internetUnit"));
	    headerAttributes.setOnnetLabel(sectionObjData.getString("internetDestinationLabel1"));
	    headerAttributes.setOnnetValue(sectionObjData.getString("internetDestinationValue1"));
	    headerAttributes.setOffnetLabel(sectionObjData.getString("internetDestinationLabel2"));
	    headerAttributes.setOffnetValue(sectionObjData.getString("internetDestinationValue2"));
	    attributeList.add(headerAttributes);
	}

	if (Utilities.ifNotEmpty(sectionObjData.getString("whatsappLable"),
		sectionObjData.getString("whatsappValue"))) {
	    headerAttributes = new HeaderAttributes();
	    headerAttributes.setTitle(sectionObjData.getString("whatsappLable"));
	    headerAttributes.setValue(sectionObjData.getString("whatsappValue"));
	    headerAttributes.setDescription(sectionObjData.getString("whatsappDesc"));
	    headerAttributes.setIconMap(sectionObjData.getString("whatsappIcon"));
	    headerAttributes.setUnit(sectionObjData.getString("whatsappUnit"));
	    headerAttributes.setOnnetLabel(sectionObjData.getString("whatsappDestinationLabel1"));
	    headerAttributes.setOnnetValue(sectionObjData.getString("whatsappDestinationValue1"));
	    headerAttributes.setOffnetLabel(sectionObjData.getString("whatsappDestinationLabel2"));
	    headerAttributes.setOffnetValue(sectionObjData.getString("whatsappDestinationValue2"));
	    attributeList.add(headerAttributes);
	}

	if (Utilities.ifNotEmpty(sectionObjData.getString("freeResourceELable"),
		sectionObjData.getString("freeResourceEValue"))) {
	    headerAttributes = new HeaderAttributes();
	    headerAttributes.setTitle(sectionObjData.getString("freeResourceELable"));
	    headerAttributes.setValue(sectionObjData.getString("freeResourceEValue"));
	    headerAttributes.setDescription(sectionObjData.getString("freeResourceEDesc"));
	    headerAttributes.setIconMap(sectionObjData.getString("freeResourceEIcon"));
	    headerAttributes.setUnit(sectionObjData.getString("freeResourceEUnit"));
	    headerAttributes.setOnnetLabel(sectionObjData.getString("freeResourceEDestinationLabel1"));
	    headerAttributes.setOnnetValue(sectionObjData.getString("freeResourceEDestinationValue1"));
	    headerAttributes.setOffnetLabel(sectionObjData.getString("freeResourceEDestinationLabel2"));
	    headerAttributes.setOffnetValue(sectionObjData.getString("freeResourceEDestinationValue2"));
	    attributeList.add(headerAttributes);
	}

	if (Utilities.ifNotEmpty(sectionObjData.getString("freeResourceFLable"),
		sectionObjData.getString("freeResourceFValue"))) {
	    headerAttributes = new HeaderAttributes();
	    headerAttributes.setTitle(sectionObjData.getString("freeResourceFLable"));
	    headerAttributes.setValue(sectionObjData.getString("freeResourceFValue"));
	    headerAttributes.setDescription(sectionObjData.getString("freeResourceFDesc"));
	    headerAttributes.setIconMap(sectionObjData.getString("freeResourceFIcon"));
	    headerAttributes.setUnit(sectionObjData.getString("freeResourceFUnit"));
	    headerAttributes.setOnnetLabel(sectionObjData.getString("freeResourceFDestinationLabel1"));
	    headerAttributes.setOnnetValue(sectionObjData.getString("freeResourceFDestinationValue1"));
	    headerAttributes.setOffnetLabel(sectionObjData.getString("freeResourceFDestinationLabel2"));
	    headerAttributes.setOffnetValue(sectionObjData.getString("freeResourceFDestinationValue2"));
	    attributeList.add(headerAttributes);
	}

	if (Utilities.ifNotEmpty(sectionObjData.getString("freeResourceGLable"),
		sectionObjData.getString("freeResourceGValue"))) {
	    headerAttributes = new HeaderAttributes();
	    headerAttributes.setTitle(sectionObjData.getString("freeResourceGLable"));
	    headerAttributes.setValue(sectionObjData.getString("freeResourceGValue"));
	    headerAttributes.setDescription(sectionObjData.getString("freeResourceGDesc"));
	    headerAttributes.setIconMap(sectionObjData.getString("freeResourceGIcon"));
	    headerAttributes.setUnit(sectionObjData.getString("freeResourceGUnit"));
	    headerAttributes.setOnnetLabel(sectionObjData.getString("freeResourceGDestinationLabel1"));
	    headerAttributes.setOnnetValue(sectionObjData.getString("freeResourceGDestinationValue1"));
	    headerAttributes.setOffnetLabel(sectionObjData.getString("freeResourceGDestinationLabel2"));
	    headerAttributes.setOffnetValue(sectionObjData.getString("freeResourceGDestinationValue2"));
	    attributeList.add(headerAttributes);
	}

	if (Utilities.ifNotEmpty(sectionObjData.getString("freeResourceHLable"),
		sectionObjData.getString("freeResourceHValue"))) {
	    headerAttributes = new HeaderAttributes();
	    headerAttributes.setTitle(sectionObjData.getString("freeResourceHLable"));
	    headerAttributes.setValue(sectionObjData.getString("freeResourceHValue"));
	    headerAttributes.setDescription(sectionObjData.getString("freeResourceHDesc"));
	    headerAttributes.setIconMap(sectionObjData.getString("freeResourceHIcon"));
	    headerAttributes.setUnit(sectionObjData.getString("freeResourceHUnit"));
	    headerAttributes.setOnnetLabel(sectionObjData.getString("freeResourceHDestinationLabel1"));
	    headerAttributes.setOnnetValue(sectionObjData.getString("freeResourceHDestinationValue1"));
	    headerAttributes.setOffnetLabel(sectionObjData.getString("freeResourceHDestinationLabel2"));
	    headerAttributes.setOffnetValue(sectionObjData.getString("freeResourceHDestinationValue2"));
	    attributeList.add(headerAttributes);
	}
	return attributeList;
    }

    private static SupplementryOffersDetailsAndDescription parseTextTitle(
	    SupplementryOffersDetailsAndDescription supplementryOffersDetailsAndDescription, JSONObject textTitleJson)
	    throws JSONException {
	String textWithTitleLabel = textTitleJson.getString("textWithTitleLabel");
	String textWithTitleDescription = textTitleJson.getString("textWithTitleDescription");

	if (textWithTitleDescription.contains(Constants.GET_BULLETS_BREAK_KEY)) {
	    /*
	     * As per template samples, below parsing is for bullets( Text with points
	     * templates). In this template we show description in bullet points rather than
	     * a paragraph.
	     */
	    TextWithPoints textWithPoints = new TextWithPoints();
	    textWithPoints.setPointsList(Utilities.processBullets(textWithTitleDescription));
	    supplementryOffersDetailsAndDescription.setTextWithPoints(textWithPoints);

	    supplementryOffersDetailsAndDescription.setTextWithTitle(null);
	    supplementryOffersDetailsAndDescription.setTextWithOutTitle(null);
	} else if (!textWithTitleLabel.trim().isEmpty()) {
	    /*
	     * As per template samples, below parsing is for text with title template. In
	     * tis template we show title and description.
	     */
	    TextWithTitle textWithTitle = new TextWithTitle();
	    textWithTitle.setTitle(textWithTitleLabel);
	    textWithTitle.setText(textWithTitleDescription);
	    supplementryOffersDetailsAndDescription.setTextWithTitle(textWithTitle);

	    supplementryOffersDetailsAndDescription.setTextWithPoints(null);
	    supplementryOffersDetailsAndDescription.setTextWithOutTitle(null);
	} else if (textWithTitleLabel.trim().isEmpty()
		&& !textWithTitleDescription.contains(Constants.GET_BULLETS_BREAK_KEY)) {
	    /*
	     * As per template samples, below parsing is for text without title template. In
	     * this template we only show description.
	     */
	    TextWithOutTitle textWithOutTitle = new TextWithOutTitle();
	    textWithOutTitle.setDescription(textWithTitleDescription);
	    supplementryOffersDetailsAndDescription.setTextWithOutTitle(textWithOutTitle);
	    if (!textWithOutTitle.getDescription().isEmpty()) {
		supplementryOffersDetailsAndDescription.setTextWithOutTitle(textWithOutTitle);
	    } else {
		supplementryOffersDetailsAndDescription.setTextWithOutTitle(null);
	    }
	    supplementryOffersDetailsAndDescription.setTextWithTitle(null);
	    supplementryOffersDetailsAndDescription.setTextWithPoints(null);
	}

	return supplementryOffersDetailsAndDescription;
    }

    private static RoamingData parseRoamingObject(String msisdn, JSONObject jsonObject, String tab)
	    throws JsonParseException, JsonMappingException, JSONException, IOException, ParseException, SQLException {
	Utilities.printDebugLog(msisdn + "-Parse Roaming Object-" + jsonObject, logger);
	RoamingData roamingData = new RoamingData();

	// Parsing Countries List
	roamingData.setCountries(getCoutryList(jsonObject));

	// Parsing Filters Data
	roamingData.setFilters(parseFiltersData(msisdn, jsonObject.getJSONObject(Constants.FILTERS)));

	// Parsing Offers
	roamingData.setOffers(parseOfferingList(msisdn, jsonObject.getJSONArray(Constants.OFFERS), tab, true));

	return roamingData;
    }

    private static List<Countries> getCoutryList(JSONObject roamingJSONObject)
	    throws JSONException, JsonParseException, JsonMappingException, IOException {
	List<Countries> responseArrayRoamingCountries = new ArrayList<Countries>();
	for (int i = 0; i < roamingJSONObject.getJSONArray("countries").length(); i++) {
	    Countries countries = new Countries();
	    countries.setFlag(roamingJSONObject.getJSONArray("countries").getJSONObject(i).getString("flag"));
	    countries.setName(roamingJSONObject.getJSONArray("countries").getJSONObject(i).getString("name"));
	    responseArrayRoamingCountries.add(countries);
	}
	return responseArrayRoamingCountries;

    }
}

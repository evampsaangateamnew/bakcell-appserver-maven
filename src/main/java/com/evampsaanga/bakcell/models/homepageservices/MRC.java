/**
 * 
 */
package com.evampsaanga.bakcell.models.homepageservices;

/**
 * @author Evamp & Saanga
 *
 */
public class MRC {
	private String mrcTitleLabel;
	private String mrcTitleValue;
	private String mrcCurrency;
	private String mrcDateLabel;
	private String mrcDate;
	private String mrcInitialDate;
	private String mrcLimit;
	private String mrcType;
	private String mrcStatus;

	public String getMrcTitleLabel() {
		return mrcTitleLabel;
	}

	public void setMrcTitleLabel(String mrcTitleLabel) {
		this.mrcTitleLabel = mrcTitleLabel;
	}

	public String getMrcTitleValue() {
		return mrcTitleValue;
	}

	public void setMrcTitleValue(String mrcTitleValue) {
		this.mrcTitleValue = mrcTitleValue;
	}

	public String getMrcCurrency() {
		return mrcCurrency;
	}

	public void setMrcCurrency(String mrcCurrency) {
		this.mrcCurrency = mrcCurrency;
	}

	public String getMrcDateLabel() {
		return mrcDateLabel;
	}

	public void setMrcDateLabel(String mrcDateLabel) {
		this.mrcDateLabel = mrcDateLabel;
	}

	public String getMrcDate() {
		return mrcDate;
	}

	public void setMrcDate(String mrcDate) {
		this.mrcDate = mrcDate;
	}

	public String getMrcLimit() {
		return mrcLimit;
	}

	public void setMrcLimit(String mrcLimit) {
		this.mrcLimit = mrcLimit;
	}

	public String getMrcInitialDate() {
		return mrcInitialDate;
	}

	public void setMrcInitialDate(String mrcInitialDate) {
		this.mrcInitialDate = mrcInitialDate;
	}

	public String getMrcType() {
		return mrcType;
	}

	public void setMrcType(String mrcType) {
		this.mrcType = mrcType;
	}

	public String getMrcStatus() {
		return mrcStatus;
	}

	public void setMrcStatus(String mrcStatus) {
		this.mrcStatus = mrcStatus;
	}

}

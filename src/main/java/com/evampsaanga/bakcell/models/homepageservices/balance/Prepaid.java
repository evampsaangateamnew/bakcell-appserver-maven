/**
 * 
 */
package com.evampsaanga.bakcell.models.homepageservices.balance;

/**
 * @author Evamp & Saanga
 *
 */
public class Prepaid {

	private Wallet mainWallet;
	private Wallet countryWideWallet;
	private Wallet bounusWallet;

	public Wallet getMainWallet() {
		return mainWallet;
	}

	public void setMainWallet(Wallet mainWallet) {
		this.mainWallet = mainWallet;
	}

	public Wallet getCountryWideWallet() {
		return countryWideWallet;
	}

	public void setCountryWideWallet(Wallet countryWideWallet) {
		this.countryWideWallet = countryWideWallet;
	}

	public Wallet getBounusWallet() {
		return bounusWallet;
	}

	public void setBounusWallet(Wallet bounusWallet) {
		this.bounusWallet = bounusWallet;
	}

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.homepageservices.balance;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Evamp & Saanga
 *
 */


public class Balance {

	private Prepaid prepaid;
	private Postpaid postpaid;
	private String minAmountTeBePaid;
	
	@JsonInclude(Include.NON_NULL)
	private String minAmountLabel;
	
	public String getMinAmountLabel() {
		return minAmountLabel;
	}

	public void setMinAmountLabel(String minAmountLabel) {
		this.minAmountLabel = minAmountLabel;
	}

	public String getMinAmountTeBePaid() {
		return minAmountTeBePaid;
	}

	public void setMinAmountTeBePaid(String minAmountTeBePaid) {
		this.minAmountTeBePaid = minAmountTeBePaid;
	}



	public Prepaid getPrepaid() {
		return prepaid;
	}

	public void setPrepaid(Prepaid prepaid) {
		this.prepaid = prepaid;
	}

	public Postpaid getPostpaid() {
		return postpaid;
	}

	public void setPostpaid(Postpaid postpaid) {
		this.postpaid = postpaid;
	}

}

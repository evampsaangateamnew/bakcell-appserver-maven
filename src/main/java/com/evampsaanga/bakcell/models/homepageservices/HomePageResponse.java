/**
 * 
 */
package com.evampsaanga.bakcell.models.homepageservices;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class HomePageResponse extends BaseResponse {
    private HomePageResponseData data;

    public HomePageResponseData getData() {
	return data;
    }

    public void setData(HomePageResponseData data) {
	this.data = data;
    }

}

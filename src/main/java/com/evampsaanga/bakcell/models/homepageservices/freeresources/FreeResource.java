/**
 * 
 */
package com.evampsaanga.bakcell.models.homepageservices.freeresources;

/**
 * @author Evamp & Saanga
 *
 */
public class FreeResource {

	private String resourcesTitleLabel;
	private String resourceType;
	private String resourceInitialUnits;
	private String resourceRemainingUnits;
	private String resourceUnitName;
	private String resourceDiscountedText;
	private String remainingFormatted;

	public String getResourcesTitleLabel() {
		return resourcesTitleLabel;
	}

	public void setResourcesTitleLabel(String resourcesTitleLabel) {
		this.resourcesTitleLabel = resourcesTitleLabel;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceInitialUnits() {
		return resourceInitialUnits;
	}

	public void setResourceInitialUnits(String resourceInitialUnits) {
		this.resourceInitialUnits = resourceInitialUnits;
	}

	public String getResourceRemainingUnits() {
		return resourceRemainingUnits;
	}

	public void setResourceRemainingUnits(String resourceRemainingUnits) {
		this.resourceRemainingUnits = resourceRemainingUnits;
	}

	public String getResourceUnitName() {
		return resourceUnitName;
	}

	public void setResourceUnitName(String resourceUnitName) {
		this.resourceUnitName = resourceUnitName;
	}

	public String getResourceDiscountedText() {
		return resourceDiscountedText;
	}

	public void setResourceDiscountedText(String resourceDiscountedText) {
		this.resourceDiscountedText = resourceDiscountedText;
	}

	public String getRemainingFormatted() {
		return remainingFormatted;
	}

	public void setRemainingFormatted(String remainingFormatted) {
		this.remainingFormatted = remainingFormatted;
	}

}

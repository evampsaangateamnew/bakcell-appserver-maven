/**
 * 
 */
package com.evampsaanga.bakcell.models.homepageservices;

import java.util.List;

import com.evampsaanga.bakcell.models.homepageservices.installments.Installment;

/**
 * @author Evamp & Saanga
 *
 */
public class Installments {
	private String installmentDescription;
	private String installmentTitle;

	List<Installment> installments;

	public String getInstallmentDescription() {
		return installmentDescription;
	}

	public void setInstallmentDescription(String installmentDescription) {
		this.installmentDescription = installmentDescription;
	}

	public String getInstallmentTitle() {
		return installmentTitle;
	}

	public void setInstallmentTitle(String installmentTitle) {
		this.installmentTitle = installmentTitle;
	}

	public List<Installment> getInstallments() {
		return installments;
	}

	public void setInstallments(List<Installment> installments) {
		this.installments = installments;
	}

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.homepageservices.balance;

/**
 * @author Evamp & Saanga
 *
 */
public class Postpaid {

	private String availableBalanceCorporateValue;
	private String availableBalanceIndividualValue;
	private String availableCreditLabel;
	private String balanceCorporateValue;
	private String balanceIndividualValue;
	private String balanceLabel;
	private String corporateLabel;
	private String currentCreditCorporateValue;
	private String currentCreditIndividualValue;
	private String currentCreditLabel;
	private String individualLabel;
	private String outstandingIndividualDebt;
	private String outstandingIndividualDebtLabel;
	private String template;

	public String getAvailableBalanceCorporateValue() {
		return availableBalanceCorporateValue;
	}

	public void setAvailableBalanceCorporateValue(String availableBalanceCorporateValue) {
		this.availableBalanceCorporateValue = availableBalanceCorporateValue;
	}

	public String getAvailableBalanceIndividualValue() {
		return availableBalanceIndividualValue;
	}

	public void setAvailableBalanceIndividualValue(String availableBalanceIndividualValue) {
		this.availableBalanceIndividualValue = availableBalanceIndividualValue;
	}

	public String getAvailableCreditLabel() {
		return availableCreditLabel;
	}

	public void setAvailableCreditLabel(String availableCreditLabel) {
		this.availableCreditLabel = availableCreditLabel;
	}

	public String getBalanceCorporateValue() {
		return balanceCorporateValue;
	}

	public void setBalanceCorporateValue(String balanceCorporateValue) {
		this.balanceCorporateValue = balanceCorporateValue;
	}

	public String getBalanceIndividualValue() {
		return balanceIndividualValue;
	}

	public void setBalanceIndividualValue(String balanceIndividualValue) {
		this.balanceIndividualValue = balanceIndividualValue;
	}

	public String getBalanceLabel() {
		return balanceLabel;
	}

	public void setBalanceLabel(String balanceLabel) {
		this.balanceLabel = balanceLabel;
	}

	public String getCorporateLabel() {
		return corporateLabel;
	}

	public void setCorporateLabel(String corporateLabel) {
		this.corporateLabel = corporateLabel;
	}

	public String getCurrentCreditCorporateValue() {
		return currentCreditCorporateValue;
	}

	public void setCurrentCreditCorporateValue(String currentCreditCorporateValue) {
		this.currentCreditCorporateValue = currentCreditCorporateValue;
	}

	public String getCurrentCreditIndividualValue() {
		return currentCreditIndividualValue;
	}

	public void setCurrentCreditIndividualValue(String currentCreditIndividualValue) {
		this.currentCreditIndividualValue = currentCreditIndividualValue;
	}

	public String getCurrentCreditLabel() {
		return currentCreditLabel;
	}

	public void setCurrentCreditLabel(String currentCreditLabel) {
		this.currentCreditLabel = currentCreditLabel;
	}

	public String getIndividualLabel() {
		return individualLabel;
	}

	public void setIndividualLabel(String individualLabel) {
		this.individualLabel = individualLabel;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getOutstandingIndividualDebt() {
		return outstandingIndividualDebt;
	}

	public void setOutstandingIndividualDebt(String outstandingIndividualDebt) {
		this.outstandingIndividualDebt = outstandingIndividualDebt;
	}

	public String getOutstandingIndividualDebtLabel() {
		return outstandingIndividualDebtLabel;
	}

	public void setOutstandingIndividualDebtLabel(String outstandingIndividualDebtLabel) {
		this.outstandingIndividualDebtLabel = outstandingIndividualDebtLabel;
	}

}

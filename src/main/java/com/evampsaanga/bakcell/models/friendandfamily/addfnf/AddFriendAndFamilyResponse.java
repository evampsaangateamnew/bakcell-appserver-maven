/**
 * 
 */
package com.evampsaanga.bakcell.models.friendandfamily.addfnf;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;
import com.evampsaanga.bakcell.models.friendandfamily.getfnf.FriendAndFamilyResponseData;

/**
 * @author Evamp & Saanga
 *
 */
public class AddFriendAndFamilyResponse extends BaseResponse {
    private FriendAndFamilyResponseData data;

    public FriendAndFamilyResponseData getData() {
	return data;
    }

    public void setData(FriendAndFamilyResponseData data) {
	this.data = data;
    }
}

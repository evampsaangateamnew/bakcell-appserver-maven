/**
 * 
 */
package com.evampsaanga.bakcell.models.friendandfamily.deletefnf;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class DeleteFriendAndFamilyRequest extends BaseRequest {
    String deleteMsisdn;
    String offeringId;

    public String getDeleteMsisdn() {
	return deleteMsisdn;
    }

    public void setDeleteMsisdn(String deleteMsisdn) {
	this.deleteMsisdn = deleteMsisdn;
    }

    public String getOfferingId() {
	return offeringId;
    }

    public void setOfferingId(String offeringId) {
	this.offeringId = offeringId;
    }

    @Override
    public String toString() {
	return "DeleteFriendAndFamilyRequest [deleteMsisdn=" + deleteMsisdn + ", offeringId=" + offeringId
		+ ", getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
		+ ", getMsisdn()=" + getMsisdn() + "]";
    }

}

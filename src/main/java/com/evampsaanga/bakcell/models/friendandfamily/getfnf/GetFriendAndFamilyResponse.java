/**
 * 
 */
package com.evampsaanga.bakcell.models.friendandfamily.getfnf;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class GetFriendAndFamilyResponse extends BaseResponse {

    private FriendAndFamilyResponseData data;

    public FriendAndFamilyResponseData getData() {
	return data;
    }

    public void setData(FriendAndFamilyResponseData data) {
	this.data = data;
    }
}

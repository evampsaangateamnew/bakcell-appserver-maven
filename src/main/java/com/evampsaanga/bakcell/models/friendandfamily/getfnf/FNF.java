/**
 * 
 */
package com.evampsaanga.bakcell.models.friendandfamily.getfnf;

/**
 * @author Evamp & Saanga
 *
 */
public class FNF {
	String msisdn;
	String createdDate;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
}

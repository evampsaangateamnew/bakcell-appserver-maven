package com.evampsaanga.bakcell.models.roaming;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class RoamingServiceResponse extends BaseResponse {

    RoamingServiceResponseData data;

    public RoamingServiceResponseData getData() {
	return data;
    }

    public void setData(RoamingServiceResponseData data) {
	this.data = data;
    }

    @Override
    public String toString() {
	return "RoamingServiceResponse [data=" + data + ", getCallStatus()=" + getCallStatus() + ", getResultCode()="
		+ getResultCode() + ", getResultDesc()=" + getResultDesc() + "]";
    }

}

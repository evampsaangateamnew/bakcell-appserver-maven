package com.evampsaanga.bakcell.models.roaming;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class RoamingServiceRequest extends BaseRequest {
    private String offeringId;
    private String actionType;

    public String getOfferingId() {
	return offeringId;
    }

    public void setOfferingId(String offeringId) {
	this.offeringId = offeringId;
    }

    public String getActionType() {
	return actionType;
    }

    public void setActionType(String actionType) {
	this.actionType = actionType;
    }

    @Override
    public String toString() {
	return "RoamingServiceRequest [offeringId=" + offeringId + ", actionType=" + actionType + ", getIsB2B()="
		+ getIsB2B() + ", getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
		+ ", getMsisdn()=" + getMsisdn() + "]";
    }

}

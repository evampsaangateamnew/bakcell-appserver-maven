package com.evampsaanga.bakcell.models.roaming.roamingstatus;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class RoamingServiceStatusResponse extends BaseResponse {
    RoamingServiceStatusResponseData data;

    public RoamingServiceStatusResponseData getData() {
	return data;
    }

    public void setData(RoamingServiceStatusResponseData data) {
	this.data = data;
    }

    @Override
    public String toString() {
	return "RoamingServiceStatusResponse [data=" + data + "]";
    }

}

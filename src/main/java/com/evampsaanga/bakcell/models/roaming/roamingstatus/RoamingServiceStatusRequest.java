package com.evampsaanga.bakcell.models.roaming.roamingstatus;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class RoamingServiceStatusRequest extends BaseRequest {
    private String offeringId;

    public String getOfferingId() {
	return offeringId;
    }

    public void setOfferingId(String offeringId) {
	this.offeringId = offeringId;
    }

    @Override
    public String toString() {
	return "RoamingServiceStatusRequest [offeringId=" + offeringId + ", getIsB2B()=" + getIsB2B() + ", getLang()="
		+ getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn()
		+ "]";
    }

}

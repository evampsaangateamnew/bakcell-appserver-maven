package com.evampsaanga.bakcell.models.roaming;

public class RoamingServiceResponseData {
    private String message;

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

}

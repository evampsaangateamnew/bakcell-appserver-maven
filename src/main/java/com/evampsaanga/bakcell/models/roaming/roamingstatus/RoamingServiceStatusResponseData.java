package com.evampsaanga.bakcell.models.roaming.roamingstatus;

public class RoamingServiceStatusResponseData {
    private boolean isActive = false;

    public boolean isActive() {
	return isActive;
    }

    public void setActive(boolean isActive) {
	this.isActive = isActive;
    }

    @Override
    public String toString() {
	return "RoamingServiceStatusResponseData [isActive=" + isActive + "]";
    }

}

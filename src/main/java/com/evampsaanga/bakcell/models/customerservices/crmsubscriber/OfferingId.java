
package com.evampsaanga.bakcell.models.customerservices.crmsubscriber;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "OfferingId",
    "OfferingCode",
    "PurchaseSeq"
})
public class OfferingId {

    @JsonProperty("OfferingId")
    private String offeringId;
    @JsonProperty("OfferingCode")
    private String offeringCode;
    @JsonProperty("PurchaseSeq")
    private Integer purchaseSeq;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("OfferingId")
    public String getOfferingId() {
        return offeringId;
    }

    @JsonProperty("OfferingId")
    public void setOfferingId(String offeringId) {
        this.offeringId = offeringId;
    }

    @JsonProperty("OfferingCode")
    public String getOfferingCode() {
        return offeringCode;
    }

    @JsonProperty("OfferingCode")
    public void setOfferingCode(String offeringCode) {
        this.offeringCode = offeringCode;
    }

    @JsonProperty("PurchaseSeq")
    public Integer getPurchaseSeq() {
        return purchaseSeq;
    }

    @JsonProperty("PurchaseSeq")
    public void setPurchaseSeq(Integer purchaseSeq) {
        this.purchaseSeq = purchaseSeq;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

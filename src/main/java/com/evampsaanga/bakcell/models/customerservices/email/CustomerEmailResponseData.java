package com.evampsaanga.bakcell.models.customerservices.email;

public class CustomerEmailResponseData {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}


package com.evampsaanga.bakcell.models.customerservices.crmsubscriber;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "TransactionId",
    "Language",
    "ChannelId",
    "TechnicalChannelId",
    "TenantId",
    "AccessUser",
    "TestFlag"
})
public class RequestHeader {

    @JsonProperty("TransactionId")
    private String transactionId;
    @JsonProperty("Language")
    private String language;
    @JsonProperty("ChannelId")
    private String channelId;
    @JsonProperty("TechnicalChannelId")
    private String technicalChannelId;
    @JsonProperty("TenantId")
    private String tenantId;
    @JsonProperty("AccessUser")
    private String accessUser;
    @JsonProperty("TestFlag")
    private String testFlag;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("TransactionId")
    public String getTransactionId() {
        return transactionId;
    }

    @JsonProperty("TransactionId")
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @JsonProperty("Language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("Language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("ChannelId")
    public String getChannelId() {
        return channelId;
    }

    @JsonProperty("ChannelId")
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    @JsonProperty("TechnicalChannelId")
    public String getTechnicalChannelId() {
        return technicalChannelId;
    }

    @JsonProperty("TechnicalChannelId")
    public void setTechnicalChannelId(String technicalChannelId) {
        this.technicalChannelId = technicalChannelId;
    }

    @JsonProperty("TenantId")
    public String getTenantId() {
        return tenantId;
    }

    @JsonProperty("TenantId")
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    @JsonProperty("AccessUser")
    public String getAccessUser() {
        return accessUser;
    }

    @JsonProperty("AccessUser")
    public void setAccessUser(String accessUser) {
        this.accessUser = accessUser;
    }

    @JsonProperty("TestFlag")
    public String getTestFlag() {
        return testFlag;
    }

    @JsonProperty("TestFlag")
    public void setTestFlag(String testFlag) {
        this.testFlag = testFlag;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

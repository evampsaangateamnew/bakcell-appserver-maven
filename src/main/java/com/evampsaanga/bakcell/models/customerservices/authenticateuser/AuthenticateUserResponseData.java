/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.authenticateuser;

import java.util.List;

import com.evampsaanga.bakcell.models.customerservices.customerinfo.CustomerInfo;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.PredefinedDataResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class AuthenticateUserResponseData {

    private CustomerInfo customerData;
    private PrimaryOfferings primaryOffering;
    private List<SupplementaryOfferings> supplementaryOfferingList;
    private PredefinedDataResponse predefinedData;

    public CustomerInfo getCustomerData() {
	return customerData;
    }

    public void setCustomerData(CustomerInfo customerData) {
	this.customerData = customerData;
    }

    public PrimaryOfferings getPrimaryOffering() {
	return primaryOffering;
    }

    public void setPrimaryOffering(PrimaryOfferings primaryOffering) {
	this.primaryOffering = primaryOffering;
    }

    public List<SupplementaryOfferings> getSupplementaryOfferingList() {
	return supplementaryOfferingList;
    }

    public void setSupplementaryOfferingList(List<SupplementaryOfferings> supplementaryOfferingList) {
	this.supplementaryOfferingList = supplementaryOfferingList;
    }

    public PredefinedDataResponse getPredefinedData() {
	return predefinedData;
    }

    public void setPredefinedData(PredefinedDataResponse predefinedData) {
	this.predefinedData = predefinedData;
    }

    @Override
    public String toString() {
	return "AuthenticateUserResponseData [customerData=" + customerData + ", primaryOffering=" + primaryOffering
		+ ", supplementaryOfferingList=" + supplementaryOfferingList + ", predefinedData=" + predefinedData
		+ "]";
    }

}

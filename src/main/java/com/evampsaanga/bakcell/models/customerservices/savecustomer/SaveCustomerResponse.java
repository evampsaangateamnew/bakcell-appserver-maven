/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.savecustomer;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class SaveCustomerResponse extends BaseResponse {
    private SaveCustomerResponseData data;

    public SaveCustomerResponseData getData() {
	return data;
    }

    public void setData(SaveCustomerResponseData data) {
	this.data = data;
    }

}

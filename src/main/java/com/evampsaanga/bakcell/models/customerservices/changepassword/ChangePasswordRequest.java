/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.changepassword;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangePasswordRequest extends BaseRequest {
    private String oldPassword;
    private String newPassword;
    private String confirmNewPassword;

    public String getOldPassword() {
	return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
	this.oldPassword = oldPassword;
    }

    public String getConfirmNewPassword() {
	return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
	this.confirmNewPassword = confirmNewPassword;
    }

    public String getNewPassword() {
	return newPassword;
    }

    public void setNewPassword(String newPassword) {
	this.newPassword = newPassword;
    }

    @Override
    public String toString() {
	return "ChangePasswordRequest [oldPassword=" + oldPassword + ", newPassword=" + newPassword
		+ ", confirmNewPassword=" + confirmNewPassword + ", getLang()=" + getLang() + ", getiP()=" + getiP()
		+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

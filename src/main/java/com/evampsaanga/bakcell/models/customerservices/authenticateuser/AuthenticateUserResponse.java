/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.authenticateuser;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticateUserResponse extends BaseResponse {

    private AuthenticateUserResponseData data;

    public AuthenticateUserResponseData getData() {
	return data;
    }

    public void setData(AuthenticateUserResponseData data) {
	this.data = data;
    }

    @Override
    public String toString() {
	return "AuthenticateUserResponse [data=" + data + "]";
    }

}

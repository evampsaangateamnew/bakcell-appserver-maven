/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.changebillinglanguage;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangeBillingLanguageResponseData {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.updatecustomeremail;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class UpdateCustomerEmailRequest extends BaseRequest {

    private String email;

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    @Override
    public String toString() {
	return "UpdateCustomerEmailRequest [email=" + email + ", getLang()=" + getLang() + ", getiP()=" + getiP()
		+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

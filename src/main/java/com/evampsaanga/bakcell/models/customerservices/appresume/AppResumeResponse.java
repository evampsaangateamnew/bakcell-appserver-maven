/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.appresume;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class AppResumeResponse extends BaseResponse {
    private AppResumeResponseData data;

    public AppResumeResponseData getData() {
	return data;
    }

    public void setData(AppResumeResponseData data) {
	this.data = data;
    }

}

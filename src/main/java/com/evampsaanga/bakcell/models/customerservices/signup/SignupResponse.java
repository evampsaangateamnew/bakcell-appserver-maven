/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.signup;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class SignupResponse extends BaseResponse {
    private SignupResponseData data;

    public SignupResponseData getData() {
	return data;
    }

    public void setData(SignupResponseData data) {
	this.data = data;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.updatecustomeremail;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class UpdateCustomerEmailResponse extends BaseResponse {
    UpdateCustomerEmailResponseData data;

    public UpdateCustomerEmailResponseData getData() {
	return data;
    }

    public void setData(UpdateCustomerEmailResponseData data) {
	this.data = data;
    }
}

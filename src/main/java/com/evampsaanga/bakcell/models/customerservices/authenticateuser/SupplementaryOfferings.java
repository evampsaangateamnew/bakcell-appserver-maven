/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.authenticateuser;

/**
 * @author Evamp & Saanga
 *
 */
public class SupplementaryOfferings {
    private String offeringId;
    private String offeringName;
    private String offeringCode;
    private String offeringShortName;
    private String status;
    private String networkType;
    private String effectiveTime;
    private String expiredTime;

    public String getOfferingId() {
	return offeringId;
    }

    public void setOfferingId(String offeringId) {
	this.offeringId = offeringId;
    }

    public String getOfferingName() {
	return offeringName;
    }

    public void setOfferingName(String offeringName) {
	this.offeringName = offeringName;
    }

    public String getOfferingCode() {
	return offeringCode;
    }

    public void setOfferingCode(String offeringCode) {
	this.offeringCode = offeringCode;
    }

    public String getOfferingShortName() {
	return offeringShortName;
    }

    public void setOfferingShortName(String offeringShortName) {
	this.offeringShortName = offeringShortName;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getNetworkType() {
	return networkType;
    }

    public void setNetworkType(String networkType) {
	this.networkType = networkType;
    }

    public String getEffectiveTime() {
	return effectiveTime;
    }

    public void setEffectiveTime(String effectiveTime) {
	this.effectiveTime = effectiveTime;
    }

    public String getExpiredTime() {
	return expiredTime;
    }

    public void setExpiredTime(String expiredTime) {
	this.expiredTime = expiredTime;
    }

    @Override
    public String toString() {
	return "SupplementaryOfferings [offeringId=" + offeringId + ", offeringName=" + offeringName + ", offeringCode="
		+ offeringCode + ", offeringShortName=" + offeringShortName + ", status=" + status + ", networkType="
		+ networkType + ", effectiveTime=" + effectiveTime + ", expiredTime=" + expiredTime + "]";
    }

}

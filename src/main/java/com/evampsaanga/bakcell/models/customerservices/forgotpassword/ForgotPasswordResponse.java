/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.forgotpassword;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class ForgotPasswordResponse extends BaseResponse {
    private ForgotPasswordResponseData data;

    public ForgotPasswordResponseData getData() {
	return data;
    }

    public void setData(ForgotPasswordResponseData data) {
	this.data = data;
    }

}

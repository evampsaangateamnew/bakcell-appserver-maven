/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.resendpin;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class ResendPINResponse extends BaseResponse {
    private ResendPINResponseData data;

    public ResendPINResponseData getData() {
	return data;
    }

    public void setData(ResendPINResponseData data) {
	this.data = data;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.changebillinglanguage;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangeBillingLanguageRequest extends BaseRequest {

    private String language;

    public String getLanguage() {
	return language;
    }

    public void setLanguage(String language) {
	this.language = language;
    }

    @Override
    public String toString() {
	return "ChangeBillingLanguageRequest [language=" + language + ", getLang()=" + getLang() + ", getiP()="
		+ getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

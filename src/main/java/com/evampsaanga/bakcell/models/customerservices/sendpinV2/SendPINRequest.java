/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.sendpinV2;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class SendPINRequest extends BaseRequest {
    private String userName;

    public String getUserName() {
	return userName;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }
}

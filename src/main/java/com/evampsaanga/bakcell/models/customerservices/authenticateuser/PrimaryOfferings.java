/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.authenticateuser;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Evamp & Saanga
 *
 */
public class PrimaryOfferings {
    @JsonProperty("OfferingId")
    private String offeringId;
    @JsonProperty("OfferingName")
    private String offeringName;
    @JsonProperty("OfferingCode")
    private String offeringCode;
    @JsonProperty("OfferingShortName")
    private String offeringShortName;
    @JsonProperty("Status")
    private String status;
    @JsonProperty("NetworkType")
    private String networkType;
    @JsonProperty("EffectiveTime")
    private String effectiveTime;
    @JsonProperty("ExpiredTime")
    private String expiredTime;

    public String getOfferingId() {
	return offeringId;
    }

    public void setOfferingId(String offeringId) {
	this.offeringId = offeringId;
    }

    public String getOfferingName() {
	return offeringName;
    }

    public void setOfferingName(String offeringName) {
	this.offeringName = offeringName;
    }

    public String getOfferingCode() {
	return offeringCode;
    }

    public void setOfferingCode(String offeringCode) {
	this.offeringCode = offeringCode;
    }

    public String getOfferingShortName() {
	return offeringShortName;
    }

    public void setOfferingShortName(String offeringShortName) {
	this.offeringShortName = offeringShortName;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getNetworkType() {
	return networkType;
    }

    public void setNetworkType(String networkType) {
	this.networkType = networkType;
    }

    public String getEffectiveTime() {
	return effectiveTime;
    }

    public void setEffectiveTime(String effectiveTime) {
	this.effectiveTime = effectiveTime;
    }

    public String getExpiredTime() {
	return expiredTime;
    }

    public void setExpiredTime(String expiredTime) {
	this.expiredTime = expiredTime;
    }

    @Override
    public String toString() {
	return "PrimaryOfferings [offeringId=" + offeringId + ", offeringName=" + offeringName + ", offeringCode="
		+ offeringCode + ", offeringShortName=" + offeringShortName + ", status=" + status + ", networkType="
		+ networkType + ", effectiveTime=" + effectiveTime + ", expiredTime=" + expiredTime + "]";
    }

}

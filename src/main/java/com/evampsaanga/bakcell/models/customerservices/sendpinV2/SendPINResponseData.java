/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.sendpinV2;

/**
 * @author Evamp & Saanga
 *
 */
public class SendPINResponseData {
	private String responseMsg;
	private String channel;
	public String getResponseMsg() {
		return responseMsg;
	}
	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	
	
	
}

/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.changepassword;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class ChangePasswordResponse extends BaseResponse {
    private ChangePasswordResponseData data;

    public ChangePasswordResponseData getData() {
	return data;
    }

    public void setData(ChangePasswordResponseData data) {
	this.data = data;
    }

}

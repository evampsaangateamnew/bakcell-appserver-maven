/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.signup;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class SignupRequest extends BaseRequest {
    private String cause;

    public String getCause() {
	return cause;
    }

    public void setCause(String cause) {
	this.cause = cause;
    }

    @Override
    public String toString() {
	return "SignupRequest [cause=" + cause + ", getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()="
		+ getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }
}

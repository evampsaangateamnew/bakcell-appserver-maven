
package com.evampsaanga.bakcell.models.customerservices.crmsubscriber;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "subscriberResponse",
    "returnMsg",
    "returnCode"
})
public class GetCrmSubscriberEsbResponse {

    @JsonProperty("subscriberResponse")
    private SubscriberResponse subscriberResponse;
    @JsonProperty("returnMsg")
    private String returnMsg;
    @JsonProperty("returnCode")
    private String returnCode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("subscriberResponse")
    public SubscriberResponse getSubscriberResponse() {
        return subscriberResponse;
    }

    @JsonProperty("subscriberResponse")
    public void setSubscriberResponse(SubscriberResponse subscriberResponse) {
        this.subscriberResponse = subscriberResponse;
    }

    @JsonProperty("returnMsg")
    public String getReturnMsg() {
        return returnMsg;
    }

    @JsonProperty("returnMsg")
    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    @JsonProperty("returnCode")
    public String getReturnCode() {
        return returnCode;
    }

    @JsonProperty("returnCode")
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

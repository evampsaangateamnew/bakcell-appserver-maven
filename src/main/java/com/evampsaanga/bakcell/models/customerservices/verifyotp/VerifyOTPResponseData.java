/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.verifyotp;

/**
 * @author Evamp & Saanga
 *
 */
public class VerifyOTPResponseData {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

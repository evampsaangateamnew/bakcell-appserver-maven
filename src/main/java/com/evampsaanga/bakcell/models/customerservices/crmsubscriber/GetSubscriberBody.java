
package com.evampsaanga.bakcell.models.customerservices.crmsubscriber;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "IMSI",
    "actualCustomerId",
    "password",
    "SubscriberId",
    "AccountId",
    "CustomerId",
    "CustomerCode",
    "ServiceNumber",
    "SubscriberType",
    "NetworkType",
    "ICCID",
    "PIN1",
    "PIN2",
    "PUK1",
    "PUK2",
    "BrandId",
    "Language",
    "WrittenLanguage",
    "EffectiveDate",
    "ExpireDate",
    "ActiveDate",
    "PrimaryOffering",
    "SupplementaryOfferingList",
    "Status",
    "StatusReason",
    "BeId",
    "ExtParamList"
})
public class GetSubscriberBody {

    @JsonProperty("IMSI")
    private String iMSI;
    @JsonProperty("actualCustomerId")
    private Object actualCustomerId;
    @JsonProperty("password")
    private Object password;
    @JsonProperty("SubscriberId")
    private Integer subscriberId;
    @JsonProperty("AccountId")
    private Integer accountId;
    @JsonProperty("CustomerId")
    private Integer customerId;
    @JsonProperty("CustomerCode")
    private String customerCode;
    @JsonProperty("ServiceNumber")
    private String serviceNumber;
    @JsonProperty("SubscriberType")
    private String subscriberType;
    @JsonProperty("NetworkType")
    private String networkType;
    @JsonProperty("ICCID")
    private String iCCID;
    @JsonProperty("PIN1")
    private String pIN1;
    @JsonProperty("PIN2")
    private String pIN2;
    @JsonProperty("PUK1")
    private String pUK1;
    @JsonProperty("PUK2")
    private String pUK2;
    @JsonProperty("BrandId")
    private String brandId;
    @JsonProperty("Language")
    private String language;
    @JsonProperty("WrittenLanguage")
    private String writtenLanguage;
    @JsonProperty("EffectiveDate")
    private String effectiveDate;
    @JsonProperty("ExpireDate")
    private String expireDate;
    @JsonProperty("ActiveDate")
    private String activeDate;
    @JsonProperty("PrimaryOffering")
    private PrimaryOffering primaryOffering;
    @JsonProperty("SupplementaryOfferingList")
    private SupplementaryOfferingList supplementaryOfferingList;
    @JsonProperty("Status")
    private String status;
    @JsonProperty("StatusReason")
    private String statusReason;
    @JsonProperty("BeId")
    private String beId;
    @JsonProperty("ExtParamList")
    private ExtParamList____ extParamList;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("IMSI")
    public String getIMSI() {
        return iMSI;
    }

    @JsonProperty("IMSI")
    public void setIMSI(String iMSI) {
        this.iMSI = iMSI;
    }

    @JsonProperty("actualCustomerId")
    public Object getActualCustomerId() {
        return actualCustomerId;
    }

    @JsonProperty("actualCustomerId")
    public void setActualCustomerId(Object actualCustomerId) {
        this.actualCustomerId = actualCustomerId;
    }

    @JsonProperty("password")
    public Object getPassword() {
        return password;
    }

    @JsonProperty("password")
    public void setPassword(Object password) {
        this.password = password;
    }

    @JsonProperty("SubscriberId")
    public Integer getSubscriberId() {
        return subscriberId;
    }

    @JsonProperty("SubscriberId")
    public void setSubscriberId(Integer subscriberId) {
        this.subscriberId = subscriberId;
    }

    @JsonProperty("AccountId")
    public Integer getAccountId() {
        return accountId;
    }

    @JsonProperty("AccountId")
    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    @JsonProperty("CustomerId")
    public Integer getCustomerId() {
        return customerId;
    }

    @JsonProperty("CustomerId")
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    @JsonProperty("CustomerCode")
    public String getCustomerCode() {
        return customerCode;
    }

    @JsonProperty("CustomerCode")
    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    @JsonProperty("ServiceNumber")
    public String getServiceNumber() {
        return serviceNumber;
    }

    @JsonProperty("ServiceNumber")
    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    @JsonProperty("SubscriberType")
    public String getSubscriberType() {
        return subscriberType;
    }

    @JsonProperty("SubscriberType")
    public void setSubscriberType(String subscriberType) {
        this.subscriberType = subscriberType;
    }

    @JsonProperty("NetworkType")
    public String getNetworkType() {
        return networkType;
    }

    @JsonProperty("NetworkType")
    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    @JsonProperty("ICCID")
    public String getICCID() {
        return iCCID;
    }

    @JsonProperty("ICCID")
    public void setICCID(String iCCID) {
        this.iCCID = iCCID;
    }

    @JsonProperty("PIN1")
    public String getPIN1() {
        return pIN1;
    }

    @JsonProperty("PIN1")
    public void setPIN1(String pIN1) {
        this.pIN1 = pIN1;
    }

    @JsonProperty("PIN2")
    public String getPIN2() {
        return pIN2;
    }

    @JsonProperty("PIN2")
    public void setPIN2(String pIN2) {
        this.pIN2 = pIN2;
    }

    @JsonProperty("PUK1")
    public String getPUK1() {
        return pUK1;
    }

    @JsonProperty("PUK1")
    public void setPUK1(String pUK1) {
        this.pUK1 = pUK1;
    }

    @JsonProperty("PUK2")
    public String getPUK2() {
        return pUK2;
    }

    @JsonProperty("PUK2")
    public void setPUK2(String pUK2) {
        this.pUK2 = pUK2;
    }

    @JsonProperty("BrandId")
    public String getBrandId() {
        return brandId;
    }

    @JsonProperty("BrandId")
    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    @JsonProperty("Language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("Language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("WrittenLanguage")
    public String getWrittenLanguage() {
        return writtenLanguage;
    }

    @JsonProperty("WrittenLanguage")
    public void setWrittenLanguage(String writtenLanguage) {
        this.writtenLanguage = writtenLanguage;
    }

    @JsonProperty("EffectiveDate")
    public String getEffectiveDate() {
        return effectiveDate;
    }

    @JsonProperty("EffectiveDate")
    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    @JsonProperty("ExpireDate")
    public String getExpireDate() {
        return expireDate;
    }

    @JsonProperty("ExpireDate")
    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    @JsonProperty("ActiveDate")
    public String getActiveDate() {
        return activeDate;
    }

    @JsonProperty("ActiveDate")
    public void setActiveDate(String activeDate) {
        this.activeDate = activeDate;
    }

    @JsonProperty("PrimaryOffering")
    public PrimaryOffering getPrimaryOffering() {
        return primaryOffering;
    }

    @JsonProperty("PrimaryOffering")
    public void setPrimaryOffering(PrimaryOffering primaryOffering) {
        this.primaryOffering = primaryOffering;
    }

    @JsonProperty("SupplementaryOfferingList")
    public SupplementaryOfferingList getSupplementaryOfferingList() {
        return supplementaryOfferingList;
    }

    @JsonProperty("SupplementaryOfferingList")
    public void setSupplementaryOfferingList(SupplementaryOfferingList supplementaryOfferingList) {
        this.supplementaryOfferingList = supplementaryOfferingList;
    }

    @JsonProperty("Status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("Status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("StatusReason")
    public String getStatusReason() {
        return statusReason;
    }

    @JsonProperty("StatusReason")
    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    @JsonProperty("BeId")
    public String getBeId() {
        return beId;
    }

    @JsonProperty("BeId")
    public void setBeId(String beId) {
        this.beId = beId;
    }

    @JsonProperty("ExtParamList")
    public ExtParamList____ getExtParamList() {
        return extParamList;
    }

    @JsonProperty("ExtParamList")
    public void setExtParamList(ExtParamList____ extParamList) {
        this.extParamList = extParamList;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

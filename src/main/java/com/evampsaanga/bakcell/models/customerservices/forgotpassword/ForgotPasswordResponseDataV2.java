/**
 * 
 */
package com.evampsaanga.bakcell.models.customerservices.forgotpassword;

import com.evampsaanga.bakcell.models.dashboardservice.LoginData;

/**
 * @author Aqeel Abbas
 *
 */
public class ForgotPasswordResponseDataV2 {

	LoginData loginData;

	public LoginData getLoginData() {
		return loginData;
	}

	public void setLoginData(LoginData loginData) {
		this.loginData = loginData;
	}

}

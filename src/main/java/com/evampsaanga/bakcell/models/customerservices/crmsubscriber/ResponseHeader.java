
package com.evampsaanga.bakcell.models.customerservices.crmsubscriber;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "RequestHeader",
    "RetCode",
    "RetMsg"
})
public class ResponseHeader {

    @JsonProperty("RequestHeader")
    private RequestHeader requestHeader;
    @JsonProperty("RetCode")
    private String retCode;
    @JsonProperty("RetMsg")
    private String retMsg;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("RequestHeader")
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    @JsonProperty("RequestHeader")
    public void setRequestHeader(RequestHeader requestHeader) {
        this.requestHeader = requestHeader;
    }

    @JsonProperty("RetCode")
    public String getRetCode() {
        return retCode;
    }

    @JsonProperty("RetCode")
    public void setRetCode(String retCode) {
        this.retCode = retCode;
    }

    @JsonProperty("RetMsg")
    public String getRetMsg() {
        return retMsg;
    }

    @JsonProperty("RetMsg")
    public void setRetMsg(String retMsg) {
        this.retMsg = retMsg;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

package com.evampsaanga.bakcell.models.acceptTNC;

public class AcceptTnCResponseData {
	private String returnMsg;

	public String getReturnMsg() {
		return returnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

}

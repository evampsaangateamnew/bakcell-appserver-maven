package com.evampsaanga.bakcell.models.acceptTNC;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class AcceptTnCRequest extends BaseRequest {
    private String userName;

    public String getUserName() {
	return userName;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }

    @Override
    public String toString() {
	return "AcceptTnCRequest [userName=" + userName + "]";
    }

}

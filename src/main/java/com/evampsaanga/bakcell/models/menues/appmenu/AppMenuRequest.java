/**
 * 
 */
package com.evampsaanga.bakcell.models.menues.appmenu;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class AppMenuRequest extends BaseRequest {

    @Override
    public String toString() {
	return "AppMenuRequest [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
		+ ", getMsisdn()=" + getMsisdn() + "]";
    }

}

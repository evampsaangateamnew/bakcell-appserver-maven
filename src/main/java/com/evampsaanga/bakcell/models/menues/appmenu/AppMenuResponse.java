/**
 * 
 */
package com.evampsaanga.bakcell.models.menues.appmenu;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class AppMenuResponse extends BaseResponse {

    private Data data = new Data();

    public Data getData() {
	return data;
    }

    public void setData(Data data) {
	this.data = data;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.menues.appmenuV2;


/**
 * @author Aqeel Abbas
 *
 */
public class AppMenuResponseData {

	private AppMenuListData data = new AppMenuListData();
	public AppMenuListData getData() {
		return data;
	}

	public void setData(AppMenuListData data) {
		this.data = data;
	}

	
}

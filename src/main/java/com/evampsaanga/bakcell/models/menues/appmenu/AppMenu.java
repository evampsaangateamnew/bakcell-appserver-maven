/**
 * 
 */
package com.evampsaanga.bakcell.models.menues.appmenu;

/**
 * @author Evamp & Saanga
 *
 */
public class AppMenu {
	private String identifier;
	private String title;
	private String sortOrder;

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

}

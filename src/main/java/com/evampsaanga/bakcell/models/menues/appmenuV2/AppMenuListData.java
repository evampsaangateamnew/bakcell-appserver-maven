/**
 * 
 */
package com.evampsaanga.bakcell.models.menues.appmenuV2;

import java.util.ArrayList;


/**
 * @author Aqeel Abbas
 *
 */
public class AppMenuListData {
	ArrayList<MenuHorizontal> menuHorizontal;
	ArrayList<MenuVertical> menuVertical;
	
	public ArrayList<MenuHorizontal> getMenuHorizontal() {
		return menuHorizontal;
	}
	public void setMenuHorizontal(ArrayList<MenuHorizontal> menuHorizontal) {
		this.menuHorizontal = menuHorizontal;
	}
	public ArrayList<MenuVertical> getMenuVertical() {
		return menuVertical;
	}
	public void setMenuVertical(ArrayList<MenuVertical> menuVertical) {
		this.menuVertical = menuVertical;
	}

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.menues.appmenuV2;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class AppMenuResponse extends BaseResponse {

    private AppMenuListData data = new AppMenuListData();

    public AppMenuListData getData() {
	return data;
    }

    public void setData(AppMenuListData data) {
	this.data = data;
    }

}

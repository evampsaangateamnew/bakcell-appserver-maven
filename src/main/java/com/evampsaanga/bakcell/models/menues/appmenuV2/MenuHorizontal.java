package com.evampsaanga.bakcell.models.menues.appmenuV2;

public class MenuHorizontal {
    private String identifier = "";
    private String title = "";
    private String sortOrder = "";
    private AppMenu[] items;

    public String getIdentifier() {
	return identifier;
    }

    public void setIdentifier(String identifier) {
	this.identifier = identifier;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getSortOrder() {
	return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
	this.sortOrder = sortOrder;
    }

    public AppMenu[] getItems() {
	return items;
    }

    public void setItems(AppMenu[] items) {
	this.items = items;
    }

}

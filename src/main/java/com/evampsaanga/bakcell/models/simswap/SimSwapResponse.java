package com.evampsaanga.bakcell.models.simswap;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class SimSwapResponse extends BaseResponse {
    private SimSwapResponseData data;

    public SimSwapResponseData getData() {
	return data;
    }

    public void setData(SimSwapResponseData data) {
	this.data = data;
    }

}

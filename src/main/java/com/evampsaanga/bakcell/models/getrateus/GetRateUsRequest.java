package com.evampsaanga.bakcell.models.getrateus;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class GetRateUsRequest extends BaseRequest {

    private String entityId;

    public String getEntityId() {
	return entityId;
    }

    public void setEntityId(String entityId) {
	this.entityId = entityId;
    }

    @Override
    public String toString() {
	return "GetRateUsRequest [entityId=" + entityId + ", getIsB2B()=" + getIsB2B() + ", getLang()=" + getLang()
		+ ", getiP()=" + getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

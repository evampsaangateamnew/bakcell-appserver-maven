package com.evampsaanga.bakcell.models.getrateus;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class GetRateUsResponse extends BaseResponse {

    private GetRateUsResponseData data;

    public GetRateUsResponseData getData() {
	return data;
    }

    public void setData(GetRateUsResponseData data) {
	this.data = data;
    }

}

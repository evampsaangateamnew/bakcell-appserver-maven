package com.evampsaanga.bakcell.models.getrateus;

public class GetRateUsResponseData {

	private String rateus_android="0";
	private String rateus_ios="0";
	private String pic_tnc;
	
	public String getRateus_android() {
		return rateus_android;
	}
	public void setRateus_android(String rateus_android) {
		this.rateus_android = rateus_android;
	}
	public String getRateus_ios() {
		return rateus_ios;
	}
	public void setRateus_ios(String rateus_ios) {
		this.rateus_ios = rateus_ios;
	}
	public String getPic_tnc() {
		return pic_tnc;
	}
	public void setPic_tnc(String pic_tnc) {
		this.pic_tnc = pic_tnc;
	}
	
}

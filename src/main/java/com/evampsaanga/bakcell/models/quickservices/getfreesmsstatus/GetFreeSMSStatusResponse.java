/**
 * 
 */
package com.evampsaanga.bakcell.models.quickservices.getfreesmsstatus;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class GetFreeSMSStatusResponse extends BaseResponse {
    GetFreeSMSStatusResponseData data;

    public GetFreeSMSStatusResponseData getData() {
	return data;
    }

    public void setData(GetFreeSMSStatusResponseData data) {
	this.data = data;
    }
}

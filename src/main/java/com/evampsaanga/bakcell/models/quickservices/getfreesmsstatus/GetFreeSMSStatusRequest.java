/**
 * 
 */
package com.evampsaanga.bakcell.models.quickservices.getfreesmsstatus;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class GetFreeSMSStatusRequest extends BaseRequest {

    @Override
    public String toString() {
	return "GetFreeSMSStatusRequest [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()="
		+ getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.quickservices.sendfreesms;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class SendFreeSMSResponseV2 extends BaseResponse {

    public SendFreeSMSResponseDataV2 data;

    public SendFreeSMSResponseDataV2 getData() {
	return data;
    }

    public void setData(SendFreeSMSResponseDataV2 data) {
	this.data = data;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.quickservices.sendfreesms;

/**
 * @author Evamp & Saanga
 *
 */
public class SendFreeSMSResponseData {

	private String onNetSMS;
	private String offNetSMS;
	private String smsSent;

	public String getOnNetSMS() {
		return onNetSMS;
	}

	public void setOnNetSMS(String onNetSMS) {
		this.onNetSMS = onNetSMS;
	}

	public String getOffNetSMS() {
		return offNetSMS;
	}

	public void setOffNetSMS(String offNetSMS) {
		this.offNetSMS = offNetSMS;
	}

	public String getSmsSent() {
		return smsSent;
	}

	public void setSmsSent(String smsSent) {
		this.smsSent = smsSent;
	}

}

package com.evampsaanga.bakcell.models.dashboardservice;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class DashboardRequest extends BaseRequest {
    private String password;

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    @Override
    public String toString() {
	return "DashboardRequest [password=" + password + ", getIsB2B()=" + getIsB2B() + ", getLang()=" + getLang()
		+ ", getiP()=" + getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

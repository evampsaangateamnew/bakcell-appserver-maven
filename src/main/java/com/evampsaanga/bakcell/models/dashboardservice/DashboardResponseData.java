package com.evampsaanga.bakcell.models.dashboardservice;

import java.util.ArrayList;

import com.evampsaanga.bakcell.models.dashboardservice.balancepic.BalancePicData;
import com.evampsaanga.bakcell.models.dashboardservice.queryinvoice.QueryInvoiceData;
import com.evampsaanga.bakcell.models.dashboardservice.usersgroup.UsersGroupData;
import com.evampsaanga.bakcell.models.dashboardservice.usersgroup.UsersGroupResponse;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.PredefinedDataResponseDataV2;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardResponseData {

	private String returnCode;

	private QueryInvoiceData[] queryInvoiceResponseData;

	private String returnMsg;

	private UsersGroupResponse groupData;

	private LoginData loginData;

	private ArrayList<UsersGroupData> users;

	private BalancePicData queryBalancePicResponseData;

	private PredefinedDataResponseDataV2 predefinedData;

	private String liveChat;
	private String userCount;

	public PredefinedDataResponseDataV2 getPredefinedData() {
		return predefinedData;
	}

	public void setPredefinedData(PredefinedDataResponseDataV2 predefinedData) {
		this.predefinedData = predefinedData;
	}

	public BalancePicData getQueryBalancePicResponseData() {
		return queryBalancePicResponseData;
	}

	public void setQueryBalancePicResponseData(BalancePicData queryBalancePicResponseData) {
		this.queryBalancePicResponseData = queryBalancePicResponseData;
	}

	public ArrayList<UsersGroupData> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<UsersGroupData> users) {
		this.users = users;
	}

	public UsersGroupResponse getGroupData() {
		return groupData;
	}

	public void setGroupData(UsersGroupResponse groupData) {
		this.groupData = groupData;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public QueryInvoiceData[] getQueryInvoiceResponseData() {
		return queryInvoiceResponseData;
	}

	public void setQueryInvoiceResponseData(QueryInvoiceData[] queryInvoiceResponseData) {
		this.queryInvoiceResponseData = queryInvoiceResponseData;
	}

	public String getReturnMsg() {
		return returnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	public LoginData getLoginData() {
		return loginData;
	}

	public void setLoginData(LoginData loginData) {
		this.loginData = loginData;
	}

	@Override
	public String toString() {
		return "ClassPojo [returnCode = " + returnCode + ", users = " + users + ", queryInvoiceResponseData = "
				+ queryInvoiceResponseData + ", returnMsg = " + returnMsg + ", groupData = " + groupData
				+ ", loginData = " + loginData + "]";
	}

	public String getLiveChat() {
		return liveChat;
	}

	public void setLiveChat(String liveChat) {
		this.liveChat = liveChat;
	}

	public String getUserCount() {
		return userCount;
	}

	public void setUserCount(String userCount) {
		this.userCount = userCount;
	}
	
/*
	public static void main(String[] args) throws JSONException {
		JSONObject jsonObject = new JSONObject(
				"{\"returnMsg\":\"Successful\",\"returnCode\":\"200\",\"loginData\":{\"entity_id\":\"227\",\"website_id\":\"1\",\"email\":\"newpic1@Test.com\",\"group_id\":\"4\",\"store_id\":\"1\",\"created_at\":\"2018-07-09 06:38:29\",\"updated_at\":\"2018-08-06 07:09:52\",\"is_active\":\"1\",\"disable_auto_group_change\":\"0\",\"created_in\":\"Default Store View\",\"firstname\":\"muhammad\",\"lastname\":\"Gufran\",\"suffix\":\"w\",\"password_hash\":\"3e163f36dca3224fe5cfdd0214097c14bea7732e986b4eef8c79afc77b74ef9e:xxxxxxxx:1\",\"rp_token\":\"4dd3b3e7b6b3f837e285df4188fda566\",\"rp_token_created_at\":\"2018-07-09 06:38:29\",\"default_billing\":\"0\",\"default_shipping\":\"0\",\"failures_num\":\"0\",\"msisdn\":\"pic_new\",\"customer_type\":\"postpaid\",\"account_id\":\"13131\",\"language\":\"en\",\"customer_id\":\"60001133\",\"channel\":\"web\",\"pic_allowed_tariffs\":\"770094093,1570082771,745348537,1270094233,1770082919,370082492,1155373228,355630537,1655630758,720469688,1420469348,1560986698,1320455071,1894870629,1295589733,1195647029,1974577397,1470253339,970250220,1770250307,1471646836,771646823,371615150,1471614990,770253481,1971646822,1070249776,1371646816,770253711,1870253814,1370253941,1471637124,170249065,970254022,1370250082,1871646816,1271646829,1771646816,1474578416,574578738,374579254,771614575,1137898528,1770083072,370094560,1670006710,770094391,1970082271,770249383,1270249534,1470249646,1571636728,1171636872,1870253615,1270249962,1171646816,1571821605,1671614769,771646833\",\"pic_tariffs_permissions\":\"PaybySubs-Upgrade,PaybySubs-Downgrade,PartPay-Upgrade,PartPay-Downgrade,FullPay-Upgrade\",\"pic_allowed_offers\":\"1126233544,526256441,1024100713,1125909660,1225913124,1725914513,1125997735,525998280,1426001158,526002541,978106505,1401459443,1913038075,1151806699,1250446338,1650511655,1420537226,1920570459,1120572128,520572937,376367781,1076367958,1376368122,322355824,1176367625,1978106711,1285890379,985891194,1932907452,1032915331,981899074,1178034269,122206970,1925294975,1778035257,920816437,1024017055,162099730,213123,906471273,1107769122,1606281323,1106368938,1906462715,1806463473,902735639,1505075143,1105075286,1505075428,506471346,1875518077,1251814288,1951814324,1651814390,751814423,1724443692,1173626120,1673626906,973627303,773626625,1673627024,1712326619,1115034347,1915034377,515034399,1815034421,730552510,1530579330,530579362,1230636525,1694871972,1695587927,1195588377,1576369121,1476368948,1076369206,1176369383,1976369604,1876369702,582648597,576369294,378020456,1178020599,1678020928,1578021015,1032976233,378020771,1663227146,1480287500,520819100,1978021082,1222030439,1720819067,1064004019,778027206,380287894,1017940386,717874807,1417874746,1917940298,1778038778,1480289370,378034097,1081743468,1878033925,1578033984,178034042,1661927660,1260989508,765649560,1764339698,161929155,520819486,160986927,520819438,320819392,1255373036,1755547152,1455547240,1117874986,917874949,320550114,716329997,516329907\",\"password_unhashed\":\"temp\",\"otp_source\":\"sms\",\"reward_update_notification\":\"0\",\"reward_warning_notification\":\"0\",\"pic_status\":\"1\",\"pic_new_reset\":\"0\",\"pic_tnc\":\"1\",\"pic_veon\":\"5555\",\"custom_profile_image\":\"pic_new.jpg\",\"pic_serial_no\":\"555\",\"pic_pin\":\"555\",\"otp_msisdn\":\"12345677788\"},\"groupData\":{\"Testing\":{\"groupName\":\"Testing\",\"userCount\":8,\"usersGroupData\":[{\"msisdn\":\"558120743\",\"tarif_id\":\"1270249534\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=14099=IN/VAS\",\"cust_last_name\":\"Testing\"},{\"msisdn\":\"555957427\",\"tarif_id\":\"371615150\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60201133\",\"group_cust_name\":\"Testing\",\"group_id\":\"1\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15445=IN/VAS\",\"cust_last_name\":\"Testing\"},{\"msisdn\":\"555957473\",\"tarif_id\":\"1270249962\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=14121=IN/VASTesting\",\"cust_last_name\":\"Testing\"},{\"msisdn\":\"555957474\",\"tarif_id\":\"771646833\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=13153=IN/VASTesting\",\"cust_last_name\":\"Testing\"},{\"msisdn\":\"555957497\",\"tarif_id\":\"1870253615\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15515=IN/VAS\",\"cust_last_name\":\"Testing\"},{\"msisdn\":\"555957754\",\"tarif_id\":\"770253711\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15772=IN/VAS\",\"cust_last_name\":\"Testing\"},{\"msisdn\":\"555957756\",\"tarif_id\":\"1370253941\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15774=IN/VAS\",\"cust_last_name\":\"Testing\"},{\"msisdn\":\"555957757\",\"tarif_id\":\"970254022\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15775=IN/VAS\",\"cust_last_name\":\"Testing\"}]}},\"users\":{\"555957497\":{\"msisdn\":\"555957497\",\"tarif_id\":\"1870253615\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15515=IN/VAS\",\"cust_last_name\":\"Testing\"},\"558120743\":{\"msisdn\":\"558120743\",\"tarif_id\":\"1270249534\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=14099=IN/VAS\",\"cust_last_name\":\"Testing\"},\"555957473\":{\"msisdn\":\"555957473\",\"tarif_id\":\"1270249962\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=14121=IN/VASTesting\",\"cust_last_name\":\"Testing\"},\"555957474\":{\"msisdn\":\"555957474\",\"tarif_id\":\"771646833\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=13153=IN/VASTesting\",\"cust_last_name\":\"Testing\"},\"555957754\":{\"msisdn\":\"555957754\",\"tarif_id\":\"770253711\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15772=IN/VAS\",\"cust_last_name\":\"Testing\"},\"555957756\":{\"msisdn\":\"555957756\",\"tarif_id\":\"1370253941\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15774=IN/VAS\",\"cust_last_name\":\"Testing\"},\"555957427\":{\"msisdn\":\"555957427\",\"tarif_id\":\"371615150\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60201133\",\"group_cust_name\":\"Testing\",\"group_id\":\"1\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15445=IN/VAS\",\"cust_last_name\":\"Testing\"},\"555957757\":{\"msisdn\":\"555957757\",\"tarif_id\":\"970254022\",\"brand_id\":\"1770078103\",\"corp_id\":\"60001133\",\"group_code\":\"60301133\",\"group_cust_name\":\"Testing\",\"group_id\":\"2\",\"corp_crm_cust_name\":\"60001133\",\"pic_name\":\"  \",\"corp_acct_code\":\"60001133\",\"corp_crm_acct_id\":\"60001133\",\"cust_fst_name\":\"TEST=15775=IN/VAS\",\"cust_last_name\":\"Testing\"}},\"queryInvoiceResponseData\": [{\"invoice_amount\": \"6781297961\",\"invoice_date\": \"20180601050551\",\"due_date\": \"20180701000000\"},{\"invoice_amount\": \"6781297961\",\"invoice_date\": \"20180601050551\",\"due_date\": \"20180701000000\"},{\"invoice_amount\": \"6781297961\",\"invoice_date\": \"20180601050551\",\"due_date\": \"20180701000000\"}]}");
		System.out.println(jsonObject.getJSONObject("loginData").get("entity_id"));
	}*/

}

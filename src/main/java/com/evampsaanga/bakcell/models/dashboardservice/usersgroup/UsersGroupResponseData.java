package com.evampsaanga.bakcell.models.dashboardservice.usersgroup;

import java.util.ArrayList;

public class UsersGroupResponseData {
	
	private ArrayList<UsersGroupData> usersData;
	private String groupName;
	private String userCount;

	
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getUserCount() {
		return userCount;
	}

	public void setUserCount(String userCount) {
		this.userCount = userCount;
	}

	public ArrayList<UsersGroupData> getUsersData() {
		return usersData;
	}

	public void setUsersData(ArrayList<UsersGroupData> usersData) {
		this.usersData = usersData;
	}

	
	
	
	

}

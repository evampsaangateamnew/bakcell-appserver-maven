package com.evampsaanga.bakcell.models.dashboardservice;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class DashboardRequestResume extends BaseRequest {

    private String channel;
    private String iP;
    private String msisdn;

    @Override
    public String getMsisdn() {
	return msisdn;
    }

    @Override
    public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
    }

    @Override
    public String getChannel() {
	return channel;
    }

    @Override
    public void setChannel(String channel) {
	this.channel = channel;
    }

    @Override
    public String getiP() {
	return iP;
    }

    @Override
    public void setiP(String iP) {
	this.iP = iP;
    }

}

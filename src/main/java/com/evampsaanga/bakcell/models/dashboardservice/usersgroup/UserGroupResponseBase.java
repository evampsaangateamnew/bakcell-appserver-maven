package com.evampsaanga.bakcell.models.dashboardservice.usersgroup;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class UserGroupResponseBase extends BaseResponse {
    private UsersGroupResponse data;

    public UsersGroupResponse getData() {
	return data;
    }

    public void setData(UsersGroupResponse data) {
	this.data = data;
    }

}

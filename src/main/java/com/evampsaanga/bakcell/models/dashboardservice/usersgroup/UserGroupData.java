package com.evampsaanga.bakcell.models.dashboardservice.usersgroup;

import java.util.ArrayList;

public class UserGroupData {
	ArrayList<UsersGroupResponseData> usersGroupData;

	public ArrayList<UsersGroupResponseData> getUsersGroupData() {
		return usersGroupData;
	}

	public void setUsersGroupData(ArrayList<UsersGroupResponseData> usersGroupData) {
		this.usersGroupData = usersGroupData;
	}
	
}

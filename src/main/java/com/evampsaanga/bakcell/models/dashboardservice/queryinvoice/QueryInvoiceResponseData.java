package com.evampsaanga.bakcell.models.dashboardservice.queryinvoice;

import java.util.List;

public class QueryInvoiceResponseData {
    private List<QueryInvoiceData> queryInvoiceResponseData;

    public List<QueryInvoiceData> getQueryInvoiceResponseData() {
	return queryInvoiceResponseData;
    }

    public void setQueryInvoiceResponseData(List<QueryInvoiceData> queryInvoiceResponseData) {
	this.queryInvoiceResponseData = queryInvoiceResponseData;
    }

}

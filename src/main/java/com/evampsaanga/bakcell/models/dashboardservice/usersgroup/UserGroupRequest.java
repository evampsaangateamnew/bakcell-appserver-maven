package com.evampsaanga.bakcell.models.dashboardservice.usersgroup;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class UserGroupRequest extends BaseRequest {
    private String customerID;

    public String getCustomerID() {
	return customerID;
    }

    public void setCustomerID(String customerID) {
	this.customerID = customerID;
    }

}

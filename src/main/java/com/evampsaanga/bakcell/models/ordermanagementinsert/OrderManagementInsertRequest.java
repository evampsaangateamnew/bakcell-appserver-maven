package com.evampsaanga.bakcell.models.ordermanagementinsert;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;
import com.evampsaanga.bakcell.common.utilities.RecieverMsisdn;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "actPrice", "msgLang", "orderKey", "recieverMsisdn", "destinationTariff", "tariffPermissions",
	"textmsg", "senderName", "amount", "offeringId", "number", "actionType", "companyValue", "totalLimit",
	"orderId", "totalUsersbyGroup", "accountId", "groupTypeTo", "groupIdTo", "customerId", "lowerLimit", "iccid",
	"transactionId", "contactNumber"

})
public class OrderManagementInsertRequest extends BaseRequest {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("contactNumber")
    private String contactNumber;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("iccid")
    private String iccid;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("transactionId")
    private String transactionId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("actPrice")
    private String actPrice;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("msgLang")
    private String msgLang;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("orderKey")
    private String orderKey;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("recieverMsisdn")
    private List<RecieverMsisdn> recieverMsisdn = null;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("destinationTariff")
    private String destinationTariff;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("tariffPermissions")
    private String tariffPermissions;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("textmsg")
    private String textmsg;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("senderName")
    private String senderName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("amount")
    private String amount;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("offeringId")
    private String offeringId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("number")
    private String number;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("actionType")
    private String actionType;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("companyValue")
    private String companyValue;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("totalLimit")
    private String totalLimit;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("orderId")
    private String orderId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("totalUsersbyGroup")
    private String totalUsersbyGroup;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("accountId")
    private String accountId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("customerId")
    private String customerId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("lowerLimit")
    private String lowerLimit;

    public String getLowerLimit() {
	return lowerLimit;
    }

    public void setLowerLimit(String lowerLimit) {
	this.lowerLimit = lowerLimit;
    }

    @JsonProperty("customerId")
    public String getCustomerId() {
	return customerId;
    }

    @JsonProperty("customerId")
    public void setCustomerId(String customerId) {
	this.customerId = customerId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("groupTypeTo")
    private String groupTypeTo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("groupIdTo")
    private String groupIdTo;

    @JsonProperty("groupIdTo")
    public String getGroupIdTo() {
	return groupIdTo;
    }

    @JsonProperty("groupIdTo")
    public void setGroupIdTo(String groupIdTo) {
	this.groupIdTo = groupIdTo;
    }

    @JsonProperty("groupTypeTo")
    public String getGroupTypeTo() {
	return groupTypeTo;
    }

    @JsonProperty("groupTypeTo")
    public void setGroupTypeTo(String groupTypeTo) {
	this.groupTypeTo = groupTypeTo;
    }

    public String getAccountId() {
	return accountId;
    }

    public void setAccountId(String accountId) {
	this.accountId = accountId;
    }

    public String getTotalUsersbyGroup() {
	return totalUsersbyGroup;
    }

    public void setTotalUsersbyGroup(String totalUsersbyGroup) {
	this.totalUsersbyGroup = totalUsersbyGroup;
    }

    public String getActPrice() {
	return actPrice;
    }

    @JsonProperty("actPrice")
    public void setActPrice(String actPrice) {
	this.actPrice = actPrice;
    }

    @JsonProperty("msgLang")
    public String getMsgLang() {
	return msgLang;
    }

    @JsonProperty("msgLang")
    public void setMsgLang(String msgLang) {
	this.msgLang = msgLang;
    }

    @JsonProperty("orderKey")
    public String getOrderKey() {
	return orderKey;
    }

    @JsonProperty("orderKey")
    public void setOrderKey(String orderKey) {
	this.orderKey = orderKey;
    }

    @JsonProperty("recieverMsisdn")
    public List<RecieverMsisdn> getRecieverMsisdn() {
	return recieverMsisdn;
    }

    @JsonProperty("recieverMsisdn")
    public void setRecieverMsisdn(List<RecieverMsisdn> recieverMsisdn) {
	this.recieverMsisdn = recieverMsisdn;
    }

    @JsonProperty("destinationTariff")
    public String getDestinationTariff() {
	return destinationTariff;
    }

    @JsonProperty("destinationTariff")
    public void setDestinationTariff(String destinationTariff) {
	this.destinationTariff = destinationTariff;
    }

    @JsonProperty("tariffPermissions")
    public String getTariffPermissions() {
	return tariffPermissions;
    }

    @JsonProperty("tariffPermissions")
    public void setTariffPermissions(String tariffPermissions) {
	this.tariffPermissions = tariffPermissions;
    }

    @JsonProperty("textmsg")
    public String getTextmsg() {
	return textmsg;
    }

    @JsonProperty("textmsg")
    public void setTextmsg(String textmsg) {
	this.textmsg = textmsg;
    }

    @JsonProperty("senderName")
    public String getSenderName() {
	return senderName;
    }

    @JsonProperty("senderName")
    public void setSenderName(String senderName) {
	this.senderName = senderName;
    }

    @JsonProperty("amount")
    public String getAmount() {
	return amount;
    }

    @JsonProperty("amount")
    public void setAmount(String amount) {
	this.amount = amount;
    }

    @JsonProperty("offeringId")
    public String getOfferingId() {
	return offeringId;
    }

    @JsonProperty("offeringId")
    public void setOfferingId(String offeringId) {
	this.offeringId = offeringId;
    }

    @JsonProperty("number")
    public String getNumber() {
	return number;
    }

    @JsonProperty("number")
    public void setNumber(String number) {
	this.number = number;
    }

    @JsonProperty("actionType")
    public String getActionType() {
	return actionType;
    }

    @JsonProperty("actionType")
    public void setActionType(String actionType) {
	this.actionType = actionType;
    }

    @JsonProperty("companyValue")
    public String getCompanyValue() {
	return companyValue;
    }

    @JsonProperty("companyValue")
    public void setCompanyValue(String companyValue) {
	this.companyValue = companyValue;
    }

    @JsonProperty("totalLimit")
    public String getTotalLimit() {
	return totalLimit;
    }

    @JsonProperty("totalLimit")
    public void setTotalLimit(String totalLimit) {
	this.totalLimit = totalLimit;
    }

    @JsonProperty("orderId")
    public String getOrderId() {
	return orderId;
    }

    @JsonProperty("orderId")
    public void setOrderId(String orderId) {
	this.orderId = orderId;
    }

    @Override
    public String toString() {
	return new ToStringBuilder(this).append("actPrice", actPrice).append("msgLang", msgLang)
		.append("orderKey", orderKey).append("recieverMsisdn", recieverMsisdn)
		.append("destinationTariff", destinationTariff).append("tariffPermissions", tariffPermissions)
		.append("textmsg", textmsg).append("senderName", senderName).append("amount", amount)
		.append("offeringId", offeringId).append("number", number).append("actionType", actionType)
		.append("companyValue", companyValue).append("transactionId", transactionId).append("iccid", iccid)
		.append("contactNumber", contactNumber).append("totalLimit", totalLimit).append("orderId", orderId)
		.toString();
    }

    public String getContactNumber() {
	return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
	this.contactNumber = contactNumber;
    }

    public String getIccid() {
	return iccid;
    }

    public void setIccid(String iccid) {
	this.iccid = iccid;
    }

    public String getTransactionId() {
	return transactionId;
    }

    public void setTransactionId(String transactionId) {
	this.transactionId = transactionId;
    }

}

package com.evampsaanga.bakcell.models.ordermanagementinsert;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class changeLimitInitialMaximumResponse extends BaseResponse {
    public changeLimitInitialMaximumData getData() {
	return data;
    }

    public void setData(changeLimitInitialMaximumData data) {
	this.data = data;
    }

    private changeLimitInitialMaximumData data = new changeLimitInitialMaximumData();

}

package com.evampsaanga.bakcell.models.ordermanagementinsert;

public class OrderManagementInsertResponseData {
	
	private String responseMsg;

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

}

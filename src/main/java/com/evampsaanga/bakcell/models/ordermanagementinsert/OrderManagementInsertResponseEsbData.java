package com.evampsaanga.bakcell.models.ordermanagementinsert;

public class OrderManagementInsertResponseEsbData {
	private String responseMsg;
	
	private String returnMsg = "";
	private String returnCode = "";

	public String getReturnMsg() {
		return returnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	
	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

}

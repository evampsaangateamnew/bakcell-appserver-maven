package com.evampsaanga.bakcell.models.generalservices.predefineddata.topup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.evampsaanga.bakcell.common.utilities.Constants;

public class PlasticCard {

	private List<CardType> cardTypes;

	public PlasticCard() {
		this.cardTypes = new ArrayList<>();
	}

	public List<CardType> getCardTypes() {
		return this.cardTypes;
	}

	public void setCardTypes(List<CardType> cardTypes) {
		this.cardTypes = cardTypes;
	}

	@Override
	public String toString() {
		return "PlasticCard [cardTypes=" + cardTypes + ", toString()=" + super.toString() + "]";
	}

}

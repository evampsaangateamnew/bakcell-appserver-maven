package com.evampsaanga.bakcell.models.generalservices.getroaming;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class GetRoamingResponse extends BaseResponse {

    com.evampsaanga.bakcell.models.generalservices.getroaming.Data data = new Data();

    public com.evampsaanga.bakcell.models.generalservices.getroaming.Data getData() {
	return data;
    }

    public void setData(com.evampsaanga.bakcell.models.generalservices.getroaming.Data data) {
	this.data = data;
    }

}

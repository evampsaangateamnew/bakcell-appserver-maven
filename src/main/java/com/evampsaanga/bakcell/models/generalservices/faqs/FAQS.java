/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.faqs;

/**
 * @author Evamp & Saanga
 *
 */
public class FAQS {

	private String question;
	private String answer;
	private String sort_order;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getSort_order() {
		return sort_order;
	}

	public void setSort_order(String sort_order) {
		this.sort_order = sort_order;
	}

	@Override
	public String toString() {
		return "FAQS [question=" + question + ", answer=" + answer + ", sort_order=" + sort_order + "]";
	}

}

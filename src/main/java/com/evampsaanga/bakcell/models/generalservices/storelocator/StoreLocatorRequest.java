package com.evampsaanga.bakcell.models.generalservices.storelocator;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class StoreLocatorRequest extends BaseRequest {

    @Override
    public String toString() {
	return "StoreLocatorRequest [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
		+ ", getMsisdn()=" + getMsisdn() + "]";
    }

}

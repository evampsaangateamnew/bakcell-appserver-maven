/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.predefineddata.topup;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class TermsAndConditions {
	List<TermsAndConditionsData> cin;
	List<TermsAndConditionsData> klass;

	public List<TermsAndConditionsData> getCin() {
		return cin;
	}

	public void setCin(List<TermsAndConditionsData> cin) {
		this.cin = cin;
	}

	public List<TermsAndConditionsData> getKlass() {
		return klass;
	}

	public void setKlass(List<TermsAndConditionsData> klass) {
		this.klass = klass;
	}

}

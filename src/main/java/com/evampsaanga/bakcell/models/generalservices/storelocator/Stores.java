package com.evampsaanga.bakcell.models.generalservices.storelocator;

import java.util.List;

public class Stores {

	private String id;
	private String store_name;
	private String address;
	private String city;
	private String type;
	private String latitude;
	private String longitude;
	private List<String> contactNumbers;
	private List<Timings> timing;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStore_name() {
		return store_name;
	}

	public void setStore_name(String store_name) {
		this.store_name = store_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public List<String> getContactNumbers() {
		return contactNumbers;
	}

	public void setContactNumbers(List<String> contactNumbers) {
		this.contactNumbers = contactNumbers;
	}

	public List<Timings> getTiming() {
		return timing;
	}

	public void setTiming(List<Timings> timing) {
		this.timing = timing;
	}

}

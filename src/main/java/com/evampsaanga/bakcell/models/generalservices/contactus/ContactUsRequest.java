package com.evampsaanga.bakcell.models.generalservices.contactus;

/**
 * 
 */

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class ContactUsRequest extends BaseRequest {

    private String storeId;

    public String getStoreId() {
	return storeId;
    }

    public void setStoreId(String storeId) {
	this.storeId = storeId;
    }

    @Override
    public String toString() {
	return "ContactUsRequest [storeId=" + storeId + ", getLang()=" + getLang() + ", getiP()=" + getiP()
		+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

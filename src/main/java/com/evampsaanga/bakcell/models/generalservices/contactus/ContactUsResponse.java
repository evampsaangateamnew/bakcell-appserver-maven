/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.contactus;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class ContactUsResponse extends BaseResponse {
    private ContactUsResponseData data;

    public ContactUsResponseData getData() {
	return data;
    }

    public void setData(ContactUsResponseData data) {
	this.data = data;
    }
}

/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.sendinternetsettings;

/**
 * @author Evamp & Saanga
 *
 */
public class SendInternetSettingsResponseData {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

package com.evampsaanga.bakcell.models.generalservices.getroaming;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class GetRoamingRequest extends BaseRequest {
    public String getBrandName() {
	return brandName;
    }

    public void setBrandName(String brandName) {
	this.brandName = brandName;
    }

    private String brandName = "";

}

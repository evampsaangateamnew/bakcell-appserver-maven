package com.evampsaanga.bakcell.models.generalservices.storelocator;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class StoreLocatorResponse extends BaseResponse {
    private StoreLocatorResponseData data;

    public StoreLocatorResponseData getData() {
	return data;
    }

    public void setData(StoreLocatorResponseData data) {
	this.data = data;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.predefineddata.topup;

import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Evamp & Saanga
 *
 */
public class TOPUP {
    private MoneyTransfer moneyTransfer;
    private GetLoan getLoan;

    //Adding Plastic Card Object
    private PlasticCard plasticCard;
    
    public MoneyTransfer getMoneyTransfer() {
	return moneyTransfer;
    }

    public void setMoneyTransfer(MoneyTransfer moneyTransfer) {
	this.moneyTransfer = moneyTransfer;
    }

    public GetLoan getGetLoan() {
	return getLoan;
    }

    public void setGetLoan(GetLoan getLoan) {
	this.getLoan = getLoan;
    }
    
    //Adding Plastic card getter setter
    public void setPlasticCard(PlasticCard plasticCard) {
    	this.plasticCard = plasticCard;
    }
    
    public PlasticCard getPlasticCard() {
    	return plasticCard;
    }

    @Override
    public String toString() {
	return "TOPUP [moneyTransfer=" + moneyTransfer + ", getLoan=" + getLoan + "]";
    }

}

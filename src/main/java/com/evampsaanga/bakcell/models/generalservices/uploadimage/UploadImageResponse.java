/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.uploadimage;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class UploadImageResponse extends BaseResponse {

    UploadImageResponseData data;

    public UploadImageResponseData getData() {
	return data;
    }

    public void setData(UploadImageResponseData data) {
	this.data = data;
    }
}

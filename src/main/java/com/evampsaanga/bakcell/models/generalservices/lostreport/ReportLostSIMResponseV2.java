/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.lostreport;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class ReportLostSIMResponseV2 extends BaseResponse {

    private LostSIMResponseDataV2 data;

    public LostSIMResponseDataV2 getData() {
	return data;
    }

    public void setData(LostSIMResponseDataV2 data) {
	this.data = data;
    }

}

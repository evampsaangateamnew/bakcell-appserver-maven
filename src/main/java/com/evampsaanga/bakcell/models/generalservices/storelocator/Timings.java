package com.evampsaanga.bakcell.models.generalservices.storelocator;

public class Timings {

	private String day;
	private String timings;

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTimings() {
		return timings;
	}

	public void setTimings(String timings) {
		this.timings = timings;
	}

}

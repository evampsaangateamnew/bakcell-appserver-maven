/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.faqs;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class FAQSListData {

	private String title;
	private List<FAQS> QAList;

	public FAQSListData() {
		QAList = new ArrayList<>();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<FAQS> getQAList() {
		return QAList;
	}

	public void setQAList(List<FAQS> qAList) {
		QAList = qAList;
	}

}

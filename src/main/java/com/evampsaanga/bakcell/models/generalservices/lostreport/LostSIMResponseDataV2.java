/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.lostreport;

public class LostSIMResponseDataV2 {

    private String responseMsg;

    public String getResponseMsg() {
	return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
	this.responseMsg = responseMsg;
    }

}

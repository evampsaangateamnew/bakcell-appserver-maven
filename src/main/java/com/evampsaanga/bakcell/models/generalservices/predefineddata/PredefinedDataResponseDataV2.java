package com.evampsaanga.bakcell.models.generalservices.predefineddata;

import java.util.List;

import com.evampsaanga.bakcell.models.dashboardservice.DocumentTypesData;

public class PredefinedDataResponseDataV2 {
	private String content;
	private String firstPopup;
	private String lateOnPopup;
	private String popupTitle;
	private String popupContent;
	private List<DocumentTypesData> documentTypes;
	private String changeTariffPopUpMessage;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFirstPopup() {
		return firstPopup;
	}

	public void setFirstPopup(String firstPopup) {
		this.firstPopup = firstPopup;
	}

	public String getLateOnPopup() {
		return lateOnPopup;
	}

	public void setLateOnPopup(String lateOnPopup) {
		this.lateOnPopup = lateOnPopup;
	}

	public String getPopupTitle() {
		return popupTitle;
	}

	public void setPopupTitle(String popupTitle) {
		this.popupTitle = popupTitle;
	}

	public String getPopupContent() {
		return popupContent;
	}

	public void setPopupContent(String popupContent) {
		this.popupContent = popupContent;
	}

	public List<DocumentTypesData> getDocumentTypes() {
		return documentTypes;
	}

	public void setDocumentTypes(List<DocumentTypesData> documentTypes) {
		this.documentTypes = documentTypes;
	}

	public String getChangeTariffPopUpMessage() {
		return changeTariffPopUpMessage;
	}

	public void setChangeTariffPopUpMessage(String changeTariffPopUpMessage) {
		this.changeTariffPopUpMessage = changeTariffPopUpMessage;
	}

}

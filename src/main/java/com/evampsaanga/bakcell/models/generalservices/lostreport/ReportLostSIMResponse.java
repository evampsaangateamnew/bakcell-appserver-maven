/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.lostreport;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class ReportLostSIMResponse extends BaseResponse {

    private LostSIMResponseData data;

    public LostSIMResponseData getData() {
	return data;
    }

    public void setData(LostSIMResponseData data) {
	this.data = data;
    }

}

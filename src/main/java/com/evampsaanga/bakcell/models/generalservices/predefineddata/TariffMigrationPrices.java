/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.predefineddata;

/**
 * @author Evamp & Saanga
 *
 */
public class TariffMigrationPrices {

	private String key;
	private String value;

	public TariffMigrationPrices(String key, String value)
	{
		this.key = key;
		this.value = value;
	}
	
	public TariffMigrationPrices()
	{
		
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}

package com.evampsaanga.bakcell.models.generalservices.predefineddata;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class PredefinedDataResponseV2 extends BaseResponse {
    PredefinedDataResponseDataV2 data;

    public PredefinedDataResponseDataV2 getData() {
	return data;
    }

    public void setData(PredefinedDataResponseDataV2 data) {
	this.data = data;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.predefineddata.fnf;

/**
 * @author Evamp & Saanga
 *
 */
public class PredefinedFNF {
    @Override
    public String toString() {
	return "PredefinedFNF [maxFNFCount=" + maxFNFCount + ", fnFAllowed=" + fnFAllowed + "]";
    }

    private String maxFNFCount;
    private String fnFAllowed;

    public String getMaxFNFCount() {
	return maxFNFCount;
    }

    public void setMaxFNFCount(String maxFNFCount) {
	this.maxFNFCount = maxFNFCount;
    }

    public String getFnFAllowed() {
	return fnFAllowed;
    }

    public void setFnFAllowed(String fnFAllowed) {
	this.fnFAllowed = fnFAllowed;
    }

}

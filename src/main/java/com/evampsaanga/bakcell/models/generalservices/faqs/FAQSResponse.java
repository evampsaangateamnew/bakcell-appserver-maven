/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.faqs;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class FAQSResponse extends BaseResponse {
    private FAQSResponseData data;

    public FAQSResponseData getData() {
	return data;
    }

    public void setData(FAQSResponseData data) {
	this.data = data;
    }

}

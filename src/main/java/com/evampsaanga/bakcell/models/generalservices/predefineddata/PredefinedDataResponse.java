/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.predefineddata;

import java.util.HashMap;
import java.util.List;

import com.evampsaanga.bakcell.models.generalservices.predefineddata.fnf.PredefinedFNF;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.notificationpopup.NotificationPopupConfig;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.topup.ESBBaseResponse;
import com.evampsaanga.bakcell.models.generalservices.predefineddata.topup.TOPUP;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Evamp & Saanga
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PredefinedDataResponse extends ESBBaseResponse {
    private TOPUP topup;
    private PredefinedFNF fnf;
    private HashMap<String, String> goldenPayMessages;
    private RedirectionLinks redirectionLinks;
    private List<TariffMigrationPrices> tariffMigrationPrices;
    private String liveChat;
    private NotificationPopupConfig notificationPopupConfig;
    private boolean isRoamingVisible = false;

    public PredefinedDataResponse() {
	redirectionLinks = new RedirectionLinks();
    }

    public TOPUP getTopup() {
	return topup;
    }

    public void setTopup(TOPUP topup) {
	this.topup = topup;
    }

    public PredefinedFNF getFnf() {
	return fnf;
    }

    public void setFnf(PredefinedFNF fnf) {
	this.fnf = fnf;
    }

    public RedirectionLinks getRedirectionLinks() {
	return redirectionLinks;
    }

    public void setRedirectionLinks(RedirectionLinks redirectionLinks) {
	this.redirectionLinks = redirectionLinks;
    }

    public List<TariffMigrationPrices> getTariffMigrationPrices() {
	return tariffMigrationPrices;
    }

    public void setTariffMigrationPrices(List<TariffMigrationPrices> tariffMigrationPrices) {
	this.tariffMigrationPrices = tariffMigrationPrices;
    }

    public String getLiveChat() {
	return liveChat;
    }

    public void setLiveChat(String liveChat) {
	this.liveChat = liveChat;
    }

    public NotificationPopupConfig getNotificationPopupConfig() {
	return notificationPopupConfig;
    }

    public void setNotificationPopupConfig(NotificationPopupConfig notificationPopupConfig) {
	this.notificationPopupConfig = notificationPopupConfig;
    }

    public boolean isRoamingVisible() {
	return isRoamingVisible;
    }

    public void setRoamingVisible(boolean isRoamingVisible) {
	this.isRoamingVisible = isRoamingVisible;
    }

    @Override
    public String toString() {
	return "PredefinedDataResponse [topup=" + topup + ", fnf=" + fnf + ", redirectionLinks=" + redirectionLinks
		+ ", tariffMigrationPrices=" + tariffMigrationPrices + ", liveChat=" + liveChat
		+ ", notificationPopupConfig=" + notificationPopupConfig + ", isRoamingVisible=" + isRoamingVisible
		+ "]";
    }

	public HashMap<String, String> getGoldenPayMessages() {
		return goldenPayMessages;
	}

	public void setGoldenPayMessages(HashMap<String, String> goldenPayMessages) {
		this.goldenPayMessages = goldenPayMessages;
	}

}

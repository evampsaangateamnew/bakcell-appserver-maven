/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.faqs;

import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class FAQSResponseData {
	private List<FAQSListData> faqlist;

	public List<FAQSListData> getFaqlist() {
		return faqlist;
	}

	public void setFaqlist(List<FAQSListData> faqlist) {
		this.faqlist = faqlist;
	}

}

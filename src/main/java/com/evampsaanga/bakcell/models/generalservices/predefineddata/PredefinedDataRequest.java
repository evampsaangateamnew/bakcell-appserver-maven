/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.predefineddata;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class PredefinedDataRequest extends BaseRequest {

    private String offeringId;

    @Override
    public String toString() {
	return "PredefinedDataRequest [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()="
		+ getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

    public String getOfferingId() {
	return offeringId;
    }

    public void setOfferingId(String offeringId) {
	this.offeringId = offeringId;
    }

}

package com.evampsaanga.bakcell.models.generalservices.predefineddata.notificationpopup;

public class NotificationPopupConfig {
    private String popupMessage;
    private String hour;

    public String getPopupMessage() {
	return popupMessage;
    }

    public void setPopupMessage(String popupMessage) {
	this.popupMessage = popupMessage;
    }

    public String getHour() {
	return hour;
    }

    public void setHour(String hour) {
	this.hour = hour;
    }

    @Override
    public String toString() {
	return "NotificationPopupConfig [popupMessage=" + popupMessage + ", hour=" + hour + "]";
    }

}

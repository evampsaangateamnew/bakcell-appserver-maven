/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.contactus;

/**
 * @author Evamp & Saanga
 *
 */
public class ContactUsResponseData {
	private String address;
	private String phone;
	private String fax;
	private String email;
	private String website;
	private String customerCareNo;
	private String facebookLink;
	private String twitterLink;
	private String googleLink;
	private String instagram;
	private String youtubeLink;
	private String linkedinLink;
	private String addressLat;
	private String addressLong;
	private String inviteFriendText = "";

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getCustomerCareNo() {
		return customerCareNo;
	}

	public void setCustomerCareNo(String customerCareNo) {
		this.customerCareNo = customerCareNo;
	}

	public String getFacebookLink() {
		return facebookLink;
	}

	public void setFacebookLink(String facebookLink) {
		this.facebookLink = facebookLink;
	}

	public String getTwitterLink() {
		return twitterLink;
	}

	public void setTwitterLink(String twitterLink) {
		this.twitterLink = twitterLink;
	}

	public String getGoogleLink() {
		return googleLink;
	}

	public void setGoogleLink(String googleLink) {
		this.googleLink = googleLink;
	}

	public String getYoutubeLink() {
		return youtubeLink;
	}

	public void setYoutubeLink(String youtubeLink) {
		this.youtubeLink = youtubeLink;
	}

	public String getLinkedinLink() {
		return linkedinLink;
	}

	public void setLinkedinLink(String linkedinLink) {
		this.linkedinLink = linkedinLink;
	}

	public String getAddressLat() {
		return addressLat;
	}

	public void setAddressLat(String addressLat) {
		this.addressLat = addressLat;
	}

	public String getAddressLong() {
		return addressLong;
	}

	public void setAddressLong(String addressLong) {
		this.addressLong = addressLong;
	}

	public String getInviteFriendText() {
		return inviteFriendText;
	}

	public void setInviteFriendText(String inviteFriendText) {
		this.inviteFriendText = inviteFriendText;
	}

	public String getInstagram() {
		return instagram;
	}

	public void setInstagram(String instagram) {
		this.instagram = instagram;
	}

}

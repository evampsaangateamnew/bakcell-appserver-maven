/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.predefineddata;

/**
 * @author Evamp & Saanga
 *
 */
public class RedirectionLinks {
    private String onlinePaymentGatewayURL_AZ;
    private String onlinePaymentGatewayURL_EN;
    private String onlinePaymentGatewayURL_RU;

    public String getOnlinePaymentGatewayURL_AZ() {
	return onlinePaymentGatewayURL_AZ;
    }

    public void setOnlinePaymentGatewayURL_AZ(String onlinePaymentGatewayURL_AZ) {
	this.onlinePaymentGatewayURL_AZ = onlinePaymentGatewayURL_AZ;
    }

    public String getOnlinePaymentGatewayURL_EN() {
	return onlinePaymentGatewayURL_EN;
    }

    public void setOnlinePaymentGatewayURL_EN(String onlinePaymentGatewayURL_EN) {
	this.onlinePaymentGatewayURL_EN = onlinePaymentGatewayURL_EN;
    }

    public String getOnlinePaymentGatewayURL_RU() {
	return onlinePaymentGatewayURL_RU;
    }

    public void setOnlinePaymentGatewayURL_RU(String onlinePaymentGatewayURL_RU) {
	this.onlinePaymentGatewayURL_RU = onlinePaymentGatewayURL_RU;
    }

    @Override
    public String toString() {
	return "RedirectionLinks [onlinePaymentGatewayURL_AZ=" + onlinePaymentGatewayURL_AZ
		+ ", onlinePaymentGatewayURL_EN=" + onlinePaymentGatewayURL_EN + ", onlinePaymentGatewayURL_RU="
		+ onlinePaymentGatewayURL_RU + "]";
    }

}

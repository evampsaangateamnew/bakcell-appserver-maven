/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.predefineddata.topup;

/**
 * @author Evamp & Saanga
 *
 */
public class MoneyTransfer {
	SelectAmount selectAmount;
	TermsAndConditions termsAndCondition;

	public SelectAmount getSelectAmount() {
		return selectAmount;
	}

	public void setSelectAmount(SelectAmount selectAmount) {
		this.selectAmount = selectAmount;
	}

	public TermsAndConditions getTermsAndCondition() {
		return termsAndCondition;
	}

	public void setTermsAndCondition(TermsAndConditions termsAndCondition) {
		this.termsAndCondition = termsAndCondition;
	}

}

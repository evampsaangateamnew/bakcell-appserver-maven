/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.lostreport;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class ReportLostSIMRequest extends BaseRequest {

    private String reasonCode;

    public String getReasonCode() {
	return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
	this.reasonCode = reasonCode;
    }

    @Override
    public String toString() {
	return "ReportLostSIMRequest [reasonCode=" + reasonCode + "]";
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.faqs;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * 
 * @author Evamp & Saanga
 *
 */
public class FAQSRequest extends BaseRequest {

    @Override
    public String toString() {
	return "FAQSRequest [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
		+ ", getMsisdn()=" + getMsisdn() + "]";
    }

}

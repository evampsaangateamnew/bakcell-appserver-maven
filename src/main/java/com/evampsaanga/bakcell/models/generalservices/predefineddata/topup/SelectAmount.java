/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.predefineddata.topup;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Evamp & Saanga
 *
 */
public class SelectAmount {
	List<SelectAmountData> cin;
	List<SelectAmountData> klass;

	public SelectAmount() {
		cin = new ArrayList<>();
		klass = new ArrayList<>();
	}

	public List<SelectAmountData> getCin() {
		return cin;
	}

	public void setCin(List<SelectAmountData> cin) {
		this.cin = cin;
	}

	public List<SelectAmountData> getKlass() {
		return klass;
	}

	public void setKlass(List<SelectAmountData> klass) {
		this.klass = klass;
	}

}

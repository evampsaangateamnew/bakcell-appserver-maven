/**
 * 
 */
package com.evampsaanga.bakcell.models.generalservices.sendinternetsettings;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class SendInternetSettingsRequest extends BaseRequest {

    @Override
    public String toString() {
	return "SendInternetSettingsRequest [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()="
		+ getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

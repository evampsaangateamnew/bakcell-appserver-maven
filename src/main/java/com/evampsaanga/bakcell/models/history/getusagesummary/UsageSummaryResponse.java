/**
 * 
 */
package com.evampsaanga.bakcell.models.history.getusagesummary;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class UsageSummaryResponse extends BaseResponse {
    UsageSummaryResponseData data;

    public UsageSummaryResponseData getData() {
	return data;
    }

    public void setData(UsageSummaryResponseData data) {
	this.data = data;
    }

}

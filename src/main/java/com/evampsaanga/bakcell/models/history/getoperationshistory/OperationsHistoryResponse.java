/**
 * 
 */
package com.evampsaanga.bakcell.models.history.getoperationshistory;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */

public class OperationsHistoryResponse extends BaseResponse {
    private OperationsHistoryResponseData data;

    public OperationsHistoryResponseData getData() {
	return data;
    }

    public void setData(OperationsHistoryResponseData data) {
	this.data = data;
    }

}

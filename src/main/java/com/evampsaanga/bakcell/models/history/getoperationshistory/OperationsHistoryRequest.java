/**
 * 
 */
package com.evampsaanga.bakcell.models.history.getoperationshistory;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class OperationsHistoryRequest extends BaseRequest {

    private String startDate;
    private String endDate;
    private String accountId;
    private String customerId;

    public String getStartDate() {
	return startDate;
    }

    public void setStartDate(String startDate) {
	this.startDate = startDate;
    }

    public String getEndDate() {
	return endDate;
    }

    public void setEndDate(String endDate) {
	this.endDate = endDate;
    }

    public String getAccountId() {
	return accountId;
    }

    public void setAccountId(String accountId) {
	this.accountId = accountId;
    }

    @Override
    public String toString() {
	return "OperationsHistoryRequest [startDate=" + startDate + ", endDate=" + endDate + ", accountId=" + accountId
		+ ", getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()=" + getChannel()
		+ ", getMsisdn()=" + getMsisdn() + "]";
    }

    public String getCustomerId() {
	return customerId;
    }

    public void setCustomerId(String customerId) {
	this.customerId = customerId;
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.history.verifypin;

/**
 * @author Evamp & Saanga
 *
 */
public class UsageDetailsVerifyPinResponseData {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}

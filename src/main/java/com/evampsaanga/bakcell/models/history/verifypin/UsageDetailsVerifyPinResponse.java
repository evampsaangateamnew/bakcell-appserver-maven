/**
 * 
 */
package com.evampsaanga.bakcell.models.history.verifypin;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class UsageDetailsVerifyPinResponse extends BaseResponse {
    UsageDetailsVerifyPinResponseData data;

    public UsageDetailsVerifyPinResponseData getData() {
	return data;
    }

    public void setData(UsageDetailsVerifyPinResponseData data) {
	this.data = data;
    }

}

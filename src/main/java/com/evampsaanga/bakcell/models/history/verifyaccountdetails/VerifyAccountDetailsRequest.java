package com.evampsaanga.bakcell.models.history.verifyaccountdetails;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

public class VerifyAccountDetailsRequest extends BaseRequest {

    private String accountId;
    private String customerId;
    private String passportNumber;

    public String getAccountId() {
	return accountId;
    }

    public void setAccountId(String accountId) {
	this.accountId = accountId;
    }

    public String getCustomerId() {
	return customerId;
    }

    public void setCustomerId(String customerId) {
	this.customerId = customerId;
    }

    public String getPassportNumber() {
	return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
	this.passportNumber = passportNumber;
    }

    @Override
    public String toString() {
	return "VerifyAccountDetailsRequest [accountId=" + accountId + ", customerId=" + customerId
		+ ", passportNumber=" + passportNumber + ", getLang()=" + getLang() + ", getiP()=" + getiP()
		+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

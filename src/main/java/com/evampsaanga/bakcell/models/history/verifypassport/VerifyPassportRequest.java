/**
 * 
 */
package com.evampsaanga.bakcell.models.history.verifypassport;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class VerifyPassportRequest extends BaseRequest {
    private String passportNumber;

    public String getPassportNumber() {
	return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
	this.passportNumber = passportNumber;
    }

    @Override
    public String toString() {
	return "VerifyPassportRequest [passportNumber=" + passportNumber + ", getLang()=" + getLang() + ", getiP()="
		+ getiP() + ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

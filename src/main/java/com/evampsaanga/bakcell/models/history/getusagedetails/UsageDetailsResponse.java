/**
 * 
 */
package com.evampsaanga.bakcell.models.history.getusagedetails;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */

public class UsageDetailsResponse extends BaseResponse {
    private UsageDetailsResponseData data;

    public UsageDetailsResponseData getData() {
	return data;
    }

    public void setData(UsageDetailsResponseData data) {
	this.data = data;
    }
}

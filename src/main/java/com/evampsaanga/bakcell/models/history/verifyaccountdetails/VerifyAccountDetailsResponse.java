package com.evampsaanga.bakcell.models.history.verifyaccountdetails;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

public class VerifyAccountDetailsResponse extends BaseResponse {
    private VerifyAccountDetailsResponseData data;

    public VerifyAccountDetailsResponseData getData() {
	return data;
    }

    public void setData(VerifyAccountDetailsResponseData data) {
	this.data = data;
    }

}

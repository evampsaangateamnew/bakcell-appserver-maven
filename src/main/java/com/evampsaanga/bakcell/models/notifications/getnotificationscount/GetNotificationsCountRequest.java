/**
 * 
 */
package com.evampsaanga.bakcell.models.notifications.getnotificationscount;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class GetNotificationsCountRequest extends BaseRequest {

    @Override
    public String toString() {
	return "GetNotificationsCountRequest [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()="
		+ getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

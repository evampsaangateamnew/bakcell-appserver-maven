/**
 * 
 */
package com.evampsaanga.bakcell.models.notifications.getnotifications;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class GetNotificationsResponse extends BaseResponse {

    private GetNotificationsResponseData data;

    public GetNotificationsResponseData getData() {
	return data;
    }

    public void setData(GetNotificationsResponseData data) {
	this.data = data;
    }
}

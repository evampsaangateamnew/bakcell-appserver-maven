/**
 * 
 */
package com.evampsaanga.bakcell.models.notifications.getnotificationsconfigurations;

/**
 * @author Evamp & Saanga
 *
 */
public class GetNotificationsConfigurationsResponseData {
	private String isEnable;
	private String ringingStatus;

	public String getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(String isEnable) {
		this.isEnable = isEnable;
	}

	public String getRingingStatus() {
		return ringingStatus;
	}

	public void setRingingStatus(String ringingStatus) {
		this.ringingStatus = ringingStatus;
	}
}

/**
 * 
 */
package com.evampsaanga.bakcell.models.notifications.getnotificationsconfigurations;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class GetNotificationsConfigurationsRequest extends BaseRequest {

    @Override
    public String toString() {
	return "GetNotificationsConfigurationsRequest [getLang()=" + getLang() + ", getiP()=" + getiP()
		+ ", getChannel()=" + getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.notifications.getnotificationscount;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class GetNotificationsCountResponse extends BaseResponse {

    GetNotificationsCountResponseData data;

    public GetNotificationsCountResponseData getData() {
	return data;
    }

    public void setData(GetNotificationsCountResponseData data) {
	this.data = data;
    }

}

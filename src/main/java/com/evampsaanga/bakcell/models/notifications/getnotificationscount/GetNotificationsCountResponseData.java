/**
 * 
 */
package com.evampsaanga.bakcell.models.notifications.getnotificationscount;

/**
 * @author Evamp & Saanga
 *
 */
public class GetNotificationsCountResponseData {
	private String notificationUnreadCount;

	public String getNotificationUnreadCount() {
		return notificationUnreadCount;
	}

	public void setNotificationUnreadCount(String notificationUnreadCount) {
		this.notificationUnreadCount = notificationUnreadCount;
	}
}

/**
 * 
 */
package com.evampsaanga.bakcell.models.notifications.getnotifications;

import com.evampsaanga.bakcell.common.utilities.BaseRequest;

/**
 * @author Evamp & Saanga
 *
 */
public class GetNotificationsRequest extends BaseRequest {

    @Override
    public String toString() {
	return "GetNotificationsRequest [getLang()=" + getLang() + ", getiP()=" + getiP() + ", getChannel()="
		+ getChannel() + ", getMsisdn()=" + getMsisdn() + "]";
    }

}

/**
 * 
 */
package com.evampsaanga.bakcell.models.notifications.addfcm;

/**
 * @author Zainab
 *
 */
public class AddFCMResponseData {

	private String isEnable;
	private String ringingStatus;

	public String getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(String isEnable) {
		this.isEnable = isEnable;
	}

	public String getRingingStatus() {
		return ringingStatus;
	}

	public void setRingingStatus(String ringingStatus) {
		this.ringingStatus = ringingStatus;
	}

}

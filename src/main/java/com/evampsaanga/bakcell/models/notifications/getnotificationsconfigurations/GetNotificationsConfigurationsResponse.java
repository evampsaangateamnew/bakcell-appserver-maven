/**
 * 
 */
package com.evampsaanga.bakcell.models.notifications.getnotificationsconfigurations;

import com.evampsaanga.bakcell.common.utilities.BaseResponse;

/**
 * @author Evamp & Saanga
 *
 */
public class GetNotificationsConfigurationsResponse extends BaseResponse {
    private GetNotificationsConfigurationsResponseData data;

    public GetNotificationsConfigurationsResponseData getData() {
	return data;
    }

    public void setData(GetNotificationsConfigurationsResponseData data) {
	this.data = data;
    }
}
